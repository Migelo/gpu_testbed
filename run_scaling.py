import argparse

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())
ND = 3

for order in range(1, 5):
    problems = dg_python.get_problems_to_run(args["cluster"], order, ND)

    for problem in problems:
        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            "output/pythontest/NC{:03d}_k{:d}",
            "00:05:00",
            args["dry_run"],
        )
