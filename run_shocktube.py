import argparse
import os
from subprocess import run
from typing import List

import matplotlib

# import matplotlib.pyplot as plt
import numpy as np
import proplot as plt
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.axes_grid1 import make_axes_locatable

matplotlib.use("Agg")
import dg_python
from dg_python.scaling_tests import Problem
from dg_python.templates import PARAM_TEMPLATE

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
parser.add_argument(
    "--prep-run",
    action="store_true",
    help="Prepare a run",
)
parser.add_argument(
    "--analyze",
    action="store_true",
    help="Plot a grid of what worked and what didn't",
)
parser.add_argument(
    "--merge-heatmaps",
    action="store_true",
    help="Merge images of previously created grids",
)
parser.add_argument(
    "--debug",
    action="store_true",
    help="Merge images of previously created grids",
)
args = vars(parser.parse_args())

DEBUG = args["debug"]

print(
    "dry run: {}\nanalyze: {}\nmerge-heatmaps: {}\ndebug: {}".format(
        args["dry_run"], args["analyze"], args["merge_heatmaps"], DEBUG
    )
)

CONFIG_TEMPLATE = """

VISCOSITY
ART_VISCOSITY
SHOCK_TUBE
TUBE2

{}                                             # viscosity type
# EPSILON0={}
# S0={}
# KAPPA={}

ND=1L                                          # sets number of dimensions
DEGREE_K={}L                                   # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D={}L                            # number of cells per dimension
"""


PARAM_TEMPLATE = """
Tend       0.15
CourantFac 0.3

DesiredDumps     32
PixelsFieldMaps  4800

ArtViscParameter .5
ArtViscOnset {}

OutputDir  ./data


MinimumSlabsPerCPURank  0

WriteRestartAfter       20000
WriteCheckpointAfter    7200

"""

JOB_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=00:05:00
#SBATCH --ntasks-per-node=1
#SBATCH --ntasks=1
#SBATCH --job-name c480k5
#SBATCH --partition=p.test
##SBATCH --gres=gpu:a100:4
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo




FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
grep Nlimited_total log.txt | cut -f11 -d' ' >| nlimited_p.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    module load anaconda && source activate py39 && python plots.py |& tee plots.log
    python plot_vel_power_spec.py
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""


ND = 1
FOLDER_WITH_PARAMETERS_TEMPLATE = "/E{:012.4e}_S{:012.4e}_K_{:.2f}_onset_{:02.0e}"
OUTPUT_FOLDER_TEMPLATE = "/freya/u/mihac/paper/shocktube_volker_varying_viscosity/shocktube_{}/tube2_k{:02d}_nc{:03d}/E{:012.4e}_S{:012.4e}_K_{:.2f}_onset_{:02.0e}"

problems = []

# epsilons = np.logspace(-5, 10, 10)
# s0s = np.logspace(-5, 10, 10)
epsilons = np.logspace(-15, 4, 1)
epsilons = np.array((1,))
s0s = np.logspace(-10, 1, 1)
kappas = np.array((0.5,))
onsets = np.logspace(-7, 0, 8)

orders = (5,)
ncs = (480,)

viscosity_types = ("VISC_SMOOTH", "VISC_HYBRID_MIN", "VISC_HYBRID", "VISC_CS")
viscosity_types = ("VISC_SIMPLE",)


def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N, 0))
    cmap_name = base.name + str(N)
    return plt.cm.colors.ListedColormap(color_list, color_list, N)


def analyze_one_run(path: str, todo: List) -> None:
    print(f"[{analyze_one_run.__name__}] working on folder {path}")
    problems = [x[0] for x in todo]
    parameters = [x[2] for x in todo]
    _, _, kappa, viscosity_type = parameters[0]

    result_grid = np.zeros((len(epsilons), len(s0s)))
    viscosity_grid = np.zeros(result_grid.shape)
    for (epsilon_idx, s0_idx), _ in np.ndenumerate(result_grid):
        problem = problems[epsilon_idx * s0s.size + s0_idx]
        epsilon = epsilons[epsilon_idx]
        s0 = s0s[s0_idx]
        path_to_check = path + FOLDER_WITH_PARAMETERS_TEMPLATE.format(
            epsilon, s0, kappa
        )

        injected_visc = None
        if os.path.exists(path_to_check + "/log.txt"):
            if DEBUG:
                print(
                    f"[{analyze_one_run.__name__}] reading {path_to_check}/logtrimmed.txt",
                    end="",
                )

            try:
                f = open(path_to_check + "/logtrimmed.txt")
            except FileNotFoundError:
                f = open(path_to_check + "/log.txt")
            except:
                print(" failed")
            data = f.read().splitlines()
            if len(data) < 2:
                continue
            if "Finished!" in data[-2]:
                result_grid[epsilon_idx, s0_idx] = 1
                if DEBUG:
                    print(" finished")
                for line in data[::-1]:
                    if "[ART_VISCOSITY]" in line:
                        _, _, injected_visc = line.split(" ")
                        injected_visc = float(injected_visc)
                        break
                viscosity_grid[epsilon_idx, s0_idx] = injected_visc
                if injected_visc == 0:
                    result_grid[epsilon_idx, s0_idx] = 2
            else:
                if DEBUG:
                    print(" failed")
            f.close()

    f, ax = plt.subplots(dpi=250)

    ax.set_xlabel(r"$S_0$")
    ax.set_ylabel(r"$\epsilon_0$")

    im = ax.imshow(result_grid, vmin=0, vmax=2, cmap=discrete_cmap(3, "viridis"))

    for (j, i), label in np.ndenumerate(viscosity_grid):
        if result_grid[j, i] == 1:
            ax.text(i, j, "{:3.1e}".format(label), ha="center", va="center", fontsize=5)

    ax.set_title("NC: {} order: {}".format(problems[0].nc, problems[0].order))
    ax.set_xticks(np.arange(s0s.size), rotate=45)
    ax.set_yticks(np.arange(epsilons.size))
    ax.set_xticklabels(["{:3.1e}".format(x) for x in s0s])
    ax.set_yticklabels(["{:3.1e}".format(x) for x in epsilons])
    ax.xaxis.set_tick_params(rotation=45)

    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_ticks(
        (2 / 6, 1, 2 / 6 * 5), labels=("failed", "triggered", "not triggered")
    )

    # f.tight_layout()

    plt.savefig(f"{path}/img.png")

    plt.close(f)


if __name__ == "__main__":
    for viscosity_type in viscosity_types:
        for order in orders:
            for nc in ncs:
                for epsilon in epsilons:
                    for s0 in s0s:
                        for kappa in kappas:
                            for onset in onsets:
                                config_file = CONFIG_TEMPLATE.format(
                                    viscosity_type, epsilon, s0, kappa, order, nc
                                )
                                problems.append(
                                    (
                                        Problem(nc, order, 0.4, 0),
                                        config_file,
                                        (epsilon, s0, kappa, viscosity_type, onset),
                                    )
                                )

    for problem, config_file, parameters in problems:
        epsilon, s0, kappa, viscosity_type, onset = parameters
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(
            viscosity_type, problem.order, problem.nc, epsilon, s0, kappa, onset
        )
        if args["prep_run"]:
            dg_python.prepare_code_run(
                problem,
                args["cluster"],
                output_folder,
                "00:05:00",
                args["dry_run"],
                config_template=config_file,
                no_restart=True,
                param_template=PARAM_TEMPLATE.format(onset),
                job_template=JOB_TEMPLATE,
                link_folders=True,
            )
    if args["analyze"]:
        for viscosity_type in viscosity_types:
            for i, order in enumerate(orders):
                for j, nc in enumerate(ncs):
                    idx0 = (i * len(ncs) + j) * (epsilons.size * s0s.size)
                    idx1 = (i * len(ncs) + j + 1) * (epsilons.size * s0s.size)

                    run_folder = OUTPUT_FOLDER_TEMPLATE[:-31].format(
                        viscosity_type, order, nc
                    )

                    analyze_one_run(run_folder, problems[idx0:idx1])

    if args["merge_heatmaps"]:
        for viscosity_type in viscosity_types:
            command = "montage -mode concatenate "
            for j, nc in enumerate(ncs):
                for i, order in enumerate(orders):
                    path_to_image = (
                        OUTPUT_FOLDER_TEMPLATE[:-31].format(viscosity_type, order, nc)
                        + "/img.png"
                    )
                    command += f"{path_to_image} "
            command += "-tile {:d}x{:d} ".format(len(orders), len(ncs))
            command += f" /freya/ptmp/mpa/mihac/gpu_testbed_output/shocktube_paper/{viscosity_type.lower()}_heatmap.png"

            run(command, shell=True, check=True)
