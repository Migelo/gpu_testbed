import argparse

import dg_python
from dg_python.scaling_tests import Problem

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())


JOB_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks={}
#SBATCH --job-name c{}k{}
#SBATCH --partition=p.gpu.ampere
#SBATCH --gres=gpu:a100:4
##SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
grep "\[DYE" slurm* > dye.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py |& tee plots.log
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""

JOB_RESTART_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks={}
#SBATCH --job-name c{}k{}
#SBATCH --partition=p.gpu.ampere
#SBATCH --gres=gpu:a100:4
##SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


FILE=./.finished
RESTART=./.restart
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1 | tee log.txt
grep "\[DYE" slurm-* > dye.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py |& tee plots.log
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""


problems = []
nprocs = {32: 4, 64: 4, 128: 4, 256: 8, 512: 8, 1024: 16, 2048: 16}

for order in (1, 2, 3, 4, 5, 6, 7, 8, 9):
    for nc in (
        # 32,
        64,
        # 128,
        # 256,
        # 512,
        # 1024,
        # 2048,
    ):
        problems.append(Problem(nc * 2, order, endtime=10, n_gpu=0))

# problems.append(Problem(nc, order, endtime=10, n_gpu=0))
problems.append(Problem(128 * 2, 7, endtime=10, n_gpu=0))
problems.append(Problem(256 * 2, 3, endtime=10, n_gpu=0))
problems.append(Problem(512 * 2, 1, endtime=10, n_gpu=0))


CONFIG_TEMPLATE = """
GPU

KELVIN_HELMHOLTZ_TEST
KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS

VISCOSITY
NAVIER_STOKES
ENABLE_DYE

ENABLE_POSITIVITY_LIMITING

GAMMA=5./3.

ND=2
DEGREE_K={}
DG_NUM_CELLS_1D={}
"""

PARAM_TEMPLATE = """
Tend       10.0
CourantFac 0.3

DesiredDumps     20
PixelsFieldMaps  2048


OutputDir  ./data
ShearViscosity          2.00000e-05
ThermalDiffusivity      2.00000e-05
DyeDiffusivity          2.00000e-05

MinimumSlabsPerCPURank  0

WriteRestartAfter       85500
WriteCheckpointAfter    720000

"""

for problem in problems:
    dg_python.prepare_code_run(
        problem,
        args["cluster"],
        "/freya/ptmp/mpa/mihac/gpu_testbed2/output/khsmooth/jump/re5/NC{:03d}_k{:d}",
        "24:00:00",
        args["dry_run"],
        config_template=CONFIG_TEMPLATE.format(problem.order, problem.nc),
        param_template=PARAM_TEMPLATE,
        job_template=JOB_TEMPLATE.format(nprocs[problem.nc], problem.nc, problem.order),
        job_restart_template=JOB_RESTART_TEMPLATE.format(
            nprocs[problem.nc], problem.nc, problem.order
        ),
    )
