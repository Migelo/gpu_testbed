


ifdef DIR
EXEC = $(DIR)/run.run
CONFIG = $(DIR)/Config.sh
BUILD_DIR = $(DIR)/build
else
EXEC   = run.run
CONFIG   = Config.sh
BUILD_DIR = build
endif

SRC_DIR = src

###################
#determine SYSTYPE#
###################
ifdef SYSTYPE
SYSTYPE := "$(SYSTYPE)"
-include Makefile.systype
else
include Makefile.systype
endif

$(info Build configuration:)
$(info SYSTYPE: $(SYSTYPE))
$(info CONFIG: $(CONFIG))
$(info EXEC: $(EXEC))
$(info )

PYTHON =  python

RESULT     := $(shell CONFIG=$(CONFIG) PYTHON=$(PYTHON) BUILD_DIR=$(BUILD_DIR) SRC_DIR=$(SRC_DIR) CURDIR=$(CURDIR) make -f buildsystem/Makefile.config)
$(info $(RESULT))
CONFIGVARS := $(shell cat $(BUILD_DIR)/runrun.h)

RESULT     := $(shell SRC_DIR=$(SRC_DIR) BUILD_DIR=$(BUILD_DIR) ./buildsystem/git_version.sh)



##########################
#define available Systems#
##########################

ifeq ($(SYSTYPE),"JuelichBooster")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -g  --device-c -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
CPP       = nvcc -g -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -g -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
else
NCC       = mpicxx -g -Wall -std=c++17
CPP       = mpicxx -g -Wall -std=c++17
LINKER    = mpicxx
endif
endif



ifeq ($(SYSTYPE),"FreyaDebug")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -O0 -g  --device-c   -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
CPP       = nvcc -O0 -g               -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -O0 -g               -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
MPI_INCL  = -I$(OPENMPI_HOME)/include
MPI_LIBS  = -L$(OPENMPI_HOME)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_HOME)/lib
else
NCC       = mpicxx -O0 -g -Wall -std=c++17
CPP       = mpicxx -O0 -g -Wall -std=c++17
LINKER    = mpicxx
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif



ifeq ($(SYSTYPE),"FreyaVolta")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  --device-c  -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70  -Xcompiler -Wall -std=c++17
CPP       = nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -lineinfo -O3 -g  -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70  -Xcompiler -Wall -std=c++17
MPI_INCL  = -I$(OPENMPI_HOME)/include
MPI_LIBS  = -L$(OPENMPI_HOME)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_HOME)/lib
else
NCC       = mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
CPP       = mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
LINKER    = mpicxx -fdiagnostics-color
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"FreyaPascal")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       =  nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  --device-c  -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_60,code=compute_60  -Xcompiler -Wall -std=c++17
CPP       =  nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_60,code=compute_60  -Xcompiler -Wall -std=c++17
LINKER    =  nvcc -lineinfo -O3 -g  -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_60,code=compute_60  -Xcompiler -Wall -std=c++17
MPI_INCL  =  -I$(OPENMPI_HOME)/include
MPI_LIBS  =  -L$(OPENMPI_HOME)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_HOME)/lib
else
NCC       =  mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
CPP       =  mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
LINKER    =  mpicxx -fdiagnostics-color
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif



ifeq ($(SYSTYPE),"Freya")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  --device-c  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
CPP       = nvcc -lineinfo -Xcompiler -fdiagnostics-color -O3 -g  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -lineinfo -O3 -g  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
MPI_INCL  = -I$(OPENMPI_HOME)/include
MPI_LIBS  = -L$(OPENMPI_HOME)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_HOME)/lib
else
NCC       = mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
CPP       = mpicxx -O3 -g -Wall -std=c++17 -fdiagnostics-color
LINKER    = mpicxx -fdiagnostics-color
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"Rusty")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -lineinfo  -O3 -g  --device-c  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
CPP       = nvcc -lineinfo -O3 -g  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -lineinfo -O3 -g  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80  -Xcompiler -Wall -std=c++17
MPI_INCL  = -I$(OPENMPI_ROOT)/include
MPI_LIBS  = -L$(OPENMPI_ROOT)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_ROOT)/lib
else
NCC       = mpicxx -O3 -g -Wall -std=c++17
CPP       = mpicxx -O3 -g -Wall -std=c++17
LINKER    = mpicxx
endif
GSL_INCL  =  -I$(GSL_ROOT)/include
GSL_LIBS  =  -L$(GSL_ROOT)/lib  -Xlinker -R -Xlinker $(GSL_ROOT)/lib
HDF5_INCL =  -I$(HDF5_ROOT)/include
HDF5_LIBS =  -L$(HDF5_ROOT)/lib -Xlinker -R -Xlinker $(HDF5_ROOT)/lib
FFTW_INCL  =  -I$(FFTW_ROOT)/include
FFTW_LIBS  =  -L$(FFTW_ROOT)/lib  -Xlinker -R -Xlinker $(FFTW_ROOT)/lib
endif

ifeq ($(SYSTYPE),"RustyHopper")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
NCC       = nvcc -lineinfo  -O3 -g  --device-c  -gencode=arch=compute_90,code=sm_90 -gencode=arch=compute_90,code=compute_90  -Xcompiler -Wall -std=c++17
CPP       = nvcc -lineinfo -O3 -g  -gencode=arch=compute_90,code=sm_90 -gencode=arch=compute_90,code=compute_90  -Xcompiler -Wall -std=c++17
LINKER    = nvcc -lineinfo -O3 -g  -gencode=arch=compute_90,code=sm_90 -gencode=arch=compute_90,code=compute_90  -Xcompiler -Wall -std=c++17
MPI_INCL  = -I$(OPENMPI_ROOT)/include
MPI_LIBS  = -L$(OPENMPI_ROOT)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_ROOT)/lib
else
NCC       = mpicxx -O3 -g -Wall -std=c++17
CPP       = mpicxx -O3 -g -Wall -std=c++17
LINKER    = mpicxx
endif
GSL_INCL  =  -I$(GSL_ROOT)/include
GSL_LIBS  =  -L$(GSL_ROOT)/lib  -Xlinker -R -Xlinker $(GSL_ROOT)/lib
HDF5_INCL =  -I$(HDF5_ROOT)/include
HDF5_LIBS =  -L$(HDF5_ROOT)/lib -Xlinker -R -Xlinker $(HDF5_ROOT)/lib
FFTW_INCL  =  -I$(FFTW_ROOT)/include
FFTW_LIBS  =  -L$(FFTW_ROOT)/lib  -Xlinker -R -Xlinker $(FFTW_ROOT)/lib
endif


ifeq ($(SYSTYPE),"FreyaIntelProfiling")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
DEBUG = -g
ifdef $(DEBUG)
HOST_DEBUG = -Xcompiler $(DEBUG)
endif
HOST_FLAGS = -ccbin=$(I_MPI_ROOT)/intel64/bin/mpicxx $(HOST_DEBUG) -Xcompiler -cxx=icc -Xcompiler -trace -Xcompiler -tcollect -Xcompiler -DUSE_VT -Xcompiler -Wall
CUDA_ARCHITECTURES = -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_70,code=sm_70  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80
NVCC_FLAGS = $(DEBUG) -std=c++17
NCC       = nvcc $(HOST_FLAGS) --device-c $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
CPP       = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
LINKER    = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
MPI_INCL  = -I$(I_MPI_ROOT)/intel64/include
MPI_LIBS  = -L$(I_MPI_ROOT)/intel64/lib -Xlinker -R -Xlinker $(I_MPI_ROOT)/intel64/lib
else
NCC       = mpicxx -g -Wall -std=c++17
CPP       = mpicxx -g -Wall -std=c++17
LINKER    = mpicxx
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"FreyaIntel")
ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
DEBUG = -g
ifdef $(DEBUG)
HOST_DEBUG = -Xcompiler $(DEBUG)
endif
HOST_FLAGS = -ccbin=$(I_MPI_ROOT)/intel64/bin/mpicxx $(HOST_DEBUG) -Xcompiler -cxx=icc -Xcompiler -Wall
CUDA_ARCHITECTURES = -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_70,code=sm_70  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80
NVCC_FLAGS = $(DEBUG) -std=c++17
NCC       = nvcc $(HOST_FLAGS) --device-c $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
CPP       = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
LINKER    = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
MPI_INCL  = -I$(I_MPI_ROOT)/intel64/include
MPI_LIBS  = -L$(I_MPI_ROOT)/intel64/lib -Xlinker -R -Xlinker $(I_MPI_ROOT)/intel64/lib
else
NCC       = mpicxx -g -Wall -std=c++17
CPP       = mpicxx -g -Wall -std=c++17
LINKER    = mpicxx
endif
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL  =  -I$(FFTW_HOME)/include
FFTW_LIBS  =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"Raven")
DEBUG = -g
ifdef $(DEBUG)
HOST_DEBUG = -Xcompiler $(DEBUG)
endif
HOST_FLAGS = $(HOST_DEBUG)  -Xcompiler -Wall -Xcompiler -std=c++17
CUDA_ARCHITECTURES = -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80
NVCC_FLAGS = $(DEBUG) -std=c++17
NCC       = nvcc $(HOST_FLAGS) --device-c $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
CPP       = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
LINKER    = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
MPI_INCL  = -I$(OPENMPI_HOME)/include
MPI_LIBS  = -L$(OPENMPI_HOME)/lib -lmpi -Xlinker -R -Xlinker $(OPENMPI_HOME)/lib
GSL_INCL  = -I$(GSL_HOME)/include
GSL_LIBS  = -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL = -I$(HDF5_HOME)/include
HDF5_LIBS = -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL = -I$(FFTW_HOME)/include
FFTW_LIBS = -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"RavenIntel")
DEBUG = -g
ifdef $(DEBUG)
HOST_DEBUG = -Xcompiler $(DEBUG)
endif
HOST_FLAGS = -ccbin=$(I_MPI_ROOT)/intel64/bin/mpicxx $(HOST_DEBUG) -Xcompiler -cxx=icc -Xcompiler -Wall
CUDA_ARCHITECTURES = -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_70,code=sm_70  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80
NVCC_FLAGS = $(DEBUG) -std=c++17
NCC       = nvcc $(HOST_FLAGS) --device-c $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
CPP       = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
LINKER    = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
MPI_INCL  = -I$(I_MPI_ROOT)/intel64/include
MPI_LIBS  = -L$(I_MPI_ROOT)/intel64/lib -lmpi -Xlinker -R -Xlinker $(I_MPI_ROOT)/intel64/lib
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL =  -I$(FFTW_HOME)/include
FFTW_LIBS =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif


ifeq ($(SYSTYPE),"RavenIntelProfiling")
DEBUG = -g
ifdef $(DEBUG)
HOST_DEBUG = -Xcompiler $(DEBUG)
endif
HOST_FLAGS = -ccbin=$(I_MPI_ROOT)/intel64/bin/mpicxx $(HOST_DEBUG) -Xcompiler -cxx=icc -Xcompiler -trace -Xcompiler -tcollect -Xcompiler -DUSE_VT -Xcompiler -Wall
CUDA_ARCHITECTURES = -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_70,code=sm_70  -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80
NVCC_FLAGS = $(DEBUG) -std=c++17
NCC       = nvcc $(HOST_FLAGS) --device-c $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
CPP       = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
LINKER    = nvcc $(HOST_FLAGS) $(CUDA_ARCHITECTURES) $(NVCC_FLAGS)
MPI_INCL  = -I$(I_MPI_ROOT)/intel64/include
MPI_LIBS  = -L$(I_MPI_ROOT)/intel64/lib -lmpi -Xlinker -R -Xlinker $(I_MPI_ROOT)/intel64/lib
GSL_INCL  =  -I$(GSL_HOME)/include
GSL_LIBS  =  -L$(GSL_HOME)/lib  -Xlinker -R -Xlinker $(GSL_HOME)/lib
HDF5_INCL =  -I$(HDF5_HOME)/include
HDF5_LIBS =  -L$(HDF5_HOME)/lib -Xlinker -R -Xlinker $(HDF5_HOME)/lib
FFTW_INCL =  -I$(FFTW_HOME)/include
FFTW_LIBS =  -L$(FFTW_HOME)/lib  -Xlinker -R -Xlinker $(FFTW_HOME)/lib
endif

ifeq ($(SYSTYPE),"Darwin")
NCC       =  mpicxx   -Wall -std=c++17
CPP       =  mpicxx   -Wall -std=c++17
GSL_INCL  =  -I/opt/homebrew/include  -I/opt/local/include
GSL_LIBS  =  -L/opt/homebrew/lib      -L/opt/local/lib
endif


ifeq ($(SYSTYPE),"Ubuntu")
NCC       =  mpicxx -g  -Wall -std=c++17
CPP       =  mpicxx  -g -Wall -std=c++17
GSL_INCL   = -I/opt/local/include
GSL_LIBS   = -L/opt/local/lib
HDF5_INCL  = -I/usr/lib/x86_64-linux-gnu/hdf5/serial/include
HDF5_LIBS  = -L/usr/lib/x86_64-linux-gnu/hdf5/serial/
FFTW_INCL  = -I/opt/local/include
FFTW_LIBS  = -L/opt/local/lib
endif


ifndef LINKER
LINKER = $(CPP)
endif


##########################################
#determine the needed object/header files#
##########################################
SUBDIRS += .


SUBDIRS += main
OBJS    += main/main.o main/init.o
INCL    += main/main.h


SUBDIRS += compute_serial
OBJS    += compute_serial/dg_compute_serial.o   compute_serial/dg_limiting_serial.o
INCL    += compute_serial/dg_compute_serial.hpp compute_serial/dg_limiting_serial.h


SUBDIRS += compute_general
OBJS    += compute_general/compute_general.o
INCL    += compute_general/compute_general.cuh


SUBDIRS += data
OBJS    += data/dg_data.o   data/allvars.o
INCL    += data/dg_data.hpp data/dg_params.h data/allvars.h data/macros.h data/constants.h data/data_types.h


SUBDIRS += io
OBJS    += io/parameters.o  io/dg_io.o
INCL    += io/parameters.h  io/dg_io.hpp


SUBDIRS += legendre
OBJS    += legendre/dg_legendre.o   legendre/dg_legendre_manual.o
INCL    += legendre/dg_legendre.hpp legendre/dg_gauss_autogen.hpp


SUBDIRS += riemann
OBJS    += riemann/riemann_hllc.o
INCL    += riemann/riemann_hllc.hpp


SUBDIRS += tests
OBJS    += tests/dg_geometry.o    tests/dg_setup_test_problems.o
INCL    += tests/dg_geometry.hpp  tests/dg_setup_test_problems.hpp


SUBDIRS += turbulence
OBJS    += turbulence/dg_ab_turb.o   turbulence/powerspectra.o  turbulence/pm_mpi_fft.o  turbulence/structure_function.o
INCL    += turbulence/dg_ab_turb.hpp turbulence/powerspectra.h  turbulence/pm_mpi_fft.h  turbulence/structure_function.hpp


SUBDIRS += utils
OBJS    += utils/cuda_utils.o  utils/misc.o  utils/dbl_compare.o   utils/dg_utils.o
INCL    += utils/cuda_utils.h  utils/misc.h  utils/dbl_compare.hpp utils/dg_utils.hpp


SUBDIRS += units
OBJS    +=
INCL    += units/units.h

SUBDIRS += cooling
OBJS    += cooling/source_function.o
INCL    += cooling/source_function.hpp cooling/cooling_function_table.hpp

SUBDIRS += bisection
OBJS    += bisection/bisection.o
INCL    += bisection/bisection.hpp


SUBDIRS += gitversion
OBJS    +=
INCL    += gitversion/version.h


ifeq (GPU,$(findstring GPU,$(CONFIGVARS)))
SUBDIRS += compute_cuda
OBJS    += compute_cuda/dg_compute_cuda.o   compute_cuda/dg_limiting_cuda.o  compute_cuda/dg_cuda_constantmemory.o    compute_cuda/dg_cuda_kernels.o   compute_cuda/dg_cuda_memory.o
INCL    += compute_cuda/dg_compute_cuda.cuh compute_cuda/dg_limiting_cuda.h  compute_cuda/dg_cuda_constantmemory.cuh  compute_cuda/dg_cuda_kernels.cuh compute_cuda/dg_cuda_memory.cuh compute_cuda/device_variables.cuh
endif

ifneq (GPU,$(findstring GPU,$(CONFIGVARS)))
LANGUAGE = -x c++
endif


################################
#determine the needed libraries#
################################



GSL_LIBS   += -lgsl -lgslcblas
HDF5_LIBS  += -lhdf5 -lz
MATH_LIBS  = -lm

ifeq (TURBULENCE,$(findstring TURBULENCE,$(CONFIGVARS)))  # test for activation of turbulence
ifeq (DOUBLEPRECISION_FFTW,$(findstring DOUBLEPRECISION_FFTW,$(CONFIGVARS)))  # test for double precision libraries
FFTW_LIBS += -lfftw3
else
FFTW_LIBS += -lfftw3f
endif
endif


MAKEFILES = $(MAKEFILE_LIST) buildsystem/Makefile.config

##########################
#combine compiler options#
##########################

CFLAGS = $(LANGUAGE) $(OPTIMIZE) $(OPT) $(HDF5_INCL) $(GSL_INCL) $(MPI_INCL) $(FFTW_INCL) -I$(BUILD_DIR) -I$(SRC_DIR)

LIBS = $(MATH_LIBS) $(HDF5_LIBS) $(GSL_LIBS) $(MPI_LIBS) $(FFTW_LIBS)


SUBDIRS := $(addprefix $(BUILD_DIR)/,$(SUBDIRS))
OBJS := $(addprefix $(BUILD_DIR)/,$(OBJS)) $(BUILD_DIR)/compile_time_info.o  $(BUILD_DIR)/version.o
INCL := $(addprefix $(SRC_DIR)/,$(INCL)) $(BUILD_DIR)/runrun.h


TO_CHECK := $(addsuffix .check, $(OBJS) $(patsubst $(SRC_DIR)%, $(BUILD_DIR)%, $(INCL)) )
TO_CHECK +=  $(BUILD_DIR)/Makefile.check
CONFIG_CHECK = $(BUILD_DIR)/$(notdir $(CONFIG)).check
DOCS_CHECK = $(BUILD_DIR)/config.check  $(BUILD_DIR)/param.check


################
#create subdirs#
################
RESULT := $(shell mkdir -p $(SUBDIRS)  )

###########################################
#create info file for command line options#
###########################################
RESULT := $(shell echo 'static const char *compiler_flags="$(CPP) $(CFLAGS)";' > $(BUILD_DIR)/compiler-command-line-args.h )

#############
#build rules#
#############


all:  check build

build: $(EXEC)

$(EXEC): $(OBJS)
	$(LINKER) $(OPTIMIZE)  $(OBJS) $(LIBS) -o $(EXEC)

clean:
	rm -f $(OBJS) $(EXEC) core*
	rm -f $(BUILD_DIR)/compile_time_info.cc  $(BUILD_DIR)/runrun.h
	rm -f $(TO_CHECK) $(CONFIG_CHECK)
	rm -f $(BUILD_DIR)/version.cc


$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp  $(INCL)  $(MAKEFILES)
	$(CPP) $(CFLAGS) -c $< -o $@

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cc  $(INCL)  $(MAKEFILES)
	$(CPP) $(CFLAGS) -c $< -o $@

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cu   $(INCL)  $(MAKEFILES)
	$(NCC) $(CFLAGS) -c  $< -o $@

$(BUILD_DIR)/compile_time_info.o: $(BUILD_DIR)/compile_time_info.cc $(MAKEFILES)
	$(CPP) $(CFLAGS) -c $< -o $@

check: $(CONFIG_CHECK)

check_docs: $(DOCS_CHECK)

$(CONFIG_CHECK): $(TO_CHECK) $(CONFIG) buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 2 $(CONFIG) $(CONFIG_CHECK) defines_extra $(TO_CHECK)

$(BUILD_DIR)/%.o.check: $(SRC_DIR)/%.cpp Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.o.check: $(SRC_DIR)/%.cu Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.o.check: $(SRC_DIR)/%.cc Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.h.check: $(SRC_DIR)/%.h Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.hpp.check: $(SRC_DIR)/%.hpp Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.cuh.check: $(SRC_DIR)/%.cuh Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.o.check: $(BUILD_DIR)/%.cc Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/%.h.check: $(BUILD_DIR)/%.h Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 1 $< $@ Template-Config.sh defines_extra

$(BUILD_DIR)/Makefile.check: Makefile Template-Config.sh defines_extra buildsystem/check.py
	@$(PYTHON) buildsystem/check.py 3 $< $@ Template-Config.sh defines_extra


valgrind: openmp.run
	valgrind \
		--leak-check=full \
		--show-leak-kinds=all \
		--track-origins=yes \
		./openmp.run

notes: docs/notes_2020-05-16.pdf

docs/%.pdf: docs/%.md
	pandoc -f markdown $^ -o $@
