import os
import shutil
import subprocess
import sys
import threading
from datetime import datetime
from functools import partial

from python.watchdog_plot import start_watchdog

run_shell = partial(subprocess.call, shell=True)


def get_timestamp():
    dateTimeObj = datetime.now()
    return dateTimeObj.strftime("%d-%m-%y-%H-%M-%S")


OUTPUT_FOLDER = "output/{}/".format(get_timestamp())

watchdog_thread = threading.Thread(
    target=start_watchdog, name="plotter", args=(OUTPUT_FOLDER,)
)

os.mkdir(OUTPUT_FOLDER)
os.mkdir(OUTPUT_FOLDER + "/data")
os.mkdir(OUTPUT_FOLDER + "/img")
if len(sys.argv) == 1:
    shutil.copy("Config.sh", OUTPUT_FOLDER)
    shutil.copy("param.txt", OUTPUT_FOLDER)
    config_file = "Config.sh"
    param_file = "param.txt"

elif len(sys.argv) == 2:
    shutil.copy(sys.argv[1], OUTPUT_FOLDER)
    shutil.copy("param.txt", OUTPUT_FOLDER)
    config_file = sys.argv[1]
    param_file = "param.txt"

else:
    shutil.copy(sys.argv[1], OUTPUT_FOLDER)
    shutil.copy(sys.argv[2], OUTPUT_FOLDER)
    config_file = sys.argv[1]
    param_file = sys.argv[2]

run_shell("make -j20 DIR={}".format(OUTPUT_FOLDER))
shutil.copy("python/plots.py", OUTPUT_FOLDER)
shutil.copy("python/watchdog_plot.py", OUTPUT_FOLDER)

watchdog_thread.start()

run_shell(
    "cd {} && ./run.run {} |& tee run.log; touch data/.finished".format(
        OUTPUT_FOLDER, param_file
    )
)


run_shell(
    "cd {} && module load anaconda && source activate py39 && nice -n20 python plots.py |& tee plots.log".format(
        OUTPUT_FOLDER
    )
)
