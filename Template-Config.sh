
#########################################
#  Pick compile-time options as needed  #
#  from this template.                  #
#########################################


#--------------------------------------- Gas physics

#GAMMA (5.0/3)                                # adiabatic index, if not set defaults to 5/3
#ISOTHERM_EQS                                 # selects isothermal equation of state

#--------------------------------------- Problem Type

#KELVIN_HELMHOLTZ_TEST
#KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS
#KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS_NO_DENSITY_JUMP
#VORTEX_SHEET_TEST
#YEE_VORTEX
#SOUNDWAVE_TEST
#SQUARE_ADVECTION
#IMPLOSION_TEST
#TURBULENCE
#ADVECTION_TEST
#GAUSSIAN_DIFFUSION_TEST
#POINT_EXPLOSION
#SHOCK_TUBE
#SHU_OSHER_SHOCKTUBE
#BOOSTED_SHOCK_TUBE
#DOUBLE_BLAST
#SINGLE_SHOCK
#SQUARE_ADVECTION
#GRAVITY_WAVES
#COOLING
#KAPPA=1
#EPSILON0=0.00001
#S0=0.1
#REFLECTIVE_X
#REFLECTIVE_Y
#REFLECTIVE_Z
#SPHERICAL_SHELL

#--------------------------------------- Number of dimenions, order, and resolution

ND=2                                          # sets number of dimensions
DEGREE_K=0                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D=64                            # number of cells per dimension

FINITE_VOLUME                                 # this goes back to a finite volume scheme and is supposed to only work with DEGREE_K=0


#--------------------------------------- Boundary conditions

#OUTFLOW


#--------------------------------------- Basic operation mode of code

#GPU                                          # enables compilation of GPU code


#--------------------------------------- Turbulence driving

#ISOTHERMAL_TURBULENCE
#AB_TURB
#AB_TURB_DECAYING
#STSPECTFORM=1                                # 0, 1, 2 or 3
#SGS_TURBULENCE_MAKE_PRODUCTION_DISSIPATION_SPECTRUM
#DECAYING_TURBULENCE
#USE_SLOW_DG_RESET_TEMPERATURE


#--------------------------------------- Riemann solver

#RIEMANN_GAMMA
#RIEMANN_HLL
#RIEMANN_HLLC
#RIEMANN_HLLD
#RIEMANN_ROSUNOV


#--------------------------------------- Viscosity

#VISCOSITY
#NAVIER_STOKES                                # enables kinematic viscosity and thermal conduction/diffusion
#ART_VISCOSITY                               # enables artificial viscosity for improved shock capturing
#ADVECT_ALPHA
#RICHTMYER_VISCOSITY                         # enables Richtmyer-Neumann style artificial viscosity
#USE_PRIMITIVE_PROJECTION
#DO_NOT_SAVE_W_PRIM                          # always recalculate primitive variables

#--------------------------------------- Limiting and floors

#ENABLE_POSITIVITY_LIMITING
#PRESSURE_FLOOR
#DENSITY_FLOOR

#ENABLE_MINMOD_LIMITING                      # makes only sense for linear order, i.e. DEGREE_K=1
#VERY_CONSERVATIVE_TIMESTEP_CRITERION        # use the positivity qpoints to estimating max tstep

#--------------------------------------- Outputs

#DATACUBE_OUTPUT

#--------------------------------------- Tests and debugging

#DEBUG
#BENCHMARK
#OUTPUT_EVERY_STEP

#--------------------------------------- Miscellaneous

#DOUBLEPRECISION_FFTW                         # if set, double precisions FFTs are used
#ENABLE_DYE                                   # if activated, this evolves a passive scalar dye
#MAPS_AT_CORNERS                              # if activated, images give values at left corner of pixels, otherwise at center of pixel
#MAPS_AT_FAR_CORNERS                          # if activated, images give values at right corner of pixels, otherwise at center of pixel
