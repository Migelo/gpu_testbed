
#include "runrun.h"

#ifdef TURBULENCE

#include <gsl/gsl_errno.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <chrono>
#include <fstream>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/macros.h"
#include "../legendre/dg_legendre.hpp"
#include "../turbulence/powerspectra.h"
#include "../utils/misc.h"

using namespace std::chrono;
typedef std::chrono::high_resolution_clock Time;

void powerspec::powerspec_turb(dg_data_t *dg, int filenr)
{
  const auto timestamp_start = Time::now();
  mpi_printf("[TURB] start turbulent powerspec computation for filenr=%d\n", filenr);

#if(ND != 3)
  Terminate("This presently only works for 3D\n");
#endif

  // initialize the FFT plan on our first call
  if(flag_finitialized == false)
    {
      powersepc_turb_init();
      flag_finitialized = true;
    }

  // allocate the memory to hold the velocity fields
  fft_real *velfield[3];

  velfield[0] = (fft_real *)malloc(maxfftsize * sizeof(fft_real));
  velfield[1] = (fft_real *)malloc(maxfftsize * sizeof(fft_real));
  velfield[2] = (fft_real *)malloc(maxfftsize * sizeof(fft_real));

  // since the outline of the FFTW slabs can be different from the way the simulation cube
  // is subdivided onto the MPI ranks, we need to import the portion of the DG weights that are needed to
  // compute the local velocity field according to the FFTW decomposition
  powerspec_turb_obtain_weights_for_local_slab(dg);

  // now obtain the velocity fields on the FFT mesh by evaluating the DG basis expansion
  powerspec_turb_obtain_fields(dg, velfield);

  // free the imported weights again
  powerspec_turb_free_weights_for_local_slab();

  // calculate the velocity dispersion from the velocity field
  powerspec_turb_calc_dispersion(velfield, vel_disp);

  // Now compute the power spectrum of the velocities
  for(int i = 0; i < BINS_PS; i++)
    {
      SumPower[i]   = 0;
      CountModes[i] = 0;
    }

  // measure the power spectrum for each component and add up the components
  // only in the first calle, we count the modes ending up in each bin
  powerspec_turb_calc_and_bin_spectrum(dg, velfield[0], 1);
  powerspec_turb_calc_and_bin_spectrum(dg, velfield[1], 0);
  powerspec_turb_calc_and_bin_spectrum(dg, velfield[2], 0);

  // collect it across the processors
  powerspec_turb_collect();

  // save the measured power spectrum
  powerspec_turb_save(dg, vel_disp, filenr);

  // free the velocity fields again
  free(velfield[2]);
  free(velfield[1]);
  free(velfield[0]);

  const auto fs            = Time::now() - timestamp_start;
  const auto scaling_timer = duration_cast<milliseconds>(fs);

  mpi_printf("[TURB] end turbulent power spectra. Time taken %lu ms\n", scaling_timer);
}

void powerspec::powerspec_turb_obtain_weights_for_local_slab(dg_data_t *dg)
{
  int *GridPlaneToTask = (int *)malloc(POWERSPEC_GRID * sizeof(int));

  for(int i = 0; i < NTask; i++)
    for(int j = 0; j < myplan.slabs_x_per_task[i]; j++)
      GridPlaneToTask[myplan.first_slab_x_of_task[i] + j] = i;

  int *SlabToTask = (int *)malloc(DG_NUM_CELLS_1D * sizeof(int));

  for(int i = 0; i < NTask; i++)
    for(int j = 0; j < NslabsPerTask[i]; j++)
      SlabToTask[FirstSlabOfTask[i] + j] = i;

  int *Recv_count  = (int *)malloc(NTask * sizeof(int));
  int *Send_count  = (int *)malloc(NTask * sizeof(int));
  int *Recv_offset = (int *)malloc(NTask * sizeof(int));
  int *Send_offset = (int *)malloc(NTask * sizeof(int));

  for(int i = 0; i < NTask; i++)
    {
      Recv_count[i] = 0;
      Send_count[i] = 0;
    }

  for(decltype(ND) x = 0; x < POWERSPEC_GRID; x++)
    {
      double xx              = (x + 0.5) / POWERSPEC_GRID * dg->getBoxSize();
      double x_in_cell_units = xx / dg->getCellSize();
      int xc                 = (int)x_in_cell_units;  // slab number

      int task     = SlabToTask[xc];  // task holding slab
      int gridtask = GridPlaneToTask[x];

      if(ThisTask == gridtask)
        Recv_count[task]++;

      if(ThisTask == task)
        Send_count[gridtask]++;
    }

  int nrecv = 0;
  int nsend = 0;

  for(int i = 0; i < NTask; i++)
    {
      nrecv += Recv_count[i];
      nsend += Send_count[i];

      if(i > 0)
        {
          Send_offset[i] = Send_offset[i - 1] + Send_count[i - 1];
          Recv_offset[i] = Recv_offset[i - 1] + Recv_count[i - 1];
        }
      else
        {
          Send_offset[0] = 0;
          Recv_offset[0] = 0;
        }
    }

  if(myplan.nslab_x != nrecv)
    Terminate(" myplan.nslab_x=%d != nrecv=%d ", myplan.nslab_x, nrecv);

  Weights = (double *)mymalloc("Weights", nrecv * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF * sizeof(double));
  double *weights_to_send =
      (double *)mymalloc("weights_to_send", nsend * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF * sizeof(double));

  for(int i = 0; i < NTask; i++)
    {
      Recv_count[i] = 0;
      Send_count[i] = 0;
    }

#ifdef USE_PRIMITIVE_PROJECTION
  vector<double> w_prim(dg->N_w, 0);
  project_to_primitives_cpu(dg, dg->w, w_prim.data());
#endif

  for(decltype(ND) x = 0; x < POWERSPEC_GRID; x++)
    {
      double xx              = (x + 0.5) / POWERSPEC_GRID * dg->getBoxSize();
      double x_in_cell_units = xx / dg->getCellSize();
      int xc                 = (int)x_in_cell_units;  // slab number

      int task     = SlabToTask[xc];  // task holding slab
      int gridtask = GridPlaneToTask[x];

      if(ThisTask == gridtask)
        Recv_count[task]++;

      if(ThisTask == task)
        {
#ifdef USE_PRIMITIVE_PROJECTION
          memcpy(weights_to_send + (Send_offset[gridtask] + Send_count[gridtask]) * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF,
                 &w_prim.data()[(xc - FirstSlabThisTask) * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF],
                 DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF * sizeof(double));
#else
          memcpy(weights_to_send + (Send_offset[gridtask] + Send_count[gridtask]) * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF,
                 &dg->w[(xc - FirstSlabThisTask) * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF],
                 DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF * sizeof(double));
#endif
          Send_count[gridtask]++;
        }
    }

  for(int i = 0; i < NTask; i++)
    {
      Recv_count[i] *= DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF;
      Send_count[i] *= DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF;
      Recv_offset[i] *= DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF;
      Send_offset[i] *= DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * NB * NF;
    }

  MPI_Alltoallv(weights_to_send, Send_count, Send_offset, MPI_DOUBLE, Weights, Recv_count, Recv_offset, MPI_DOUBLE, MyComm);

  myfree(weights_to_send);

  myfree(Send_offset);
  myfree(Recv_offset);
  myfree(Send_count);
  myfree(Recv_count);

  myfree(SlabToTask);
  myfree(GridPlaneToTask);
}

void powerspec::powerspec_turb_free_weights_for_local_slab(void)
{
  // free the imported weights again

  myfree(Weights);
}

/* this function determines the velocity fields by using the nearest cell's values
 */
void powerspec::powerspec_turb_obtain_fields(dg_data_t *dg, fft_real *velfield[3])
{
  mpi_printf("TURB: Start finding velocity fields\n");

  for(int x = myplan.slabstart_x; x < myplan.slabstart_x + myplan.nslab_x; x++)
    for(decltype(ND) y = 0; y < POWERSPEC_GRID; y++)
      for(decltype(ND) z = 0; z < POWERSPEC_GRID; z++)
        {
          double xx = (x + 0.5) / POWERSPEC_GRID * dg->getBoxSize();
          double yy = (y + 0.5) / POWERSPEC_GRID * dg->getBoxSize();

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;
#if ND == 2
          uint64_t cell = DG_NUM_CELLS_1D * (x - myplan.slabstart_x) + yc;
#endif

#if(ND == 3)
          double zz              = (z + 0.5) / POWERSPEC_GRID * dg->getBoxSize();
          double z_in_cell_units = zz / dg->getCellSize();
          int zc                 = (int)z_in_cell_units;
          double zrel            = 2.0 * (z_in_cell_units - zc) - 1.0;
          uint64_t cell          = (DG_NUM_CELLS_1D * (x - myplan.slabstart_x) + yc) * DG_NUM_CELLS_1D + zc;
#endif

          double fields[NF];
          for(decltype(ND) f = 0; f < NF; f++)
            {
              fields[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
#if(ND == 2)
                  fields[f] += Weights[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
#endif
#if(ND == 3)
                  fields[f] += Weights[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel, zrel);
#endif
                }
            }

          turb_large_array_offset off = POWERSPEC_GRID2 * (POWERSPEC_GRID * (x - myplan.slabstart_x) + y) + z;

#ifdef USE_PRIMITIVE_PROJECTION
          velfield[0][off] = fields[1];
          velfield[1][off] = fields[2];
#if(ND == 3)
          velfield[2][off] = fields[3];
#endif
#else
          velfield[0][off] = fields[1] / fields[0];
          velfield[1][off] = fields[2] / fields[0];
#if(ND == 3)
          velfield[2][off] = fields[3] / fields[0];
#endif
#endif
        }

  mpi_printf("done finding velocity field\n");
}

void powerspec::powersepc_turb_init(void)
{
  /* temporarily allocate some arrays to make sure that out-of-place plans are created */
  fft_real *densityfield = (fft_real *)malloc(POWERSPEC_GRID2 * sizeof(fft_real));
  fft_real *workspace    = (fft_real *)malloc(POWERSPEC_GRID2 * sizeof(fft_real));

  /* for single precision, the start of our FFT columns is presently only guaranteed to be 8-byte aligned */
  int alignflag = FFTW_UNALIGNED;

  int stride = POWERSPEC_GRIDz;

  int ndim[1] = {POWERSPEC_GRID}; /* dimension of the 1D transforms */
  /* Set up the FFTW plan  */
  myplan.forward_plan_zdir = FFTW(plan_many_dft_r2c)(1, ndim, 1, densityfield, 0, 1, POWERSPEC_GRID2, (fft_complex *)workspace, 0, 1,
                                                     POWERSPEC_GRIDz, FFTW_ESTIMATE | FFTW_DESTROY_INPUT | alignflag);

  myplan.forward_plan_ydir = FFTW(plan_many_dft)(1, ndim, 1, (fft_complex *)densityfield, 0, stride, POWERSPEC_GRIDz * POWERSPEC_GRID,
                                                 (fft_complex *)workspace, 0, stride, POWERSPEC_GRIDz * POWERSPEC_GRID, FFTW_FORWARD,
                                                 FFTW_ESTIMATE | FFTW_DESTROY_INPUT | alignflag);

  myplan.forward_plan_xdir = FFTW(plan_many_dft)(1, ndim, 1, (fft_complex *)densityfield, 0, stride, POWERSPEC_GRIDz * POWERSPEC_GRID,
                                                 (fft_complex *)workspace, 0, stride, POWERSPEC_GRIDz * POWERSPEC_GRID, FFTW_FORWARD,
                                                 FFTW_ESTIMATE | FFTW_DESTROY_INPUT | alignflag);

  free(workspace);
  free(densityfield);

  my_slab_based_fft_init(&myplan, POWERSPEC_GRID, POWERSPEC_GRID, POWERSPEC_GRID);

  maxfftsize =
      std::max<int>(myplan.largest_x_slab * POWERSPEC_GRID, myplan.largest_y_slab * POWERSPEC_GRID) * ((size_t)POWERSPEC_GRID2);
}

void powerspec::powerspec_turb_calc_and_bin_spectrum(dg_data_t *dg, fft_real *field, int flag)
{
  K0     = 2 * M_PI / dg->getBoxSize(); /* minimum k */
  K1     = K0 * POWERSPEC_GRID / 2;     /* maximum k */
  binfac = BINS_PS / (log(K1) - log(K0));

  fft_real *workspace = (fft_real *)malloc(maxfftsize * sizeof(fft_real));

  /* Do the FFT of field */
  my_slab_based_fft(&myplan, &field[0], &workspace[0], 1);

  free(workspace);

  fft_complex *fft_of_field = (fft_complex *)&field[0];

  for(decltype(ND) x = 0; x < POWERSPEC_GRID; x++)
    for(decltype(ND) y = myplan.slabstart_y; y < static_cast<decltype(ND)>(myplan.slabstart_y + myplan.nslab_y); y++)
      for(decltype(ND) z = 0; z < POWERSPEC_GRID; z++)
        {
          auto zz = z;
          if(z >= POWERSPEC_GRID / 2 + 1)
            zz = POWERSPEC_GRID - z;

          double kx, ky, kz;

          if(x > POWERSPEC_GRID / 2)
            kx = x - POWERSPEC_GRID;
          else
            kx = x;
          if(y > POWERSPEC_GRID / 2)
            ky = y - POWERSPEC_GRID;
          else
            ky = y;
          if(z > POWERSPEC_GRID / 2)
            kz = z - POWERSPEC_GRID;
          else
            kz = z;

          double k2 = kx * kx + ky * ky + kz * kz;

          turb_large_array_offset ip =
              ((turb_large_array_offset)POWERSPEC_GRIDz) * (POWERSPEC_GRID * (y - myplan.slabstart_y) + x) + zz;

          double po = (fft_of_field[ip][0] * fft_of_field[ip][0] + fft_of_field[ip][1] * fft_of_field[ip][1]) / pow(POWERSPEC_GRID, 6);

          if(k2 > 0)
            {
              if(k2 < (POWERSPEC_GRID / 2.0) * (POWERSPEC_GRID / 2.0))
                {
                  double k = sqrt(k2) * 2 * M_PI / dg->getBoxSize();

                  if(k >= K0 && k < K1)
                    {
                      int bin = log(k / K0) * binfac;

                      SumPower[bin] += po;

                      if(flag)
                        CountModes[bin] += 1;
                    }
                }
            }
        }
}

void powerspec::powerspec_turb_collect(void)
{
  long long int *countbuf = (long long int *)malloc(NTask * BINS_PS * sizeof(long long));
  double *powerbuf        = (double *)malloc(NTask * BINS_PS * sizeof(double));

  MPI_Allgather(CountModes, BINS_PS * sizeof(long long), MPI_BYTE, countbuf, BINS_PS * sizeof(long long), MPI_BYTE, MyComm);

  for(int i = 0; i < BINS_PS; i++)
    {
      CountModes[i] = 0;
      for(int n = 0; n < NTask; n++)
        CountModes[i] += countbuf[n * BINS_PS + i];
    }

  MPI_Allgather(SumPower, BINS_PS * sizeof(double), MPI_BYTE, powerbuf, BINS_PS * sizeof(double), MPI_BYTE, MyComm);

  for(int i = 0; i < BINS_PS; i++)
    {
      SumPower[i] = 0;
      for(int n = 0; n < NTask; n++)
        SumPower[i] += powerbuf[n * BINS_PS + i];
    }

  free(powerbuf);
  free(countbuf);

  for(int i = 0; i < BINS_PS; i++)
    {
      Kbin[i] = exp((i + 0.5) / binfac + log(K0));

      if(CountModes[i] > 0)
        Power[i] = SumPower[i] / CountModes[i];
      else
        Power[i] = 0;
    }
}

void powerspec::powerspec_turb_save(dg_data_t *dg, double *disp, int filenr)
{
  if(ThisTask == 0)
    {
      char fname[2 * MAXLEN_PATH];
      sprintf(fname, "%s/powerspec_vel_%03d.txt", All.OutputDir, filenr);

      FILE *fd = fopen(fname, "w");
      if(!fd)
        Terminate("can't open file `%s`\n", fname);

      fprintf(fd, "%g\n", dg->Time);
      fprintf(fd, "%d\n", (int)POWERSPEC_GRID);
      fprintf(fd, "%d\n", (int)BINS_PS);

      fprintf(fd, "%g\n", disp[0]);
      fprintf(fd, "%g\n", disp[1]);
      fprintf(fd, "%g\n", disp[2]);

      for(int i = 0; i < BINS_PS; i++)
        {
          fprintf(fd, "%g %g %g %g\n", Kbin[i], Power[i], (double)CountModes[i], SumPower[i]);
        }

      fclose(fd);
    }
}

void powerspec::powerspec_turb_calc_dispersion(fft_real *velfield[3], double vel_disp[3])
{
  for(int dim = 0; dim < 3; dim++)
    {
      double vsum = 0, vsum_all, vmean, vdisp = 0, vdisp_all;

      for(decltype(myplan.nslab_x) i = 0; i < myplan.nslab_x; i++)
        for(decltype(ND) j = 0; j < POWERSPEC_GRID; j++)
          for(decltype(ND) k = 0; k < POWERSPEC_GRID; k++)
            {
              int ip = POWERSPEC_GRID2 * (POWERSPEC_GRID * i + j) + k;

              vsum += velfield[dim][ip];
            }

      MPI_Allreduce(&vsum, &vsum_all, 1, MPI_DOUBLE, MPI_SUM, MyComm);
      vmean = vsum_all / pow(POWERSPEC_GRID, 3);

      for(decltype(myplan.nslab_x) i = 0; i < myplan.nslab_x; i++)
        for(decltype(ND) j = 0; j < POWERSPEC_GRID; j++)
          for(decltype(ND) k = 0; k < POWERSPEC_GRID; k++)
            {
              int ip = POWERSPEC_GRID2 * (POWERSPEC_GRID * i + j) + k;

              velfield[dim][ip] -= vmean;
            }

      for(decltype(myplan.nslab_x) i = 0; i < myplan.nslab_x; i++)
        for(decltype(ND) j = 0; j < POWERSPEC_GRID; j++)
          for(decltype(ND) k = 0; k < POWERSPEC_GRID; k++)
            {
              int ip = POWERSPEC_GRID2 * (POWERSPEC_GRID * i + j) + k;

              vdisp += velfield[dim][ip] * velfield[dim][ip];
            }

      MPI_Allreduce(&vdisp, &vdisp_all, 1, MPI_DOUBLE, MPI_SUM, MyComm);

      vel_disp[dim] = vdisp_all / pow(POWERSPEC_GRID, 3);
    }
}

powerspec ps;  // create an instance as a global variable

#endif
