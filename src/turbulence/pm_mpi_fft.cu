
#include "runrun.h"

#ifdef TURBULENCE

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../data/allvars.h"
#include "../turbulence/pm_mpi_fft.h"
#include "../utils/misc.h"

/*! \brief Initializes slab based FFT.
 *
 *  \param[out] plan FFT plan.
 *  \param[in] NgridX Number of grid points in X direction.
 *  \param[in] NgridY Number of grid points in Y direction.
 *  \param[in] NgridZ Number of grid points in Z direction.
 *
 *  \return void
 */
void my_slab_based_fft_init(fft_plan *plan, int NgridX, int NgridY, int NgridZ)
{
  subdivide_evenly(NgridX, NTask, ThisTask, &plan->slabstart_x, &plan->nslab_x);
  subdivide_evenly(NgridY, NTask, ThisTask, &plan->slabstart_y, &plan->nslab_y);

  plan->slab_to_task = (int *)mymalloc("slab_to_task", NgridX * sizeof(int));

  for(int task = 0; task < NTask; task++)
    {
      int start, n;

      subdivide_evenly(NgridX, NTask, task, &start, &n);

      for(int i = start; i < start + n; i++)
        plan->slab_to_task[i] = task;
    }

  MPI_Allreduce(&plan->nslab_x, &plan->largest_x_slab, 1, MPI_INT, MPI_MAX, MyComm);
  MPI_Allreduce(&plan->nslab_y, &plan->largest_y_slab, 1, MPI_INT, MPI_MAX, MyComm);

  plan->slabs_x_per_task = (int *)mymalloc("slabs_x_per_task", NTask * sizeof(int));
  MPI_Allgather(&plan->nslab_x, 1, MPI_INT, plan->slabs_x_per_task, 1, MPI_INT, MyComm);

  plan->first_slab_x_of_task = (int *)mymalloc("first_slab_x_of_task", NTask * sizeof(int));
  MPI_Allgather(&plan->slabstart_x, 1, MPI_INT, plan->first_slab_x_of_task, 1, MPI_INT, MyComm);

  plan->slabs_y_per_task = (int *)mymalloc("slabs_y_per_task", NTask * sizeof(int));
  MPI_Allgather(&plan->nslab_y, 1, MPI_INT, plan->slabs_y_per_task, 1, MPI_INT, MyComm);

  plan->first_slab_y_of_task = (int *)mymalloc("first_slab_y_of_task", NTask * sizeof(int));
  MPI_Allgather(&plan->slabstart_y, 1, MPI_INT, plan->first_slab_y_of_task, 1, MPI_INT, MyComm);

  plan->NgridX = NgridX;
  plan->NgridY = NgridY;
  plan->NgridZ = NgridZ;

  int Ngridz = NgridZ / 2 + 1; /* dimension needed in complex space */

  plan->Ngridz = Ngridz;
  plan->Ngrid2 = 2 * Ngridz;
}

/*! \brief Transposes the array field.
 *
 *  The array field is transposed such that the data in x direction is local
 *  to only one task. This is done, so the force in x-direction can be
 *  obtained by finite differencing. However the array is not fully
 *  transposed, i.e. the x-direction is not the fastest running array index.
 *
 *  \param[in] plan FFT pan.
 *  \param[in, out] field The array to transpose.
 *  \param[out] scratch Scratch space used during communication (same size as
 *              field).
 *
 *  \return void
 */
void my_slab_transposeA(fft_plan *plan, fft_real *field, fft_real *scratch)
{
  int flag_big = 0, flag_big_all = 0;

  int prod = NTask * plan->nslab_x;

  for(int n = 0; n < prod; n++)
    {
      int x    = n / NTask;
      int task = n % NTask;

      for(int y = plan->first_slab_y_of_task[task]; y < plan->first_slab_y_of_task[task] + plan->slabs_y_per_task[task]; y++)
        memcpy(scratch + ((size_t)plan->NgridZ) * (plan->first_slab_y_of_task[task] * plan->nslab_x +
                                                   x * plan->slabs_y_per_task[task] + (y - plan->first_slab_y_of_task[task])),
               field + ((size_t)plan->Ngrid2) * (plan->NgridY * x + y), plan->NgridZ * sizeof(fft_real));
    }

  size_t *scount = (size_t *)mymalloc("scount", NTask * sizeof(size_t));
  size_t *rcount = (size_t *)mymalloc("rcount", NTask * sizeof(size_t));
  size_t *soff   = (size_t *)mymalloc("soff", NTask * sizeof(size_t));
  size_t *roff   = (size_t *)mymalloc("roff", NTask * sizeof(size_t));

  for(int task = 0; task < NTask; task++)
    {
      scount[task] = plan->nslab_x * plan->slabs_y_per_task[task] * (plan->NgridZ * sizeof(fft_real));
      rcount[task] = plan->nslab_y * plan->slabs_x_per_task[task] * (plan->NgridZ * sizeof(fft_real));

      soff[task] = plan->first_slab_y_of_task[task] * plan->nslab_x * (plan->NgridZ * sizeof(fft_real));
      roff[task] = plan->first_slab_x_of_task[task] * plan->nslab_y * (plan->NgridZ * sizeof(fft_real));

      if(scount[task] > MPI_MESSAGE_SIZELIMIT_IN_BYTES)
        flag_big = 1;
    }

  MPI_Allreduce(&flag_big, &flag_big_all, 1, MPI_INT, MPI_MAX, MyComm);

  myMPI_Alltoallv(scratch, scount, soff, field, rcount, roff, 1, flag_big_all, MyComm);

  myfree(roff);
  myfree(soff);
  myfree(rcount);
  myfree(scount);
}

/*! \brief Undo the transposition of the array field.
 *
 *  The transposition of the array field is undone such that the data in
 *  x direction is distributed among all tasks again. Thus the result of
 *  force computation in x-direction is sent back to the original task.
 *
 *  \param[in] plan FFT plan.
 *  \param[in, out] field The array to transpose.
 *  \param[out] scratch Scratch space used during communication (same size as
 *              field).
 *
 *  \return void
 */
void my_slab_transposeB(fft_plan *plan, fft_real *field, fft_real *scratch)
{
  int flag_big = 0, flag_big_all = 0;

  size_t *scount = (size_t *)mymalloc("scount", NTask * sizeof(size_t));
  size_t *rcount = (size_t *)mymalloc("rcount", NTask * sizeof(size_t));
  size_t *soff   = (size_t *)mymalloc("soff", NTask * sizeof(size_t));
  size_t *roff   = (size_t *)mymalloc("roff", NTask * sizeof(size_t));

  for(int task = 0; task < NTask; task++)
    {
      rcount[task] = plan->nslab_x * plan->slabs_y_per_task[task] * (plan->NgridZ * sizeof(fft_real));
      scount[task] = plan->nslab_y * plan->slabs_x_per_task[task] * (plan->NgridZ * sizeof(fft_real));

      roff[task] = plan->first_slab_y_of_task[task] * plan->nslab_x * (plan->NgridZ * sizeof(fft_real));
      soff[task] = plan->first_slab_x_of_task[task] * plan->nslab_y * (plan->NgridZ * sizeof(fft_real));

      if(scount[task] > MPI_MESSAGE_SIZELIMIT_IN_BYTES)
        flag_big = 1;
    }

  MPI_Allreduce(&flag_big, &flag_big_all, 1, MPI_INT, MPI_MAX, MyComm);

  myMPI_Alltoallv(field, scount, soff, scratch, rcount, roff, 1, flag_big_all, MyComm);

  myfree(roff);
  myfree(soff);
  myfree(rcount);
  myfree(scount);

  int prod = NTask * plan->nslab_x;

  for(int n = 0; n < prod; n++)
    {
      int x    = n / NTask;
      int task = n % NTask;

      for(int y = plan->first_slab_y_of_task[task]; y < plan->first_slab_y_of_task[task] + plan->slabs_y_per_task[task]; y++)
        memcpy(field + ((size_t)plan->Ngrid2) * (plan->NgridY * x + y),
               scratch + ((size_t)plan->NgridZ) * (plan->first_slab_y_of_task[task] * plan->nslab_x +
                                                   x * plan->slabs_y_per_task[task] + (y - plan->first_slab_y_of_task[task])),
               plan->NgridZ * sizeof(fft_real));
    }
}

/*  \brief Transpose a slab decomposed 3D field.
 *
 *  Given a slab-decomposed 3D field a[...] with total dimension
 *  [nx x ny x nz], whose first dimension is split across the processors, this
 *  routine outputs in b[] the transpose where then the second dimension is
 *  split across the processors. sx[] gives for each MPI task how many slabs
 *  it has, and firstx[] is the first slab for a given task. Likewise,
 *  sy[]/firsty[] gives the same thing for the transposed order. Note, the
 *  contents of the array a[] will be destroyed by the routine.
 *
 *  An element (x,y,z) is accessed in a[] with index
 *  [([x - firstx] * ny + y) * nz + z] and in b[] as
 *  [((y - firsty) * nx + x) * nz + z]
 *
 *  \param[in, out] av Pointer to array a.
 *  \param[in, out] bv Pointer to array b.
 *  \param[in] sx Array storing number of slabs in each task.
 *  \param[in] fristx Array with first slab in each task.
 *  \param[in] sy Array storing number of transposed slabs in each task.
 *  \param[in] firsty Array storing first transposed slab in each task.
 *  \param[in] nx Number of elements in x direction.
 *  \param[in] ny Number of elements in y direction.
 *  \param[in] nz Number of elements in z direction.
 *  \param[in] mode If mode = 1, the reverse operation is carried out.
 *
 *  \return void
 */
static void my_slab_transpose(void *av, void *bv, int *sx, int *firstx, int *sy, int *firsty, int nx, int ny, int nz, int mode)
{
  char *a = (char *)av;
  char *b = (char *)bv;

  size_t *scount = (size_t *)mymalloc("scount", NTask * sizeof(size_t));
  size_t *rcount = (size_t *)mymalloc("rcount", NTask * sizeof(size_t));
  size_t *soff   = (size_t *)mymalloc("soff", NTask * sizeof(size_t));
  size_t *roff   = (size_t *)mymalloc("roff", NTask * sizeof(size_t));
  int prod, flag_big = 0, flag_big_all = 0;

  for(int i = 0; i < NTask; i++)
    {
      scount[i] = sy[i] * sx[ThisTask] * ((size_t)nz);
      rcount[i] = sy[ThisTask] * sx[i] * ((size_t)nz);
      soff[i]   = firsty[i] * sx[ThisTask] * ((size_t)nz);
      roff[i]   = sy[ThisTask] * firstx[i] * ((size_t)nz);

      if(scount[i] * sizeof(fft_complex) > MPI_MESSAGE_SIZELIMIT_IN_BYTES)
        flag_big = 1;
    }

  /* produce a flag if any of the send sizes is above our transfer limit, in this case we will
   * transfer the data in chunks.
   */
  MPI_Allreduce(&flag_big, &flag_big_all, 1, MPI_INT, MPI_MAX, MyComm);

  if(mode == 0)
    {
      /* first pack the data into contiguous blocks */
      prod = NTask * sx[ThisTask];

      for(int n = 0; n < prod; n++)
        {
          int k = n / NTask;
          int i = n % NTask;

          for(int j = 0; j < sy[i]; j++)
            memcpy(b + (k * sy[i] + j + firsty[i] * sx[ThisTask]) * (nz * sizeof(fft_complex)),
                   a + (k * ny + (firsty[i] + j)) * (nz * sizeof(fft_complex)), nz * sizeof(fft_complex));
        }

      /* tranfer the data */
      myMPI_Alltoallv(b, scount, soff, a, rcount, roff, sizeof(fft_complex), flag_big_all, MyComm);

      /* unpack the data into the right order */
      prod = NTask * sy[ThisTask];

      for(int n = 0; n < prod; n++)
        {
          int j = n / NTask;
          int i = n % NTask;

          for(int k = 0; k < sx[i]; k++)
            memcpy(b + (j * nx + k + firstx[i]) * (nz * sizeof(fft_complex)),
                   a + ((k + firstx[i]) * sy[ThisTask] + j) * (nz * sizeof(fft_complex)), nz * sizeof(fft_complex));
        }
    }
  else
    {
      /* first pack the data into contiguous blocks */
      prod = NTask * sy[ThisTask];

      for(int n = 0; n < prod; n++)
        {
          int j = n / NTask;
          int i = n % NTask;

          for(int k = 0; k < sx[i]; k++)
            memcpy(b + ((k + firstx[i]) * sy[ThisTask] + j) * (nz * sizeof(fft_complex)),
                   a + (j * nx + k + firstx[i]) * (nz * sizeof(fft_complex)), nz * sizeof(fft_complex));
        }

      /* tranfer the data */
      myMPI_Alltoallv(b, rcount, roff, a, scount, soff, sizeof(fft_complex), flag_big_all, MyComm);

      /* unpack the data into the right order */
      prod = NTask * sx[ThisTask];

      for(int n = 0; n < prod; n++)
        {
          int k = n / NTask;
          int i = n % NTask;

          for(int j = 0; j < sy[i]; j++)
            memcpy(b + (k * ny + (firsty[i] + j)) * (nz * sizeof(fft_complex)),
                   a + (k * sy[i] + j + firsty[i] * sx[ThisTask]) * (nz * sizeof(fft_complex)), nz * sizeof(fft_complex));
        }
    }

  /* now the result is in b[] */

  myfree(roff);
  myfree(soff);
  myfree(rcount);
  myfree(scount);
}

/*! \brief Performs a slab-based Fast Fourier transformation.
 *
 *  \param[in] plan FFT plan.
 *  \param[in, out] data Array to be Fourier transformed.
 *  \param[out] workspace Workspace to temporary operate in.
 *  \param[in] forward Forward (1) or backward (-1) Fourier transform?
 *
 *  \return void
 */
void my_slab_based_fft(fft_plan *plan, void *data, void *workspace, int forward)
{
  int prod;
  int slabsx = plan->slabs_x_per_task[ThisTask];
  int slabsy = plan->slabs_y_per_task[ThisTask];

  int ngridx  = plan->NgridX;
  int ngridy  = plan->NgridY;
  int ngridz  = plan->Ngridz;
  int ngridz2 = 2 * ngridz;

  size_t ngridx_long  = ngridx;
  size_t ngridy_long  = ngridy;
  size_t ngridz_long  = ngridz;
  size_t ngridz2_long = ngridz2;

  fft_real *data_real       = (fft_real *)data;
  fft_complex *data_complex = (fft_complex *)data, *workspace_complex = (fft_complex *)workspace;

  if(forward == 1)
    {
      /* do the z-direction FFT, real to complex */
      prod = slabsx * ngridy;

      for(int n = 0; n < prod; n++)
        {
          FFTW(execute_dft_r2c)(plan->forward_plan_zdir, data_real + n * ngridz2_long, workspace_complex + n * ngridz_long);
        }

      /* do the y-direction FFT, complex to complex */
      prod = slabsx * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->forward_plan_ydir, workspace_complex + i * ngridz * ngridy_long + j, data_complex + i * ngridz * ngridy_long + j);
        }

      /* now our data resides in data_complex[] */

      /* do the transpose */
      my_slab_transpose(data_complex, workspace_complex, plan->slabs_x_per_task, plan->first_slab_x_of_task, plan->slabs_y_per_task,
                        plan->first_slab_y_of_task, ngridx, ngridy, ngridz, 0);

      /* now the data is in workspace_complex[] */

      /* finally, do the transform along the x-direction (we are in transposed order, x and y have interchanged */
      prod = slabsy * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->forward_plan_xdir, workspace_complex + i * ngridz * ngridx_long + j, data_complex + i * ngridz * ngridx_long + j);
        }

      /* now the result is in data_complex[] */
    }
  else
    {
      prod = slabsy * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->backward_plan_xdir, data_complex + i * ngridz * ngridx_long + j, workspace_complex + i * ngridz * ngridx_long + j);
        }

      my_slab_transpose(workspace_complex, data_complex, plan->slabs_x_per_task, plan->first_slab_x_of_task, plan->slabs_y_per_task,
                        plan->first_slab_y_of_task, ngridx, ngridy, ngridz, 1);

      prod = slabsx * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->backward_plan_ydir, data_complex + i * ngridz * ngridy_long + j, workspace_complex + i * ngridz * ngridy_long + j);
        }

      prod = slabsx * ngridy;

      for(int n = 0; n < prod; n++)
        {
          FFTW(execute_dft_c2r)(plan->backward_plan_zdir, workspace_complex + n * ngridz_long, data_real + n * ngridz2_long);
        }

      /* now the result is in data[] */
    }
}

/*! \brief Performs a slab-based complex to complex Fast Fourier
 *         transformation.
 *
 *  \param[in] plan FFT plan.
 *  \param[in, out] data Array to be Fourier transformed.
 *  \param[out] workspace Workspace to temporary operate in.
 *  \param[in] forward Forward (1) or backward (-1) Fourier transformaiton?
 *
 *  \return void
 */
void my_slab_based_fft_c2c(fft_plan *plan, void *data, void *workspace, int forward)
{
  int prod;
  int slabsx = plan->slabs_x_per_task[ThisTask];
  int slabsy = plan->slabs_y_per_task[ThisTask];

  int ngridx = plan->NgridX;
  int ngridy = plan->NgridY;
  int ngridz = plan->NgridZ;

  size_t ngridx_long = ngridx;
  size_t ngridy_long = ngridy;

  fft_complex *data_start   = (fft_complex *)data;
  fft_complex *data_complex = (fft_complex *)data, *workspace_complex = (fft_complex *)workspace;

  if(forward == 1)
    {
      /* do the z-direction FFT, complex to complex */
      prod = slabsx * ngridy;

      for(int n = 0; n < prod; n++)
        {
          FFTW(execute_dft)(plan->forward_plan_zdir, data_start + n * ngridz, workspace_complex + n * ngridz);
        }

      /* do the y-direction FFT, complex to complex */
      prod = slabsx * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->forward_plan_ydir, workspace_complex + i * ngridz * ngridy_long + j, data_complex + i * ngridz * ngridy_long + j);
        }

      /* now our data resides in data_complex[] */

      /* do the transpose */
      my_slab_transpose(data_complex, workspace_complex, plan->slabs_x_per_task, plan->first_slab_x_of_task, plan->slabs_y_per_task,
                        plan->first_slab_y_of_task, ngridx, ngridy, ngridz, 0);

      /* now the data is in workspace_complex[] */

      /* finally, do the transform along the x-direction (we are in transposed order, x and y have interchanged */
      prod = slabsy * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->forward_plan_xdir, workspace_complex + i * ngridz * ngridx_long + j, data_complex + i * ngridz * ngridx_long + j);
        }

      /* now the result is in data_complex[] */
    }
  else
    {
      prod = slabsy * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->backward_plan_xdir, data_complex + i * ngridz * ngridx_long + j, workspace_complex + i * ngridz * ngridx_long + j);
        }

      my_slab_transpose(workspace_complex, data_complex, plan->slabs_x_per_task, plan->first_slab_x_of_task, plan->slabs_y_per_task,
                        plan->first_slab_y_of_task, ngridx, ngridy, ngridz, 1);

      prod = slabsx * ngridz;

      for(int n = 0; n < prod; n++)
        {
          int i = n / ngridz;
          int j = n % ngridz;

          FFTW(execute_dft)
          (plan->backward_plan_ydir, data_complex + i * ngridz * ngridy_long + j, workspace_complex + i * ngridz * ngridy_long + j);
        }

      prod = slabsx * ngridy;

      for(int n = 0; n < prod; n++)
        {
          FFTW(execute_dft)(plan->backward_plan_zdir, workspace_complex + n * ngridz, data_start + n * ngridz);
        }

      /* now the result is in data[] */
    }
}

#endif
