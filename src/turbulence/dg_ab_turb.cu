
#include "runrun.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_spline.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../tests/dg_geometry.hpp"
#include "../turbulence/dg_ab_turb.hpp"

#ifdef TURBULENCE
#if(ND > 1)

void init_turb(dg_data_t *dg)
{
  int ikxmax = dg->getBoxSize() * All.StKmax / (2 * M_PI);
  int ikymax = dg->getBoxSize() * All.StKmax / (2 * M_PI);
#if ND == 3
  int ikzmax = dg->getBoxSize() * All.StKmax / (2 * M_PI);
#else
  int ikzmax = 0;
#endif

  for(int rep = 0; rep < 2; rep++)
    {
      dg->StNModes = 0;

      for(int ikx = -ikxmax; ikx <= ikxmax; ikx++)
        for(int iky = -ikymax; iky <= ikymax; iky++)
          for(int ikz = -ikzmax; ikz <= ikzmax; ikz++)
            {
              if(ikx < 0)
                continue;

              if(ikx == 0 && iky < 0)
                continue;

              if(ikx == 0 && iky == 0 && ikz < 0)
                continue;

              double kx = 2. * M_PI * ikx / dg->getBoxSize();
              double ky = 2. * M_PI * iky / dg->getBoxSize();
              double kz = 2. * M_PI * ikz / dg->getBoxSize();
              double k  = sqrt(kx * kx + ky * ky + kz * kz);

              if(k > 0 && k >= All.StKmin && k <= All.StKmax)
                {
                  if(rep == 0)
                    {
                      // just count modes upon initial iteration
                      dg->StNModes++;
                    }

                  if(rep == 1)
                    {
                      double ampl = 0;

                      if(All.StSpectForm == 0)
                        {
                          ampl = 1.;
                        }
                      else if(All.StSpectForm == 1)
                        {
                          double kc   = 0.5 * (All.StKmin + All.StKmax);
                          double amin = 0.;
                          ampl = 4.0 * (amin - 1.0) / ((All.StKmax - All.StKmin) * (All.StKmax - All.StKmin)) * ((k - kc) * (k - kc)) +
                                 1.0;
                        }
                      else if(All.StSpectForm == 2)
                        {
                          ampl = pow(All.StKmin, sqrt(5. / 3)) / pow(k, sqrt(5. / 3));
                        }
                      else if(All.StSpectForm == 3)
                        {
                          ampl = pow(All.StKmin, 2.) / pow(k, 2.);
                        }
                      else
                        {
                          Terminate("unkown spectral form");
                        }

                      dg->StAmpl[dg->StNModes]         = ampl;
                      dg->StMode[3 * dg->StNModes + 0] = kx;
                      dg->StMode[3 * dg->StNModes + 1] = ky;
                      dg->StMode[3 * dg->StNModes + 2] = kz;
                      mpi_printf("TURB: Mode: %d, ikx=%d, iky=%d, ikz=%d, kx=%f, ky=%f, kz=%f, ampl=%f\n", dg->StNModes, ikx, iky, ikz,
                                 kx, ky, kz, ampl);

                      dg->StNModes++;
                    }
                }
            }

      if(rep == 0)
        {
          mpi_printf("TURB: Using %d modes, %d %d %d\n", dg->StNModes, ikxmax, ikymax, ikzmax);
#ifdef GPU
          gpuErrchk(cudaMallocHost((void **)&dg->StAmpl, dg->StNModes * sizeof(double)));
          gpuErrchk(cudaMallocHost((void **)&dg->StAka, 3 * dg->StNModes * sizeof(double)));
          gpuErrchk(cudaMallocHost((void **)&dg->StAkb, 3 * dg->StNModes * sizeof(double)));
          gpuErrchk(cudaMallocHost((void **)&dg->StMode, 3 * dg->StNModes * sizeof(double)));
          gpuErrchk(cudaMallocHost((void **)&dg->StOUPhases, 6 * dg->StNModes * sizeof(double)));
#else
          dg->StAmpl     = (double *)malloc(dg->StNModes * sizeof(double));
          dg->StAka      = (double *)malloc(3 * dg->StNModes * sizeof(double));
          dg->StAkb      = (double *)malloc(3 * dg->StNModes * sizeof(double));
          dg->StMode     = (double *)malloc(3 * dg->StNModes * sizeof(double));
          dg->StOUPhases = (double *)malloc(6 * dg->StNModes * sizeof(double));
#endif  // GPU
        }
    }

#ifdef GPU
  if(LocalDeviceRank >= 0)
    {
      myCudaMalloc(&dg->d_StAmpl, dg->StNModes * sizeof(double));
      myCudaMalloc(&dg->d_StAka, 3 * dg->StNModes * sizeof(double));
      myCudaMalloc(&dg->d_StAkb, 3 * dg->StNModes * sizeof(double));
      myCudaMalloc(&dg->d_StMode, 3 * dg->StNModes * sizeof(double));
    }
#endif

  dg->StOUVar = sqrt(All.StEnergy / All.StDecay);

#if ND == 3
  dg->StSolWeightNorm =
      sqrt(3.0 / 3.0) * sqrt(3.0) * 1.0 / sqrt(1.0 - 2.0 * All.StSolWeight + 3.0 * All.StSolWeight * All.StSolWeight);
#endif
#if ND == 2
  dg->StSolWeightNorm =
      sqrt(3.0 / 2.0) * sqrt(3.0) * 1.0 / sqrt(1.0 - 2.0 * All.StSolWeight + 2.0 * All.StSolWeight * All.StSolWeight);
#endif

  dg->StRng = gsl_rng_alloc(gsl_rng_ranlxd1);
  gsl_rng_set(dg->StRng, All.StSeed);  // note: with this Random number generator, want the *same* random numbers on every task
  // int gsl_rng_fread (FILE * stream, gsl_rng * r);
  // int gsl_rng_fwrite (FILE * stream, const gsl_rng * r);

  st_init_ouseq(dg);
  st_calc_phases(dg);

  mpi_printf("TURB: calling set_turb_ampl in init_turb\n");
  set_turb_ampl(dg, 0);

  mpi_printf("TURB: init done.\n");

  char buf[1000];
  sprintf(buf, "%s/turbulence.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdTurb = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}

void st_init_ouseq(dg_data_t *dg)
{
  for(int i = 0; i < 6 * dg->StNModes; i++)
    {
      dg->StOUPhases[i] = st_grn(dg) * dg->StOUVar;
    }
}

void st_update_ouseq(dg_data_t *dg)
{
  double damping = exp(-All.StDtFreq / All.StDecay);

  for(int i = 0; i < 6 * dg->StNModes; i++)
    {
      dg->StOUPhases[i] = dg->StOUPhases[i] * damping + dg->StOUVar * sqrt(1. - damping * damping) * st_grn(dg);
    }
}

double st_grn(dg_data_t *dg)
{
  double r0 = gsl_rng_uniform(dg->StRng);
  double r1 = gsl_rng_uniform(dg->StRng);

  // mpi_printf("r0=%f, r1=%f\n", r0, r1);

  return sqrt(2. * log(1. / r0)) * cos(2. * M_PI * r1);
}

void st_calc_phases(dg_data_t *dg)
{
  for(int i = 0; i < dg->StNModes; i++)
    {
      double ka = 0.;
      double kb = 0.;
      double kk = 0.;

      const int dim = ND;

      for(int j = 0; j < dim; j++)
        {
          kk += dg->StMode[3 * i + j] * dg->StMode[3 * i + j];
          ka += dg->StMode[3 * i + j] * dg->StOUPhases[6 * i + 2 * j + 1];
          kb += dg->StMode[3 * i + j] * dg->StOUPhases[6 * i + 2 * j + 0];
        }
      for(int j = 0; j < dim; j++)
        {
          double diva  = dg->StMode[3 * i + j] * ka / kk;
          double divb  = dg->StMode[3 * i + j] * kb / kk;
          double curla = dg->StOUPhases[6 * i + 2 * j + 0] - divb;
          double curlb = dg->StOUPhases[6 * i + 2 * j + 1] - diva;

          dg->StAka[3 * i + j] = All.StSolWeight * curla + (1. - All.StSolWeight) * divb;
          dg->StAkb[3 * i + j] = All.StSolWeight * curlb + (1. - All.StSolWeight) * diva;
        }
    }

#ifdef TURBULENCE
#ifdef GPU
  if(LocalDeviceRank >= 0)
    {
      gpuErrchk(cudaMemcpy(dg->d_StAmpl, dg->StAmpl, dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StAka, dg->StAka, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StAkb, dg->StAkb, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StMode, dg->StMode, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
    }
#endif
#endif
}

void set_turb_ampl(dg_data_t *dg, const double delta)
{
  dg->StTimeSinceLastUpdate += delta;

  while(dg->StTimeSinceLastUpdate > All.StDtFreq)
    {
      st_update_ouseq(dg);
      st_calc_phases(dg);
      mpi_printf("TURB: Updated turbulent stirring field at time %f.\n", dg->Time + delta);

      dg->StTimeSinceLastUpdate -= All.StDtFreq;
    }
}

double dg_get_ekin_serial(dg_data_t *dg)
{
  double Ekin = 0;

  for(decltype(Nc) c = 0; c < Nc; c++)
    {
      double rho = dg->w[w_idx_internal(0, 0, c)];  // mean cell density

#if(ND == 3)
      const double p2 =
          pow(dg->w[w_idx_internal(0, 1, c)], 2) + pow(dg->w[w_idx_internal(0, 2, c)], 2) + pow(dg->w[w_idx_internal(0, 3, c)], 2);
      const double ekin = 0.5 * p2 / rho;  // kinetic energy density
#endif

#if(ND == 2)
      const double p2   = pow(dg->w[w_idx_internal(0, 1, c)], 2) + pow(dg->w[w_idx_internal(0, 2, c)], 2);
      const double ekin = 0.5 * p2 / rho;  // kinetic energy density
#endif

      Ekin += ekin * dg->getCellVolume();
    }

  return Ekin;
}

double dg_get_mass_serial(dg_data_t *dg)
{
  double Mass = 0;

  for(decltype(Nc) c = 0; c < Nc; c++)
    {
      double rho = dg->w[w_idx_internal(0, 0, c)];  // mean cell density

      Mass += rho * dg->getCellVolume();
    }

  return Mass;
}

void output_turbulence_log(dg_data_t *dg)
{
  /* compute mean Mach number */

  double Ekin = 0, Mass = 0;

  if(LocalDeviceRank >= 0)
    {
#ifdef GPU
      Ekin = dg_get_ekin_cuda(dg);
      Mass = dg_get_mass_cuda(dg);
#endif
    }
  else
    {
      Ekin = dg_get_ekin_serial(dg);
      Mass = dg_get_mass_serial(dg);
    }

  MPI_Allreduce(MPI_IN_PLACE, &Ekin, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &Mass, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  double Mach = sqrt(2 * Ekin / Mass) / All.ViscParam.IsoSoundSpeed;

  double Total_TurbInjectedEnergy, Total_TurbDissipatedEnergy;

  MPI_Allreduce(&dg->TurbInjectedEnergy, &Total_TurbInjectedEnergy, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  MPI_Allreduce(&dg->TurbDissipatedEnergy, &Total_TurbDissipatedEnergy, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  if(ThisTask == 0)
    {
      printf("[OUTPUT_TURBULENCE_LOG] %1.8f  %1.8f  %1.8f  %1.8f  %1.8f  %1.8f\n", dg->Time, Total_TurbInjectedEnergy,
             Total_TurbDissipatedEnergy, Mach, dg->LimiterInjectedMass, dg->LimiterInjectedEnergy);
      fprintf(dg->FdTurb, "%1.8f  %1.8f  %1.8f  %1.8f  %1.8f  %1.8f\n", dg->Time, Total_TurbInjectedEnergy, Total_TurbDissipatedEnergy,
              Mach, dg->LimiterInjectedMass, dg->LimiterInjectedEnergy);
      fflush(dg->FdTurb);
    }
}

#endif  // TURBULENCE
#endif  // (ND > 1)
