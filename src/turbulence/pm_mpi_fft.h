#ifndef FFT_H_
#define FFT_H_

#include "runrun.h"

#ifdef TURBULENCE

#ifndef FFTW
#define CONCAT(prefix, name) prefix##name
#ifdef DOUBLEPRECISION_FFTW
#define FFTW(x) CONCAT(fftw_, x)
#else
#define FFTW(x) CONCAT(fftwf_, x)
#endif
#endif

#define FFTW_NO_Complex
#include <fftw3.h>

#ifdef DOUBLEPRECISION_FFTW
typedef double fft_real;
typedef fftw_complex fft_complex;
#else
typedef float fft_real;
typedef fftwf_complex fft_complex;
#endif
typedef ptrdiff_t fft_ptrdiff_t;

struct fft_plan
{
  int NgridX, NgridY, NgridZ;
  int Ngridz, Ngrid2;

  FFTW(plan) forward_plan_zdir;
  FFTW(plan) forward_plan_xdir;
  FFTW(plan) forward_plan_ydir;

  FFTW(plan) backward_plan_zdir;
  FFTW(plan) backward_plan_ydir;
  FFTW(plan) backward_plan_xdir;

  int *slab_to_task; /*!< Maps a slab index to the task responsible for the slab */
  int *slabs_x_per_task;
  int *first_slab_x_of_task; /*!< Array containing the index of the first slab of each task */
  int *slabs_y_per_task;     /*!< Array containing the number of slabs each task is responsible for */
  int *first_slab_y_of_task; /*!< Array containing the index of the first slab of each task */

  int nslab_x, slabstart_x, nslab_y, slabstart_y;
  int largest_x_slab; /*!< size of the largest slab in x direction */
  int largest_y_slab; /*!< size of the largest slab in y direction */
};

void my_slab_based_fft(fft_plan *plan, void *data, void *workspace, int forward);
void my_slab_based_fft_c2c(fft_plan *plan, void *data, void *workspace, int forward);

void my_slab_based_fft_init(fft_plan *plan, int NgridX, int NgridY, int NgridZ);
void my_slab_transposeA(fft_plan *plan, fft_real *field, fft_real *scratch);
void my_slab_transposeB(fft_plan *plan, fft_real *field, fft_real *scratch);

void my_column_based_fft_init(fft_plan *plan, int NgridX, int NgridY, int NgridZ);
void my_column_based_fft_init_c2c(fft_plan *plan, int NgridX, int NgridY, int NgridZ);
void my_column_based_fft(fft_plan *plan, void *data, void *workspace, int forward);
void my_column_based_fft_c2c(fft_plan *plan, void *data, void *workspace, int forward);

void my_fft_swap23(fft_plan *plan, fft_real *data, fft_real *out);
void my_fft_swap13(fft_plan *plan, fft_real *data, fft_real *out);
void my_fft_swap23back(fft_plan *plan, fft_real *data, fft_real *out);
void my_fft_swap13back(fft_plan *plan, fft_real *data, fft_real *out);

#endif
#endif
