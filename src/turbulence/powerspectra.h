// Copyright 2021 Volker Springel
#ifndef DG_POWERSPEC_H_
#define DG_POWERSPEC_H_

#include "runrun.h"

#ifdef TURBULENCE

#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../turbulence/pm_mpi_fft.h"

#define POWERSPEC_GRID (DG_NUM_CELLS_1D * DG_NUM_QPOINTS_1D)
#define POWERSPEC_GRIDz (POWERSPEC_GRID / 2 + 1)
#define POWERSPEC_GRID2 (2 * POWERSPEC_GRIDz)

#define BINS_PS 2000 /* number of bins for power spectrum computation */

#if(POWERSPEC_GRID > 1024)
typedef long long turb_large_array_offset;
#else
typedef unsigned int turb_large_array_offset;
#endif

class powerspec
{
 public:
  size_t maxfftsize;
  fft_plan myplan;
  bool flag_finitialized = false;

  long long CountModes[BINS_PS];
  double SumPower[BINS_PS];
  double Power[BINS_PS];
  double Kbin[BINS_PS];
  double K0;
  double K1;
  double binfac;

  double vel_disp[3];

  double *Weights;

  void powerspec_turb(dg_data_t *dg, int filenr);
  void powerspec_turb_obtain_fields(dg_data_t *dg, fft_real *velfield[3]);
  void powersepc_turb_init(void);
  void powerspec_turb_calc_and_bin_spectrum(dg_data_t *dg, fft_real *field, int flag);
  void powerspec_turb_collect(void);
  void powerspec_turb_save(dg_data_t *dg, double *disp, int filenr);
  void powerspec_turb_calc_dispersion(fft_real *velfield[3], double vel_disp[3]);
  void powerspec_turb_obtain_weights_for_local_slab(dg_data_t *dg);
  void powerspec_turb_free_weights_for_local_slab(void);
};

extern powerspec ps;  // create an instance as a global variable

#endif

#endif
