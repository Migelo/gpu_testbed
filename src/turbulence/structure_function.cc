
#include "runrun.h"

#ifdef TURBULENCE

#include "./structure_function.hpp"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <chrono>
#include <fstream>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/macros.h"
#include "../legendre/dg_legendre.hpp"
#include "../turbulence/powerspectra.h"
#include "../utils/misc.h"

using ::std::max;
using ::std::min;

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::initialise_random_number_generator()
{
  mt.seed(rd() + ThisTask);
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_random_distances()
{
  distances.resize(SAMPLES_PER_TASK);
  std::fill(distances.begin(), distances.end(), 0.0);
  int distances_idx = 0;
  for(int nbin = 0; nbin < NBINS; nbin++)
    {
      std::uniform_real_distribution<double> dist(distance_bin_edges.at(nbin), distance_bin_edges.at(nbin + 1));
      for(int i = 0; i < SAMPLES_PER_BIN_THIS_TASK; i++)
        {
          const auto random_distance  = dist(mt);
          distances.at(distances_idx) = random_distance;
          distances_idx++;
        }
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_random_angles()
{
  phi.resize(SAMPLES_PER_TASK);
  theta.resize(SAMPLES_PER_TASK);
  std::fill(phi.begin(), phi.end(), 0.0);
  std::fill(theta.begin(), theta.end(), 0.0);

  std::uniform_real_distribution<double> dist(0.0, 2.0 * M_PI);
  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      phi.at(i) = dist(mt);
    }

  std::uniform_real_distribution<double> dist2(-1.0, 1.0);
  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      theta.at(i) = acos(dist2(mt));
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_first_random_point()
{
  point_pairs.resize(SAMPLES_PER_TASK);
  std::fill(point_pairs.begin(), point_pairs.end(), point_pair_t());

  std::uniform_real_distribution<double> xyz_random(0.0, box_size);

  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      auto *point1 = &point_pairs.at(i).point1;

      for(int j = 0; j < 3; j++)
        point1->coords[j] = xyz_random(mt);
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_second_point_from_distances_and_angles()
{
  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      auto *point_pair = &point_pairs.at(i);

      const auto point1      = point_pair->point1;
      const double global_x1 = point1.x;
      const double global_y1 = point1.y;
      const double global_z1 = point1.z;

      auto point2        = point_pair->point2;
      const double r     = distances.at(i);
      const double theta = this->theta.at(i);
      const double phi   = this->phi.at(i);
      point2.x           = global_x1 + r * sin(theta) * cos(phi);
      point2.y           = global_y1 + r * sin(theta) * sin(phi);
      point2.z           = global_z1 + r * cos(theta);

      point2.periodic_wrap(box_size);

      point_pair->point2 = point2;
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_velocity_at_point(const dg_data_t *dg, int pnr)
{
  vector<int> number_of_elements_we_are_sending_to_other_ranks(NTask);
  int total_number_of_elements_we_are_sending_to_other_ranks;
  vector<int> send_displacements(NTask, 0);
  vector<point_t> send_buffer;

  for(int phase = 0; phase < 2; phase++)
    {
      std::fill(number_of_elements_we_are_sending_to_other_ranks.begin(), number_of_elements_we_are_sending_to_other_ranks.end(), 0);
      total_number_of_elements_we_are_sending_to_other_ranks = 0;

      for(int i = 0; i < SAMPLES_PER_TASK; i++)
        {
          point_t &myPoint     = (pnr == 0) ? this->point_pairs.at(i).point1 : this->point_pairs.at(i).point2;
          const int which_rank = SlabToTask[myPoint.x / cellSize];

          int nelem = number_of_elements_we_are_sending_to_other_ranks.at(which_rank)++;
          total_number_of_elements_we_are_sending_to_other_ranks++;

          if(phase == 1)
            send_buffer[send_displacements[which_rank] + nelem] = myPoint;
        }

      if(phase == 0)
        {
          send_buffer.resize(total_number_of_elements_we_are_sending_to_other_ranks);
          int cumsum = 0;
          for(int i = 1; i < NTask; i++)
            {
              cumsum += number_of_elements_we_are_sending_to_other_ranks.at(i - 1);
              send_displacements.at(i) = cumsum;
            }
        }
    }

  vector<int> number_of_elements_we_get_from_each_rank(NTask, 0);

  MPI_Alltoall(number_of_elements_we_are_sending_to_other_ranks.data(), 1, MPI_INT, number_of_elements_we_get_from_each_rank.data(), 1,
               MPI_INT, MPI_COMM_WORLD);

  const int total_number_of_elements_we_are_receiving_from_other_ranks =
      std::accumulate(number_of_elements_we_get_from_each_rank.begin(), number_of_elements_we_get_from_each_rank.end(), 0);

  vector<point_t> receive_buffer(total_number_of_elements_we_are_receiving_from_other_ranks);
  vector<int> receive_displacements(NTask, 0);
  {
    int cumsum = 0;
    for(int i = 1; i < NTask; i++)
      {
        cumsum += number_of_elements_we_get_from_each_rank.at(i - 1);
        receive_displacements.at(i) = cumsum;
      }
  }

  MPI_Datatype point_type;
  MPI_Type_contiguous(sizeof(point_t), MPI_BYTE, &point_type);
  MPI_Type_commit(&point_type);

  MPI_Alltoallv(send_buffer.data(), number_of_elements_we_are_sending_to_other_ranks.data(), send_displacements.data(), point_type,
                receive_buffer.data(), number_of_elements_we_get_from_each_rank.data(), receive_displacements.data(), point_type,
                MPI_COMM_WORLD);

  // now replace the coordinates with the velocity at the same coordinate
  for(int i = 0; i < total_number_of_elements_we_are_receiving_from_other_ranks; i++)
    {
      get_velocity_at_point(receive_buffer[i], dg);
    }

  // send them back to where the coordinates came from
  MPI_Alltoallv(receive_buffer.data(), number_of_elements_we_get_from_each_rank.data(), receive_displacements.data(), point_type,
                send_buffer.data(), number_of_elements_we_are_sending_to_other_ranks.data(), send_displacements.data(), point_type,
                MPI_COMM_WORLD);

  // fill this in to the original point array
  std::fill(number_of_elements_we_are_sending_to_other_ranks.begin(), number_of_elements_we_are_sending_to_other_ranks.end(), 0);

  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      point_t &myPoint     = (pnr == 0) ? this->point_pairs.at(i).point1 : this->point_pairs.at(i).point2;
      const int which_rank = SlabToTask[myPoint.x / cellSize];

      int nelem = number_of_elements_we_are_sending_to_other_ranks.at(which_rank)++;

      myPoint = send_buffer[send_displacements[which_rank] + nelem];
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::initialise_mpi_environment()
{
  SlabToTask.resize(DG_NUM_CELLS_1D);

  for(int i = 0; i < NTask; i++)
    for(int j = 0; j < NslabsPerTask[i]; j++)
      SlabToTask[FirstSlabOfTask[i] + j] = i;
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::extract_necessary_data_from_dg(const dg_data_t *dg)
{
  box_size      = dg->getBoxSize();
  cellSize      = dg->getCellSize();
  N_w           = dg->N_w;
  w             = dg->w;
  output_number = dg->dump;
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::project_to_primitives(const dg_data_t *dg)
{
  w_prim.resize(N_w);
  std::fill(w_prim.begin(), w_prim.end(), 0.0);

  project_to_primitives_cpu(dg, w, w_prim.data());
}

template <int SAMPLES_PER_BIN>
int structure_function<SAMPLES_PER_BIN>::get_bin_index_from_distance(const double distance)
{
  int bin = log(distance / MIN_DISTANCE) / log(MAX_DISTANCE / MIN_DISTANCE) * NBINS;
  if(bin < 0 || bin >= NBINS)
    Terminate("strange bin=%d\n", bin);

  return bin;
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::bin_vdiff()
{
  distance_bin_counts.resize(NBINS);
  distance_bin_velocity_sum.resize(NBINS);
  std::fill(distance_bin_counts.begin(), distance_bin_counts.end(), 0);
  std::fill(distance_bin_velocity_sum.begin(), distance_bin_velocity_sum.end(), 0.0);

  MachNumber = 0;
  TotCount   = 0;

  for(int i = 0; i < SAMPLES_PER_TASK; i++)
    {
      double v2diff = 0;
      for(int j = 0; j < 3; j++)
        {
          v2diff += pow(point_pairs[i].point1.coords[j] - point_pairs[i].point2.coords[j], 2);
          MachNumber += pow(point_pairs[i].point1.coords[j], 2);
        }

      const auto bin_idx = get_bin_index_from_distance(distances[i]);

      distance_bin_counts.at(bin_idx)++;
      distance_bin_velocity_sum.at(bin_idx) += v2diff;

      TotCount++;
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::generate_bins()
{
  distance_bin_edges.resize(NBINS + 1);
  distance_bin_edges.at(0) = MIN_DISTANCE;
  constexpr auto fac       = pow(MAX_DISTANCE / MIN_DISTANCE, 1.0 / NBINS);
  for(int i = 1; i < NBINS + 1; i++)
    {
      distance_bin_edges.at(i) = distance_bin_edges.at(i - 1) * fac;
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::exchange_bin_counts_and_sums_between_tasks()
{
  MPI_Allreduce(MPI_IN_PLACE, distance_bin_counts.data(), NBINS, MPI_UINT64_T, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, distance_bin_velocity_sum.data(), NBINS, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  MPI_Allreduce(MPI_IN_PLACE, &MachNumber, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &TotCount, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::average_bin_velocity_sum()
{
  distance_bin_velocity_average.resize(NBINS);
  std::fill(distance_bin_velocity_average.begin(), distance_bin_velocity_average.end(), 0.0);

  for(int i = 0; i < NBINS; i++)
    {
      if(distance_bin_counts.at(i) > 0)
        distance_bin_velocity_average.at(i) = distance_bin_velocity_sum.at(i) / distance_bin_counts.at(i);
    }

  MachNumber = sqrt(MachNumber / TotCount);
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::write_results_to_file(const dg_data_t *dg)
{
  if(ThisTask == 0)
    {
      char filepath[1024];
      sprintf(filepath, "%s/velocity_structure_function_nsamples%d_%04d.dat", All.OutputDir, SAMPLES_PER_BIN, output_number);
      std::ofstream fout(filepath);
      fout << NBINS << std::endl;
      fout << dg->Time << std::endl;
      fout << MachNumber << std::endl;
      for(int i = 0; i < NBINS; i++)
        {
          fout << sqrt(distance_bin_edges.at(i) * distance_bin_edges.at(i + 1)) << " " << distance_bin_velocity_average.at(i)
               << std::endl;
        }
      fout.close();
    }
}

template <int SAMPLES_PER_BIN>
void structure_function<SAMPLES_PER_BIN>::get_velocity_at_point(point_t &point, const dg_data_t *dg)
{
  const auto xx = point.x;
  const auto yy = point.y;
  const auto zz = point.z;

  double x_in_cell_units = xx / cellSize;
  double y_in_cell_units = yy / cellSize;
  double z_in_cell_units = zz / cellSize;

  int xc = (int)x_in_cell_units;
  int yc = (int)y_in_cell_units;
  int zc = (int)z_in_cell_units;

  double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
  double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;
  double zrel = 2.0 * (z_in_cell_units - zc) - 1.0;

  const auto cell = dg_geometry_cell_to_idx(xc, yc, zc, DG_NUM_CELLS_1D) - cell_stride_x * FirstSlabThisTask;

  assert((uint64_t)cell < Nc);

  vector<double> fields(3, 0);
  for(decltype(ND) f = 1; f < 4; f++)
    {
      for(decltype(ND) b = 0; b < NB; b++)
        {
          fields[f - 1] += w_prim[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel, zrel);
        }
    }

  // as output, we replace the coordinate with the velocity value at that coordinate
  for(int i = 0; i < 3; i++)
    {
      point.coords[i] = fields[i];
    }
}

template class structure_function<10'000>;
template class structure_function<100'000>;
template class structure_function<1'000'000>;
#endif  // TURBULENCE
