#pragma once

#include "runrun.h"

#ifdef TURBULENCE

#include <gsl/gsl_errno.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <chrono>
#include <fstream>
#include <random>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/macros.h"
#include "../legendre/dg_legendre.hpp"
#include "../turbulence/powerspectra.h"
#include "../utils/misc.h"

using ::std::vector;

constexpr int NBINS           = 100;
constexpr double MIN_DISTANCE = 1.0 / (DG_NUM_CELLS_1D * DG_ORDER);
constexpr double MAX_DISTANCE = 0.5;

struct point_t
{
  union
  {
    double coords[3];
    struct
    {
      double x;
      double y;
      double z;
    };
  };
  void periodic_wrap(const double box_size)
  {
    for(int i = 0; i < 3; i++)
      this->coords[i] = fmod(this->coords[i], box_size);

    for(int i = 0; i < 3; i++)
      this->coords[i] += (this->coords[i] < 0) * box_size;
  }
};

struct point_pair_t
{
  point_t point1;
  point_t point2;
};
template<int SAMPLES_PER_BIN>
class structure_function
{
 public:
  std::random_device rd;
  std::mt19937 mt;

  structure_function(const dg_data_t *dg)
  {
    mpi_printf("[STRUCTURE_FUNCTION] starting\n");

    extract_necessary_data_from_dg(dg);

    initialise_random_number_generator();

    initialise_mpi_environment();

    generate_bins();

    project_to_primitives(dg);

    get_random_distances();

    get_random_angles();

    get_first_random_point();

    get_second_point_from_distances_and_angles();

    get_velocity_at_point(dg, 0);  // first point

    get_velocity_at_point(dg, 1);  // second point

    bin_vdiff();

    exchange_bin_counts_and_sums_between_tasks();

    average_bin_velocity_sum();

    write_results_to_file(dg);

    mpi_printf("[STRUCTURE_FUNCTION] finished\n");
  }

  int samples_per_bin;
  const int SAMPLES_PER_BIN_THIS_TASK = (ThisTask == 0) ? SAMPLES_PER_BIN / NTask + SAMPLES_PER_BIN % NTask : SAMPLES_PER_BIN / NTask;
  const int SAMPLES_PER_TASK          = SAMPLES_PER_BIN_THIS_TASK * NBINS;

  vector<int> SlabToTask;

  vector<double> distance_bin_edges;
  vector<uint64_t> distance_bin_counts;
  vector<double> distance_bin_velocity_sum;
  vector<double> distance_bin_velocity_average;

  vector<double> distances;
  vector<double> phi;
  vector<double> theta;
  vector<point_pair_t> point_pairs;

  vector<double> w_prim;

  double MachNumber;
  double TotCount;
  double box_size;
  double cellSize;

  int output_number;

  uint64_t N_w;
  double *w;

  void initialise_random_number_generator();
  void initialise_mpi_environment();
  void extract_necessary_data_from_dg(const dg_data_t *dg);
  void project_to_primitives(const dg_data_t *dg);
  void get_random_distances();
  void get_random_angles();
  void get_first_random_point();
  void get_second_point_from_distances_and_angles();
  void get_velocity_at_point(const dg_data_t *dg, int pnr);
  void bin_vdiff();
  void generate_bins();
  int get_bin_index_from_distance(const double distance);
  void exchange_bin_counts_and_sums_between_tasks();
  void average_bin_velocity_sum();
  void write_results_to_file(const dg_data_t *dg);
  void get_velocity_at_point(point_t &point, const dg_data_t *dg);
};

#endif
