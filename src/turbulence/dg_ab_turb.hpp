// Copyright 2020 Miha Cernetic
#ifndef DG_AB_TURB_HPP_
#define DG_AB_TURB_HPP_

#include "runrun.h"

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../utils/dg_utils.hpp"

void init_turb(dg_data_t *dg);
void st_init_ouseq(dg_data_t *dg);
void st_update_ouseq(dg_data_t *dg);
double st_grn(dg_data_t *dg);
void st_calc_phases(dg_data_t *dg);
void set_turb_ampl(dg_data_t *dg, const double delta);

double dg_get_ekin_cuda(dg_data_t *dg);
double dg_get_mass_cuda(dg_data_t *dg);
double dg_get_ekin_serial(dg_data_t *dg);
double dg_get_mass_serial(dg_data_t *dg);

#endif  // DG_AB_TURB_HPP_
