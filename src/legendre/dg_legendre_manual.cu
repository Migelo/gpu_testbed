// Copyright 2020 Miha Cernetic

#include "runrun.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include <cassert>
#include <iostream>
#include <vector>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../legendre/dg_legendre.hpp"

using ::std::cout;
using ::std::endl;

double legendre_normalized(int order, double x) { return sqrt(2 * order + 1) * legendre(order, x); }

double legendre_diff_normalized(int order, double x) { return sqrt(2 * order + 1) * legendre_diff(order, x); }

double legendre(int order, double x)
{
  switch(order)
    {
      case 0:
        return 1;
      case 1:
        return x;
      case 2:
        return 0.5 * (3 * x * x - 1);
      case 3:
        return 0.5 * (5 * pow(x, 3) - 3 * x);
      case 4:
        return (1.0 / 8) * (35 * pow(x, 4) - 30 * x * x + 3);
      case 5:
        return (1.0 / 8) * (63 * pow(x, 5) - 70 * pow(x, 3) + 15 * x);
      case 6:
        return (1.0 / 16) * (231 * pow(x, 6) - 315 * pow(x, 4) + 105 * x * x - 5);
      case 7:
        return (1.0 / 16) * (429 * pow(x, 7) - 693 * pow(x, 5) + 315 * pow(x, 3) - 35 * x);
      case 8:
        return (1.0 / 128) * (6435 * pow(x, 8) - 12012 * pow(x, 6) + 6930 * pow(x, 4) - 1260 * x * x + 35);
      case 9:
        return (1.0 / 128) * (12155 * pow(x, 9) - 25740 * pow(x, 7) + 18018 * pow(x, 5) - 4620 * pow(x, 3) + 315 * x);
      case 10:
        return (1.0 / 256) * (46189 * pow(x, 10) - 109395 * pow(x, 8) + 90090 * pow(x, 6) - 30030 * pow(x, 4) + 3465 * x * x - 63);
      default:
        Terminate("undefined\n");
        break;
    }
}

double legendre_diff(int order, double x)
{
  switch(order)
    {
      case 0:
        return 0;
      case 1:
        return 1;
      case 2:
        return 3 * x;
      case 3:
        return 0.5 * (15 * pow(x, 2) - 3);
      case 4:
        return (1.0 / 8) * (35 * 4 * pow(x, 3) - 60 * x);
      case 5:
        return (1.0 / 8) * (63 * 5 * pow(x, 4) - 70 * 3 * pow(x, 2) + 15);
      case 6:
        return (1.0 / 16) * (231 * 6 * pow(x, 5) - 315 * 4 * pow(x, 3) + 105 * 2 * x);
      case 7:
        return (1.0 / 16) * (429 * 7 * pow(x, 6) - 693 * 5 * pow(x, 4) + 315 * 3 * pow(x, 2) - 35);
      case 8:
        return (1.0 / 128) * (6435 * 8 * pow(x, 7) - 12012 * 6 * pow(x, 5) + 6930 * 4 * pow(x, 3) - 1260 * 2 * x);
      case 9:
        return (1.0 / 128) * (12155 * 9 * pow(x, 8) - 25740 * 7 * pow(x, 6) + 18018 * 5 * pow(x, 4) - 4620 * 3 * pow(x, 2) + 315);
      case 10:
        return (1.0 / 256) *
               (46189 * 10 * pow(x, 9) - 109395 * 8 * pow(x, 7) + 90090 * 6 * pow(x, 5) - 30030 * 4 * pow(x, 3) + 3465 * 2 * x);
      default:
        Terminate("undefined\n");
        break;
    }
}
