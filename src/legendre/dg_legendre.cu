// Copyright 2020 Miha Cernetic

#include "runrun.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include <cassert>
#include <iostream>
#include <vector>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../legendre/dg_legendre.hpp"

using ::std::cout;
using ::std::endl;
