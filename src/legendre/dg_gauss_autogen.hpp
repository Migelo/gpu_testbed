/*!
 * \copyright   This file is part of the AREPO code developed by Volker
 Springel.
 * \copyright   Copyright (C) 2013  by Volker Springel
 (volker.springel@h-its.org)
 * \copyright   and contributing authors.
 *
 * \file        src/dg/dg_gauss_autogen.h
 * \date        07/2017
 * \author      Thomas Guillet
 * \brief       Auto-generated points and weights for Gauss-Legendre quadrature
 * \details
 *              Generated using make_dg_gauss.py on 2017-07-28 14:57:25.321847
 *              Uses numpy.polynomial.legendre.leggauss(), with numpy
 version 1.12.1

 * \par Major modifications and contributions:
 *
 * - DD.MM.YYYY Description
 */

#ifndef DG_GAUSS_AUTOGEN_HPP_
#define DG_GAUSS_AUTOGEN_HPP_

#include <assert.h>
#include <stdlib.h>
#include <vector>

#include "../legendre/dg_legendre.hpp"

using ::std::vector;

void dg_gauss_get_points_weights(const size_t npoints, vector<double> &pos, vector<double> &wgt)
{
  assert((npoints > 0) && (npoints <= 16));
  assert((npoints == pos.size()) && (npoints == wgt.size()));
  switch(npoints)
    {
      case 1:
        pos.at(0) = 0.000000000000000000;
        wgt.at(0) = 2.000000000000000000;
        break;

      case 2:
        pos.at(0) = -0.577350269189625731;
        pos.at(1) = 0.577350269189625731;
        wgt.at(0) = 1.000000000000000000;
        wgt.at(1) = 1.000000000000000000;
        break;

      case 3:
        pos.at(0) = -0.774596669241483404;
        pos.at(1) = 0.000000000000000000;
        pos.at(2) = 0.774596669241483404;
        wgt.at(0) = 0.555555555555555691;
        wgt.at(1) = 0.888888888888888840;
        wgt.at(2) = 0.555555555555555691;
        break;

      case 4:
        pos.at(0) = -0.861136311594052573;
        pos.at(1) = -0.339981043584856257;
        pos.at(2) = 0.339981043584856257;
        pos.at(3) = 0.861136311594052573;
        wgt.at(0) = 0.347854845137453683;
        wgt.at(1) = 0.652145154862546206;
        wgt.at(2) = 0.652145154862546206;
        wgt.at(3) = 0.347854845137453683;
        break;

      case 5:
        pos.at(0) = -0.906179845938663964;
        pos.at(1) = -0.538469310105683108;
        pos.at(2) = 0.000000000000000000;
        pos.at(3) = 0.538469310105683108;
        pos.at(4) = 0.906179845938663964;
        wgt.at(0) = 0.236926885056189418;
        wgt.at(1) = 0.478628670499366193;
        wgt.at(2) = 0.568888888888888999;
        wgt.at(3) = 0.478628670499366193;
        wgt.at(4) = 0.236926885056189418;
        break;

      case 6:
        pos.at(0) = -0.932469514203152050;
        pos.at(1) = -0.661209386466264482;
        pos.at(2) = -0.238619186083196932;
        pos.at(3) = 0.238619186083196932;
        pos.at(4) = 0.661209386466264482;
        pos.at(5) = 0.932469514203152050;
        wgt.at(0) = 0.171324492379169746;
        wgt.at(1) = 0.360761573048138939;
        wgt.at(2) = 0.467913934572691370;
        wgt.at(3) = 0.467913934572691370;
        wgt.at(4) = 0.360761573048138939;
        wgt.at(5) = 0.171324492379169746;
        break;

      case 7:
        pos.at(0) = -0.949107912342758486;
        pos.at(1) = -0.741531185599394460;
        pos.at(2) = -0.405845151377397184;
        pos.at(3) = 0.000000000000000000;
        pos.at(4) = 0.405845151377397184;
        pos.at(5) = 0.741531185599394460;
        pos.at(6) = 0.949107912342758486;
        wgt.at(0) = 0.129484966168870647;
        wgt.at(1) = 0.279705391489276589;
        wgt.at(2) = 0.381830050505118312;
        wgt.at(3) = 0.417959183673468959;
        wgt.at(4) = 0.381830050505118312;
        wgt.at(5) = 0.279705391489276589;
        wgt.at(6) = 0.129484966168870647;
        break;

      case 8:
        pos.at(0) = -0.960289856497536176;
        pos.at(1) = -0.796666477413626728;
        pos.at(2) = -0.525532409916328991;
        pos.at(3) = -0.183434642495649780;
        pos.at(4) = 0.183434642495649780;
        pos.at(5) = 0.525532409916328991;
        pos.at(6) = 0.796666477413626728;
        pos.at(7) = 0.960289856497536176;
        wgt.at(0) = 0.101228536290376689;
        wgt.at(1) = 0.222381034453374343;
        wgt.at(2) = 0.313706645877887047;
        wgt.at(3) = 0.362683783378361768;
        wgt.at(4) = 0.362683783378361768;
        wgt.at(5) = 0.313706645877887047;
        wgt.at(6) = 0.222381034453374343;
        wgt.at(7) = 0.101228536290376689;
        break;

      case 9:
        pos.at(0) = -0.968160239507626086;
        pos.at(1) = -0.836031107326635770;
        pos.at(2) = -0.613371432700590358;
        pos.at(3) = -0.324253423403808916;
        pos.at(4) = 0.000000000000000000;
        pos.at(5) = 0.324253423403808916;
        pos.at(6) = 0.613371432700590358;
        pos.at(7) = 0.836031107326635770;
        pos.at(8) = 0.968160239507626086;
        wgt.at(0) = 0.081274388361574718;
        wgt.at(1) = 0.180648160694857118;
        wgt.at(2) = 0.260610696402935660;
        wgt.at(3) = 0.312347077040002807;
        wgt.at(4) = 0.330239355001259671;
        wgt.at(5) = 0.312347077040002807;
        wgt.at(6) = 0.260610696402935660;
        wgt.at(7) = 0.180648160694857118;
        wgt.at(8) = 0.081274388361574718;
        break;

      case 10:
        pos.at(0) = -0.973906528517171743;
        pos.at(1) = -0.865063366688984536;
        pos.at(2) = -0.679409568299024436;
        pos.at(3) = -0.433395394129247213;
        pos.at(4) = -0.148874338981631216;
        pos.at(5) = 0.148874338981631216;
        pos.at(6) = 0.433395394129247213;
        pos.at(7) = 0.679409568299024436;
        pos.at(8) = 0.865063366688984536;
        pos.at(9) = 0.973906528517171743;
        wgt.at(0) = 0.066671344308688069;
        wgt.at(1) = 0.149451349150580365;
        wgt.at(2) = 0.219086362515982014;
        wgt.at(3) = 0.269266719309996516;
        wgt.at(4) = 0.295524224714752981;
        wgt.at(5) = 0.295524224714752981;
        wgt.at(6) = 0.269266719309996516;
        wgt.at(7) = 0.219086362515982014;
        wgt.at(8) = 0.149451349150580365;
        wgt.at(9) = 0.066671344308688069;
        break;

      case 11:
        pos.at(0)  = -0.978228658146056973;
        pos.at(1)  = -0.887062599768095317;
        pos.at(2)  = -0.730152005574049356;
        pos.at(3)  = -0.519096129206811807;
        pos.at(4)  = -0.269543155952344959;
        pos.at(5)  = 0.000000000000000000;
        pos.at(6)  = 0.269543155952344959;
        pos.at(7)  = 0.519096129206811807;
        pos.at(8)  = 0.730152005574049356;
        pos.at(9)  = 0.887062599768095317;
        pos.at(10) = 0.978228658146056973;
        wgt.at(0)  = 0.055668567116173164;
        wgt.at(1)  = 0.125580369464904695;
        wgt.at(2)  = 0.186290210927734429;
        wgt.at(3)  = 0.233193764591990677;
        wgt.at(4)  = 0.262804544510246763;
        wgt.at(5)  = 0.272925086777900894;
        wgt.at(6)  = 0.262804544510246763;
        wgt.at(7)  = 0.233193764591990677;
        wgt.at(8)  = 0.186290210927734429;
        wgt.at(9)  = 0.125580369464904695;
        wgt.at(10) = 0.055668567116173164;
        break;

      case 12:
        pos.at(0)  = -0.981560634246719244;
        pos.at(1)  = -0.904117256370474798;
        pos.at(2)  = -0.769902674194304693;
        pos.at(3)  = -0.587317954286617483;
        pos.at(4)  = -0.367831498998180184;
        pos.at(5)  = -0.125233408511468913;
        pos.at(6)  = 0.125233408511468913;
        pos.at(7)  = 0.367831498998180184;
        pos.at(8)  = 0.587317954286617483;
        pos.at(9)  = 0.769902674194304693;
        pos.at(10) = 0.904117256370474798;
        pos.at(11) = 0.981560634246719244;
        wgt.at(0)  = 0.047175336386512022;
        wgt.at(1)  = 0.106939325995318885;
        wgt.at(2)  = 0.160078328543346110;
        wgt.at(3)  = 0.203167426723065647;
        wgt.at(4)  = 0.233492536538354639;
        wgt.at(5)  = 0.249147045813402690;
        wgt.at(6)  = 0.249147045813402690;
        wgt.at(7)  = 0.233492536538354639;
        wgt.at(8)  = 0.203167426723065647;
        wgt.at(9)  = 0.160078328543346110;
        wgt.at(10) = 0.106939325995318885;
        wgt.at(11) = 0.047175336386512022;
        break;

      case 13:
        pos.at(0)  = -0.984183054718588135;
        pos.at(1)  = -0.917598399222977923;
        pos.at(2)  = -0.801578090733309878;
        pos.at(3)  = -0.642349339440340228;
        pos.at(4)  = -0.448492751036446813;
        pos.at(5)  = -0.230458315955134774;
        pos.at(6)  = 0.000000000000000000;
        pos.at(7)  = 0.230458315955134774;
        pos.at(8)  = 0.448492751036446813;
        pos.at(9)  = 0.642349339440340228;
        pos.at(10) = 0.801578090733309878;
        pos.at(11) = 0.917598399222977923;
        pos.at(12) = 0.984183054718588135;
        wgt.at(0)  = 0.040484004765315877;
        wgt.at(1)  = 0.092121499837728604;
        wgt.at(2)  = 0.138873510219787361;
        wgt.at(3)  = 0.178145980761945516;
        wgt.at(4)  = 0.207816047536888565;
        wgt.at(5)  = 0.226283180262897149;
        wgt.at(6)  = 0.232551553230873898;
        wgt.at(7)  = 0.226283180262897149;
        wgt.at(8)  = 0.207816047536888565;
        wgt.at(9)  = 0.178145980761945516;
        wgt.at(10) = 0.138873510219787361;
        wgt.at(11) = 0.092121499837728604;
        wgt.at(12) = 0.040484004765315877;
        break;

      case 14:
        pos.at(0)  = -0.986283808696812314;
        pos.at(1)  = -0.928434883663573518;
        pos.at(2)  = -0.827201315069765020;
        pos.at(3)  = -0.687292904811685479;
        pos.at(4)  = -0.515248636358154100;
        pos.at(5)  = -0.319112368927889745;
        pos.at(6)  = -0.108054948707343668;
        pos.at(7)  = 0.108054948707343668;
        pos.at(8)  = 0.319112368927889745;
        pos.at(9)  = 0.515248636358154100;
        pos.at(10) = 0.687292904811685479;
        pos.at(11) = 0.827201315069765020;
        pos.at(12) = 0.928434883663573518;
        pos.at(13) = 0.986283808696812314;
        wgt.at(0)  = 0.035119460331752374;
        wgt.at(1)  = 0.080158087159760305;
        wgt.at(2)  = 0.121518570687902963;
        wgt.at(3)  = 0.157203167158193408;
        wgt.at(4)  = 0.185538397477937628;
        wgt.at(5)  = 0.205198463721295549;
        wgt.at(6)  = 0.215263853463157656;
        wgt.at(7)  = 0.215263853463157656;
        wgt.at(8)  = 0.205198463721295549;
        wgt.at(9)  = 0.185538397477937628;
        wgt.at(10) = 0.157203167158193408;
        wgt.at(11) = 0.121518570687902963;
        wgt.at(12) = 0.080158087159760305;
        wgt.at(13) = 0.035119460331752374;
        break;

      case 15:
        pos.at(0)  = -0.987992518020485377;
        pos.at(1)  = -0.937273392400705951;
        pos.at(2)  = -0.848206583410427206;
        pos.at(3)  = -0.724417731360170070;
        pos.at(4)  = -0.570972172608538830;
        pos.at(5)  = -0.394151347077563385;
        pos.at(6)  = -0.201194093997434514;
        pos.at(7)  = 0.000000000000000000;
        pos.at(8)  = 0.201194093997434514;
        pos.at(9)  = 0.394151347077563385;
        pos.at(10) = 0.570972172608538830;
        pos.at(11) = 0.724417731360170070;
        pos.at(12) = 0.848206583410427206;
        pos.at(13) = 0.937273392400705951;
        pos.at(14) = 0.987992518020485377;
        wgt.at(0)  = 0.030753241996118647;
        wgt.at(1)  = 0.070366047488108069;
        wgt.at(2)  = 0.107159220467171773;
        wgt.at(3)  = 0.139570677926153908;
        wgt.at(4)  = 0.166269205816993781;
        wgt.at(5)  = 0.186161000015561878;
        wgt.at(6)  = 0.198431485327111246;
        wgt.at(7)  = 0.202578241925560898;
        wgt.at(8)  = 0.198431485327111246;
        wgt.at(9)  = 0.186161000015561878;
        wgt.at(10) = 0.166269205816993781;
        wgt.at(11) = 0.139570677926153908;
        wgt.at(12) = 0.107159220467171773;
        wgt.at(13) = 0.070366047488108069;
        wgt.at(14) = 0.030753241996118647;
        break;

      case 16:
        pos.at(0)  = -0.989400934991649939;
        pos.at(1)  = -0.944575023073232600;
        pos.at(2)  = -0.865631202387831755;
        pos.at(3)  = -0.755404408355002999;
        pos.at(4)  = -0.617876244402643771;
        pos.at(5)  = -0.458016777657227370;
        pos.at(6)  = -0.281603550779258915;
        pos.at(7)  = -0.095012509837637454;
        pos.at(8)  = 0.095012509837637454;
        pos.at(9)  = 0.281603550779258915;
        pos.at(10) = 0.458016777657227370;
        pos.at(11) = 0.617876244402643771;
        pos.at(12) = 0.755404408355002999;
        pos.at(13) = 0.865631202387831755;
        pos.at(14) = 0.944575023073232600;
        pos.at(15) = 0.989400934991649939;
        wgt.at(0)  = 0.027152459411754037;
        wgt.at(1)  = 0.062253523938647706;
        wgt.at(2)  = 0.095158511682492591;
        wgt.at(3)  = 0.124628971255534030;
        wgt.at(4)  = 0.149595988816576764;
        wgt.at(5)  = 0.169156519395002619;
        wgt.at(6)  = 0.182603415044923612;
        wgt.at(7)  = 0.189450610455068585;
        wgt.at(8)  = 0.189450610455068585;
        wgt.at(9)  = 0.182603415044923612;
        wgt.at(10) = 0.169156519395002619;
        wgt.at(11) = 0.149595988816576764;
        wgt.at(12) = 0.124628971255534030;
        wgt.at(13) = 0.095158511682492591;
        wgt.at(14) = 0.062253523938647706;
        wgt.at(15) = 0.027152459411754037;
        break;

      default:
        assert(0);
        abort();
        break;
    }
}

double get_gauss_lobatto_coordinate(int m, int n)
{
  // int m : degree of polynomial
  // int n: qpoint number
  if(n < 0 || n >= m)
    Terminate("invalid combination.");

  switch(m)
    {
      case 2:
        switch(n)
          {
            case 0:
              return -1.0;
            case 1:
              return 1.0;
          }
        break;

      case 3:
        switch(n)
          {
            case 0:
              return -1.0;
            case 1:
              return 0.0;
            case 2:
              return 1.0;
          }
        break;

      case 4:
        switch(n)
          {
            case 0:
              return -1.0;
            case 1:
              return -sqrt(1. / 5.);
            case 2:
              return sqrt(1. / 5.);
            case 3:
              return 1.0;
          }
        break;

      case 5:
        switch(n)
          {
            case 0:
              return -1.0;
            case 1:
              return -sqrt(3. / 7.);
            case 2:
              return 0;
            case 3:
              return sqrt(3. / 7.);
            case 4:
              return 1.0;
          }
        break;
    }

  Terminate("we should not get here\n");
  return 0;
}

void dg_get_point_weights_positivity(vector<double> &wgts, vector<double> &bfunc_internal, vector<double> &bfunc_external)
{
  for(decltype(ND) q = 0; q < DG_NUM_INNER_QPOINTS; q++)
    for(decltype(ND) b = 0; b < NB; b++)
      wgts[q * NB + b] = bfunc_internal[bfunc_internal_idx(q, b)];

  int qidx = DG_NUM_INNER_QPOINTS;

  for(decltype(ND) axis = 0; axis < ND; axis++)
    for(int side = 0; side < 2; side++)
      for(decltype(ND) q = 0; q < DG_NUM_OUTER_QPOINTS; q++)
        {
          for(decltype(ND) b = 0; b < NB; b++)
            wgts[qidx * NB + b] = bfunc_external[bfunc_external_side_idx(q, b, axis + ND * side)];

          qidx++;
        }
}

void dg_gauss_get_points_weights_internal(const int npoints, vector<double> &wgt, vector<double> &wgt_internal, const int dimension)
{
  if(dimension == 3)
    {
      for(int i = 0; i < npoints; i++)
        for(int j = 0; j < npoints; j++)
          for(int k = 0; k < npoints; k++)
            wgt_internal.at(((i * npoints) + j) * npoints + k) = wgt.at(i) * wgt.at(j) * wgt.at(k);
    }
  else if(dimension == 2)
    {
      for(int i = 0; i < npoints; i++)
        for(int j = 0; j < npoints; j++)
          wgt_internal.at(i * npoints + j) = wgt.at(i) * wgt.at(j);
    }
  else if(dimension == 1)
    {
      for(int i = 0; i < npoints; i++)
        wgt_internal.at(i) = wgt.at(i);
    }
}

void dg_gauss_get_points_weights_external(const int npoints, vector<double> &wgt, vector<double> &wgt_external, const int dimension)
{
  if(dimension == 3)
    {
      for(int i = 0; i < npoints; i++)
        for(int j = 0; j < npoints; j++)
          wgt_external.at(i * npoints + j) = wgt.at(i) * wgt.at(j);
    }
  else if(dimension == 2)
    {
      for(int i = 0; i < npoints; i++)
        wgt_external.at(i) = wgt.at(i);
    }
  else if(dimension == 1)
    {
      wgt_external.at(0) = 1.0;
    }
}

#endif  // DG_GAUSS_AUTOGEN_HPP_
