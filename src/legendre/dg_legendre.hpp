// Copyright 2020 Miha Cernetic

#ifndef DG_LEGENDRE_HPP_
#define DG_LEGENDRE_HPP_

// functions for manual definitions

double legendre(int order, double x);
double legendre_diff(int order, double x);
double legendre_normalized(int order, double x);
double legendre_diff_normalized(int order, double x);

////  functions for autogen

void dg_legendre_autogen_3d_bfunc_diff_x(int bfunc, double x, double y, double z, double *dv);
void dg_legendre_autogen_3d_bfunc_diff_y(int bfunc, double x, double y, double z, double *dv);
void dg_legendre_autogen_3d_bfunc_diff_z(int bfunc, double x, double y, double z, double *dv);

void dg_legendre_autogen_2d_bfunc_eval(int bfunc, double x, double y, double *v);
void dg_legendre_autogen_3d_bfunc_eval(int bfunc, double x, double y, double z, double *v);

void dg_legendre_autogen_2d_bfunc_diff_x(int bfunc, double x, double y, double *dv);
void dg_legendre_autogen_2d_bfunc_diff_y(int bfunc, double x, double y, double *dv);

void dg_legendre_autogen_2d_bfunc_eval_test();
void dg_legendre_autogen_3d_bfunc_eval_test();
double dg_legendre_normalize_coords(int qpoints_per_side, int coor);
void dg_legendre_normalize_coords_test();

#endif  // DG_LEGENDRE_HPP_
