#include "runrun.h"

#include <stdio.h>
#include <stdlib.h>
#include <cassert>
#include <cmath>
#include <iostream>

#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../riemann/riemann_hllc.hpp"
#include "../tests/dg_geometry.hpp"
#include "../utils/cuda_utils.h"

using ::std::isnan;
using ::std::max;
using ::std::min;
using ::std::sqrt;

__host__ __device__ double godunov_flux_3d(state &st_L, state &st_R, state &st_face, fluxes &flux, const visc_params *vp)
{
  for(decltype(ND) f = 0; f < NF; f++)
    flux.field_flx[f] = 0;

#ifndef ISOTHERM_EQS
  return godunov_flux_3d_hllc(st_L, st_R, st_face, flux);
#else
  return godunov_flux_3d_isothermal(st_L, st_R, st_face, flux, vp->IsoSoundSpeed);
#endif
}

__host__ __device__ void get_viscous_flux_from_conserved_fields_and_their_gradients(double *fields, double *grad_fields,
                                                                                    double *full_flux, const visc_params *vp)
{
  for(decltype(ND) f = 0; f < NF * ND; f++)
    full_flux[f] = 0;

#ifdef ART_VISCOSITY

  if(fields[ALPHA_FIELD] > 0)  // this field is our viscosity parameter
    {
      state st;
      get_primitive_state_from_conserved_fields(fields, st, vp);

#ifndef ISOTHERM_EQS
      double cs = 0;
      if(st.press > 0 && st.rho > 0)
        cs = sqrt(GAMMA * st.press / st.rho);
#else
      double cs = vp->IsoSoundSpeed;
#endif

      double res = vp->Resolution;

      double alpha = fields[ALPHA_FIELD];

      if(alpha > vp->ArtViscMax)
        alpha = vp->ArtViscMax;

      double visc = alpha * cs * res;

      for(decltype(ND) f = 0; f < NF_VISC; f++)
        for(decltype(ND) d = 0; d < ND; d++)
          full_flux[f * ND + d] += visc * grad_fields[f * ND + d];
    }

#endif  // ART_VISCOSITY

#ifdef NAVIER_STOKES
  // get matrix of velocity derivatives
  double vel[ND], dvel[ND][ND], u, du[ND], dye, ddye[ND];

  double rho = get_primitive_variable_gradients(fields, grad_fields, vel, dvel, u, du, dye, ddye);

  double divvel = 0;
  for(decltype(ND) i = 0; i < ND; i++)
    divvel += dvel[i][i];

  // set up viscous stress tensor Pi
  double Pi[ND][ND];

  for(decltype(ND) i = 0; i < ND; i++)
    for(decltype(ND) j = 0; j < ND; j++)
      Pi[i][j] = vp->ShearViscosity * rho * (dvel[i][j] + dvel[j][i] - (i == j ? (2.0 / 3) * divvel : 0));

  // add stress tensor to viscous flux
  for(decltype(ND) i = 0; i < ND; i++)
    for(decltype(ND) j = 0; j < ND; j++)
      full_flux[(1 + i) * ND + j] += Pi[i][j];

#ifndef ISOTHERM_EQS
  // add work against stress to viscous energy dissipation
  for(decltype(ND) i = 0; i < ND; i++)
    for(decltype(ND) j = 0; j < ND; j++)
      full_flux[(1 + ND) * ND + i] += Pi[i][j] * vel[j];

  // now add thermal diffusivity
  if(vp->ThermalDiffusivity != 0)
    {
      for(decltype(ND) i = 0; i < ND; i++)
        full_flux[(1 + ND) * ND + i] += vp->ThermalDiffusivity * (GAMMA - 1) * rho * du[i];
    }
#endif

#ifdef ENABLE_DYE
  if(vp->DyeDiffusivity != 0)
    {
      for(decltype(ND) i = 0; i < ND; i++)
        full_flux[DYE_FIELD * ND + i] += vp->DyeDiffusivity * rho * ddye[i];
    }
#endif

#endif  // NAVIER_STOKES
}

#ifndef ISOTHERM_EQS

__host__ __device__ double hllc_get_fluxes_from_state(state &st, fluxes &flux)
{
  flux.field_flx[0] = st.rho * st.velx;
  flux.field_flx[1] = st.rho * st.velx * st.velx + st.press;
#if(ND >= 2)
  flux.field_flx[2] = st.rho * st.velx * st.vely;
#endif
#if(ND == 3)
  flux.field_flx[3] = st.rho * st.velx * st.velz;
#endif
#if(ND == 1)
  double st_Energy = st.press / GAMMA_MINUS1 + 0.5 * st.rho * (st.velx * st.velx);
#endif
#if(ND == 2)
  double st_Energy = st.press / GAMMA_MINUS1 + 0.5 * st.rho * (st.velx * st.velx + st.vely * st.vely);
#endif
#if(ND == 3)
  double st_Energy = st.press / GAMMA_MINUS1 + 0.5 * st.rho * (st.velx * st.velx + st.vely * st.vely + st.velz * st.velz);
#endif
  flux.field_flx[1 + ND] = (st_Energy + st.press) * st.velx;

#ifdef ENABLE_DYE
  flux.field_flx[DYE_FIELD] = st.dye * st.rho * st.velx;
#endif

  return st_Energy;
}

__host__ __device__ double get_hllc_star_fluxes(const state &st, const double st_Energy, const fluxes &flux, fluxes &hllc_flux,
                                                double S_star, double S)
{
  double Q0 = st.rho * (S - st.velx) / (S - S_star);
  double Q1 = Q0 * S_star;
#if(ND >= 2)
  double Q2 = Q0 * st.vely;
#endif
#if(ND == 3)
  double Q3 = Q0 * st.velz;
#endif
  double Q4 = Q0 * (st_Energy / st.rho + (S_star - st.velx) * (S_star + st.press / (st.rho * (S - st.velx))));

  hllc_flux.field_flx[0] = flux.field_flx[0] + S * (Q0 - st.rho);

  hllc_flux.field_flx[1] = flux.field_flx[1] + S * (Q1 - st.rho * st.velx);
#if(ND >= 2)
  hllc_flux.field_flx[2] = flux.field_flx[2] + S * (Q2 - st.rho * st.vely);
#endif
#if(ND == 3)
  hllc_flux.field_flx[3] = flux.field_flx[3] + S * (Q3 - st.rho * st.velz);
#endif
  hllc_flux.field_flx[1 + ND] = flux.field_flx[1 + ND] + S * (Q4 - st_Energy);

  return Q0;
}

__host__ __device__ double godunov_flux_3d_hllc(state &st_L, state &st_R, state &st_face, fluxes &flux)
{
#ifdef DEBUG
  assert(!isnan(st_L.rho));
  assert(!isnan(st_L.velx));
#if(ND > 1)
  assert(!isnan(st_L.vely));
#endif
#if(ND > 2)
  assert(!isnan(st_L.velz));
#endif
  assert(!isnan(st_L.press));
  assert(!isnan(st_R.rho));
  assert(!isnan(st_R.velx));
#if(ND > 1)
  assert(!isnan(st_R.vely));
#endif
#if(ND > 2)
  assert(!isnan(st_R.velz));
#endif
  assert(!isnan(st_R.press));
#endif  //  DEBUG

  if(st_L.rho > 0 && st_R.rho > 0 && st_L.press > 0 && st_R.press > 0)
    {
      fluxes flux_L, flux_R;

      double st_L_csnd = sqrt(GAMMA * st_L.press / st_L.rho);
      double st_R_csnd = sqrt(GAMMA * st_R.press / st_R.rho);

      /* first estimate wave speeds */
      double S_L = min(st_L.velx - st_L_csnd, st_R.velx - st_R_csnd);
      double S_R = max(st_L.velx + st_L_csnd, st_R.velx + st_R_csnd);

      double rho_hat    = 0.5 * (st_L.rho + st_R.rho);
      double csnd_hat   = 0.5 * (st_L_csnd + st_R_csnd);
      double Press_star = 0.5 * ((st_L.press + st_R.press) + (st_L.velx - st_R.velx) * (rho_hat * csnd_hat));
      double S_star     = 0.5 * ((st_L.velx + st_R.velx) + (st_L.press - st_R.press) / (rho_hat * csnd_hat));

      /* compute fluxes for the left and right states */
      double st_L_Energy = hllc_get_fluxes_from_state(st_L, flux_L);
      double st_R_Energy = hllc_get_fluxes_from_state(st_R, flux_R);

      if(S_L >= 0.0) /* F_hllc = F_L */
        {
          /* copy the fluxes from the left state */
          flux = flux_L;

          /* set the primitive variables at the face */
          st_face = st_L;
        }
      else if(S_R <= 0.0) /* F_hllc = F_R */
        {
          /* copy the fluxes from the left state */
          flux = flux_R;

          /* set the primitive variables at the face */
          st_face = st_R;
        }
      else if(S_L <= 0.0 && S_star >= 0.0) /* F_hllc = F*_L */
        {
          /* compute star flux */
          double rho_star = get_hllc_star_fluxes(st_L, st_L_Energy, flux_L, flux, S_star, S_L);

          /* set the primitive variables at the face */
          st_face.rho  = rho_star;
          st_face.velx = S_star;
#if(ND >= 2)
          st_face.vely = st_L.vely;
#endif
#if(ND == 3)
          st_face.velz = st_L.velz;
#endif
          st_face.press = Press_star;
#ifdef ENABLE_DYE
          st_face.dye               = st_L.dye;
          flux.field_flx[DYE_FIELD] = st_face.dye * st_face.rho * st_face.velx;
#endif
#ifdef ADVECT_ALPHA
          st_face.alpha               = st_L.alpha;
          flux.field_flx[ALPHA_FIELD] = st_face.alpha * st_face.velx * st_face.rho;
#endif
        }
      else /* F_hllc = F*_R */
        {
          /* compute star flux */
          double rho_star = get_hllc_star_fluxes(st_R, st_R_Energy, flux_R, flux, S_star, S_R);

          /* set the primitive variables at the face */
          st_face.rho  = rho_star;
          st_face.velx = S_star;
#if(ND >= 2)
          st_face.vely = st_R.vely;
#endif
#if(ND == 3)
          st_face.velz = st_R.velz;
#endif
          st_face.press = Press_star;
#ifdef ENABLE_DYE
          st_face.dye               = st_R.dye;
          flux.field_flx[DYE_FIELD] = st_face.dye * st_face.rho * st_face.velx;
#endif
#ifdef ADVECT_ALPHA
          st_face.alpha               = st_L.alpha;
          flux.field_flx[ALPHA_FIELD] = st_face.alpha * st_face.velx * st_face.rho;
#endif
        }

#ifdef DEBUG
      if(!std::isfinite(st_face.rho) || !std::isfinite(st_face.velx) || !std::isfinite(st_face.press))
        {
          printf("\nRiemann solver problem:\n");
          printf("Riemann solver input:  Left   rho=%g  press=%g   vel=%g\n", st_L.rho, st_L.press, st_L.velx);
          printf("Riemann solver input:  Right  rho=%g  press=%g   vel=%g\n", st_R.rho, st_R.press, st_R.velx);
          printf("Riemann solver returned garbage:  state on face, rho=%g  press=%g   vel=%g\n", st_face.rho, st_face.velx,
                 st_face.press);
        }
#endif
    }
  else
    {
      printf(
          "\nRiemann solver problem:"
          "Left:  st_L.press=%g st_L.rho=%g  st_L.velx=%g  "
          "Right: st_R.press=%g st_R.rho=%g  st_R.velx=%g\n",
          st_L.press, st_L.rho, st_L.velx, st_R.press, st_R.rho, st_R.velx);
      assert(0);
    }

  return st_face.press;
}

#else

/*! \brief Riemann-solver for isothermal gas.
 *
 *  \param[in] st_L Left hand side state.
 *  \param[in] st_R Right hand side state.
 *  \param[in, out] Rho Central density; needs some initial guess.
 *  \param[out] Vel Velocity in central region.
 *  \param[in] csnd Sound speed.
 *
 *  \return void
 */

__host__ __device__ double godunov_flux_3d_isothermal(state &st_L, state &st_R, state &st_face, fluxes &flux, double IsoSoundSpeed)
{

#define MAXITER 100
#define TOL 1.0e-8

  if(st_L.rho <= 0 || st_R.rho <= 0)
    {
      printf("isothermal Riemann solver was called with zero or negative density\n");
      assert(0);
    }

  double dVel = (st_R.velx - st_L.velx) / IsoSoundSpeed;

  double rho;
  if(dVel > 0)
    rho = sqrt(st_L.rho * st_R.rho * exp(-dVel));
  else
    rho = 0.5 * (st_L.rho + st_R.rho);

  int iter = 0;
  double F_L, FD_L, F_R, FD_R, rhoold, drho;

  do /* newton-raphson scheme */
    {
      isothermal_function(rho, st_L.rho, &F_L, &FD_L);
      isothermal_function(rho, st_R.rho, &F_R, &FD_R);

      rhoold = rho;
      drho   = -0.5 * (F_L + F_R + dVel) / (FD_L + FD_R);

      if(fabs(drho) > 0.25 * rho)
        drho = 0.25 * rho * fabs(drho) / drho;

      rho += drho;

      iter++;
    }
  while(2 * fabs(rho - rhoold) / (st_L.rho + st_R.rho) > TOL && iter < MAXITER);

  if(iter >= MAXITER)
    {
      printf("ICs for isothermal riemann solver lead to divergence. stopping.\n");
      assert(0);
    }

  /* prepare output values */
  st_face.rho  = rho;
  st_face.velx = 0.5 * (st_L.velx + st_R.velx + IsoSoundSpeed * (F_R - F_L));

  if(st_face.velx < 0)
    {
#if(ND >= 2)
      st_face.vely = st_R.vely;
#endif
#if(ND == 3)
      st_face.velz = st_R.velz;
#endif
#ifdef ENABLE_DYE
      st_face.dye = st_R.dye;
#endif
    }
  else
    {
#if(ND >= 2)
      st_face.vely = st_L.vely;
#endif
#if(ND == 3)
      st_face.velz = st_L.velz;
#endif
#ifdef ENABLE_DYE
      st_face.dye = st_L.dye;
#endif
    }

  st_face.press = st_face.rho * IsoSoundSpeed * IsoSoundSpeed;

  // calculate flux

  flux.field_flx[0] = st_face.rho * st_face.velx;
  flux.field_flx[1] = st_face.velx * st_face.rho * st_face.velx + st_face.press;
#if(ND >= 2)
  flux.field_flx[2] = st_face.velx * st_face.rho * st_face.vely;
#endif
#if(ND == 3)
  flux.field_flx[3] = st_face.velx * st_face.rho * st_face.velz;
#endif

#ifdef ENABLE_DYE
  flux.field_flx[DYE_FIELD] = st_face.dye * st_face.rho * st_face.velx;
#endif

  return st_face.press;
}

/*! \brief "Pressure" function for isothermal gas.
 *
 *  Needed for root-finding in riemann_isotherm.
 *
 *  \param[in] rhostar Central density.
 *  \param[in] rho External density.
 *  \param[out] F Isotherma function.
 *  \param[out] FD Derivative of isothermal function.
 *
 *  \return void
 */
__host__ __device__ void isothermal_function(double rhostar, double rho, double *F, double *FD)
{
  if(rhostar <= rho) /* rarefaction wave */
    {
      *F  = log(rhostar / rho);
      *FD = 1.0 / rho;
    }
  else /* shock wave */
    {
      *F  = (rhostar - rho) / sqrt(rhostar * rho);
      *FD = 0.5 / rhostar * (sqrt(rhostar / rho) + sqrt(rho / rhostar));
    }
}

#endif
