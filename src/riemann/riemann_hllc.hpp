// Copyright Miha Cernetic 2020
#ifndef RIEMANN_HLLC_HPP_
#define RIEMANN_HLLC_HPP_

#include "../tests/dg_geometry.hpp"

__host__ __device__ double godunov_flux_3d(state &st_L, state &st_R, state &st_face, fluxes &flux, const visc_params *vp);

__host__ __device__ double hllc_get_fluxes_from_state(state &st, fluxes &flux);
__host__ __device__ double get_hllc_star_fluxes(const state &st, const double st_Energy, const fluxes &flux, fluxes &hllc_flux,
                                                double S_star, double S);
__host__ __device__ double godunov_flux_3d_hllc(state &st_L, state &st_R, state &st_face, fluxes &flux);

__host__ __device__ double godunov_flux_3d_isothermal(state &st_L, state &st_R, state &st_face, fluxes &flux, double isosoundspeed);
__host__ __device__ void isothermal_function(double rhostar, double rho, double *F, double *FD);

__host__ __device__ void get_viscous_flux_from_conserved_fields_and_their_gradients(double *fields, double *grad_fields,
                                                                                    double *full_flux, const visc_params *vp, int axis,
                                                                                    state &st_L, state &st_R);

//----------------- inlined functions come here: ------------------

__host__ __device__ __forceinline__ void get_primitive_state_from_conserved_fields(const double *fields, state &st,
                                                                                   const visc_params *vp)
{
  st.rho = fields[0];

  const double irho = 1.0 / st.rho;
  st.velx           = fields[1] * irho;
#if(ND >= 2)
  st.vely = fields[2] * irho;
#endif
#if(ND == 3)
  st.velz = fields[3] * irho;
#endif

#if !defined(ISOTHERM_EQS) && !defined(TURBULENCE)
#if(ND == 1)
  const double p2 = fields[1] * fields[1];
  st.press        = (fields[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
  const double p2 = fields[1] * fields[1] + fields[2] * fields[2];
  st.press        = (fields[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
  const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];
  st.press        = (fields[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#else
  st.press = st.rho * vp->IsoSoundSpeed * vp->IsoSoundSpeed;
#endif

#ifdef ENABLE_DYE
  st.dye = fields[DYE_FIELD] * irho;
#endif

#ifdef ADVECT_ALPHA
  st.alpha = fields[ALPHA_FIELD] * irho;
#endif
}

__host__ __device__ __forceinline__ void get_flux_from_primitive_state_and_conserved_fields(const double *fields, state &st,
                                                                                            double *flux)
{
  for(decltype(ND) f = 0; f < NF * ND; f++)
    flux[f] = 0;

// calculate flux
#if(ND == 1)
  flux[0 * NF + 0] = fields[1];
  flux[0 * NF + 1] = st.velx * fields[1] + st.press;
#ifndef ISOTHERM_EQS
  flux[0 * NF + 2] = st.velx * (fields[2] + st.press);
#endif
#ifdef ENABLE_DYE
  flux[0 * NF + DYE_FIELD] = st.dye * fields[1];
#endif
#ifdef ADVECT_ALPHA
  flux[0 * NF + ALPHA_FIELD] = st.alpha * fields[1];
#endif
#endif

#if(ND == 2)
  flux[0 * NF + 0] = fields[1];
  flux[0 * NF + 1] = st.velx * fields[1] + st.press;
  flux[0 * NF + 2] = st.velx * fields[2];
#ifndef ISOTHERM_EQS
  flux[0 * NF + 3] = st.velx * (fields[3] + st.press);
#endif

  flux[1 * NF + 0] = fields[2];
  flux[1 * NF + 1] = st.vely * fields[1];
  flux[1 * NF + 2] = st.vely * fields[2] + st.press;
#ifndef ISOTHERM_EQS
  flux[1 * NF + 3] = st.vely * (fields[3] + st.press);
#endif

#ifdef ENABLE_DYE
  flux[0 * NF + DYE_FIELD] = st.dye * fields[1];
  flux[1 * NF + DYE_FIELD] = st.dye * fields[2];
#endif

#ifdef ADVECT_ALPHA
  flux[0 * NF + ALPHA_FIELD] = st.alpha * fields[1];
  flux[1 * NF + ALPHA_FIELD] = st.alpha * fields[2];
#endif
#endif

#if(ND == 3)
  flux[0 * NF + 0] = fields[1];
  flux[0 * NF + 1] = st.velx * fields[1] + st.press;
  flux[0 * NF + 2] = st.velx * fields[2];
  flux[0 * NF + 3] = st.velx * fields[3];
#ifndef ISOTHERM_EQS
  flux[0 * NF + 4] = st.velx * (fields[4] + st.press);
#endif

  flux[1 * NF + 0] = fields[2];
  flux[1 * NF + 1] = st.vely * fields[1];
  flux[1 * NF + 2] = st.vely * fields[2] + st.press;
  flux[1 * NF + 3] = st.vely * fields[3];
#ifndef ISOTHERM_EQS
  flux[1 * NF + 4] = st.vely * (fields[4] + st.press);
#endif

  flux[2 * NF + 0] = fields[3];
  flux[2 * NF + 1] = st.velz * fields[1];
  flux[2 * NF + 2] = st.velz * fields[2];
  flux[2 * NF + 3] = st.velz * fields[3] + st.press;
#ifndef ISOTHERM_EQS
  flux[2 * NF + 4] = st.velz * (fields[4] + st.press);
#endif

#ifdef ENABLE_DYE
  flux[0 * NF + DYE_FIELD] = st.dye * fields[1];
  flux[1 * NF + DYE_FIELD] = st.dye * fields[2];
  flux[2 * NF + DYE_FIELD] = st.dye * fields[3];
#endif

#ifdef ADVECT_ALPHA
  flux[0 * NF + ALPHA_FIELD] = st.alpha * fields[1];
  flux[1 * NF + ALPHA_FIELD] = st.alpha * fields[2];
  flux[2 * NF + ALPHA_FIELD] = st.alpha * fields[3];
#endif
#endif
}

#if defined(NAVIER_STOKES) || defined(ART_VISCOSITY) || defined(RICHTMYER_VISCOSITY)
__host__ __device__ __forceinline__ double get_primitive_variable_gradients(double *fields, double *grad_fields, double vel[ND],
                                                                            double dvel[ND][ND], double &u, double du[ND], double &dye,
                                                                            double ddye[ND])
{
  // get matrix of velocity derivatives
  double rho = fields[0];

  for(decltype(ND) i = 0; i < ND; i++)
    {
      vel[i] = fields[1 + i] / rho;

      for(decltype(ND) j = 0; j < ND; j++)
        {
          double drhovel = grad_fields[(1 + i) * ND + j];
          double drho    = grad_fields[0 * ND + j];

          dvel[i][j] = (drhovel - vel[i] * drho) / rho;
        }
    }

#ifndef ISOTHERM_EQS
  // need to get gradient of thermal energy density

  double v2 = 0;
  for(decltype(ND) i = 0; i < ND; i++)
    v2 += vel[i] * vel[i];

  u = fields[1 + ND] / rho - 0.5 * v2;

  double dvv[ND];
  for(decltype(ND) i = 0; i < ND; i++)
    {
      dvv[i] = 0;
      for(decltype(ND) j = 0; j < ND; j++)
        dvv[i] += vel[j] * dvel[i][j];
    }

  for(decltype(ND) i = 0; i < ND; i++)
    du[i] = (grad_fields[(1 + ND) * ND + i] - (u + 0.5 * v2) * grad_fields[0 * ND + i] - rho * dvv[i]) / rho;
#endif

#ifdef ENABLE_DYE
  dye = fields[DYE_FIELD] / rho;

  for(decltype(ND) i = 0; i < ND; i++)
    ddye[i] = (grad_fields[DYE_FIELD * ND + i] - grad_fields[0 * ND + i] * dye) / rho;
#endif

  return rho;
}
#endif

/*!
 * minimum of three numbers
 */
template <typename T>
__host__ __device__ __forceinline__ double tripple_min(T a, T b, T c)
{
  if(a < b)
    {
      if(a < c)
        return a;
      else
        return c;
    }
  else
    {
      if(b < c)
        return b;
      else
        return c;
    }
}

/*!
 * maximum of three numbers
 */
template <typename T>
__host__ __device__ __forceinline__ double tripple_max(T a, T b, T c)
{
  if(a > b)
    {
      if(a > c)
        return a;
      else
        return c;
    }
  else
    {
      if(b > c)
        return b;
      else
        return c;
    }
}

/*!
 * multivariable minmod function
 */
template <typename T>
__host__ __device__ __forceinline__ double minmod(T a, T b, T c)
{
  if(a > 0 && b > 0 && c > 0)
    {
      return tripple_min(a, b, c);
    }
  else if(a < 0 && b < 0 && c < 0)
    {
      return tripple_max(a, b, c);
    }
  else
    {
      return 0;
    }
}

__host__ __device__ __forceinline__ double slope_limiter(double val_L, double val, double val_R)
{
  double slope_mc = minmod(0.5 * (val_R - val_L), 2 * (val_R - val), 2 * (val - val_L));  // MC limiter
  return slope_mc;

  // double slope_minmod = minmod(0.5 * (val_R - val_L), (val_R - val), (val - val_L));   // minmod
  // return slope_minmod;

  /*
    if(val_R != val_L)
      {
        double f = (val - val_L) / (val_R - val_L);

        if(f > 0 && f < 1)
          {
            // van-Leer
            double phi_vanleer   = 4 * f * (1 - f);                      // van-Leer
            double slope_vanleer = 0.5 * (val_R - val_L) * phi_vanleer;  // van-Leer
            return slope_vanleer;

            // superbee
            double phi_superbee;
            if(f < 1.0 / 3)
              phi_superbee = 4 * f;
            else if(f < 0.5)
              phi_superbee = 2 * (1 - f);
            else if(f < 2.0 / 3)
              phi_superbee = 2 * f;
            else
              phi_superbee = 4 * (1 - f);

            slope_superbee = 0.5 * (val_R - val_L) * phi_superbee;
            return slope_superbee;
          }
      }
    else
      return 0;
      */
}

#ifdef FINITE_VOLUME
__host__ __device__ __forceinline__ void do_slope_limiting(state &st_L, state &st, state &st_R, int axis, slopes &sl)
{
  sl.rho  = slope_limiter(st_L.rho, st.rho, st_R.rho);
  sl.velx = slope_limiter(st_L.velx, st.velx, st_R.velx);
#if(ND >= 2)
  sl.vely = slope_limiter(st_L.vely, st.vely, st_R.vely);
#endif
#if(ND == 3)
  sl.velz = slope_limiter(st_L.velz, st.velz, st_R.velz);
#endif
#ifndef ISOTHERM_EQS
  sl.press = slope_limiter(st_L.press, st.press, st_R.press);
#endif
#ifdef ENABLE_DYE
  sl.dye = slope_limiter(st_L.dye, st.dye, st_R.dye);
#endif
}
#endif

#endif  // RIEMANN_HLLC_HPP_
