// Copyright 2020 Miha Cernetic

#ifndef SRC_TESTS_DG_SETUP_TEST_PROBLEMS_HPP_
#define SRC_TESTS_DG_SETUP_TEST_PROBLEMS_HPP_

#include "../data/dg_data.hpp"

void dg_setup_test_problem_advection(dg_data_t *dg);
void dg_setup_test_problem_advection_2(dg_data_t *dg);

void dg_setup_test_problem_vortex_sheet(dg_data_t *dg);

void dg_setup_test_problem_implosion(dg_data_t *dg);
void dg_setup_test_problem_implosion(dg_data_t *dg);

void dg_setup_test_problem_kelvin_helmholtz(dg_data_t *dg);
double get_dye_entropy(dg_data_t *dg);

void dg_setup_test_problem_yee_vortex(dg_data_t *dg);
double dg_L1_test_problem_yee_vortex(dg_data_t *dg, double time);
double dg_L1_test_problem_square_advection(dg_data_t *dg, double time);

void dg_setup_test_problem_soundwave(dg_data_t *dg);
double dg_L1_test_problem_soundwave(dg_data_t *dg, double time);
double dg_L1_test_problem_vortex_sheet(dg_data_t *dg, double time);

void dg_setup_test_problem_gaussian_diffusion(dg_data_t *dg);
double dg_L1_test_problem_gaussian_diffusion(dg_data_t *dg, double time);

void dg_setup_test_problem_square_advection(dg_data_t *dg);

void dg_setup_test_problem_turbulence(dg_data_t *dg);

void dg_setup_test_problem_point_explosion(dg_data_t *dg);

void dg_setup_test_problem_double_blast(dg_data_t *dg);

void dg_setup_test_problem_single_shock(dg_data_t *dg);

void dg_setup_test_problem_shock_tube(dg_data_t *dg);

void dg_setup_test_problem_shock_tube_boosted(dg_data_t *dg);

void dg_setup_test_problem_constant_density_and_pressure(dg_data_t *dg);

void dg_setup_test_problem_isentropic_vortex(dg_data_t *dg);
double dg_L1_test_problem_isentropic_vortex(dg_data_t *dg);

void points_on_sphere(int num_pts, coordinates *coords);

void dg_setup_test_problem_gravity_waves(dg_data_t *dg);
void dg_setup_test_problem_gravity_waves_boundary(dg_data_t *dg);

void dg_setup_test_problem_cooling(dg_data_t *dg);

#endif  // SRC_TESTS_DG_SETUP_TEST_PROBLEMS_HPP_
