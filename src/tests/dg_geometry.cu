// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include "./dg_geometry.hpp"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <cassert>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../utils/dg_utils.hpp"

using ::std::cout;
using ::std::endl;
using ::std::vector;

void dg_geometry_idx_to_cell_test()
{
#if(ND >= 2)

  cout << "[TEST] dg_geometry_idx_to_cell";
  xyz xyz;

  // z = 0

  xyz = dg_geometry_idx_to_cell(0, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 0);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(1, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 0);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(2, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 0);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(3, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 1);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(4, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 1);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(5, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 1);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(6, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 2);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(7, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 2);
  assert(xyz.z == 0);

  xyz = dg_geometry_idx_to_cell(8, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 2);
  assert(xyz.z == 0);

#if(ND == 3)
  // z = 1

  xyz = dg_geometry_idx_to_cell(9, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 0);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(10, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 0);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(11, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 0);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(12, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 1);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(13, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 1);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(14, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 1);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(15, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 2);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(16, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 2);
  assert(xyz.z == 1);

  xyz = dg_geometry_idx_to_cell(17, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 2);
  assert(xyz.z == 1);

  // z = 2

  xyz = dg_geometry_idx_to_cell(18, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 0);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(19, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 0);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(20, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 0);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(21, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 1);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(22, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 1);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(23, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 1);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(24, 3);
  assert(xyz.x == 0);
  assert(xyz.y == 2);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(25, 3);
  assert(xyz.x == 1);
  assert(xyz.y == 2);
  assert(xyz.z == 2);

  xyz = dg_geometry_idx_to_cell(26, 3);
  assert(xyz.x == 2);
  assert(xyz.y == 2);
  assert(xyz.z == 2);

#endif

  cout << "...PASSED" << endl;
#endif
}

bool dg_geometry_is_cell_in_this_rank(const int32_t cell)
{
  bool cell_in_rank = false;
  if((cell >= dg_geometry_first_cell_in_rank()) && (cell <= dg_geometry_last_cell_in_rank()))
    cell_in_rank = true;
  return cell_in_rank;
}
int32_t dg_geometry_first_cell_in_rank()
{
#if(ND == 1)
  return dg_geometry_cell_to_idx(FirstSlabThisTask, DG_NUM_CELLS_1D);
#endif
#if(ND == 2)
  return dg_geometry_cell_to_idx(FirstSlabThisTask, 0, DG_NUM_CELLS_1D);
#endif
#if(ND == 3)
  return dg_geometry_cell_to_idx(FirstSlabThisTask, 0, 0, DG_NUM_CELLS_1D);
#endif
}

int32_t dg_geometry_last_cell_in_rank()
{
#if(ND == 1)
  return dg_geometry_cell_to_idx(FirstSlabThisTask + Nslabs - 1, DG_NUM_CELLS_1D);
#endif
#if(ND == 2)
  return dg_geometry_cell_to_idx(FirstSlabThisTask + Nslabs - 1, DG_NUM_CELLS_1D - 1, DG_NUM_CELLS_1D);
#endif
#if(ND == 3)
  return dg_geometry_cell_to_idx(FirstSlabThisTask + Nslabs - 1, DG_NUM_CELLS_1D - 1, DG_NUM_CELLS_1D - 1, DG_NUM_CELLS_1D);
#endif
}

int32_t dg_geometry_global_cell_idx_to_local(const int32_t cell_global_idx) { return cell_global_idx % Nc; }

coords dg_geometry_get_cell_center(const xyz cell_idx, const double cell_size)
{
  /* Given cell index and cell size,
  this function returns the cell center in global coordinates */

  coords cell_center;
  cell_center.x = (cell_idx.x + .5) * cell_size;
#if(ND >= 2)
  cell_center.y = (cell_idx.y + .5) * cell_size;
#endif
#if(ND == 3)
  cell_center.z = (cell_idx.z + .5) * cell_size;
#endif  // (ND == 3)
  return cell_center;
}

coords dg_geometry_get_qpoint_location(const xyz q_idx, const coords cell_center_coords, const vector<double>& q_locations,
                                       const double cell_size)
{
  /* Given q_idx, cell center, qpoint locations in 1D and cell size,
  this function returns the qpoint coordinates in global coordinates */

  coords q_coords;

  q_coords.x = cell_center_coords.x + q_locations[q_idx.x] * cell_size / 2;
#if(ND >= 2)
  q_coords.y = cell_center_coords.y + q_locations[q_idx.y] * cell_size / 2;
#endif
#if(ND == 3)
  q_coords.z = cell_center_coords.z + q_locations[q_idx.z] * cell_size / 2;
#endif  // (ND == 3)

  return q_coords;
}

coords dg_geometry_coords_in_cell_units(const coords coordinates, double cellSize)
{
  coords to_return;
  to_return.x = coordinates.x / cellSize;
#if(ND > 1)
  to_return.y = coordinates.y / cellSize;
#endif
#if(ND > 2)
  to_return.z = coordinates.z / cellSize;
#endif
  return to_return;
}

xyz dg_geometry_coords_to_xyz(const coords coordinates)
{
  xyz to_return;
  to_return.x = (int)coordinates.x;
#if(ND > 1)
  to_return.y = (int)coordinates.y;
#endif
#if(ND > 2)
  to_return.z = (int)coordinates.z;
#endif
  return to_return;
}
//           const double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
coords dg_geometry_get_local_qpoint_coord_from_global_qpoint_coord_and_cell_center(const coords coordinates_in_cell_units,
                                                                                   const xyz cell_center_idx)
{
  coords to_return;

  to_return.x = 2.0 * (coordinates_in_cell_units.x - cell_center_idx.x) - 1.0;
#if(ND > 1)
  to_return.y = 2.0 * (coordinates_in_cell_units.y - cell_center_idx.y) - 1.0;
#endif
#if(ND > 2)
  to_return.z = 2.0 * (coordinates_in_cell_units.z - cell_center_idx.z) - 1.0;
#endif
  return to_return;
}

uint64_t dg_geometry_cell_id_this_rank(const xyz cell_idx)
{
  if(ND == 1)
    {
      return (cell_idx.x - FirstSlabThisTask) + cell_idx.y;
    }
  if(ND == 2)
    {
      return DG_NUM_CELLS_1D * (cell_idx.x - FirstSlabThisTask) + cell_idx.y;
    }
  if(ND == 3)
    {
      return DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * (cell_idx.x - FirstSlabThisTask) + cell_idx.y * DG_NUM_CELLS_1D + cell_idx.z;
    }
}

__device__ __host__ bool is_cell_on_outer_boundary(const int x, const int y, const int z)
{
  if((x == 0) || (x == NUM_CELLS_X - 1))
    return true;
#if(ND >= 2)
  if((y == 0) || (y == NUM_CELLS_Y - 1))
    return true;
#endif
#if(ND == 3)
  if((z == 0) || (z == NUM_CELLS_Z - 1))
    return true;
#endif
  return false;
}

__device__ __host__ bool is_cell_on_boundary_in_direction_of_current_axis(const int x, const int y, const int z, const int axis)
{
#if defined(OUTFLOW) || defined(REFLECTIVE_X) || defined(REFLECTIVE_Y) || defined(REFLECTIVE_Z)

  if((axis == 0) && (x == NUM_CELLS_X - 1))
    return true;
#if(ND >= 2)
  if((axis == 1) && (y == NUM_CELLS_Y - 1))
    return true;
#endif
#if(ND == 3)
  if((axis == 2) && (z == NUM_CELLS_Z - 1))
    return true;
#endif
  return false;

#else
  return false;
#endif
}

__device__ __host__ bool is_cell_on_boundary_in_direction_opposite_of_current_axis(const int x, const int y, const int z,
                                                                                   const int axis)
{
#if defined(OUTFLOW) || defined(REFLECTIVE_X) || defined(REFLECTIVE_Y) || defined(REFLECTIVE_Z)

  if((axis == 0) && (x == -1))
    return true;
#if(ND >= 2)
  if((axis == 1) && (y == -1))
    return true;
#endif
#if(ND == 3)
  if((axis == 2) && (z == -1))
    return true;
#endif
  return false;

#else
  return false;
#endif
}

__host__ uint64_t calculate_cell_offset() { return FirstSlabThisTask * intPower<ND - 1>(DG_NUM_CELLS_1D); }

__device__ __host__ double calculate_qpoint_integration_factor(const double cellVolume) { return powConstexpr(.5, ND) * cellVolume; }
