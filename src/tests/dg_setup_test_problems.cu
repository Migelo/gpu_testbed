// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include <string.h>

#include <cassert>
#include <fstream>
#include <vector>

#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"

using ::std::vector;

void dg_gauss_get_points_weights_internal(const int npoints, vector<double> &wgt, vector<double> &wgt_internal, const int dimension);
void dg_gauss_get_points_weights_external(const int npoints, vector<double> &wgt, vector<double> &wgt_external, const int dimension);
void dg_gauss_get_points_weights(const size_t npoints, vector<double> &pos, vector<double> &wgt);

#if defined(ADVECTION_TEST)
void dg_setup_test_problem_advection(dg_data_t *dg)
{
  mpi_printf("Setting up a simple advection test\n");

  dg->setBoxSize(5.0);

  const double rho0 = 1.0;
  const double P0   = 1.0;

  const double vel0[3] = {1.0, 0, 0};

  const double rho1 = 2.0;

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  // set everything to a uniform background state equal to density rho0, pressure P0, and velocity vel0.
  for(int c = 0; c < Nc; c++)
    {
      vector<double> fields(NF, 0);

      fields[0] = rho0;
      fields[1] = rho0 * vel0[0];
      fields[2] = rho0 * vel0[1];

#if(ND == 3)
      fields[3] = rho0 * vel0[2];

      double v2 = vel0[0] * vel0[0] + vel0[1] * vel0[1] + vel0[2] * vel0[2];
      fields[4] = 0.5 * rho0 * v2 + P0 / (GAMMA - 1.0);
#endif

#if(ND == 2)
      double v2 = vel0[0] * vel0[0] + vel0[1] * vel0[1];
      fields[3] = 0.5 * rho0 * v2 + P0 / (GAMMA - 1.0);
#endif

      /* the 0-th weight is the cell average, we only set this */
      for(int f = 0; f < NF; f++)
        dg->w[w_idx_internal(0, f, c)] = fields[f];
    }

  // now fill in a central quarter of the cells with density rho1, at equal pressure

  for(int x = (3 * DG_NUM_CELLS_1D) / 8; x < (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4; x++)
    for(int y = (3 * DG_NUM_CELLS_1D) / 8; y < (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4; y++)
#if(ND == 3)
      for(int z = (3 * DG_NUM_CELLS_1D) / 8; z < (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4; z++)
#endif
        {
          int c = x * DG_NUM_CELLS_1D + y;
#if(ND == 3)
          c = c * DG_NUM_CELLS_1D + z;
#endif

          vector<double> fields(NF, 0);

          fields[0] = rho1;
          fields[1] = rho1 * vel0[0];
          fields[2] = rho1 * vel0[1];

#if(ND == 3)
          fields[3] = rho1 * vel0[2];

          double v2 = vel0[0] * vel0[0] + vel0[1] * vel0[1] + vel0[2] * vel0[2];
          fields[4] = 0.5 * rho1 * v2 + P0 / (GAMMA - 1.0);
#endif

#if(ND == 2)
          double v2 = vel0[0] * vel0[0] + vel0[1] * vel0[1];
          fields[3] = 0.5 * rho1 * v2 + P0 / (GAMMA - 1.0);
#endif

          /* the 0-th weight is the cell average, we only set this */
          for(int f = 0; f < NF; f++)
            dg->w[w_idx_internal(0, f, c)] = fields[f];
        }

  // advect once through box
  // const double Press  = 1.0;
  // const double rhomin = 1.0;
  const double vmax = 1.0;

  All.Tend = dg->getBoxSize() / vmax;
  mpi_printf("fixing T end to %g to only advect through the box once.\n", All.Tend);
}

void dg_setup_test_problem_advection_2(dg_data_t *dg)
{
  // initial conditions
  const double rho0    = 1.0;
  const double P0      = 1.0;
  const double vel0[3] = {1.0, 0, 0};

  const double rho1 = 2.0;

  dg->setBoxSize(5.0);

  // set tabula rasa
  dg->init_constant_density_and_pressure_no_velocity(rho0, P0);

// set the central quarter to rho1 with vel0
#if(ND == 3)
  dg->set_field_at_coords("rho", rho1, (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4);
  dg->set_field_at_coords("vx", vel0[0], (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4);
  dg->set_field_at_coords("vy", vel0[1], (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4);
  dg->set_field_at_coords("vz", vel0[2], (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4);
  dg->set_field_at_coords("e", 0.5 * rho1 * (vel0[0] * vel0[0] + vel0[1] * vel0[1] + vel0[2] * vel0[2]) + P0 / (GAMMA - 1.0),
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4);
#elif(ND == 2)
  dg->set_field_at_coords("rho", rho1, (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, 0, 1);
  dg->set_field_at_coords("vx", vel0[0], (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, 0, 1);
  dg->set_field_at_coords("vy", vel0[0], (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4,
                          (3 * DG_NUM_CELLS_1D) / 8, (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, 0, 1);
  dg->set_field_at_coords("e", 0.5 * rho1 * (vel0[0] * vel0[0] + vel0[1] * vel0[1]) + P0 / (GAMMA - 1.0), (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, (3 * DG_NUM_CELLS_1D) / 8,
                          (3 * DG_NUM_CELLS_1D) / 8 + DG_NUM_CELLS_1D / 4, 0, 1);
#endif
}
#endif  // ADVECTION_TEST

#if defined(SOUNDWAVE_TEST)
void dg_setup_test_problem_soundwave(dg_data_t *dg)
{
  mpi_printf("Setting up a simple traveling sound wave test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  const double ampl   = 1.0e-6;
  double L            = 1.0;
  const double rho0   = 1.0;
  const double press0 = 1.0 / GAMMA;
  const double cs     = sqrt(GAMMA * press0 / rho0);

  dg->setBoxSize(L);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(int c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;
          //       const double yy = qpoint_coor.y;

          // primitive variables
          double rho   = rho0 * (1.0 + ampl * sin(2 * M_PI * xx / L));
          double vx    = ampl * cs * sin(2 * M_PI * xx / L);
          double vy    = 0;
          double press = press0 * pow(rho / rho0, GAMMA);

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }

  char buf[1000];
  sprintf(buf, "%s/L1.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdL1 = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}

double dg_L1_test_problem_soundwave(dg_data_t *dg, double time)
{
  mpi_printf("Computing L1 error norm for the traveling sound wave test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  const double ampl   = 1.0e-6;
  double L            = 1.0;
  const double rho0   = 1.0;
  const double press0 = 1.0 / GAMMA;
  const double cs     = sqrt(GAMMA * press0 / rho0);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  double L1 = 0;

  for(int c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          // primitive variables of analytic solution

          double xx_shifted = xx - cs * time;  // shift of the wave with time
          while(xx_shifted < 0)
            xx_shifted += L;

          double rho   = rho0 * (1.0 + ampl * sin(2 * M_PI * xx_shifted / L));
          double vx    = ampl * cs * sin(2 * M_PI * xx_shifted / L);
          double vy    = 0;
          double press = press0 * pow(rho / rho0, GAMMA);

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

          // now lets also get the numerical solution

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          int cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%d   c=%d", cell, c);

          double field_numerical[NF];

          for(int f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 4) * fabs(fields[0] - field_numerical[0]) * wtg2d.at(q);
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  return L1;
}

#endif  // SOUNDWAVE_TEST

#ifdef VORTEX_SHEET_TEST
double dg_L1_test_problem_vortex_sheet(dg_data_t *dg, double time)
{
  mpi_printf("Computing L1 error norm for the vortex sheet test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  // const double rhoL = 1.0;
  // const double velL = -1.0;

  // const double rhoR = 1.0;
  /// const double velR = 1.0;

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  double L1 = 0;

  for(int c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          // primitive variables of analytic solution

          double rho = 1.0;
          double vx  = erf((yy - 0.25 * dg->getBoxSize()) / (2 * sqrt(All.ShearViscosity * time))) -
                      erf((yy - 0.75 * dg->getBoxSize()) / (2 * sqrt(All.ShearViscosity * time))) - 1;

          double vy = 0;

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;

#ifndef ISOTHERM_EQS
          double press    = 2.0;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;
          fields[3]       = e;
#endif

          // now lets also get the numerical solution

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          int cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%d   c=%d", cell, c);

          double field_numerical[NF];

          for(int f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 4) * fabs(fields[1] - field_numerical[1]) * wtg2d.at(q);  // velocity times density
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  return L1;
}

void dg_setup_test_problem_vortex_sheet(dg_data_t *dg)
{
  mpi_printf("Setting up a simple Vortex sheet diffusion test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  dg->setBoxSize(2.0);

  const double rhoL = 1.0;
  const double velL = -1.0;

  const double rhoR = 1.0;
  const double velR = 1.0;

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          // const double xx = qpoint_coor.x;
          const double yy = qpoint_coor.y;

          // primitive variables
          double vx, rho, vy = 0;

          if(fabs(yy - 0.5 * dg->getBoxSize()) < 0.25 * dg->getBoxSize())
            {
              rho = rhoR;
              vx  = velR;
            }
          else
            {
              rho = rhoL;
              vx  = velL;
            }

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;

          vector<double> fields(NF, 0);
          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;

#ifndef ISOTHERM_EQS
          double press    = 2.0;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;
          fields[3]       = e;
#endif

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }

  char buf[1000];
  sprintf(buf, "%s/L1.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdL1 = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}
#endif

#ifdef GAUSSIAN_DIFFUSION_TEST
double dg_L1_test_problem_gaussian_diffusion(dg_data_t *dg, double time)
{
  mpi_printf("Computing L1 error norm for the Gaussian diffusion test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif
#ifndef ISOTHERM_EQS
  Terminate("requires ISOTHERM at the moment");
#endif
#ifndef ENABLE_DYE
  Terminate("requires ENABLE_DYE at the moment");
#endif

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);
  const double dyeg = 1.0;
  const double dyeb = 0.1;

  const double D      = 1.0 / 128;
  const double tstart = 1.0;
  const double sigma2 = 2 * D * (tstart + time);

  const auto cell_offset = getCellOffset();

  double L1 = 0;

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          // primitive variables of analytic solution

          double dye = dyeb;

          for(int dx = -1; dx <= 1; dx++)
            for(int dy = -1; dy <= 1; dy++)
              {
                double x = (xx - 0.5 * dg->getBoxSize());
                double y = (yy - 0.5 * dg->getBoxSize());

                double xg = dx * dg->getBoxSize();
                double yg = dy * dg->getBoxSize();

                double r2 = pow(x - xg, 2) + pow(y - yg, 2);

                dye += dyeg * exp(-r2 / (2 * sigma2)) / (2 * M_PI * sigma2);
              }

          vector<double> fields(NF, 0);
          fields[0]         = 1.0;
          fields[1]         = 0;
          fields[2]         = 0;
          fields[DYE_FIELD] = dye;

          // now lets also get the numerical solution

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          uint64_t cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%lu   c=%lu", cell, c);

          double field_numerical[NF];

          for(int f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 4) * fabs(fields[DYE_FIELD] - field_numerical[DYE_FIELD]) * wtg2d.at(q);  // velocity times density
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  return L1;
}

void dg_setup_test_problem_gaussian_diffusion(dg_data_t *dg)
{
  mpi_printf("Setting up a simple Gaussian diffusion test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif
#ifndef ISOTHERM_EQS
  Terminate("requires ISOTHERM at the moment");
#endif
#ifndef ENABLE_DYE
  Terminate("requires ENABLE_DYE at the moment");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  dg->setBoxSize(2.0);

  const double dyeg = 1.0;
  const double dyeb = 0.1;

  const double D      = 1.0 / 128;
  const double tstart = 1.0;
  const double sigma2 = 2 * D * tstart;

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;
          const double yy = qpoint_coor.y;

          // primitive variables
          double dye = dyeb;

          for(int dx = -1; dx <= 1; dx++)
            for(int dy = -1; dy <= 1; dy++)
              {
                double x = (xx - 0.5 * dg->getBoxSize());
                double y = (yy - 0.5 * dg->getBoxSize());

                double xg = dx * dg->getBoxSize();
                double yg = dy * dg->getBoxSize();

                double r2 = pow(x - xg, 2) + pow(y - yg, 2);

                dye += dyeg * exp(-r2 / (2 * sigma2)) / (2 * M_PI * sigma2);
              }

          vector<double> fields(NF, 0);
          fields[0]         = 1.0;
          fields[1]         = 0;
          fields[2]         = 0;
          fields[DYE_FIELD] = dye;

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }

  char buf[1000];
  sprintf(buf, "%s/L1.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdL1 = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}
#endif

#if defined(SHU_OSHER_SHOCKTUBE)
void dg_setup_test_problem_implosion(dg_data_t *dg)
{
  mpi_printf("Setting up the Shu & Osher shock tube\n");

#if(ND != 1)
  Terminate("This version is for 1D!\n");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  const double L = 10.0;
  dg->setBoxSize(L);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints       = DEGREE_K + 3;
  constexpr size_t npoints1d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg1d(npoints1d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg1d, ND);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints1d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;

          xx -= 5.0;

          // (ρ=3.857143; Vx= 2.629369, P = 10.33333) Right: (ρ=1 + 0.2 sin(5 π x); Vx=0; P=1)

          // primitive variables
          double rho, vx, press;

          if(xx < -4)
            {
              rho   = 3.857143;
              vx    = 2.629369;
              press = 10.33333;
            }
          else
            {
              rho   = 1.0 + 0.2 * sin(5 * xx);
              vx    = 0;
              press = 1.0;
            }
          // conserved variables
          const double px = vx * rho;
          const double v2 = vx * vx;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = e;

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 2 * fields[f] * wtg1d.at(q) * bfunc;
                }
            }
        }
    }
}
#endif

#if defined(IMPLOSION_TEST)
void dg_setup_test_problem_implosion(dg_data_t *dg)
{
  mpi_printf("Setting up the Implosion test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  const double L = 0.3;
  dg->setBoxSize(L);

  /*
  for x+y > 0.15, the initial density and pressure is 1.0, otherwise ρ = 0.125 and P = 0.14. The gas constant is γ = 1.4. Initial
  velocities are zero everywhere.
  */

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          // primitive variables
          double vx = 0;
          double vy = 0;

          double z = sqrt(2) / 2 * xx + sqrt(2) / 2 * yy - sqrt(2 * pow(0.15 / 2, 2));  // distance to the line xx+yy = 0.15
          double h = L / DG_NUM_CELLS_1D / DG_ORDER;

          /*
                    if(xx + yy > 0.15)
                      {
                        press = 1.0;
                        rho   = 1.0;
                      }
                    else
                      {
                        press = 0.14;
                        rho   = 0.125;
                      }
          */
          /* smooth things out with a tanh(x), because the above could not be resolved inside cells */

          double press1 = 0.14;
          double rho1   = 0.125;

          double press2 = 1.0;
          double rho2   = 1.0;

          double press = 0.5 * (press2 - press1) * tanh(z / h) + 0.5 * (press1 + press2);
          double rho   = 0.5 * (rho2 - rho1) * tanh(z / h) + 0.5 * (rho1 + rho2);

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }
}
#endif

#if defined(YEE_VORTEX)
void dg_setup_test_problem_yee_vortex(dg_data_t *dg)
{
  mpi_printf("Setting up a Yee isentropic vortex test\n");

#if(ND != 2)
  Terminate("This version is for 2D!\n");
#endif

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  const double L      = 10.0;
  const double beta   = 5.0;
  const double vboost = 1.0;

  dg->setBoxSize(L);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          xx -= L / 2;
          yy -= L / 2;

          double r = sqrt(xx * xx + yy * yy);

          // primitive variables
          double vx    = vboost - beta * yy / (2 * M_PI) * exp((1 - r * r) / 2);
          double vy    = vboost + beta * xx / (2 * M_PI) * exp((1 - r * r) / 2);
          double u     = 1 - beta * beta / (8 * GAMMA * M_PI * M_PI) * exp(1 - r * r);
          double rho   = pow((GAMMA - 1) * u, 1.0 / (GAMMA - 1));
          double press = GAMMA_MINUS1 * rho * u;

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }

  char buf[1000];
  sprintf(buf, "%s/L1.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdL1 = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}

double dg_L1_test_problem_yee_vortex(dg_data_t *dg, double time)
{
  mpi_printf("Computing L1 error norm for the Yee vortex test\n");

  static_assert(ND == 2, "This version is for 2D!\n");

  const double L      = 10.0;
  const double beta   = 5.0;
  const double vboost = 1.0;

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  double L1 = 0;

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x;
          double yy = qpoint_coor.y;

          xx -= L / 2;
          yy -= L / 2;

          xx -= vboost * time;
          yy -= vboost * time;

          if(xx < -L / 2)
            xx += L;

          if(yy < -L / 2)
            yy += L;

          double r = sqrt(xx * xx + yy * yy);

          // primitive variables
          double vx    = vboost - beta * yy / (2 * M_PI) * exp((1 - r * r) / 2);
          double vy    = vboost + beta * xx / (2 * M_PI) * exp((1 - r * r) / 2);
          double u     = 1 - beta * beta / (8 * GAMMA * M_PI * M_PI) * exp(1 - r * r);
          double rho   = pow((GAMMA - 1) * u, 1.0 / (GAMMA - 1));
          double press = GAMMA_MINUS1 * rho * u;

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

          // now lets also get the numerical solution

          xx = qpoint_coor.x;
          yy = qpoint_coor.y;

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          const auto cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%d   c=%d", cell, c);

          vector<double> field_numerical(NF, 0);

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 4) * fabs(fields[0] - field_numerical[0]) * wtg2d.at(q);  // velocity times density
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  return L1;
}

#endif

#if defined(KELVIN_HELMHOLTZ_TEST)
void dg_setup_test_problem_kelvin_helmholtz(dg_data_t *dg)
{
  static_assert(ND == 2, "KH test is for 2D only");

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

#ifndef KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS
  mpi_printf("Setting up a simple Kelvin Helmholtz test\n");

  dg->setBoxSize(1.0);

  const double rhoL = 1.0;
  const double velL = -0.5;

  const double rhoR = 2.0;
  const double velR = 0.5;

  const double press = 2.5;

  for(int x = FirstSlabThisTask; x < FirstSlabThisTask + Nslabs; x++)
    for(decltype(ND) y = 0; y < DG_NUM_CELLS_1D; y++)
      {
        double xx = (x + 0.5) / DG_NUM_CELLS_1D * dg->getBoxSize();
        double yy = (y + 0.5) / DG_NUM_CELLS_1D * dg->getBoxSize();

        double rho, velx, vely;

        if(fabs(yy - 0.5 * dg->getBoxSize()) < 0.25 * dg->getBoxSize())
          {
            rho  = rhoR;
            velx = velR;
          }
        else
          {
            rho  = rhoL;
            velx = velL;
          }

        double w0  = 0.2;
        double sig = 0.025;

        // perturb the y-velocity slightly to seed instability
        vely = w0 * sin(2.0 * 2 * M_PI / dg->getBoxSize() * xx) *
               (exp(-pow((yy - 0.25 * dg->getBoxSize()) / (2 * sig), 2)) + exp(-pow((yy - 0.75 * dg->getBoxSize()) / (2 * sig), 2)));

        vector<double> fields(NF, 0);

        fields[0] = rho;
        fields[1] = rho * velx;
        fields[2] = rho * vely;

        double v2 = velx * velx + vely * vely;
        fields[3] = 0.5 * rho * v2 + press / (GAMMA - 1.0);

        uint64_t cell = DG_NUM_CELLS_1D * (x - FirstSlabThisTask) + y;

        /* the 0-th weight is the cell average, we only set this */
        for(int f = 0; f < NF; f++)
          dg->w[w_idx_internal(0, f, cell)] = fields[f];
      }

#else
  // smooth ICs following Lecoanet et al. (2016)
  mpi_printf("Using smooth initial conditions Kelvin Helmholtz test\n");

  const double ampl = 0.01;
  const double L    = 2.0;
#ifdef KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS_NO_DENSITY_JUMP
  const double rhojump = 0.0;
#else
  const double rhojump = 1.0;
#endif
  const double press0 = 10.0;
  const double uflow  = 1.0;
  const double y1     = 0.5;
  const double y2     = 1.5;
  const double a      = 0.05;
  const double sig    = 0.2;

  dg->setBoxSize(L);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints2d = npoints * npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, 2);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;
          const double yy = qpoint_coor.y;

          // primitive variables
          double rho   = 1 + rhojump * 0.5 * (tanh((yy - y1) / a) - tanh((yy - y2) / a));
          double vx    = uflow * (tanh((yy - y1) / a) - tanh((yy - y2) / a) - 1);
          double vy    = ampl * sin(2 * M_PI * xx) * (exp(-pow((yy - y1) / sig, 2)) + exp(-pow((yy - y2) / sig, 2)));
          double press = press0;

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = py;
          fields[3] = e;

#ifdef ENABLE_DYE
          double dye = 0.5 * (tanh((yy - y2) / a) - tanh((yy - y1) / a) + 2);
          fields[4]  = rho * dye;
#endif

          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 4 * fields[f] * wtg2d.at(q) * bfunc;
                }
            }
        }
    }

#endif
}
#endif  // KELVIN_HELMHOLTZ_TEST

double get_dye_entropy(dg_data_t *dg)
{
  double concentration = 0;

  constexpr size_t npoints = DEGREE_K + 3;

  constexpr size_t npoints2d = intPower<ND>(npoints);

  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg2d(npoints2d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg2d, ND);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints2d; q++)
        {
          double dye_rho = 0;
          double rho     = 0;

          const auto qpoint_idx  = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          // const double xx = qpoint_coor.x;
          // const double yy = qpoint_coor.y;

          // now lets also get the numerical solution

          const coords coords_in_cell_units = dg_geometry_coords_in_cell_units(qpoint_coor, dg->getCellSize());
          // const double x_in_cell_units = xx / dg->getCellSize();
          // const double y_in_cell_units = yy / dg->getCellSize();

          // const int xc = (int)x_in_cell_units;
          // const int yc = (int)y_in_cell_units;

          const auto xyz_cell_idx = dg_geometry_coords_to_xyz(coords_in_cell_units);

          const coords coords_rel =
              dg_geometry_get_local_qpoint_coord_from_global_qpoint_coord_and_cell_center(coords_in_cell_units, xyz_cell_idx);

          // const double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          // const double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          const auto cell = dg_geometry_cell_id_this_rank(xyz_cell_idx);

          if(cell != c)
            Terminate("unexpected rank %d: cell=%lu   c=%lu FirstSlabThisTask=%d", ThisTask, cell, c, FirstSlabThisTask);

          for(decltype(ND) b = 0; b < NB; b++)
            {
              const double bfunc = dg->dg_legendre_bfunc_eval_coords_wrapper(b, coords_rel);
              dye_rho += dg->w[w_idx_internal(b, NF - 1, c)] * bfunc;
            }
          for(decltype(ND) b = 0; b < NB; b++)
            {
              const double bfunc = dg->dg_legendre_bfunc_eval_coords_wrapper(b, coords_rel);
              rho += dg->w[w_idx_internal(b, 0, c)] * bfunc;
            }

          const auto c   = std::max(1e-300, dye_rho / rho);
          const double s = -c * log(c);
          concentration += (1. / 4) * s * rho * wtg2d.at(q);
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &concentration, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  // factor of 2 instead of 4 because we normalize for the volume of Lecoanet
  concentration /= (NC / 2);

  return concentration;
}

#if defined(TURBULENCE)
void dg_setup_test_problem_turbulence(dg_data_t *dg)
{
  mpi_printf("Setting up the Turbulence driving test\n");

#if !(defined(ISOTHERM_EQS) || defined(COOLING))
  // GAMMA has to be 1.001 for isothermal turbulence driving
  static_assert(GAMMA - 1.0001 < 1e-12, "For turbulence simulations gamma has to be 1.0001");
#endif  // !(defined(ISOTHERM_EQS) || defined(COOLING))

  // initial conditions
  const double rho0 = 1.0;
  const double P0   = rho0 * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed;

  mpi_printf("setting all density to %g and pressure to %g\n", rho0, P0);

  dg->setBoxSize(1.0);

  // set tabula rasa
  dg->init_constant_density_and_pressure_no_velocity(rho0, P0);
}
#endif  // TURBULENCE

#if defined(POINT_EXPLOSION)
void dg_setup_test_problem_point_explosion(dg_data_t *dg)
{
  mpi_printf("Setting up the Sedov-Taylor point explosion test\n");

  constexpr double L = 1.0;

  dg->setBoxSize(L);

  constexpr double E     = 1.0;  // energy in exploding cell
  constexpr double rho0  = 1.0;  // background density
  constexpr double mcell = rho0 * powConstexpr(L, ND) / NC;

  // for setting the background pressure, we assume that the background has 0.001 times the energy E
  constexpr double u0 = 0.001 * E / ((NC - 1) * mcell);
  constexpr double P0 = GAMMA_MINUS1 * rho0 * u0;

  // make tabula rasa by initializing to background
  dg->init_constant_density_and_pressure_no_velocity(rho0, P0);

  // energy of exploding cells, we use 2^ND cells to maintain symmetry for even number of cells per dimension
  constexpr double u1 = E / (powConstexpr(2.0, ND) * mcell);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      double xx = cell_center.x - 0.5;
      if(xx > 0.5 * L)
        xx -= L;
      double r2 = xx * xx;
#if(ND >= 2)
      double yy = cell_center.y - 0.5;
      if(yy > 0.5 * L)
        yy -= L;
      r2 += yy * yy;
#endif
#if(ND >= 3)
      double zz = cell_center.z - 0.5;
      if(zz > 0.5 * L)
        zz -= L;
      r2 += zz * zz;
#endif

      const double r = sqrt(r2);

      if(r < dg->getCellSize())
        dg->w[w_idx_internal(0, 1 + ND, c)] = rho0 * u1;
    }
}
#endif  // POINT_EXPLOSION

#if defined(SHOCK_TUBE)
void dg_setup_test_problem_shock_tube(dg_data_t *dg)
{
  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  // initial conditions
  const double rhoL   = 1;
  const double pressL = 1;
  const double velL   = 0;

  const double rhoR   = 0.1;  //   0.1;
  const double pressR = 0.1;
  const double velR   = 0.00;

  dg->setBoxSize(1.0);

  // the initial conditions are integrated at two orders higher than our method works at
  constexpr size_t npoints   = DEGREE_K + 3;
  constexpr size_t npoints1d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg1d(npoints1d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg1d, ND);
  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints1d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;

          double rho, press, velx;

          // #if(ND == 1)
          if(xx < 0.5 * dg->getBoxSize())
            // #if(ND == 2)
            //             if(yy > xx)
            // #endif
            {
              rho   = rhoL;
              press = pressL;
              velx  = velL;
            }
          else
            {
              rho   = rhoR;
              press = pressR;
              velx  = velR;
            }

          // conserved variables
          const double px = velx * rho;
          const double v2 = velx * velx;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);  // fields at the cell
          fields[0]      = rho;
          fields[1]      = px;
          fields[ND + 1] = e;

          constexpr double fac = powConstexpr(.5, ND);

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#if(ND == 1)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
#endif
#if(ND == 2)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
#endif
#if(ND == 3)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
#endif
                  dg->w[w_idx_internal(b, f, c)] += fac * fields[f] * wtg1d.at(q) * bfunc;
                }
            }
        }
    }
}

#endif  // defined(SHOCK_TUBE)

#if defined(BOOSTED_SHOCK_TUBE)
void dg_setup_test_problem_shock_tube_boosted(dg_data_t *dg)
{
  static_assert((ND == 1) || (ND == 2));

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  // initial conditions
  const double rhoL   = 1;
  const double pressL = 1;
  const double velL   = .200;

  const double rhoR   = 0.85;
  const double pressR = 0.90;
  const double velR   = .200;

  dg->setBoxSize(1.0);

  // the initial conditions are integrated at two orders higher than our method works at
  constexpr size_t npoints   = DEGREE_K + 3;
  constexpr size_t npoints1d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg1d(npoints1d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg1d, ND);
  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints1d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;
#if(ND == 2)
          const double yy = qpoint_coor.y;
#endif
          double rho, press, velx;

#if(ND == 1)
          if(xx < 0.5 * dg->getBoxSize())
#else
          if(yy > xx)
#endif
            {
              rho   = rhoL;
              press = pressL;
              velx  = velL;
            }
          else
            {
              rho   = rhoR;
              press = pressR;
              velx  = velR;
            }

          // conserved variables
          const double px = velx * rho;
          const double v2 = velx * velx;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);  // fields at the cell
          fields[0]      = rho;
          fields[1]      = px;
          fields[ND + 1] = e;
          // if(c + cell_offset == 31 || c + cell_offset == 32 || c + cell_offset == 33)
          //   fields[ALPHA_FIELD] = 1;

          constexpr double fac = powConstexpr(.5, ND);

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#if(ND == 1)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
#endif
#if(ND == 2)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
#endif
                  dg->w[w_idx_internal(b, f, c)] += fac * fields[f] * wtg1d.at(q) * bfunc;
                }
            }
        }
    }
}

#endif  // defined(BOOSTED_SHOCK_TUBE)

#if defined(DOUBLE_BLAST)
void dg_setup_test_problem_double_blast(dg_data_t *dg)
{
#if(ND != 1)
  Terminate("This version is for 1D!\n");
#endif

  // setup the double blast problem of Woodward & Colella (1984)

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  dg->setBoxSize(2.0);

  // the initial conditions are integrated at two orders higher than our method works at
  const size_t npoints   = DEGREE_K + 3;
  const size_t npoints1d = npoints;
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg1d(npoints1d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg1d, 1);

  const auto cell_offset = getCellOffset();

  for(int c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints1d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;

          double rho = 1.0, press, velx = 0.0;

          if(xx < 0.1 || xx > 1.9)
            {
              press = 1000.0;
            }
          else if(xx > 0.9 && xx < 1.1)
            {
              press = 100.0;
            }
          else
            press = 0.01;

          // conserved variables
          const double px = velx * rho;
          const double v2 = velx * velx;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0] = rho;
          fields[1] = px;
          fields[2] = e;

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 2 * fields[f] * wtg1d.at(q) * bfunc;
                }
            }
        }
    }
}
#endif

#if defined(SINGLE_SHOCK)
void dg_setup_test_problem_single_shock(dg_data_t *dg)
{
  // setup the double blast problem of Woodward & Colella (1984)

  // make tabula rasa
  memset((void **)dg->w, 0, dg->N_w * sizeof(double));

  dg->setBoxSize(1.0);

  // the initial conditions are integrated at two orders higher than our method works at
  constexpr size_t npoints   = DEGREE_K + 3;
  constexpr size_t npoints1d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg1d(npoints1d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg1d, ND);

  const auto cell_offset = getCellOffset();

  double M = 3.5;  //   Mach number of shock

  double rho_R = 1.0, press_R = 1.0;

  double rho_L   = (GAMMA + 1) * M * M / (GAMMA_MINUS1 * M * M + 2) * rho_R;
  double press_L = (2 * GAMMA * M * M - (GAMMA_MINUS1)) / (GAMMA + 1) * press_R;

  double velx_R = sqrt((press_L - press_R) / rho_R / (1 - rho_R / rho_L));
  double velx_L = velx_R * rho_R / rho_L;

  printf("PressL=%g  rhoL=%g  velL=%g  velR=%g\n", press_L, rho_L, velx_L, velx_R);

  for(uint c = 0; c < Nc; c++)
    {
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints1d; q++)
        {
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x;

          double rho, press, velx;

          if(xx < 1.0 / DG_NUM_CELLS_1D)
            {
              rho   = rho_L;
              press = press_L;
              velx  = -velx_L + velx_R;
            }
          else
            {
              rho   = rho_R;
              press = press_R;
              velx  = -velx_R + velx_R;
            }

          // conserved variables
          const double px = velx * rho;
          const double v2 = velx * velx;
          const double e  = 0.5 * rho * v2 + press / GAMMA_MINUS1;

          vector<double> fields(NF, 0);

          fields[0]      = rho;
          fields[1]      = px;
          fields[ND + 1] = e;

          constexpr double fac = powConstexpr(.5, ND);

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#if(ND == 1)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
#endif
#if(ND == 2)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
#endif
#if(ND == 3)
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
#endif

                  dg->w[w_idx_internal(b, f, c)] += fac * fields[f] * wtg1d.at(q) * bfunc;
                }
            }
        }
    }

  double endpoint = 0.5;
  mpi_printf("\n----> Shock front reaches %g at time %g\n\n", endpoint, (endpoint - 1.0 / DG_NUM_CELLS_1D) / (velx_R));
}
#endif

#if defined(ISENTROPIC_VORTEX)
void dg_setup_test_problem_isentropic_vortex(dg_data_t *dg)
{
  if(abs(GAMMA - 7. / 5) > 1e-3)
    Terminate("Gamma for Isentropic Vortex has to be 7/5.");
  if((ND != 3) and (ND != 2))
    Terminate("Isentropic Vortex test problem is only in 2D and 3D.");

  // the initial conditions are set at one order higher than our method works at,
  // const int nb           = ((DEGREE_K + 1 + 1) * (DEGREE_K + 2 + 1) * (DEGREE_K + 3 + 1) / 6);
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(10.0);

  // initial conditions
  const double beta = .5;
  const double y0   = dg->getBoxSize() / 2.;
  const double x0   = dg->getBoxSize() / 2.;

  // set tabula rasa
  dg->init_constant_density_and_pressure_no_velocity(0, 0);

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;

          const double r = sqrt(xx * xx + yy * yy);

          // primitive variables
          const double rho =
              pow((1 - ((GAMMA_MINUS1 * beta * beta) / (8 * GAMMA * M_PI * M_PI)) * (pow(M_E, 1 - r * r))), 1 / GAMMA_MINUS1);
          const double p  = pow(rho, GAMMA);
          const double vx = -yy * beta / 2 / M_PI * pow(M_E, (1 - r * r) / 2);
          const double vy = xx * beta / 2 / M_PI * pow(M_E, (1 - r * r) / 2);

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = .5 * rho * v2 + p / GAMMA_MINUS1;

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
#ifdef THREEDIMS
          fields.at(3) = 0;
          fields.at(4) = e;
#else
          fields.at(3) = e;
#endif  // THREEDIMS
          assert(rho > 0);
          assert(e > 0);
          assert(p > 0);

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef THREEDIMS
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
                  const double fac   = 1. / 8;
#else
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  const double fac   = 1. / 4;

#endif  // THREEDIMS
                  dg->w[w_idx_internal(b, f, c)] += fac * fields.at(f) * wtg3d.at(q) * bfunc;
                }
            }
        }
    }
}

double dg_L1_test_problem_isentropic_vortex(dg_data_t *dg)
{
  mpi_printf("Computing L1 error norm for the isentropic vortex test...\n");

  if((ND != 3) and (ND != 2))
    Terminate("Isentropic Vortex test problem is only in 2D and 3D.");

  // the initial conditions are set at one order higher than our method works at,
  // const int nb           = ((DEGREE_K + 1 + 1) * (DEGREE_K + 2 + 1) * (DEGREE_K + 3 + 1) / 6);
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(10.0);

  // initial conditions
  const double beta = .5;

  // cell center reference coordinates
  const double y0 = dg->getBoxSize() / 2.;
  const double x0 = dg->getBoxSize() / 2.;

  const auto cell_offset = getCellOffset();

  double L1 = 0;

  for(uint64_t c = 0; c < Nc; c++)
    {
      // global cell index
      const xyz cell_idx = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      // cell center in global coordinates
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // gpoint index in all dimensions
          const xyz qpoint_idx = dg_geometry_idx_to_cell(q, npoints);

          // global qpoint coordinates
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          // xx and yy are qpoint coordinates relative to box center
          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;
          const double r  = sqrt(xx * xx + yy * yy);

          // primitive variables of analytic solution

          const double rho =
              pow((1 - ((GAMMA_MINUS1 * beta * beta) / (8 * GAMMA * M_PI * M_PI)) * (pow(M_E, 1 - r * r))), 1 / GAMMA_MINUS1);
          const double p  = pow(rho, GAMMA);
          const double vx = -yy * beta / 2 / M_PI * pow(M_E, (1 - r * r) / 2);
          const double vy = xx * beta / 2 / M_PI * pow(M_E, (1 - r * r) / 2);

          // conserved variables
          const double px = vx * rho;
          const double py = vy * rho;
          const double v2 = vx * vx + vy * vy;
          const double e  = .5 * rho * v2 + p / GAMMA_MINUS1;

          // conserved variables
          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
#ifdef THREEDIMS
          fields.at(3) = 0;
          fields.at(4) = e;
#else
          fields.at(3) = e;
#endif  // THREEDIMS
          assert(rho > 0);
          assert(e > 0);
          assert(p > 0);

          // now lets also get the numerical solution

          // double x_in_cell_units = xx / dg->getCellSize();
          // double y_in_cell_units = yy / dg->getCellSize();

          // int xc = (int)x_in_cell_units;
          // int yc = (int)y_in_cell_units;

          // double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          // double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          // int cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          // if(cell != c)
          //   Terminate("unexpected:   cell=%d   c=%d", cell, c);

          vector<double> field_numerical(NF, 0);

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef TWODIMS
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos[qpoint_idx.x], pos[qpoint_idx.y]);
#else
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos[qpoint_idx.x], pos[qpoint_idx.y], pos[qpoint_idx.z]);
#endif  // TWODIMS

                  field_numerical[f] += dg->w[w_idx_internal(b, f, c)] * bfunc;
                }
            }
          constexpr double fac = powConstexpr(.5, ND);

          L1 += fac * fabs(fields[0] - field_numerical[0]) * wtg3d.at(q);
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  mpi_printf("[L1] %g\n", L1);

  return L1;
}
#endif  // defined(ISENTROPIC_VORTEX)

#ifdef SQUARE_ADVECTION
void dg_setup_test_problem_square_advection(dg_data_t *dg)
{
  static_assert(abs(GAMMA - 7. / 5) > 1e-3);

#if(ND == 1)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double x0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;

          // conserved variables
          double rho;
          double px;
          double v2;
          double e;

          if((xx >= x_left_corner) && (xx <= x_right_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              v2  = vx * vx;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              v2  = vx * vx;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          assert(e > 0);
          assert(p > 0);

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = e;

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x));
                  const double fac   = 1. / 2;

                  dg->w[w_idx_internal(b, f, c)] += fac * fields.at(f) * wtg3d.at(q) * bfunc;
                }
            }
        }
    }
#endif

#if(ND == 2)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double vy    = 50;
  const double y0    = 0;
  const double x0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;
  const double y_lower_corner = .25;
  const double y_upper_corner = .75;

  const auto cell_offset = getCellOffset();

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;

          // conserved variables
          double rho;
          double px;
          double py;
          double v2;
          double e;

          if((xx >= x_left_corner) && (xx <= x_right_corner) && (yy >= y_lower_corner) && (yy <= y_upper_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              py  = vy * rho;
              v2  = vx * vx + vy * vy;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              py  = vy * rho;
              v2  = vx * vx + vy * vy;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          assert(e > 0);
          assert(p > 0);

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
          fields.at(3) = e;

          for(uint f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y));
                  const double fac   = 1. / 4;

                  dg->w[w_idx_internal(b, f, c)] += fac * fields.at(f) * wtg3d.at(q) * bfunc;
                }
            }
        }
    }
#endif

#if(ND == 3)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double vy    = 50;
  const double vz    = -20;
  const double y0    = 0;
  const double x0    = 0;
  const double z0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;
  const double y_lower_corner = .25;
  const double y_upper_corner = .75;
  const double z_lower_corner = .25;
  const double z_upper_corner = .75;

  const auto cell_offset = getCellOffset();

  for(uint32_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;
          const double zz = qpoint_coor.z - z0;

          // conserved variables
          double rho;
          double px;
          double py;
          double pz;
          double v2;
          double e;

          if((xx >= x_left_corner) && (xx <= x_right_corner) && (yy >= y_lower_corner) && (yy <= y_upper_corner) &&
             (zz >= z_lower_corner) && (zz <= z_upper_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              py  = vy * rho;
              pz  = vz * rho;
              v2  = vx * vx + vy * vy + vz * vz;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              py  = vy * rho;
              pz  = vz * rho;
              v2  = vx * vx + vy * vy + vz * vz;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          assert(e > 0);
          assert(p > 0);

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
          fields.at(3) = pz;
          fields.at(4) = e;

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
                  const double fac   = 1. / 8;

                  dg->w[w_idx_internal(b, f, c)] += fac * fields.at(f) * wtg3d.at(q) * bfunc;
                }
            }
        }
    }
#endif

  char buf[1000];
  sprintf(buf, "%s/L1.txt", All.OutputDir);

  if(ThisTask == 0)
    {
      char fmode[10];
      if(All.Restart_bool)
        sprintf(fmode, "%s", "a");
      else
        sprintf(fmode, "%s", "w");

      if(!(dg->FdL1 = fopen(buf, fmode)))
        Terminate("can't open file '%s'\n", buf);
    }
}

double dg_L1_test_problem_square_advection(dg_data_t *dg, double time)
{
  mpi_printf("Computing L1 error norm for square advection test\n");

  double L1 = 0;

#if(ND == 1)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double x0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;

  const auto cell_offset = getCellOffset();

  for(int32_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          double xx = qpoint_coor.x - x0;

          // conserved variables
          double rho;
          double px;
          double v2;
          double e;

          xx += vx * time;
          while(xx > 1.0)
            xx -= 1.0;

          if((xx >= x_left_corner) && (xx <= x_right_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              v2  = vx * vx;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              v2  = vx * vx;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          assert(e > 0);
          assert(p > 0);

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = e;

          // now lets also get the numerical solution

          xx = qpoint_coor.x;

          double x_in_cell_units = xx / dg->getCellSize();

          int xc = (int)x_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;

          int cell = (xc - FirstSlabThisTask);

          if(cell != c)
            Terminate("unexpected:   cell=%d   c=%d", cell, c);

          double field_numerical[NF];

          for(int f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel);
                }
            }

          L1 += (1. / 2) * fabs(fields[0] - field_numerical[0]) * wtg3d.at(q);  // velocity times density
        }
    }
#endif

#if(ND == 2)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double vy    = 50;
  const double y0    = 0;
  const double x0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;
  const double y_lower_corner = .25;
  const double y_upper_corner = .75;

  const auto cell_offset = getCellOffset();

  for(uint32_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          auto xx = qpoint_coor.x - x0;
          auto yy = qpoint_coor.y - y0;

          // conserved variables
          double rho;
          double px;
          double py;
          double v2;
          double e;

          xx += vx * time;
          yy += vy * time;
          while(xx > 1.0)
            xx -= 1.0;
          while(yy > 1.0)
            yy -= 1.0;

          if((xx >= x_left_corner) && (xx <= x_right_corner) && (yy >= y_lower_corner) && (yy <= y_upper_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              py  = vy * rho;
              v2  = vx * vx + vy * vy;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              py  = vy * rho;
              v2  = vx * vx + vy * vy;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
          fields.at(3) = e;

          // now lets also get the numerical solution

          xx = qpoint_coor.x;
          yy = qpoint_coor.y;

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          const auto cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%lu   c=%u", cell, c);

          double field_numerical[NF];

          for(uint f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 4) * fabs(fields[0] - field_numerical[0]) * wtg3d.at(q);  // velocity times density
        }
    }
#endif

#if(ND == 3)
  // initial conditions are projected at a higher spatial order
  const size_t npoints   = DEGREE_K + 2;
  const size_t npoints3d = pow(npoints, ND);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  // set box size
  dg->setBoxSize(1.0);

  // initial conditions for background
  const double rho_b = 1;
  const double p     = 2.5;
  const double vx    = 100;
  const double vy    = 50;
  const double vz    = -20;
  const double y0    = 0;
  const double x0    = 0;
  const double z0    = 0;

  // squared box in the middle
  const double rho_s          = 4;
  const double x_left_corner  = .25;
  const double x_right_corner = .75;
  const double y_lower_corner = .25;
  const double y_upper_corner = .75;
  const double z_lower_corner = .25;
  const double z_upper_corner = .75;

  const auto cell_offset = getCellOffset();

  for(decltype(ND) c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const xyz cell_idx       = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const coords cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;
          const double zz = qpoint_coor.z - z0;

          // conserved variables
          double rho;
          double px;
          double py;
          double pz;
          double v2;
          double e;

          xx += vx * time;
          yy += vy * time;
          zz += vz * time;
          while(xx > 1.0)
            xx -= 1.0;
          while(yy > 1.0)
            yy -= 1.0;
          while(zz < 0.0)
            zz += 1.0;

          if((xx >= x_left_corner) && (xx <= x_right_corner) && (yy >= y_lower_corner) && (yy <= y_upper_corner) &&
             (zz >= z_lower_corner) && (zz <= z_upper_corner))
            {
              rho = rho_s;
              px  = vx * rho;
              py  = vy * rho;
              pz  = vz * rho;
              v2  = vx * vx + vy * vy + vz * vz;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }
          else
            {
              rho = rho_b;
              px  = vx * rho;
              py  = vy * rho;
              pz  = vz * rho;
              v2  = vx * vx + vy * vy + vz * vz;
              e   = .5 * rho * v2 + p / GAMMA_MINUS1;
            }

          assert(e > 0);
          assert(p > 0);

          vector<double> fields(NF, 0);
          fields.at(0) = rho;
          fields.at(1) = px;
          fields.at(2) = py;
          fields.at(3) = pz;
          fields.at(4) = e;

          // now lets also get the numerical solution

          xx = qpoint_coor.x;
          yy = qpoint_coor.y;

          double x_in_cell_units = xx / dg->getCellSize();
          double y_in_cell_units = yy / dg->getCellSize();

          int xc = (int)x_in_cell_units;
          int yc = (int)y_in_cell_units;

          double xrel = 2.0 * (x_in_cell_units - xc) - 1.0;
          double yrel = 2.0 * (y_in_cell_units - yc) - 1.0;

          int cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;

          if(cell != c)
            Terminate("unexpected:   cell=%d   c=%d", cell, c);

          double field_numerical[NF];

          for(int f = 0; f < NF; f++)
            {
              field_numerical[f] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  field_numerical[f] += dg->w[w_idx_internal(b, f, cell)] * dg->dg_legendre_bfunc_eval(b, xrel, yrel);
                }
            }

          L1 += (1. / 8) * fabs(fields[0] - field_numerical[0]) * wtg2d.at(q);  // velocity times density
        }
    }
#endif

  MPI_Allreduce(MPI_IN_PLACE, &L1, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  L1 /= NC;

  return L1;
}

#endif  // SQUARE_ADVECTION

void dg_setup_test_problem_constant_density_and_pressure(dg_data_t *dg)
{
  mpi_printf("Setting up the constant density and pressure test.\n");

  constexpr double L = 1.0;

  dg->setBoxSize(L);

  constexpr double rho = 1.0;
  constexpr double p   = 1.0;

  dg->init_constant_density_and_pressure_no_velocity(rho, p);
}

#ifdef COOLING
constexpr double calculate_pressure_from_temperature_and_density_constexpr(const double temperature, const double density)
{
  return 3 * temperature * density * BOLTZMANN_CONSTANT * GAMMA_MINUS1 / (2 * MEAN_PARTICLE_COUNT * PROTON_MASS);
}

void dg_setup_test_problem_cooling(dg_data_t *dg)
{
  mpi_printf("Setting up the cooling test \n");

  // a note on units:
  // there are code and physics units
  // to go from physics to code units, multiply by the appropriate factor
  // e.g. to go from [kpc] to [code units], multiply by LENGTH_FACTOR
  // the code units are:
  // [l] = kpc
  // [m] = 10^10 M_sun
  // [t] = [kpc / km) s ~ 10^8 years
  // [v] = kpc / (s kpc / km) = km / s

  constexpr double L = 1 * KPC * LENGTH_FACTOR;  // [kpc]

  dg->setBoxSize(L);

  constexpr double hydrogen_number_density = 0.1;                                    // [N cm^{-3}]
  constexpr double physical_density        = hydrogen_number_density * PROTON_MASS;  // [g cm^{-3}]
  constexpr double code_density            = physical_density * DENSITY_FACTOR;      // [10^10 M_sun / kpc^3]
  constexpr double physical_temperature    = 1e6;                                    // [K]
  constexpr double physical_pressure =
      calculate_pressure_from_temperature_and_density_constexpr(physical_temperature, physical_density);
  constexpr double code_pressure = physical_pressure * PRESSURE_FACTOR;

  printf("with density, pressure / temperature in physical units: density=%e pressure=%e temperature=%e\n", physical_density,
         physical_pressure, physical_temperature);
  printf("with density, pressure / temperature in code units:     density=%e pressure=%e\n", code_density, code_pressure);

  dg->init_constant_density_and_pressure_no_velocity(code_density, code_pressure);
}
#endif

#ifdef GRAVITY_WAVES
template <typename T>
T clamp(T x, T lowerlimit, T upperlimit)
{
  if(x < lowerlimit)
    x = lowerlimit;
  if(x > upperlimit)
    x = upperlimit;
  return x;
}

template <typename T>
T smootherstep(T edge0, T edge1, T x)
{
  // Scale, and clamp x to 0..1 range
  x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
  // Evaluate polynomial
  return x * x * x * (x * (x * 6 - 15) + 10);
}

void points_on_sphere(int num_pts, coordinates *coords)
{
  vector<double> indices(num_pts, 0);
  for(int i = 0; i < num_pts; i++)
    indices[i] = i + .5;

  vector<double> phi(num_pts, 0);
  for(int i = 0; i < num_pts; i++)
    phi[i] = acos(1 - 2 * indices[i] / num_pts);

  vector<double> theta(num_pts, 0);
  for(int i = 0; i < num_pts; i++)
    theta[i] = M_PI * (1 + sqrt(5)) * indices[i];

  coords->x.resize(num_pts);
  coords->y.resize(num_pts);
  coords->z.resize(num_pts);
  for(int i = 0; i < num_pts; i++)
    {
      coords->x[i] = cos(theta[i]) * sin(phi[i]);
      coords->y[i] = sin(theta[i]) * sin(phi[i]);
      coords->z[i] = cos(phi[i]);
    }
}

void dg_setup_test_problem_gravity_waves(dg_data_t *dg)
{
  double p_min   = std::numeric_limits<double>::infinity();  // minimum pressure
  double rho_min = std::numeric_limits<double>::infinity();  // minimum density

  double mass_total = 0;
  double e_total    = 0;
  // g = data[:, 0]  # gravitational acceleration
  // d = data[:, 5]  # density(rho)
  // d_p = data[:, 6]  # d(\rho)/dr
  // p = data[:, 7]  # pressure(p)
  // p_p = data[:, 8]  # dp/dr
  // s = data[:, 9]  # entropy
  // BV2 = data[:, 11]  # Brunt-Vaisala frequency
  // tcool = data[:, 12]  # cooling time
  // dlnlamdlnT = data[:, 13]  # dlog(\lamda)/dlog(T)
  // tff = data[:, 14]  # free-fall time
  // r = data[:, 15]  # radial grid
  constexpr int row         = 100000;
  constexpr int col         = 4;
  constexpr int col_perturb = 5;
  double input_data[row][col];
  double perturbation_data[row][col_perturb];

  // open IC file
  std::ifstream input_file("/freya/u/mihac/gravity_waves/gravity_wagves/background_flash.dat");
  if(!input_file.is_open())
    Terminate("Error opening file");

  for(int r = 0; r < row; r++)
    {
      for(int c = 0; c < col; c++)
        {
          input_file >> input_data[r][c];
        }
    }
  input_file.close();

  vector<double> r_arr(row, 0);
  vector<double> rho_arr(row, 0);
  vector<double> p_arr(row, 0);

  for(int r = 0; r < row; r++)
    {
      r_arr[r]   = input_data[r][0] * LENGTH_FACTOR;
      rho_arr[r] = input_data[r][1] * DENSITY_FACTOR;
      p_arr[r]   = input_data[r][2] * PRESSURE_FACTOR;
      // s_arr[r]   = input_data[r][3];
    }
  const auto R_MAX = r_arr[row - 1];

  // define equation to get r_arr index
  const double k  = r_arr[1] - r_arr[0];
  const double n0 = r_arr[0];

  std::ifstream input_file_perturbation("/freya/u/mihac/gravity_waves/gravity_wagves/eigenmode_flash.dat");
  if(!input_file_perturbation.is_open())
    Terminate("Error opening file");

  for(int r = 0; r < row; r++)
    {
      for(int c = 0; c < col_perturb; c++)
        {
          input_file_perturbation >> perturbation_data[r][c];
        }
    }
  input_file_perturbation.close();
  vector<double> rho_perturb_arr(row, 0);
  vector<double> p_perturb_arr(row, 0);
  vector<double> vr_perturb_arr(row, 0);
  vector<double> vtheta_perturb_arr(row, 0);
  for(int r = 0; r < row; r++)
    {
      rho_perturb_arr[r]    = perturbation_data[r][1];
      p_perturb_arr[r]      = perturbation_data[r][2];
      vr_perturb_arr[r]     = perturbation_data[r][3];
      vtheta_perturb_arr[r] = perturbation_data[r][4];
    }

  // set box size
  // dg->setBoxSize(R_MAX * 2 / sqrt(3));  // box fully embedded into the halo
  dg->setBoxSize(R_MAX * 2 * 1.2);  // box = 2R * 1.1, halo fully embedded into the box, +10%
  // dg->setBoxSize(50);  // box = 2R * 1.1, halo fully embedded into the box, +10%

  // initial conditions are projected at a higher spatial order
  constexpr size_t npoints   = DEGREE_K + 2;
  constexpr size_t npoints3d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  const auto y0 = dg->getBoxSize() / 2;
  const auto x0 = dg->getBoxSize() / 2;
  const auto z0 = dg->getBoxSize() / 2;

  double rr_min = std::numeric_limits<double>::infinity();

  const auto cell_offset = getCellOffset();
  mpi_printf("r_min = %g    r_max = %g\n", r_arr[0], r_arr[row - 1]);

  const auto p_out   = p_arr[row - 1];
  const auto rho_out = rho_arr[row - 1];
  const auto p_in    = p_arr[0];
  const auto rho_in  = rho_arr[0];
  const auto cs0     = sqrt(p_in / rho_in);
  double v_max       = 0;

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const auto cell_idx    = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const auto cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      if(c % 1000 == 0)
        mpi_printf("setting ICs for cell %4.2f\n", 1. * c / Nc);

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;
          const double zz = qpoint_coor.z - z0;
          const double rr = sqrt(xx * xx + yy * yy + zz * zz);

          assert(xx < dg->getBoxSize());
          assert(yy < dg->getBoxSize());
          assert(zz < dg->getBoxSize());

          if(rr < rr_min)
            rr_min = rr;

          // // project g onto the x-y plane
          const auto rr2d  = sqrt(xx * xx + yy * yy);
          const auto irr2d = 1. / rr2d;
          const auto irr   = 1. / rr;
          // const auto cos_theta = xx * irr2d;
          // const auto sin_theta = yy * irr2d;
          // const auto cos_phi   = zz * irr;
          // const auto sin_phi   = rr2d * irr;
          const double cos_theta = zz * irr;
          const double sin_theta = rr2d * irr;
          const double cos_phi   = xx * irr2d;
          const double sin_phi   = yy * irr2d;

          // conserved variables are to be interpolated from input files
          double rho_interpolated = 0, p_interpolated = 0;
          double rho_perturb = 0, p_perturb = 0;
          vector<double> velocity_perturb(2, 0);
          if(1)
            {
              for(int i = std::min((int)((rr - n0) / k), row - 2); i < row - 1; i++)
                {
                  // printf("old_i = %d\n", old_i);
                  if(rr > R_MAX)  // outside the halo
                    {
                      p_interpolated   = p_out;
                      rho_interpolated = rho_out;
                      break;
                    }
                  else if((rr > r_arr[i]) && (rr <= r_arr[i + 1]))  // inside the halo
                    {
                      const double fac    = (rr - r_arr[i]) / (r_arr[i + 1] - r_arr[i]);
                      p_interpolated      = p_arr[i + 1] * fac + (1 - fac) * p_arr[i];
                      rho_interpolated    = rho_arr[i + 1] * fac + (1 - fac) * rho_arr[i];
                      rho_perturb         = rho_perturb_arr[i + 1] * fac + (1 - fac) * rho_perturb_arr[i];
                      p_perturb           = p_perturb_arr[i + 1] * fac + (1 - fac) * p_perturb_arr[i];
                      velocity_perturb[0] = vr_perturb_arr[i + 1] * fac + (1 - fac) * vr_perturb_arr[i];
                      velocity_perturb[1] = vtheta_perturb_arr[i + 1] * fac + (1 - fac) * vtheta_perturb_arr[i];
                      break;
                    }
                  else if(rr < r_arr[0])
                    {
                      rho_interpolated = rho_in;
                      p_interpolated   = p_in;
                    }
                  else
                    {
                      if(i == row - 1)
                        Terminate("how did we end up here?\n c = %lu q = %d rr = %f\n", c, q, rr);
                    }
                }
            }

          const double sim_fluct_norm = All.WaveAmplitude;
          const auto Yml              = 0.25 * sqrt(5.0 / M_PI) * (3.0 * (cos_theta * cos_theta) - 1.0);
          const auto dYml_dtheta      = -1.5 * sqrt(5.0 / M_PI) * cos_theta * sin_theta;

          const auto rho = rho_interpolated * (1.0 + sim_fluct_norm * rho_perturb * Yml);
          const auto p   = p_interpolated * (1.0 + sim_fluct_norm * p_perturb * Yml);

          const auto dvr     = sim_fluct_norm * velocity_perturb[0] * cs0 * Yml;
          const auto dvtheta = sim_fluct_norm * velocity_perturb[1] * cs0 * dYml_dtheta;
          const auto dvphi   = sim_fluct_norm * 0;  //! dvtheta / sinTheta * dYml_dphi;

          // !projecting perturbations onto Cartesian grid
          // !source: https://www.mathworks.com/help/phased/ref/sph2cartvec.html
          const auto a11 = -sin_phi;
          const auto a12 = cos_theta * cos_phi;
          const auto a13 = sin_theta * cos_phi;
          const auto a21 = cos_phi;
          const auto a22 = cos_theta * sin_phi;
          const auto a23 = sin_theta * sin_phi;
          const auto a31 = 0;
          const auto a32 = -sin_theta;
          const auto a33 = cos_theta;

          const auto vx = a11 * dvphi + a12 * dvtheta + a13 * dvr;
          const auto vy = a21 * dvphi + a22 * dvtheta + a23 * dvr;
          const auto vz = a31 * dvphi + a32 * dvtheta + a33 * dvr;
          // const auto vx = -sin_theta * dvtheta + cos_phi * cos_theta * dvr;  //+ a11 * dvphi;
          // const auto vy = cos_theta * dvtheta + cos_phi * sin_theta * dvr;   //+ a21 * dvphi;
          // const auto vz = sin_phi * dvr;                                     //+ a31 * dvphi;

          assert(rho > 0);
          assert(p > 0);

          const auto v2 = vx * vx + vy * vy + vz * vz;
          const auto e  = .5 * rho * v2 + p / GAMMA_MINUS1;

          v_max = std::max(v_max, sqrt(v2));

          vector<double> fields(NF, 0);
          fields[0] = rho;
          fields[1] = vx * rho;
          fields[2] = vy * rho;
          fields[3] = vz * rho;
          fields[4] = e;

          p_min   = min(p_min, p);
          rho_min = min(rho_min, rho);

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 8 * fields[f] * wtg3d.at(q) * bfunc;
                }
            }
        }
    }

  for(uint64_t c = 0; c < Nc; c++)
    {
      int b = 0;
      int f = 0;
      mass_total += dg->w[w_idx_internal(b, f, c)] * dg->getCellVolume();

      f = NF - 1;
      e_total += dg->w[w_idx_internal(b, f, c)] * dg->getCellVolume();
    }
  MPI_Allreduce(MPI_IN_PLACE, &mass_total, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &e_total, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  MPI_Allreduce(MPI_IN_PLACE, &p_min, 1, MPI_DOUBLE, MPI_MIN, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &rho_min, 1, MPI_DOUBLE, MPI_MIN, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  mpi_printf("[mass_total] %e\n", mass_total);
  mpi_printf("[e_total] %e\n", e_total);
  mpi_printf("[u_min] %e\n", p_min);
  mpi_printf("[rho_min] %e\n", rho_min);
  mpi_printf("[cs0] %e\n", cs0);
  mpi_printf("[v_max] %e\n", v_max);
}

void dg_setup_test_problem_gravity_waves_boundary(dg_data_t *dg)
{
  double p_min   = std::numeric_limits<double>::infinity();  // minimum pressure
  double rho_min = std::numeric_limits<double>::infinity();  // minimum density

  double mass_total = 0;
  double e_total    = 0;
  // g = data[:, 0]  # gravitational acceleration
  // d = data[:, 5]  # density(rho)
  // d_p = data[:, 6]  # d(\rho)/dr
  // p = data[:, 7]  # pressure(p)
  // p_p = data[:, 8]  # dp/dr
  // s = data[:, 9]  # entropy
  // BV2 = data[:, 11]  # Brunt-Vaisala frequency
  // tcool = data[:, 12]  # cooling time
  // dlnlamdlnT = data[:, 13]  # dlog(\lamda)/dlog(T)
  // tff = data[:, 14]  # free-fall time
  // r = data[:, 15]  # radial grid
  constexpr int row         = 100000;
  constexpr int col         = 4;
  constexpr int col_perturb = 5;
  double input_data[row][col];
  double perturbation_data[row][col_perturb];

  // open IC file
  std::ifstream input_file("/freya/u/mihac/gravity_waves/gravity_wagves/background_flash.dat");
  if(!input_file.is_open())
    Terminate("Error opening file");

  for(int r = 0; r < row; r++)
    {
      for(int c = 0; c < col; c++)
        {
          input_file >> input_data[r][c];
        }
    }
  input_file.close();

  vector<double> r_arr(row, 0);
  vector<double> rho_arr(row, 0);
  vector<double> p_arr(row, 0);

  for(int r = 0; r < row; r++)
    {
      r_arr[r]   = input_data[r][0] * LENGTH_FACTOR;
      rho_arr[r] = input_data[r][1] * DENSITY_FACTOR;
      p_arr[r]   = input_data[r][2] * PRESSURE_FACTOR;
      // s_arr[r]   = input_data[r][3];
    }
  const auto R_MAX = r_arr[row - 1];

  // define equation to get r_arr index
  const double k  = r_arr[1] - r_arr[0];
  const double n0 = r_arr[0];

  // set box size
  dg->setBoxSize(R_MAX * 2 / sqrt(3) * .8);  // box fully embedded into the halo
  // dg->setBoxSize(R_MAX * 2 * 1.2);  // box = 2R * 1.1, halo fully embedded into the box, +10%
  // dg->setBoxSize(50);  // box = 2R * 1.1, halo fully embedded into the box, +10%

  std::ifstream input_file_perturbation("/freya/u/mihac/gravity_waves/gravity_wagves/eigenmode_flash.dat");
  if(!input_file_perturbation.is_open())
    Terminate("Error opening file");

  for(int r = 0; r < row; r++)
    {
      for(int c = 0; c < col_perturb; c++)
        {
          input_file_perturbation >> perturbation_data[r][c];
        }
    }
  input_file_perturbation.close();
  vector<double> rho_perturb_arr(row, 0);
  vector<double> p_perturb_arr(row, 0);
  vector<double> vr_perturb_arr(row, 0);
  vector<double> vtheta_perturb_arr(row, 0);
  for(int r = 0; r < row; r++)
    {
      rho_perturb_arr[r]    = perturbation_data[r][1];
      p_perturb_arr[r]      = perturbation_data[r][2];
      vr_perturb_arr[r]     = perturbation_data[r][3];
      vtheta_perturb_arr[r] = perturbation_data[r][4];
    }

  // initial conditions are projected at a higher spatial order
  constexpr size_t npoints   = DEGREE_K + 2;
  constexpr size_t npoints3d = intPower<ND>(npoints);
  vector<double> pos(npoints, 0);
  vector<double> wtg(npoints, 0);
  vector<double> wtg3d(npoints3d, 0);

  dg_gauss_get_points_weights(npoints, pos, wtg);
  dg_gauss_get_points_weights_internal(npoints, wtg, wtg3d, ND);

  const auto y0 = dg->getBoxSize() / 2;
  const auto x0 = dg->getBoxSize() / 2;
  const auto z0 = dg->getBoxSize() / 2;

  double rr_min = std::numeric_limits<double>::infinity();

  const auto cell_offset = getCellOffset();
  mpi_printf("r_min = %g    r_max = %g\n", r_arr[0], r_arr[row - 1]);

  const auto p_out   = p_arr[row - 1];
  const auto rho_out = rho_arr[row - 1];
  const auto p_in    = p_arr[0];
  const auto rho_in  = rho_arr[0];
  const auto cs0     = sqrt(p_in / rho_in);
  double v_max       = 0;

  for(uint64_t c = 0; c < Nc; c++)
    {
      // get coordinates of cell center
      const auto cell_idx    = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);
      const auto cell_center = dg_geometry_get_cell_center(cell_idx, dg->getCellSize());

      // only run this for cells on the boundary
      const auto x                      = cell_idx.x;
      const auto y                      = cell_idx.y;
      const auto z                      = cell_idx.z;
      const auto cell_on_outer_boundary = is_cell_on_outer_boundary(x + FirstSlabThisTask, y, z);
      if(!cell_on_outer_boundary)
        continue;

      // if(c % 1000 == 0)
      mpi_printf("setting fixed boundary for cell at x=%d y=%d z=%d\n", x, y, z);

      for(uint q = 0; q < npoints3d; q++)
        {
          // add qpoint coordinates
          const xyz qpoint_idx   = dg_geometry_idx_to_cell(q, npoints);
          const auto qpoint_coor = dg_geometry_get_qpoint_location(qpoint_idx, cell_center, pos, dg->getCellSize());

          const double xx = qpoint_coor.x - x0;
          const double yy = qpoint_coor.y - y0;
          const double zz = qpoint_coor.z - z0;
          const double rr = sqrt(xx * xx + yy * yy + zz * zz);

          assert(xx < dg->getBoxSize());
          assert(yy < dg->getBoxSize());
          assert(zz < dg->getBoxSize());

          if(rr < rr_min)
            rr_min = rr;

          // // project g onto the x-y plane
          const auto rr2d  = sqrt(xx * xx + yy * yy);
          const auto irr2d = 1. / rr2d;
          const auto irr   = 1. / rr;
          // const auto cos_theta = xx * irr2d;
          // const auto sin_theta = yy * irr2d;
          // const auto cos_phi   = zz * irr;
          // const auto sin_phi   = rr2d * irr;
          const double cos_theta = zz * irr;
          const double sin_theta = rr2d * irr;
          const double cos_phi   = xx * irr2d;
          const double sin_phi   = yy * irr2d;

          // conserved variables are to be interpolated from input files
          double rho_interpolated = 0, p_interpolated = 0;
          double rho_perturb = 0, p_perturb = 0;
          vector<double> velocity_perturb(2, 0);
          if(1)
            {
              for(int i = std::min((int)((rr - n0) / k), row - 2); i < row - 1; i++)
                {
                  // printf("old_i = %d\n", old_i);
                  if(rr > R_MAX)  // outside the halo
                    {
                      p_interpolated   = p_out;
                      rho_interpolated = rho_out;
                      break;
                    }
                  else if((rr > r_arr[i]) && (rr <= r_arr[i + 1]))  // inside the halo
                    {
                      const double fac    = (rr - r_arr[i]) / (r_arr[i + 1] - r_arr[i]);
                      p_interpolated      = p_arr[i + 1] * fac + (1 - fac) * p_arr[i];
                      rho_interpolated    = rho_arr[i + 1] * fac + (1 - fac) * rho_arr[i];
                      rho_perturb         = rho_perturb_arr[i + 1] * fac + (1 - fac) * rho_perturb_arr[i];
                      p_perturb           = p_perturb_arr[i + 1] * fac + (1 - fac) * p_perturb_arr[i];
                      velocity_perturb[0] = vr_perturb_arr[i + 1] * fac + (1 - fac) * vr_perturb_arr[i];
                      velocity_perturb[1] = vtheta_perturb_arr[i + 1] * fac + (1 - fac) * vtheta_perturb_arr[i];
                      break;
                    }
                  else if(rr < r_arr[0])
                    {
                      rho_interpolated = rho_in;
                      p_interpolated   = p_in;
                    }
                  else
                    {
                      if(i == row - 1)
                        Terminate("how did we end up here?\n c = %lu q = %d rr = %f\n", c, q, rr);
                    }
                }
            }

          const double sim_fluct_norm = .75;
          const auto Yml              = 0.25 * sqrt(5.0 / M_PI) * (3.0 * (cos_theta * cos_theta) - 1.0);
          const auto dYml_dtheta      = -1.5 * sqrt(5.0 / M_PI) * cos_theta * sin_theta;

          const auto rho = rho_interpolated * (1.0 + sim_fluct_norm * rho_perturb * Yml);
          const auto p   = p_interpolated * (1.0 + sim_fluct_norm * p_perturb * Yml);

          const auto dvr     = sim_fluct_norm * velocity_perturb[0] * cs0 * Yml;
          const auto dvtheta = sim_fluct_norm * velocity_perturb[1] * cs0 * dYml_dtheta;
          const auto dvphi   = sim_fluct_norm * 0;  //! dvtheta / sinTheta * dYml_dphi;

          // !projecting perturbations onto Cartesian grid
          // !source: https://www.mathworks.com/help/phased/ref/sph2cartvec.html
          const auto a11 = -sin_phi;
          const auto a12 = cos_theta * cos_phi;
          const auto a13 = sin_theta * cos_phi;
          const auto a21 = cos_phi;
          const auto a22 = cos_theta * sin_phi;
          const auto a23 = sin_theta * sin_phi;
          const auto a31 = 0;
          const auto a32 = -sin_theta;
          const auto a33 = cos_theta;

          const auto vx = a11 * dvphi + a12 * dvtheta + a13 * dvr;
          const auto vy = a21 * dvphi + a22 * dvtheta + a23 * dvr;
          const auto vz = a31 * dvphi + a32 * dvtheta + a33 * dvr;
          // const auto vx = -sin_theta * dvtheta + cos_phi * cos_theta * dvr;  //+ a11 * dvphi;
          // const auto vy = cos_theta * dvtheta + cos_phi * sin_theta * dvr;   //+ a21 * dvphi;
          // const auto vz = sin_phi * dvr;                                     //+ a31 * dvphi;

          assert(rho > 0);
          assert(p > 0);

          const auto v2 = vx * vx + vy * vy + vz * vz;
          const auto e  = .5 * rho * v2 + p / GAMMA_MINUS1;

          v_max = std::max(v_max, sqrt(v2));

          vector<double> fields(NF, 0);
          fields[0] = rho;
          fields[1] = vx * rho;
          fields[2] = vy * rho;
          fields[3] = vz * rho;
          fields[4] = e;

          p_min   = min(p_min, p);
          rho_min = min(rho_min, rho);

          for(int f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  const double bfunc = dg->dg_legendre_bfunc_eval(b, pos.at(qpoint_idx.x), pos.at(qpoint_idx.y), pos.at(qpoint_idx.z));
                  dg->w[w_idx_internal(b, f, c)] += 1. / 8 * fields[f] * wtg3d.at(q) * bfunc;
                }
            }
        }
    }

  for(uint64_t c = 0; c < Nc; c++)
    {
      int b = 0;
      int f = 0;
      mass_total += dg->w[w_idx_internal(b, f, c)] * dg->getCellVolume();

      f = NF - 1;
      e_total += dg->w[w_idx_internal(b, f, c)] * dg->getCellVolume();
    }
  MPI_Allreduce(MPI_IN_PLACE, &mass_total, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &e_total, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  MPI_Allreduce(MPI_IN_PLACE, &p_min, 1, MPI_DOUBLE, MPI_MIN, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &rho_min, 1, MPI_DOUBLE, MPI_MIN, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  mpi_printf("[mass_total] %e\n", mass_total);
  mpi_printf("[e_total] %e\n", e_total);
  mpi_printf("[u_min] %e\n", p_min);
  mpi_printf("[rho_min] %e\n", rho_min);
  mpi_printf("[cs0] %e\n", cs0);
  mpi_printf("[v_max] %e\n", v_max);
}

#endif  // GRAVITY_WAVES
