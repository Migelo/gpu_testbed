// Copyright 2020 Miha Cernetic
#ifndef SRC_TESTS_DG_GEOMETRY_HPP_
#define SRC_TESTS_DG_GEOMETRY_HPP_

#include "runrun.h"

#include <cassert>
#include <iostream>
#include <tuple>
#include <vector>

#ifndef GPU
#define __host__
#define __device__
#define __forceinline__ inline
#endif

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../data/allvars.h"
#include "../data/data_types.h"
#include "../data/dg_params.h"

#include <math.h>

using ::std::min;
using ::std::vector;

#if(ND == 1)
__host__ __device__ __forceinline__ int32_t dg_geometry_cell_to_idx(int32_t x, const int cells_per_side)
{
  assert(cells_per_side >= 0);

  if(x < 0)
    x = cells_per_side - 1;
  if(x >= cells_per_side)
    x = 0;

  return x;
}
#endif

#if(ND == 2)
__host__ __device__ __forceinline__ int32_t dg_geometry_cell_to_idx(int32_t x, int32_t y, const int cells_per_side)
{
  assert(cells_per_side >= 0);

  if(x < 0)
    x = cells_per_side - 1;
  if(x >= cells_per_side)
    x = 0;

  if(y < 0)
    y = cells_per_side - 1;
  if(y >= cells_per_side)
    y = 0;

  return x * cells_per_side + y;
}
#endif

#if(ND == 3)
__host__ __device__ __forceinline__ int32_t dg_geometry_cell_to_idx(int32_t x, int32_t y, int32_t z, const int cells_per_side)
{
  assert(cells_per_side >= 0);

  if(x < 0)
    x = cells_per_side - 1;
  if(x >= cells_per_side)
    x = 0;

  if(y < 0)
    y = cells_per_side - 1;
  if(y >= cells_per_side)
    y = 0;

  if(z < 0)
    z = cells_per_side - 1;
  if(z >= cells_per_side)
    z = 0;

  return x * cells_per_side * cells_per_side + y * cells_per_side + z;
}
#endif

__device__ __host__ __forceinline__ void reflect_state_if_needed(state& st, int axis)
{
#if defined(REFLECTIVE_X)
  if(axis == 0)
    st.velx = -st.velx;
#endif
#if defined(REFLECTIVE_Y)
  if(axis == 1)
    st.vely = -st.vely;
#endif
#if defined(REFLECTIVE_Z)
  if(axis == 2)
    st.velz = -st.velz;
#endif
}

__host__ __device__ __forceinline__ xyz dg_geometry_idx_to_cell(uint64_t idx, const int cells_per_side)
{
  xyz coords = {0, 0, 0};
#ifdef DEBUG
  assert(cells_per_side >= 0);
#endif

#if(ND == 1)
  coords.x = idx;
  coords.y = 0;
  coords.z = 0;
#endif
#if(ND == 2)
  coords.x = idx / cells_per_side;
  coords.y = idx % cells_per_side;
  coords.z = 0;
#endif
#if(ND == 3)
  coords.x = idx / (cells_per_side * cells_per_side);
  coords.y = (idx - coords.x * cells_per_side * cells_per_side) / cells_per_side;
  coords.z = idx - coords.x * cells_per_side * cells_per_side - coords.y * cells_per_side;
#endif

  return coords;
}

int32_t dg_geometry_first_cell_in_rank();

int32_t dg_geometry_last_cell_in_rank();

bool dg_geometry_is_cell_in_this_rank(const int32_t cell);

int32_t dg_geometry_global_cell_idx_to_local(const int32_t cell_global_idx);

void dg_geometry_idx_to_cell_test();

coords dg_geometry_get_cell_center(const xyz cell_idx, const double cell_size);

coords dg_geometry_get_qpoint_location(const xyz q_idx, const coords cell_center_coords, const vector<double>& q_locations,
                                       const double cell_size);

coords dg_geometry_coords_in_cell_units(const coords coordinates, double cellSize);

xyz dg_geometry_coords_to_xyz(const coords coordinates);

coords dg_geometry_get_local_qpoint_coord_from_global_qpoint_coord_and_cell_center(const coords coordinates_in_cell_units,
                                                                                   const xyz cell_center_idx);

uint64_t dg_geometry_cell_id_this_rank(const xyz cell_idx);

__device__ __host__ bool is_cell_on_outer_boundary(const int x, const int y, const int z);
__device__ __host__ bool is_cell_on_boundary_in_direction_of_current_axis(const int x, const int y, const int z, const int axis);
__device__ __host__ bool is_cell_on_boundary_in_direction_opposite_of_current_axis(const int x, const int y, const int z,
                                                                                   const int axis);

__host__ uint64_t calculate_cell_offset();

__device__ __host__ double calculate_qpoint_integration_factor(const double cellVolume);

#endif  // SRC_TESTS_DG_GEOMETRY_HPP_
