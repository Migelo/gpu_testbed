#include "compiler-command-line-args.h"
#include "runrun.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#ifdef GPU
#include <cuda_runtime.h>
// #include "nvToolsExt.h"
#endif

#include "../data/allvars.h"
#include "../data/macros.h"
#include "../gitversion/version.h"
#include "../main/main.h"

void hello(void)
{
  printf(
      "\n"
      "______           ______\n"
      "| ___ \\          | ___ \\\n"
      "| |_/ /   _ _ __ | |_/ /   _ _ __  \n"
      "|    / | | | '_ \\|    / | | | '_ \\\n"
      "| |\\ \\ |_| | | | | |\\ \\ |_| | | | |\n"
      "\\_| \\_\\__,_|_| |_\\_| \\_\\__,_|_| |_|\n\n");

  printf("This is RunRun.\nGit commit %s, %s\n\n", GIT_COMMIT, GIT_DATE);

  printf("Code was compiled with the following compiler and flags:\n%s\n\n\n", compiler_flags);

  printf("Code was compiled with the following settings:\n");

  output_compile_time_options();
}

void begrun(const char *parameterFile)
{
  All.register_parameters();

  int error_flag = All.read_parameter_file(parameterFile); /* ... read in parameters for this run on task 0*/

  if(error_flag)
    {
      mpi_printf("\nStopping because of errors in parameterfile.\n");
      MPI_Finalize();
      exit(0);
    }

  // All.write_used_parameters(All.OutputDir, "parameters-usedvalues");

  All.some_parameter_checks();

  // set_units();
}

void setup_parallel_environment(int argc, char **argv)
{
  /* initialize MPI, this may already impose some pinning */
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &World_ThisTask);
  MPI_Comm_size(MPI_COMM_WORLD, &World_NTask);

  ThisTask = World_ThisTask;  // temporary, for mpi_printf()

  if(World_ThisTask == 0)
    hello();

  // partitioning us into the shared memory nodes
  MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &SharedMemComm);
  MPI_Comm_rank(SharedMemComm, &Island_ThisTask);
  MPI_Comm_size(SharedMemComm, &Island_NTask);

  // find out how many GPUs we can access on each node
  int ngpus = 0;
#ifdef GPU
  if(Island_ThisTask == 0)
    cudaGetDeviceCount(&ngpus);
#endif

  MPI_Bcast(&ngpus, 1, MPI_INT, 0, SharedMemComm);

  if(Island_ThisTask == 0)
    {
      char name[MPI_MAX_PROCESSOR_NAME];
      int len;
      MPI_Get_processor_name(name, &len);

      printf("Node='%s' has  %d  GPU CUDA devices(s)  and %d  MPI-ranks\n", name, ngpus, Island_NTask);
      fflush(stdout);

      if(Island_NTask < ngpus)
        {
          printf("on node '%s', we have more GPUs than MPI ranks!\n", name);
          ngpus = Island_NTask;
        }
    }

  if(Island_ThisTask < ngpus)
    LocalDeviceRank = Island_ThisTask;

  if(Island_ThisTask != 0)
    ngpus = 0;
  MPI_Allreduce(&ngpus, &NtotGPUs, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  mpi_printf("We have in total %d GPUs and %d MPI ranks at our disposal.\n", NtotGPUs, World_NTask);

  if(argc < 2)
    {
      mpi_printf("\nParameterfile is missing.\n");
      mpi_printf("Start as ./run.run <ParameterFile>\n");
      mpi_printf("\n");

      MPI_Finalize();
      exit(0);
    }

  /* read parameterfile and set-up run */
  begrun(argv[1]);

  /* now assign the problem to the CPUs and GPUs (if present) */

  if(((World_NTask - NtotGPUs) * All.MinimumSlabsPerCPURank + NtotGPUs) > static_cast<int>(DG_NUM_CELLS_1D))
    Terminate(
        "We have too many MPI ranks and GPUs to satisfy  (World_NTask - NtotGPU) * All.MinimumSlabsPerCPURank + NtotGPUs <= "
        "DG_NUM_CELLS_1D\n");

  int slabs_to_distribute = DG_NUM_CELLS_1D;
  Nslabs                  = 0;

  if(LocalDeviceRank < 0)  // these MPI-ranks have no GPU
    Nslabs += All.MinimumSlabsPerCPURank;

  slabs_to_distribute -= (World_NTask - NtotGPUs) * All.MinimumSlabsPerCPURank;

  if(NtotGPUs > 0)
    {
      // assign the rest of the slabs to the available GPUs
      if(slabs_to_distribute < NtotGPUs)
        Terminate("strange, too many GPUs for remaining problem size");

      if(LocalDeviceRank >= 0)
        {
          GlobalDeviceRank = 0;
          Nslabs += (slabs_to_distribute / NtotGPUs);
        }

      slabs_to_distribute -= (slabs_to_distribute / NtotGPUs) * NtotGPUs;

      int *tmp_dvc = (int *)malloc(World_NTask * sizeof(int));
      MPI_Allgather(&LocalDeviceRank, 1, MPI_INT, tmp_dvc, 1, MPI_INT, MPI_COMM_WORLD);
      for(int i = 0; i < World_ThisTask; i++)
        if(LocalDeviceRank >= 0 && tmp_dvc[i] >= 0)
          GlobalDeviceRank++;
      free(tmp_dvc);

      if(GlobalDeviceRank >= 0 && GlobalDeviceRank < slabs_to_distribute)
        Nslabs++;
    }
  else
    {
      // here we have a pure CPU run
      Nslabs += (slabs_to_distribute / World_NTask);
      slabs_to_distribute -= (slabs_to_distribute / World_NTask) * World_NTask;

      if(World_ThisTask < slabs_to_distribute)
        Nslabs++;
    }

  /* check that we really assigned all slabs */
  int ntotslabs;
  MPI_Allreduce(&Nslabs, &ntotslabs, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  if(ntotslabs != DG_NUM_CELLS_1D)
    Terminate("something wrong in slab assignment: ntotslab=%d", ntotslabs);

  // we now split the communicator to get rid off MPI ranks without slabs, if such ranks are present
  int color = 0;
  if(Nslabs > 0)
    color = 1;
  MPI_Comm_split(MPI_COMM_WORLD, color, World_ThisTask, &MyComm);

  MPI_Comm_rank(MyComm, &ThisTask);
  MPI_Comm_size(MyComm, &NTask);
  // #ifdef GPU
  //   char name[256];
  //   sprintf(name, "MPI Rank %d", ThisTask);
  //   nvtxNameOsThread(pthread_self(), name);
  //   nvtxNameCudaDeviceA(ThisTask, name);
  // #endif  // GPU
  if(color == 1)
    {
      NslabsPerTask   = (int *)malloc(NTask * sizeof(int));
      FirstSlabOfTask = (int *)malloc(NTask * sizeof(int));
      DeviceIDList    = (int *)malloc(NTask * sizeof(int));

      MPI_Allgather(&Nslabs, 1, MPI_INT, NslabsPerTask, 1, MPI_INT, MyComm);
      MPI_Allgather(&LocalDeviceRank, 1, MPI_INT, DeviceIDList, 1, MPI_INT, MyComm);
      for(int i = 0; i < NTask; i++)
        FirstSlabOfTask[i] = (i > 0) ? FirstSlabOfTask[i - 1] + NslabsPerTask[i - 1] : 0;

      if(ThisTask == 0)
        {
          printf("\nWe will be using %d MPI ranks in total (out of %d that were used to launch the job)\n\n", NTask, World_NTask);
          for(int i = 0; i < NTask; i++)
            printf("MPI-Rank=%4d:   Slabs = %4d   GPU = %s\n", i, NslabsPerTask[i], DeviceIDList[i] >= 0 ? "yes" : "no");
          printf("\n");
        }

      FirstSlabThisTask = FirstSlabOfTask[ThisTask];

#if(ND == 1)
      Nc = Nslabs;
#endif

#if(ND == 2)
      Nc = Nslabs * DG_NUM_CELLS_1D;
#endif

#if(ND == 3)
      Nc = Nslabs * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D;
#endif

      if(LocalDeviceRank >= 0)  // we have a GPU
        {
          if((Nslabs & 1) > 0 || (DG_NUM_CELLS_1D & 1) > 0)
            Terminate("When GPUs are in use, current code requires that Nslabs as well as DG_NUM_CELLS_1D for GPUs are *even*\n");
        }
    }
  else
    {
      // MPI ranks arriving here are not needed and do nothing
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Finalize();
      exit(0);
    }
}
