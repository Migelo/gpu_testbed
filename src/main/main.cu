#include "runrun.h"

#include <assert.h>
#include <mpi.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <chrono>
#include <csignal>
#include <fstream>
#include <iostream>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../io/dg_io.hpp"
#include "../main/main.h"
#include "../tests/dg_setup_test_problems.hpp"
#include "../turbulence/dg_ab_turb.hpp"
#include "../turbulence/powerspectra.h"
#include "../turbulence/structure_function.hpp"
#include "../utils/cuda_utils.h"

using namespace std;
using namespace std::chrono;
typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::seconds s;
typedef std::chrono::duration<double> fsec;

int flag_restart     = 0;
int sigterm_received = 0;
void sigterm_handler(int signal)
{
  if((sigterm_received == 0) && (ThisTask == 0))
    {
      char buf[1000];
      sprintf(buf, "%s/stop", All.OutputDir);
      mpi_printf("SIGCONT received. Writing %s file to stop the code...\n", buf);
      std::ofstream output(buf);
      return;
      if(ThisTask == 0)
        sigterm_received = 1;
    }
}

int main(int argc, char **argv)
{
  signal(SIGTERM, sigterm_handler);
  signal(SIGCONT, sigterm_handler);

  All.Restart_bool = 0;
  if(argc > 2)
    All.Restart_bool = 1;

  auto execution_start_timestamp = Time::now();
  auto checkpoint_timer          = Time::now();
  setup_parallel_environment(argc, argv);

  mpi_printf("Initializing data\n");
  dg_data_t dg;

#ifdef GPU
  dg_data_setup_gpu_memory_usage(&dg);
#endif  // GPU

  // if(!All.Restart_bool)
  //   {
#if defined(ADVECTION_TEST)
  dg_setup_test_problem_advection(&dg);
#endif

#if defined(IMPLOSION_TEST)
  dg_setup_test_problem_implosion(&dg);
#endif

#if defined(SHU_OSHER_SHOCKTUBE)
  dg_setup_test_problem_implosion(&dg);
#endif

#if defined(SOUNDWAVE_TEST)
  dg_setup_test_problem_soundwave(&dg);
#endif

#if defined(KELVIN_HELMHOLTZ_TEST)  // Klevin-Helmholz test
  dg_setup_test_problem_kelvin_helmholtz(&dg);
#endif

#if defined(YEE_VORTEX)  // Yee-Vortex test
  dg_setup_test_problem_yee_vortex(&dg);
#endif

#if defined(VORTEX_SHEET_TEST)
  dg_setup_test_problem_vortex_sheet(&dg);
#endif

#if defined(GAUSSIAN_DIFFUSION_TEST)
  dg_setup_test_problem_gaussian_diffusion(&dg);
#endif

#if(defined(TURBULENCE) && !defined(COOLING))
  dg_setup_test_problem_turbulence(&dg);
#endif

#if defined(SHOCK_TUBE)
  dg_setup_test_problem_shock_tube(&dg);
#endif

#if defined(BOOSTED_SHOCK_TUBE)
  dg_setup_test_problem_shock_tube_boosted(&dg);
#endif

#if defined(DOUBLE_BLAST)
  dg_setup_test_problem_double_blast(&dg);
#endif

#if defined(SINGLE_SHOCK)
  dg_setup_test_problem_single_shock(&dg);
#endif

#ifdef SQUARE_ADVECTION
  dg_setup_test_problem_square_advection(&dg);
#endif

#if defined(POINT_EXPLOSION)
  dg_setup_test_problem_point_explosion(&dg);
#endif
#if defined(ISENTROPIC_VORTEX)
  dg_setup_test_problem_isentropic_vortex(&dg);
#endif  // ISENTROPIC_VORTEX
#if defined(GRAVITY_WAVES)
  dg_setup_test_problem_gravity_waves(&dg);
#endif  // GRAVITY_WAVES
#if defined(BENCHMARK)
  dg_setup_test_problem_constant_density_and_pressure(&dg);
#endif
#if defined(COOLING)
  dg_setup_test_problem_cooling(&dg);
#endif
  dg.next_dump = All.Tend / All.DesiredDumps;

#if defined(TURBULENCE)
  dg.next_powerspectra = (All.Tend - All.PowerspectraDelay) / All.DesiredPowerspectra + All.PowerspectraDelay;
  init_turb(&dg);  // init turbulence driving field
#endif
  // }

  // restarting
  if(argc > 2)
    {
      if(argc == 3)
        {
          mpi_printf("[RESTART] Starting the run from a restart file\n");
          mpi_printf("[RESTART] argc=%d, argv[2]=%s\n", argc, argv[2]);
          dg_io_read_restart(&dg);
        }
      else if(argc == 4)
        {
          mpi_printf("[RESTART] Starting the run from a checkpoint file\n");
          mpi_printf("[RESTART] argc=%d, argv[2]=%s argv[3]=%s\n", argc, argv[2], argv[3]);
          dg_io_read_checkpoint(&dg, atoi(argv[3]));
        }
    }

#ifdef GPU
  if(LocalDeviceRank >= 0)
    dg_data_setup_gpu(&dg);
#endif

  if(argc < 3)
    {
      mpi_printf("Saving initial field values...\n");
      dg.dg_output_highres_field_maps(All.PixelsFieldMaps, dg.dump);
#if defined(TURBULENCE) && !defined(BENCHMARK)
      structure_function<10'000> velocity_structure_function_10k(&dg);
      structure_function<100'000> velocity_structure_function_100k(&dg);
      structure_function<1'000'000> velocity_structure_function_1M(&dg);
#endif

#ifdef DATACUBE_OUTPUT
      dg.dg_output_data_cube(All.PixelsFieldCubes, dg.dump);
#endif  // DATACUBE_OUTPUT
      dg.dump++;
      mpi_printf("Finished saving initial field values.\n");
#ifdef ENABLE_DYE
      auto dye = get_dye_entropy(&dg);
      mpi_printf("[DYE] %g %g\n", dg.Time, dye);
#endif
    }

  bool we_are_done = false;
  bool do_dump     = false;
#ifdef DATACUBE_OUTPUT
  bool do_cube_dump = false;
#endif  // DATACUBE_OUTPUT

#ifdef TURBULENCE
  set_turb_ampl(&dg, 0);
#endif

  const auto timestamp_after_initialization = Time::now();
  auto initialization_duration              = duration_cast<seconds>(timestamp_after_initialization - execution_start_timestamp);
  mpi_printf("Setup step done in %lu s\n", initialization_duration);
  mpi_printf("Computing, entering the main loop...\n");
  for(; dg.Time <= All.Tend; dg.step++)
    {
      All.step = dg.step;

      do_dump = false;
#ifdef DATACUBE_OUTPUT
      do_cube_dump = false;
#endif
      dg.TimeStep = getMaxTistep(&dg);

      if(dg.TimeStep + dg.Time > All.Tend)
        {
          dg.TimeStep = All.Tend - dg.Time;
          we_are_done = true;
          do_dump     = true;
#ifdef DATACUBE_OUTPUT
          do_cube_dump = true;
#endif  // DATACUBE_OUTPUT
        }
#ifndef OUTPUT_EVERY_STEP
      if(dg.TimeStep + dg.Time >= dg.next_dump)
        {
          dg.TimeStep = dg.next_dump - dg.Time;
          do_dump     = true;
        }
#ifdef DATACUBE_OUTPUT
      if(dg.TimeStep + dg.Time >= dg.next_cube_dump)
        {
          dg.TimeStep  = dg.next_cube_dump - dg.Time;
          do_cube_dump = true;
        }
#endif  // DATACUBE_OUTPUT
#else
      do_dump = true;
#endif  // OUTPUT_EVERY_STEP
      mpi_printf("[TIMESTEP] max timestep set to %1.8g\n", dg.TimeStep);

#ifdef GPU
      if(ThisTask == 0)
        {
          size_t free_mem, total_mem;

          FILE *fdgpu = fopen("memory.txt", "a");
          fprintf(fdgpu, "%f", dg.Time);
          fprintf(fdgpu, " %lu\n", All.GpuMemoryAllocation / 1024 / 1024);
          fclose(fdgpu);
          cudaSetDevice(LocalDeviceRank);
          cudaMemGetInfo(&free_mem, &total_mem);
          gpu_printf("GPU memory before: %lu / %lu MB\n", (total_mem - free_mem) / 1024 / 1024, total_mem / 1024 / 1024);
        }
#endif  // GPU
      auto start = Time::now();

      bool flag_reduced = false;

      if(LocalDeviceRank >= 0)
        {
#ifdef GPU
          dg_advance_timestep_cuda(&dg, dg.TimeStep);
#endif
        }
      else
        {
          flag_reduced = dg_advance_timestep_serial(&dg, dg.TimeStep);
        }

#if defined(GRAVITY_WAVES)
      dg_setup_test_problem_gravity_waves_boundary(&dg);
#endif  // GRAVITY_WAVES

#ifdef TURBULENCE
      set_turb_ampl(&dg, dg.TimeStep);
#endif  // TURBULENCE

      dg.Time += dg.TimeStep;

      if(flag_reduced)
        dg.num_reduced_steps += 1;

      mpi_printf("Finished step:  %d  (reduced steps %d)  with timestep size %g  Reached TIME: %g\n", dg.step + 1,
                 dg.num_reduced_steps, dg.TimeStep, dg.Time);
      auto stop     = Time::now();
      auto duration = duration_cast<microseconds>(stop - start);
      mpi_printf("Step took: %lu μs\n", duration);

#ifdef TURBULENCE
      output_turbulence_log(&dg);
#endif  // TURBULENCE

      if(do_dump)
        {
#ifdef GPU
          if(LocalDeviceRank >= 0)
            {
              gpuErrchk(cudaMemcpy(dg.w, dg.d_w, dg.N_w * sizeof(double), cudaMemcpyDeviceToHost));
#ifdef ART_VISCOSITY
              gpuErrchk(cudaMemcpy(dg.smthness, dg.d_smthness, Nc * sizeof(double), cudaMemcpyDeviceToHost));
              gpuErrchk(cudaMemcpy(dg.smthrate, dg.d_smthrate, Nc * sizeof(double), cudaMemcpyDeviceToHost));
#endif
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
            }
#endif  // GPU
          dg.dg_output_highres_field_maps(All.PixelsFieldMaps, dg.dump);
#ifdef TURBULENCE
          structure_function<10'000> velocity_structure_function_10k(&dg);
          structure_function<100'000> velocity_structure_function_100k(&dg);
          structure_function<1'000'000> velocity_structure_function_1M(&dg);
#endif
#ifdef GPU
          dg_cuda_memory_writeout_allocations();
#endif  // GPU
#ifdef ENABLE_DYE
          auto dye = get_dye_entropy(&dg);
          mpi_printf("[DYE] %g %g\n", dg.Time, dye);
#endif
          dg.dump++;
          dg.next_dump += All.Tend / All.DesiredDumps;
        }
#ifdef DATACUBE_OUTPUT
      if(do_cube_dump)
        {
#ifdef GPU
          if(LocalDeviceRank >= 0)
            {
              gpuErrchk(cudaMemcpy(dg.w, dg.d_w, dg.N_w * sizeof(double), cudaMemcpyDeviceToHost));
#ifdef ART_VISCOSITY
              gpuErrchk(cudaMemcpy(dg.smthness, dg.d_smthness, Nc * sizeof(double), cudaMemcpyDeviceToHost));
              gpuErrchk(cudaMemcpy(dg.smthrate, dg.d_smthrate, Nc * sizeof(double), cudaMemcpyDeviceToHost));
#endif  // ART_VISCOSITY
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
            }
#endif  // GPU
          dg.dg_output_data_cube(All.PixelsFieldCubes, dg.dump_cube_count);
#ifdef GPU
          dg_cuda_memory_writeout_allocations();
#endif  // GPU
          dg.dump_cube_count++;
          dg.next_cube_dump += All.Tend / All.DesiredCubeDumps;
        }
#endif  // DATACUBE_OUTPUT

#ifdef TURBULENCE
      if(dg.Time >= dg.next_powerspectra)
        {
#ifdef GPU
          if(LocalDeviceRank >= 0)
            {
              gpuErrchk(cudaMemcpy(dg.w, dg.d_w, dg.N_w * sizeof(double), cudaMemcpyDeviceToHost));
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
            }
#endif  // GPU
          ps.powerspec_turb(&dg, dg.powerspectra_output_count);

          dg.powerspectra_output_count++;
          dg.next_powerspectra += (All.Tend - All.PowerspectraDelay) / All.DesiredPowerspectra;
        }
#endif  // TURBULENCE

      auto current_time           = Time::now();
      fsec fs                     = current_time - timestamp_after_initialization;
      s execution_time            = duration_cast<s>(fs);
      fsec fs_checkpoint          = current_time - checkpoint_timer;
      s execution_time_checkpoint = duration_cast<s>(fs_checkpoint);
      mpi_printf("current execution time: %d s\n", execution_time);
      if(dg.step > 0)
        mpi_printf("estimated time remaining: %.0f s\n", 1. * execution_time / dg.Time * (All.Tend - dg.Time));
      mpi_printf("time= %g next_dump time=%g dump=%d execution_time_checkpoint=%ld next powerspectra output at %g \n", dg.Time,
                 dg.next_dump, dg.dump, execution_time_checkpoint.count(), dg.next_powerspectra);

      // int flag_restart    = 0;
      int flag_checkpoint = 0;
      if(sigterm_received == 1)
        {
          char buf[1000];
          sprintf(buf, "%s/stop", All.OutputDir);
          std::ofstream output(buf);
          //          mpi_printf(
          //              "Detecting sigterm_flag to be set on rank 0, to avoid sync problems we do another step and only then write
          //              the restart" "files...\n");
          //          sigterm_received++;
        }
      else if(sigterm_received == 2)
        {
          flag_restart = 1;
        }

      if(execution_time.count() > All.WriteRestartAfter)
        flag_restart = 1;
      if(execution_time_checkpoint.count() > All.WriteCheckpointAfter)
        flag_checkpoint = 1;

      if(ThisTask == 0)
        {
          // check whether there is a stop file, and if yes, interrupt the run
          char buf[1000];
          sprintf(buf, "%s/stop", All.OutputDir);
          FILE *fd = fopen(buf, "r");
          if(fd)
            {
              mpi_printf("[STOP] found a stop file, removing it and setting flag_restart to 1");
              fclose(fd);
              remove(buf);
              flag_restart = 1;
            }
        }

      MPI_Bcast(&flag_restart, 1, MPI_INT, 0, MPI_COMM_WORLD);
      MPI_Bcast(&flag_checkpoint, 1, MPI_INT, 0, MPI_COMM_WORLD);

      if(flag_restart)
        {
#ifdef GPU
          if(LocalDeviceRank >= 0)
            {
              gpuErrchk(cudaMemcpy(dg.w, dg.d_w, dg.N_w * sizeof(double), cudaMemcpyDeviceToHost));
#ifdef ART_VISCOSITY
              gpuErrchk(cudaMemcpy(dg.smthness, dg.d_smthness, Nc * sizeof(double), cudaMemcpyDeviceToHost));
              gpuErrchk(cudaMemcpy(dg.smthrate, dg.d_smthrate, Nc * sizeof(double), cudaMemcpyDeviceToHost));
#endif
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
            }
#endif  // GPU
          MPI_Barrier(MPI_COMM_WORLD);

          // write restart files, one per rank
          mpi_printf("writing out restart files\n");
          dg_io_write_restart(&dg);
          if(ThisTask == 0)
            std::ofstream("./.restart");
          MPI_Barrier(MPI_COMM_WORLD);
          MPI_Finalize();
          return 0;
        }

      if(flag_checkpoint)
        {
          flag_checkpoint  = 0;
          checkpoint_timer = Time::now();
#ifdef GPU
          if(LocalDeviceRank >= 0)
            {
              gpuErrchk(cudaMemcpy(dg.w, dg.d_w, dg.N_w * sizeof(double), cudaMemcpyDeviceToHost));
#ifdef ART_VISCOSITY
              gpuErrchk(cudaMemcpy(dg.smthness, dg.d_smthness, Nc * sizeof(double), cudaMemcpyDeviceToHost));
              gpuErrchk(cudaMemcpy(dg.smthrate, dg.d_smthrate, Nc * sizeof(double), cudaMemcpyDeviceToHost));
#endif
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
            }
#endif  // GPU
          MPI_Barrier(MPI_COMM_WORLD);

          // write restart files, one per rank
          mpi_printf("writing out checkpoint files\n");
          dg_io_write_checkpoint(&dg);
          MPI_Barrier(MPI_COMM_WORLD);
        }
#ifdef BENCHMARK
      if(dg.step == 0)
        we_are_done = true;
#endif  // BENCHMARK
      if(we_are_done)
        break;
    }

  mpi_printf("Finished!\n");

  if(ThisTask == 0)
    {
#ifdef TURBULENCE
      fclose(dg.FdTurb);
#endif  // TURBULENCE
      std::remove("./.finished");
    }
#ifdef ISENTROPIC_VORTEX
  dg_L1_test_problem_isentropic_vortex(&dg);
#endif  // ISENTROPIC_VORTEX

  MPI_Barrier(MPI_COMM_WORLD);

  fsec fs            = Time::now() - timestamp_after_initialization;
  auto scaling_timer = duration_cast<milliseconds>(fs);
  mpi_printf("[SCALING_TIMER] finished %d timesteps in : %lu ms\n", dg.step, scaling_timer);

  MPI_Finalize();
  return 0;
}
