// Copyright 2021 Volker Springel
#ifndef SRC_MAIN_MAIN_H_
#define SRC_MAIN_MAIN_H_

#include "runrun.h"

#include "../data/dg_data.hpp"

void hello(void);
void output_compile_time_options(void);
void begrun(const char *parameterFile);
void setup_parallel_environment(int argc, char **argv);

void kevin_helmholtz_test(dg_data_t &dg);
void advection_test(dg_data_t &dg);
void turbulence(dg_data_t &dg);
void shock_tube(dg_data_t &dg);

#endif  // SRC_MAIN_MAIN_H_
