// Copyright 2021 Miha Cernetic

#ifndef SRC_IO_DG_IO_HPP_
#define SRC_IO_DG_IO_HPP_

#include <string>

#include "../data/dg_data.hpp"
#include "../data/dg_params.h"

template <typename T>
void myfwriteread(T *argument, size_t n_elements, FILE *fd, int mode)
{
  if((mode == 0) || (mode == 2))
    fwrite(argument, n_elements, sizeof(T), fd);
  else if((mode == 1) || (mode == 3))
    fread(argument, n_elements, sizeof(T), fd);
  else
    {
      mpi_printf("mode is 0 for writing and 1 for reading");
      assert(0);
    }
}

void dg_output_turbulence_driving_data(dg_data_t *dg, int snapnum);
string dg_data_io_datetime_string();
void dg_io_restart(dg_data_t *dg, int mode, char *buf);
void dg_io_write_restart(dg_data_t *dg);
void dg_io_read_restart(dg_data_t *dg);
void dg_io_write_checkpoint(dg_data_t *dg);
void dg_io_read_checkpoint(dg_data_t *dg, int rotation);

// void dg_data_io_create_outputdir(dg_data_t *dg);

#endif  // SRC_IO_DG_IO_HPP_
