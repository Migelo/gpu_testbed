// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../io/dg_io.hpp"
#include "../legendre/dg_legendre.hpp"
#include "../tests/dg_setup_test_problems.hpp"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"

using ::std::ios_base;
using ::std::ofstream;
using ::std::string;
using ::std::stringstream;
using ::std::to_string;
using ::std::vector;

using namespace std;
using namespace std::chrono;
typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::seconds s;
typedef std::chrono::duration<double> fsec;

void dg_data_t::dg_output_highres_field_maps(const int pix, const int snapnum)
{
  // in 3D, we're outputting effectively the z=0 plane, which always lies on task=0 in full

#ifdef USE_PRIMITIVE_PROJECTION
  vector<double> w_prim(this->N_w, 0);
  project_to_primitives_cpu(this, w, w_prim.data());
#endif

#if defined(MAPS_AT_CORNERS)
  const double pixref = 0.0;
#elif defined(MAPS_AT_FAR_CORNERS)
  const double pixref = 1.0;
#else
  const double pixref = 0.5;
#endif

#if(ND == 1)
  int num_pix = pix;
#endif
#if(ND >= 2)
  int num_pix = pix * pix;
#endif

#ifdef GRAVITY_WAVES
  FILE *fdg = NULL;
  // FILE *fdgravity = NULL;

  char bufg[1000];
  double *radius;
  radius = (double *)malloc(num_pix * sizeof(double));
  memset(radius, 0, num_pix * sizeof(double));
  // char bufgravity[1000];
  // double *gravity;
  // gravity = (double *)malloc(3 * num_pix * sizeof(double));
  // memset(gravity, 0, 3 * num_pix * sizeof(double));

  if(snapnum == 0)
    {
      sprintf(bufg, "%s/radius_%04d.dat", All.OutputDir, snapnum);
      // sprintf(bufgravity, "%s/gravity_%04d.dat", All.OutputDir, snapnum);

      // mpi_printf("writing radius to '%s'\n", bufg);

      if(ThisTask == 0)
        {
          fdg = fopen(bufg, "w");
          if(!fdg)
            Terminate("can't open file '%s'\n", bufg);
          // fdgravity = fopen(bufgravity, "w");
          // if(!fdgravity)
          //   Terminate("can't open file '%s'\n", bufgravity);
        }
    }
#endif  // GRAVITY_WAVES

#ifdef USE_PRIMITIVE_PROJECTION
  int lastfield = 2 * NF;
#else
  int lastfield = NF;
#endif

  for(decltype(ND) f = 0; f < lastfield; f++)
    {
      char buf[1000];
      sprintf(buf, "%s/field_%lu_%04d.dat", All.OutputDir, f, snapnum);

      int fac = 1;
      if(ND == 3)
        fac = 3;
      mpi_printf("writing %d MB field %d to '%s'\n", (int)(fac * num_pix * sizeof(double) / 1e6), f, buf);

      FILE *fd = NULL;

      if(ThisTask == 0)
        {
          fd = fopen(buf, "w");
          if(!fd)
            {
              Terminate("can't open file '%s'\n", buf);
            }

          fwrite(&pix, 1, sizeof(int), fd);
          fwrite(&Time, 1, sizeof(double), fd);
        }

      double *map  = (double *)malloc(num_pix * sizeof(double));
      double *smth = (double *)malloc(num_pix * sizeof(double));
      double *rate = (double *)malloc(num_pix * sizeof(double));

#if(ND == 1)
      int xc_old = -1;
#endif

#if(ND == 3)
      for(int dir = 0; dir < 3; dir++)
#endif
        {
          memset(map, 0, num_pix * sizeof(double));
          memset(smth, 0, num_pix * sizeof(double));

          for(int x = 0; x < pix; x++)
#if(ND >= 2)
            for(int y = 0; y < pix; y++)
#endif
              {
                double xx              = (x + pixref) / pix * getBoxSize();
                double x_in_cell_units = xx / getCellSize();
                int xc                 = (int)x_in_cell_units;
                double xrel            = 2.0 * (x_in_cell_units - xc) - 1.0;

#if(ND >= 2)
                double yy              = (y + pixref) / pix * getBoxSize();
                double y_in_cell_units = yy / getCellSize();
                int yc                 = (int)y_in_cell_units;
                double yrel            = 2.0 * (y_in_cell_units - yc) - 1.0;
#endif
#if(ND == 3)
                // in 3D, we use the mid plane in z
                int zc      = DG_NUM_CELLS_1D / 2 - 1;
                double zrel = 1.0;

                if(dir == 1)
                  {
                    // swap xc,xrel and zc,zrel
                    int tmp       = xc;
                    xc            = zc;
                    zc            = tmp;
                    double tmprel = xrel;
                    xrel          = zrel;
                    zrel          = tmprel;
                  }
                else if(dir == 2)
                  {
                    // swap yc,yrel and zc,zrel
                    int tmp       = yc;
                    yc            = zc;
                    zc            = tmp;
                    double tmprel = yrel;
                    yrel          = zrel;
                    zrel          = tmprel;
                  }
#endif

                if(xc >= FirstSlabThisTask && xc < FirstSlabThisTask + Nslabs)
                  {
#if(ND == 1)
                    uint64_t cell = (xc - FirstSlabThisTask);
#endif
#if(ND == 2)
                    uint64_t cell = DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc;
#endif
#if(ND == 3)
                    uint64_t cell = (DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc) * DG_NUM_CELLS_1D + zc;
#endif

                    double val = 0;

                    for(decltype(ND) b = 0; b < NB; b++)
                      {
                        if(f < NF)
                          {
#if(ND == 1)
                            val += w[w_idx_internal(b, f, cell)] * dg_legendre_bfunc_eval(b, xrel);
#endif
#if(ND == 2)
                            val += w[w_idx_internal(b, f, cell)] * dg_legendre_bfunc_eval(b, xrel, yrel);
#endif
#if(ND == 3)
                            val += w[w_idx_internal(b, f, cell)] * dg_legendre_bfunc_eval(b, xrel, yrel, zrel);
#endif
                          }
#ifdef USE_PRIMITIVE_PROJECTION
                        else
                          {
#if(ND == 1)
                            val += w_prim[w_idx_internal(b, f - NF, cell)] * dg_legendre_bfunc_eval(b, xrel);
#endif
#if(ND == 2)
                            val += w_prim[w_idx_internal(b, f - NF, cell)] * dg_legendre_bfunc_eval(b, xrel, yrel);
#endif
#if(ND == 3)
                            val += w_prim[w_idx_internal(b, f - NF, cell)] * dg_legendre_bfunc_eval(b, xrel, yrel, zrel);
#endif
                          }
#endif
                      }
#if(ND >= 2)
                    map[x * pix + y] = val;
#else
                map[x] = val;
#endif

#ifdef GRAVITY_WAVES
                    if((f == 0) && (dir == 0) && (snapnum == 0))
                      {
                        const double xxr = (x + pixref) / pix * getBoxSize() - 0.5 * getBoxSize();
                        const double yyr = (y + pixref) / pix * getBoxSize() - 0.5 * getBoxSize();

                        const double zzr = (zc + 1.) * getCellSize() - 0.5 * getBoxSize();
                        const double rr  = sqrt(xxr * xxr + yyr * yyr + zzr * zzr);

                        radius[x * pix + y] = rr;

                        // // constants in cgs
                        // constexpr auto G0 = 4.3279e-8 * LENGTH_FACTOR / TIME_FACTOR / TIME_FACTOR;
                        // constexpr auto RS = 50;

                        // const double radial_g = G0 * tanh(rr / RS);

                        // // project g onto the x-y plane
                        // const double rr2d    = sqrt(xxr * xxr + yyr * yyr);
                        // const double irr2d   = 1. / rr2d;
                        // const auto irr       = 1. / rr;
                        // const auto cos_theta = xxr * irr2d;
                        // const auto sin_theta = yyr * irr2d;
                        // const auto cos_phi   = zzr * irr;
                        // const auto sin_phi   = rr2d * irr;

                        // // compute force density at the desired point
                        // const auto x_g = radial_g * cos_theta * sin_phi;
                        // const auto y_g = radial_g * sin_theta * sin_phi;
                        // const auto z_g = radial_g * cos_phi;

                        // auto fx = -x_g;
                        // auto fy = -y_g;
                        // auto fz = -z_g;

                        // gravity[0 * pix * pix + x * pix + y] = fx;
                        // gravity[1 * pix * pix + x * pix + y] = fy;
                        // gravity[2 * pix * pix + x * pix + y] = radial_g;
                      }
#endif  // GRAVITY_WAVE

#if(ND == 1)
                    if(NTask == 1)
                      {
                        if(xc == xc_old)
                          {
                            smth[x] = smth[x - 1];
                            rate[x] = rate[x - 1];
                          }
                        else
                          {
#ifdef ART_VISCOSITY
                            smth[x] = smthness[xc];
                            rate[x] = smthrate[xc];
#endif
                            xc_old = xc;
                          }
                      }
#endif
                  }
              }

          MPI_Allreduce(MPI_IN_PLACE, map, num_pix, MPI_DOUBLE, MPI_SUM, MyComm);

          if(ThisTask == 0)
            {
              fwrite(map, num_pix, sizeof(double), fd);
#if(ND == 1)
              //            fwrite(smth, num_pix, sizeof(double), fd);
              //            fwrite(rate, num_pix, sizeof(double), fd);
#endif
            }
        }

      free(rate);
      free(smth);
      free(map);

#if defined(POINT_EXPLOSION) && defined(ART_VISCOSITY)
      if(f == 0)
        {
          double *smthness = (double *)malloc(Nc * sizeof(double));

          for(uint64_t c = 0; c < Nc; c++)  // loop over local cell
            smthness[c] = dg_compute_smoothness_indicator(this->w, c);

          int *list = (int *)malloc(NTask * sizeof(int));
          MPI_Allgather(&Nc, 1, MPI_INT, list, 1, MPI_INT, MyComm);

          int *off = (int *)malloc(NTask * sizeof(int));
          off[0]   = 0;
          for(int i = 1; i < NTask; i++)
            off[i] = off[i - 1] + list[i - 1];

          double *smth_all = (double *)malloc(NC * sizeof(double));

          MPI_Allgatherv(smthness, Nc, MPI_DOUBLE, smth_all, list, off, MPI_DOUBLE, MyComm);

          if(ThisTask == 0)
            {
              int n = NC;
              fwrite(&n, 1, sizeof(int), fd);
              fwrite(smth_all, NC, sizeof(double), fd);
            }

          free(smth_all);
          free(off);
          free(list);
          free(smthness);
        }
#endif

      if(ThisTask == 0)
        fclose(fd);
    }

#if defined(SOUNDWAVE_TEST)
  double L1 = dg_L1_test_problem_soundwave(this, Time);
#endif
#if defined(VORTEX_SHEET_TEST)
  double L1 = dg_L1_test_problem_vortex_sheet(this, Time);
#endif
#if defined(GAUSSIAN_DIFFUSION_TEST)
  double L1 = dg_L1_test_problem_gaussian_diffusion(this, Time);
#endif
#if defined(YEE_VORTEX)
  double L1 = dg_L1_test_problem_yee_vortex(this, Time);
#endif
#if defined(SQUARE_ADVECTION)
  double L1 = dg_L1_test_problem_square_advection(this, Time);
#endif

#if defined(SOUNDWAVE_TEST) || defined(VORTEX_SHEET_TEST) || defined(YEE_VORTEX) || defined(GAUSSIAN_DIFFUSION_TEST) || \
    defined(SQUARE_ADVECTION)
  if(ThisTask == 0)
    {
      fprintf(FdL1, "%g  %g\n", Time, L1);
      fflush(FdL1);
    }
#endif

#ifdef GRAVITY_WAVES
  if(snapnum == 0)
    {
      MPI_Allreduce(MPI_IN_PLACE, radius, num_pix, MPI_DOUBLE, MPI_SUM, MyComm);
      // MPI_Allreduce(MPI_IN_PLACE, gravity, 3 * num_pix, MPI_DOUBLE, MPI_SUM, MyComm);
    }
  if((snapnum == 0) && (ThisTask == 0))
    {
      fwrite(radius, num_pix, sizeof(double), fdg);
      free(radius);
      fclose(fdg);

      // fwrite(gravity, 3 * num_pix, sizeof(double), fdgravity);
      // free(gravity);
      // fclose(fdgravity);
    }
#endif  // GRAVITY_WAVE
}

#ifdef THREEDIMS
#ifdef DATACUBE_OUTPUT
void dg_data_t::dg_output_data_cube(int64_t pix, int snapnum)
{
  static_assert(ND == 3);

  constexpr double pixref = 0.5;

  const auto num_pix             = pix * pix * pix;
  const auto num_pix_per_rank    = pix * pix * (pix / NTask);
  const auto first_pix_this_rank = num_pix_per_rank * ThisTask;

  mpi_printf("num_pix=%ld\nnum_pix_per_rank=%ld\nfirst_pix_this_rank=%ld\n", num_pix, num_pix_per_rank, first_pix_this_rank);

  if((snapnum == 0) && (ThisTask == 0))
    {
      double *map = (double *)malloc(pix * sizeof(double));
      memset(map, 0, pix * sizeof(double));

      char buf[1000];
      sprintf(buf, "%s/x.dat", All.OutputDir);
      mpi_printf("writing a %d kB of x coords to '%s'\n", (int)(pix * sizeof(double) / 1e3), buf);

      FILE *fd = NULL;

      fd = fopen(buf, "w");
      if(!fd)
        Terminate("can't open file '%s'\n", buf);

      for(int64_t x = 0; x < pix; x++)
        {
          const double xx = (x + pixref) / pix * getBoxSize() - getBoxSize() * .5;

          map[x] = xx;
        }

      fwrite(map, pix, sizeof(double), fd);
      fclose(fd);
      free(map);
    }

  MPI_Barrier(MPI_COMM_WORLD);

  vector<double> map;
  map.resize(num_pix_per_rank);

#ifdef GRAVITY_WAVES
  // r cube output
  {
    char buf[1000];
    FILE *fd = NULL;
    sprintf(buf, "%s/r.dat", All.OutputDir);

    mpi_printf("writing a %d GB cube r to '%s'\n", (int)(num_pix * sizeof(double) / 1e9), buf);

    if(ThisTask == 0)
      {
        fd = fopen(buf, "w");
        if(!fd)
          Terminate("can't open file '%s'\n", buf);
      }

    {
      int64_t idx = 0;
      // for(int64_t x = first_pix_this_rank; x < first_pix_this_rank + (pix / NTask); x++)
      for(int64_t x = pix / NTask * ThisTask; x < pix / NTask * ThisTask + (pix / NTask); x++)
        for(int64_t y = 0; y < pix; y++)
          for(int64_t z = 0; z < pix; z++)
            {
              const double xx = (x + pixref) / pix * getBoxSize() - getBoxSize() * .5;
              const double yy = (y + pixref) / pix * getBoxSize() - getBoxSize() * .5;
              const double zz = (z + pixref) / pix * getBoxSize() - getBoxSize() * .5;

              map.at(idx++) = sqrt(xx * xx + yy * yy + zz * zz);
            }
    }
    // rank 0 writes out its data
    mpi_printf("Rank 0 wiriting out its map\n");
    if(ThisTask == 0)
      fwrite(map.data(), num_pix_per_rank, sizeof(double), fd);

    // rank 0 receives data from the other ranks and writes it out, one by one
    if(ThisTask > 0)
      {
        printf("Rank %d starting a Ssend\n", ThisTask);
        MPI_Ssend(map.data(), num_pix_per_rank, MPI_DOUBLE, 0, 1234, MyComm);
      }
    if(ThisTask == 0)
      {
        for(int i = 1; i < NTask; i++)
          {
            printf("Rank %d starting a Recv from rank %d\n", ThisTask, i);
            MPI_Recv(map.data(), num_pix_per_rank, MPI_DOUBLE, i, 1234, MyComm, MPI_STATUS_IGNORE);
            // printf("first 3 elements of the new map are: %f %f %f\n", map[0], map[1], map[2]);
            fwrite(map.data(), num_pix_per_rank, sizeof(double), fd);
          }
      }
    if(ThisTask == 0)
      fclose(fd);
  }

  mpi_printf("finished writing out r cube\n");
  MPI_Barrier(MyComm);
#endif  // GRAVITY_WAVES

  // data cube output
  for(decltype(ND) f = 0; f < NF; f++)
    {
      char buf[1000];
      FILE *fd = NULL;
      sprintf(buf, "%s/field_cube_%lu_%04d.dat", All.OutputDir, f, snapnum);

      mpi_printf("writing a %d GB cube of field %d to '%s'\n", (int)(num_pix * sizeof(double) / 1e9), f, buf);

      if(ThisTask == 0)
        {
          fd = fopen(buf, "w");
          if(!fd)
            Terminate("can't open file '%s'\n", buf);

          fwrite(&pix, 1, sizeof(int), fd);
          fwrite(&Time, 1, sizeof(double), fd);
        }

      {
        int64_t idx = 0;
        for(int64_t x = pix / NTask * ThisTask; x < pix / NTask * ThisTask + (pix / NTask); x++)
          for(int64_t y = 0; y < pix; y++)
            for(int64_t z = 0; z < pix; z++)
              {
                double xx              = (x + pixref) / pix * getBoxSize();
                double x_in_cell_units = xx / getCellSize();
                int64_t xc             = (int64_t)x_in_cell_units;
                double xrel            = 2.0 * (x_in_cell_units - xc) - 1.0;

                double yy              = (y + pixref) / pix * getBoxSize();
                double y_in_cell_units = yy / getCellSize();
                int64_t yc             = (int64_t)y_in_cell_units;
                double yrel            = 2.0 * (y_in_cell_units - yc) - 1.0;

                double zz              = (z + pixref) / pix * getBoxSize();
                double z_in_cell_units = zz / getCellSize();
                int64_t zc             = (int64_t)z_in_cell_units;
                double zrel            = 2.0 * (z_in_cell_units - zc) - 1.0;

                int64_t cell = (DG_NUM_CELLS_1D * (xc - FirstSlabThisTask) + yc) * DG_NUM_CELLS_1D + zc;

                double val = 0;

                for(decltype(ND) b = 0; b < NB; b++)
                  val += w[w_idx_internal(b, f, cell)] * dg_legendre_bfunc_eval(b, xrel, yrel, zrel);

                map.at(idx++) = val;
              }

        // rank 0 writes out its data
        mpi_printf("Rank 0 wiriting out its map\n");
        if(ThisTask == 0)
          fwrite(map.data(), num_pix_per_rank, sizeof(double), fd);

        // rank 0 receives data from the other ranks and writes it out, one by one
        if(ThisTask > 0)
          {
            printf("Rank %d starting a Ssend\n", ThisTask);
            printf("first 3 elements of the new map  of rank %d are: %f %f %f\n", ThisTask, map[0], map[1], map[2]);
            MPI_Ssend(map.data(), num_pix_per_rank, MPI_DOUBLE, 0, 1234, MyComm);
          }
        if(ThisTask == 0)
          {
            for(int i = 1; i < NTask; i++)
              {
                printf("Rank %d starting a Recv from rank %d\n", ThisTask, i);
                MPI_Recv(map.data(), num_pix_per_rank, MPI_DOUBLE, i, 1234, MyComm, MPI_STATUS_IGNORE);
                // printf("first 3 elements of the new map are: %f %f %f\n", map[0], map[1], map[2]);
                fwrite(map.data(), num_pix_per_rank, sizeof(double), fd);
              }
          }
        MPI_Barrier(MyComm);
        if(ThisTask == 0)
          fclose(fd);
      }
    }
  MPI_Barrier(MyComm);
}
#endif  // DATACUBE_OUTPUT
#endif  // THREEDIMS

#ifdef TURBULENCE
void dg_output_turbulence_driving_data(dg_data_t *dg, int snapnum)
{
  /*
  static int nc = Nc;

  char buf[1000];
  sprintf(buf, "%s/turbstat_%04d.dat", All.OutputDir, snapnum);

  cout << "writing turublence driving statistics to: " << buf << endl;

  double *map = (double *)malloc(Nc * sizeof(double));

  for(int c = 0; c < Nc; c++)
    {
      map[c] = dg->EgyDrive[c];
    }

  FILE *fd = fopen(buf, "w");
  if(!fd)
    {
      Terminate("can't open file '%s'\n", buf);
    }

  fwrite(&nc, 1, sizeof(int), fd);

  fwrite(map, nc, sizeof(double), fd);
  fclose(fd);

  free(map);
  */
}

string dg_data_io_datetime_string() { return to_string(std::time(0)); }

// void dg_data_io_create_outputdir(dg_data_t *dg)
// {
//   // set output folder string
//   string output_dir = "output/";
//   output_dir.append(dg_data_io_datetime_string());
//   output_dir.append("/");
//   dg->output_dir = output_dir;
//   cout << "OUTPUT DIR: " << dg->output_dir << endl;

//   // create the output directory
//   namespace fs = std::filesystem;
//   fs::create_directories(dg->output_dir);
// }
#endif  // TURBULENCE

void dg_io_restart(dg_data_t *dg, int mode, char *buf)
{
  const auto start = Time::now();
  mpi_printf("[RESTART] Starting to write restart/checkpoint files RotationID=%d\n", dg->RotationID);

  /*
    mode: 0 - writing restart files
          1 - reading restart files
          2 - writing checkpoint
          3 - reading checkpoint
  */

  // make sure the restart folder exists

  char dirname[1000];
  sprintf(dirname, "%s/restart", All.OutputDir);

  struct stat st = {0};
  if(stat(dirname, &st) == -1)
    {
      mkdir(dirname, 0755);
      printf("creating folder %s\n", dirname);
    }

  dg->step++;

  // write out parameters for all processes
  FILE *fd;
  if((mode == 0) || (mode == 2))
    fd = fopen(buf, "wb");
  else if((mode == 1) || (mode == 3))
    fd = fopen(buf, "rb");
  else
    assert(0);

  if(!fd)
    {
      Terminate("can't open file '%s'\n", buf);
    }
  else
    {
      if(mode == 0)
        printf("Task %d writing to restart file '%s'\n", ThisTask, buf);
      else if(mode == 1)
        printf("Task %d reading from restart file '%s'\n", ThisTask, buf);
    }
  myfwriteread(&dg->N_w, 1, fd, mode);
  myfwriteread(&dg->scale_factor, 1, fd, mode);
  myfwriteread(&dg->Time, 1, fd, mode);
  myfwriteread(&dg->dump, 1, fd, mode);
#ifdef DATACUBE_OUTPUT
  myfwriteread(&dg->dump_cube_count, 1, fd, mode);
  myfwriteread(&dg->next_cube_dump, 1, fd, mode);
#endif  // DATACUBE_OUTPUT
  myfwriteread(&dg->next_dump, 1, fd, mode);
  myfwriteread(&dg->step, 1, fd, mode);
  myfwriteread(&dg->RotationID, 1, fd, mode);
  myfwriteread(&dg->LimiterInjectedEnergy, 1, fd, mode);
  myfwriteread(&dg->LimiterInjectedMass, 1, fd, mode);
  myfwriteread(dg->w, dg->N_w, fd, mode);

#ifdef ART_VISCOSITY
  myfwriteread(dg->smthness, Nc, fd, mode);
  myfwriteread(dg->smthrate, Nc, fd, mode);
#endif

#ifdef TURBULENCE
  if((mode == 0) || (mode == 2))
    gsl_rng_fwrite(fd, dg->StRng);
  else
    gsl_rng_fread(fd, dg->StRng);

  myfwriteread(&dg->next_powerspectra, 1, fd, mode);
  myfwriteread(&dg->powerspectra_output_count, 1, fd, mode);
  myfwriteread(&dg->StOUVar, 1, fd, mode);
  myfwriteread(&dg->StNModes, 1, fd, mode);
  myfwriteread(&dg->RefDensity, 1, fd, mode);
  myfwriteread(&dg->TurbInjectedEnergy, 1, fd, mode);
  myfwriteread(&dg->TurbDissipatedEnergy, 1, fd, mode);
  myfwriteread(&dg->TimeBetTurbSpectrum, 1, fd, mode);
  myfwriteread(&dg->TimeNextTurbSpectrum, 1, fd, mode);
  myfwriteread(&dg->StSolWeightNorm, 1, fd, mode);
  myfwriteread(&dg->StTimeSinceLastUpdate, 1, fd, mode);
  myfwriteread(dg->StAmpl, dg->StNModes, fd, mode);
  myfwriteread(dg->StAka, 3 * dg->StNModes, fd, mode);
  myfwriteread(dg->StAkb, 3 * dg->StNModes, fd, mode);
  myfwriteread(dg->StMode, 3 * dg->StNModes, fd, mode);
  myfwriteread(dg->StOUPhases, 6 * dg->StNModes, fd, mode);

#ifdef AB_TURB_DECAYING
  myfwriteread(&dg->TimeTurbDecay, 1, fd, mode);
#endif  // AB_TURB_DECAYING
#endif  // TURBULENCE

  fclose(fd);

#ifdef TURBULENCE
#ifdef GPU
  if((mode == 1) || (mode == 3))
    {
      gpuErrchk(cudaMemcpy(dg->d_StAmpl, dg->StAmpl, dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StAka, dg->StAka, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StAkb, dg->StAkb, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
      gpuErrchk(cudaMemcpy(dg->d_StMode, dg->StMode, 3 * dg->StNModes * sizeof(double), cudaMemcpyHostToDevice));
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
    }
#endif  // GPU
#endif  // TURBULENCE

  if(mode > 1)
    dg->step--;

  const auto stop = Time::now();
  auto duration   = duration_cast<seconds>(stop - start);
  mpi_printf("[RESTART] Writing restart/checkpoint files took: %lu s\n", duration);
}

void dg_io_write_restart(dg_data_t *dg)
{
  char buf[1000];
  sprintf(buf, "%s/restart/restart.%05d", All.OutputDir, ThisTask);
  dg_io_restart(dg, 0, buf);
}

void dg_io_read_restart(dg_data_t *dg)
{
  char buf[1000];
  sprintf(buf, "%s/restart/restart.%05d", All.OutputDir, ThisTask);
  dg_io_restart(dg, 1, buf);
}

void dg_io_write_checkpoint(dg_data_t *dg)
{
  char buf[1000];
  sprintf(buf, "%s/restart/checkpoint.rotation%1d.task%05d", All.OutputDir, dg->RotationID, ThisTask);
  if(dg->RotationID)
    dg->RotationID = 0;
  else
    dg->RotationID = 1;
  dg_io_restart(dg, 2, buf);
}

void dg_io_read_checkpoint(dg_data_t *dg, int rotation)
{
  char buf[1000];
  sprintf(buf, "%s/restart/checkpoint.rotation%1d.task%05d", All.OutputDir, rotation, ThisTask);
  dg_io_restart(dg, 1, buf);
}
