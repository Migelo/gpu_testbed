#ifndef UNITS_H_
#define UNITS_H_

// in cgs
constexpr inline double KPC             = 3.086e21;
constexpr inline double LENGTH_FACTOR   = 1. / KPC;
constexpr inline double VOLUME_FACTOR   = 1 / LENGTH_FACTOR / LENGTH_FACTOR / LENGTH_FACTOR;
constexpr inline double MSOL            = 1.98847e33;
constexpr inline double MASS_FACTOR     = 1 / (1e10 * MSOL);
constexpr inline double TIME_FACTOR     = 1 / (KPC / 1e5);  // t = (KPC / km) seconds ~ 10^8 years  (1e5 km/s)
constexpr inline double DENSITY_FACTOR  = MASS_FACTOR / LENGTH_FACTOR / LENGTH_FACTOR / LENGTH_FACTOR;
constexpr inline double PRESSURE_FACTOR = MASS_FACTOR / LENGTH_FACTOR / TIME_FACTOR / TIME_FACTOR;
constexpr inline double SPEED_FACTOR    = LENGTH_FACTOR / TIME_FACTOR;
constexpr inline double ENERGY_FACTOR   = MASS_FACTOR * SPEED_FACTOR * SPEED_FACTOR;
#ifdef GRAVITY_WAVES
// constants in cgs
constexpr auto G0 = 4.3279e-8 * LENGTH_FACTOR / TIME_FACTOR / TIME_FACTOR;
constexpr auto RS = 50;

#endif  // GRAVITY_WAVES

#endif  // UNITS_H_
