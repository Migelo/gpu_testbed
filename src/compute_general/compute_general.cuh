#pragma once

#include "runrun.h"

#include "../data/dg_data.hpp"

__device__ __host__ void zero_out_higher_orders_of_field_in_cell(double* weights, const uint field, const uint cell,
                                                                 const visc_params* ViscParam);
__device__ __host__ void zero_out_higher_orders_in_cell(double* weights, const uint cell, const visc_params* ViscParam);
__device__ __host__ void get_average_fields_of_cell(double* average_fields, const double* weights, const uint cell);
__device__ __host__ double calculate_velocity_squared_from_fields(const double* fields);
__device__ __host__ double calculate_momenta_squared_from_fields(const double* fields);
__device__ __host__ double calculate_pressure_from_fields(double* fields, const double* weights);
__device__ __host__ double calculate_energy_from_fields_and_IsoSoundSpeed(const double* fields, const dg_data_for_gpu dggpu);
__device__ __host__ state convert_state_from_code_to_physical_units(const state& state_code_units);
__host__ uint64_t getCellOffset();
double getMaxTistep(dg_data_t* dg);
__device__ __host__ bool check_cell_for_trouble(double* qpoint_weights_positivity, double* w, uint64_t cell,
                                                const visc_params* ViscParam);
__device__ __host__ void project_cell_to_primitives(const uint64_t cell, const double* w, double* w_prim, const double* bfunc_internal,
                                                    const double* qpoint_weights_internal, const visc_params* ViscParam);
__device__ __host__ void project_single_cell_to_primitives(uint64_t cell, double* w, double* w_prim, double* bfunc_internal,
                                                           double* qpoint_weights_internal, const visc_params* ViscParam);
