#include "runrun.h"

#include "./compute_general.cuh"

#include <fenv.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/time.h>

#include <cassert>
#include <iostream>
#include <list>
#include <string>
#include <vector>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../riemann/riemann_hllc.hpp"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"

__device__ __host__ void get_average_fields_of_cell(double *average_fields, const double *weights, const uint cell)
{
  for(uint field = 0; field < NF; field++)
    {
      average_fields[field] = weights[w_idx_internal(0, field, cell)];
    }
}

__device__ __host__ double calculate_velocity_squared_from_fields(const double *fields)
{
  const double irho2 = 1.0 / fields[0] / fields[0];
#if(ND == 1)
  const double v2 = irho2 * fields[1] * fields[1];
#endif
#if(ND == 2)
  const double v2 = irho2 * (fields[1] * fields[1] + fields[2] * fields[2]);
#endif
#if(ND == 3)
  const double v2 = irho2 * (fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3]);
#endif
  return v2;
}

__device__ __host__ double calculate_momenta_squared_from_fields(const double *fields)
{
#if(ND == 1)
  const double p2 = fields[1] * fields[1];
#endif
#if(ND == 2)
  const double p2 = fields[1] * fields[1] + fields[2] * fields[2];
#endif
#if(ND == 3)
  const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];
#endif
  return p2;
}

#ifndef ISOTHERM_EQS
__device__ __host__ double calculate_pressure_from_fields(double *fields, const double *weights)
{
  const auto rho  = fields[0];
  const auto irho = 1.0 / rho;
  const auto e    = fields[ND + 1];

  const auto p2     = calculate_momenta_squared_from_fields(fields);
  const double ptot = (e - 0.5 * p2 * irho) * GAMMA_MINUS1;

  return ptot;
}
#endif

__device__ __host__ void zero_out_higher_orders_of_field_in_cell(double *weights, const uint field, const uint cell,
                                                                 const visc_params *ViscParam)
{
  for(uint b = 1; b < NB; b++)
    {
      weights[w_idx_internal(b, field, cell)] = 0;
    }
}

__device__ __host__ void zero_out_higher_orders_in_cell(double *weights, const uint cell, const visc_params *ViscParam)
{
  for(uint field = 0; field < NF; field++)
    {
      zero_out_higher_orders_of_field_in_cell(weights, field, cell, ViscParam);
    }
#ifdef DENSITY_FLOOR
  if(weights[w_idx_internal(0, 0, cell)] < ViscParam->DensityFloor)
    {
      weights[w_idx_internal(0, 0, cell)] = 2.0 * ViscParam->DensityFloor;
    }
#endif
}

#ifndef ISOTHERM_EQS
__device__ double calculate_energy_from_fields_and_IsoSoundSpeed(const double *fields, const dg_data_for_gpu dggpu)
{
  const auto rho  = fields[0];
  const auto irho = 1.0 / rho;
  const auto p2   = calculate_momenta_squared_from_fields(fields);
#ifdef TURBULENCE
  const auto etot = 0.5 * p2 * irho + rho * dggpu.ViscParam.IsoSoundSpeed * dggpu.ViscParam.IsoSoundSpeed / GAMMA_MINUS1;
#else
  const auto etot = 0.5 * p2 * irho + rho * 1 * 1 / GAMMA_MINUS1;

#endif  // TURBULENCE
  return etot;
}
#endif

__device__ __host__ state convert_state_from_code_to_physical_units(const state &state_code_units)
{
  state state_physical_units;

  state_physical_units.rho   = state_code_units.rho / DENSITY_FACTOR;
  state_physical_units.press = state_code_units.press / PRESSURE_FACTOR;
  state_physical_units.velx  = state_code_units.velx / SPEED_FACTOR;
#if(ND > 1)
  state_physical_units.vely = state_code_units.vely / SPEED_FACTOR;
#endif
#if(ND > 2)
  state_physical_units.velz = state_code_units.velz / SPEED_FACTOR;
#endif

  return state_physical_units;
}

double getMaxTistep(dg_data_t *dg)
{
  double timeStep;
#ifdef GPU
  if(LocalDeviceRank >= 0)
    {
      timeStep = getMaxTistepGpu(dg);
    }
  else
    {
      timeStep = getMaxTistepCpu(dg);
    }
#else
  timeStep = getMaxTistepCpu(dg);
#endif  // GPU

#ifdef TURBULENCE
  timeStep = fmin(timeStep, All.StDtFreq);
#endif  // TURBULENCE

  return timeStep;
}

__host__ uint64_t getCellOffset() { return FirstSlabThisTask * intPower<ND - 1>(DG_NUM_CELLS_1D); }

__device__ __host__ bool check_cell_for_trouble(double *qpoint_weights_positivity, double *w, uint64_t cell,
                                                const visc_params *ViscParam)
{
  bool trouble = false;

  // check all needed Gauss points (including the points on the outside)

  for(int q = 0; q < DG_NUM_POSITIVE_POINTS; q++)
    {
      double fields[NF] = {0};
      for(int f = 0; f < NF; f++)
        for(int b = 0; b < NB; b++)
          fields[f] += w[w_idx_internal(b, f, cell)] * qpoint_weights_positivity[q * NB + b];

      state st;
      get_primitive_state_from_conserved_fields(fields, st, ViscParam);

#ifdef DENSITY_FLOOR
      if(st.rho < ViscParam->DensityFloor || st.press < 0)
        trouble = true;
#else
      if(st.rho < 0 || st.press < 0)
        trouble = true;
#endif
      if(trouble)
        {
          break;
        }
    }

  return trouble;
}

__device__ __host__ void project_cell_to_primitives(const uint64_t cell, const double *w, double *w_prim, const double *bfunc_internal,
                                                    const double *qpoint_weights_internal, const visc_params *ViscParam)
{
  constexpr double fac = powConstexpr(0.5, ND);

  for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
    {
      double fields[NF];

      get_conserved_fields_from_weights(w, bfunc_internal, fields, cell, q);

      state st;
      get_primitive_state_from_conserved_fields(fields, st, ViscParam);

      for(int f = 0; f < NF; f++)
        {
          for(int b = 0; b < NB; b++)
            {
              double temp = fac * st.s[f] * bfunc_internal[bfunc_internal_idx(q, b)] * qpoint_weights_internal[q];
              w_prim[w_idx_internal(b, f, cell)] += temp;
            }
        }
    }
}

__device__ __host__ void project_single_cell_to_primitives(uint64_t cell, double *w, double *w_prim, double *bfunc_internal,
                                                           double *qpoint_weights_internal, const visc_params *ViscParam)
{
  constexpr double fac = powConstexpr(0.5, ND);

  for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
    {
      double fields[NF];

      get_conserved_fields_from_weights(w, bfunc_internal, fields, cell, q);

      state st;
      get_primitive_state_from_conserved_fields(fields, st, ViscParam);

      for(int f = 0; f < NF; f++)
        {
          for(int b = 0; b < NB; b++)
            {
              w_prim[b + f * NB] += fac * st.s[f] * bfunc_internal[bfunc_internal_idx(q, b)] * qpoint_weights_internal[q];
            }
        }
    }
}
