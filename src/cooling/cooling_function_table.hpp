#ifndef COOLING_HPP
#define COOLING_HPP

#include "runrun.h"

#ifdef COOLING

#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../data/allvars.h"
#include "../data/data_types.h"
#include "../data/dg_params.h"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"

using ::std::abs;
using ::std::array;
using ::std::cout;
using ::std::endl;
using ::std::string;
using ::std::vector;

using vector2d = vector<vector<double>>;
using vector3d = vector<vector<std::vector<double>>>;

constexpr double ATOMIC_MASS_UNIT       = 1.660539040e-24;
constexpr double HYDROGEN_MASS_FRACTION = 0.762;
constexpr double BOLTZMANN_CONSTANT     = 1.38064852e-16;
constexpr double TEMPERATURE_FLOOR      = 1e4;
constexpr double PROTON_MASS            = 1.6726219e-24;
constexpr double MEAN_PARTICLE_COUNT    = 0.58;

constexpr double ATOMIC_MASS_UNIT_CODE_UNITS   = ATOMIC_MASS_UNIT * MASS_FACTOR;
constexpr double BOLTZMANN_CONSTANT_CODE_UNITS = BOLTZMANN_CONSTANT * ENERGY_FACTOR;

template <typename T>
vector<T> read_n_elements_from_binary_into_vector(const string fpath, const size_t n)
{
  // n has to be larger than 0
  assert(n > 0);

  // Open the binary file for reading
  std::ifstream file(fpath.c_str(), std::ios::binary);

  if(!file)
    {
      mpi_printf("[read_n_elements_from_binary_into_vector] Failed to open %s\n", fpath.c_str());
    }
  mpi_printf("[read_n_elements_from_binary_into_vector] File %s opened successfully, ", fpath.c_str());

  // Determine the file size
  file.seekg(0, std::ios::end);
  std::streampos fileSize = file.tellg();
  file.seekg(0, std::ios::beg);

  // print fileSize
  mpi_printf("read_n_elements_from_binary_into_vector file size: %ld bytes\n", fileSize);

  // Calculate the number of doubles in the file
  size_t numElements = fileSize / sizeof(T);

  // Assert number of elements is correct
  assert(numElements == n);

  // Print the file size and the number of doubles using printf
  mpi_printf("[read_n_elements_from_binary_into_vector] size: %ld bytes, number of elements: %ld\n", fileSize, numElements);

  // Create a vector to store the double values
  vector<T> data(numElements);

  // Read the doubles from the file into the vector
  file.read(reinterpret_cast<char *>(data.data()), fileSize);

  // Close the file
  file.close();

  return data;
}

class CoolingFunctionTable
{
 private:
  vector2d cooling_function_table;
  vector<double> temperature_bins{};
  vector<double> density_bins{};

 public:
  CoolingFunctionTable() = default;

  void load_bins(const string fpath, const size_t bins_count, vector<double> &bins)
  {
    const auto bins_data = read_n_elements_from_binary_into_vector<double>(fpath, bins_count);

    assert(bins_data.size() == bins_count);

    bins.resize(bins_count);

    bins.assign(bins_data.begin(), bins_data.end());
  }

  void load_temperature_bins(const string fpath, const size_t bins_count) { load_bins(fpath, bins_count, temperature_bins); }

  void load_density_bins(const string fpath, const size_t bins_count) { load_bins(fpath, bins_count, density_bins); }

  void load_file_into_table(const string fpath, vector2d &table)
  {
    const auto file_content =
        read_n_elements_from_binary_into_vector<double>(fpath, get_density_bins_count() * get_temperature_bins_count());

    table.resize(get_density_bins_count());
    for(size_t rho_idx = 0; rho_idx < get_density_bins_count(); rho_idx++)
      {
        table[rho_idx].resize(get_temperature_bins_count());
        for(size_t tem_idx = 0; tem_idx < get_temperature_bins_count(); tem_idx++)
          {
            table.at(rho_idx).at(tem_idx) = file_content.at(tem_idx * get_density_bins_count() + rho_idx);
          }
      }
  }

  void load_cooling_function_table(const string fpath) { load_file_into_table(fpath, cooling_function_table); }

  size_t get_density_bins_count() const { return density_bins.size(); }
  size_t get_temperature_bins_count() const { return temperature_bins.size(); }

  vector<double> get_density_bins() const { return density_bins; }
  vector<double> get_temperature_bins() const { return temperature_bins; }
  vector2d get_cooling_function_table() const { return cooling_function_table; }

  size_t pick_bin(double value, vector<double> bins) const
  {
    return std::distance(bins.begin(), std::lower_bound(bins.begin(), bins.end(), value));
  }
  size_t pick_density_bin(const double rho) const { return pick_bin(rho, density_bins); }
  size_t pick_temperature_bin(const double temperature) const { return pick_bin(temperature, temperature_bins); }

  double interpolate_table_at_temperature(const vector2d table, const double temperature, const size_t tem_idx,
                                          const size_t rho_idx) const
  {
    const auto left_idx        = tem_idx - 1;
    const auto right_idx       = tem_idx;
    const auto temperature_min = temperature_bins.at(left_idx);
    const auto temperature_max = temperature_bins.at(right_idx);

    const auto cooling_function_at_temperature_min = table.at(rho_idx).at(left_idx);
    const auto cooling_function_at_temperature_max = table.at(rho_idx).at(right_idx);

    const auto temperature_bin_width            = abs(temperature_max - temperature_min);
    const auto temperature_diff_to_min          = abs(temperature_min - temperature);
    const auto temperature_interpolation_factor = temperature_diff_to_min / temperature_bin_width;
    const auto interpolated_cooling_function    = cooling_function_at_temperature_min * (1 - temperature_interpolation_factor) +
                                               cooling_function_at_temperature_max * (temperature_interpolation_factor);
    return interpolated_cooling_function;
  }

  double get_table_at_temperature_and_density(const vector2d table, const double temperature_input, const double rho_input) const
  {
    const auto rho_min         = density_bins.front();
    const auto rho_max         = density_bins.back();
    const auto temperature_min = temperature_bins.front();
    const auto temperature_max = temperature_bins.back();

    const auto temperature = log10(temperature_input);
    const auto rho         = log10(rho_input / ATOMIC_MASS_UNIT);

    assert(rho >= rho_min && rho <= rho_max);
    assert(temperature >= temperature_min && temperature <= temperature_max);

    const size_t rho_idx = pick_density_bin(rho);
    const size_t tem_idx = pick_temperature_bin(temperature);

    const auto cooling_table_value = interpolate_table_at_temperature(table, temperature, tem_idx, rho_idx);

    return cooling_table_value;
  }

  double get_cooling_table_at_temperature_and_density(const double temperature, const double rho) const
  {
    return get_table_at_temperature_and_density(cooling_function_table, temperature, rho);
  }

  double calculate_temperature_from_pressure_and_density(const double pressure, const double density) const
  {
    return 2 * MEAN_PARTICLE_COUNT * PROTON_MASS * pressure / (3 * BOLTZMANN_CONSTANT * density * GAMMA_MINUS1);
  }

  double calculate_energy_from_temperature_and_density(const double temperature, const double density) const
  {
    return 3 * BOLTZMANN_CONSTANT * density * temperature / (2 * MEAN_PARTICLE_COUNT * PROTON_MASS);
  }

  double implicit_solver_function(const double energy_n, const double density, const double time_step, const double energy_n1) const
  {
    const double pressure_n1           = GAMMA_MINUS1 * energy_n1;
    const auto hydrogen_number_density = density / PROTON_MASS;

    const double temperature           = calculate_temperature_from_pressure_and_density(pressure_n1, density);
    const double physical_cooling_rate = get_cooling_table_at_temperature_and_density(temperature, density) * hydrogen_number_density *
                                         hydrogen_number_density;  // in erg cm^-3 s^-1
    const auto to_return = energy_n1 - energy_n + physical_cooling_rate * time_step;

    return to_return;
  }

  void init(const string cooling_function_table_fpath, const size_t temperature_bins_count, const string temperature_bins_fpath,
            const size_t density_bins_count, const string density_bins_fpath)
  {
    cout << "Initializing cooling function table..." << endl;
    load_temperature_bins(temperature_bins_fpath, temperature_bins_count);
    load_density_bins(density_bins_fpath, density_bins_count);

    load_cooling_function_table(cooling_function_table_fpath);
  }
};

#endif

#endif  // COOLING_HPP
