#pragma once
#include "runrun.h"

#ifdef COOLING

#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../data/data_types.h"
#include "../data/dg_params.h"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"
#include "cooling_function_table.hpp"

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#define __constant__
#endif

__constant__ extern double d_cooling_function_table[176];
__constant__ extern double d_cooling_function_temperature_bins[176];

using ::std::vector;
using vector2d = vector<vector<double>>;
using vector3d = vector<vector<std::vector<double>>>;

class CoolingFunctionTableGpu
{
 public:
  CoolingFunctionTableGpu() = default;

  double* temperature_bins;
  double* cooling_function_table;

  static constexpr size_t temperature_bins_count = 176;
  static constexpr size_t density_bins_count     = 41;

  size_t get_temperature_bins_count() const { return temperature_bins_count; }
  size_t get_density_bins_count() const { return density_bins_count; }

  __device__ size_t guess_temp_bin_index(double temp) const
  {
    size_t index = fmax(0.0, temp - 2.0) / 0.039758321511653705;
    // restrict index to be within the bounds of the table
    if(index >= temperature_bins_count)
      {
        index = temperature_bins_count - 1;
      }
    return index;
  }

  //   make a getter for temperature_bins and cooling_function_table
  double* get_temperature_bins() { return temperature_bins; }
  double* get_cooling_function_table() { return cooling_function_table; }

  __device__ size_t pick_temperature_bin(const double temperature) const
  {
    for(size_t i = guess_temp_bin_index(temperature); i < temperature_bins_count; i++)
      {
        if(d_cooling_function_temperature_bins[i] > temperature)
          {
            return i;
          }
      }
    // Terminate("ERROR: temperature is greater than the last value in the table");
    assert(0);
    // return temperature_bins_count - 1;
  }

  __device__ double interpolate_table_at_temperature(const double temperature, const size_t tem_idx) const
  {
    const auto left_idx        = tem_idx - 1;
    const auto right_idx       = tem_idx;
    const auto temperature_min = d_cooling_function_temperature_bins[left_idx];
    const auto temperature_max = d_cooling_function_temperature_bins[right_idx];

    const auto cooling_function_at_temperature_min = d_cooling_function_table[left_idx];
    const auto cooling_function_at_temperature_max = d_cooling_function_table[right_idx];

    const auto temperature_bin_width            = fabs(temperature_max - temperature_min);
    const auto temperature_diff_to_min          = fabs(temperature_min - temperature);
    const auto temperature_interpolation_factor = temperature_diff_to_min / temperature_bin_width;
    const auto interpolated_cooling_function    = cooling_function_at_temperature_min * (1 - temperature_interpolation_factor) +
                                               cooling_function_at_temperature_max * (temperature_interpolation_factor);
    return interpolated_cooling_function;
  }

  __device__ double get_table_at_temperature(const double* table, const double temperature_input) const
  {
    const auto temperature_min = d_cooling_function_temperature_bins[0];
    const auto temperature_max = d_cooling_function_temperature_bins[temperature_bins_count - 1];

    const auto temperature = log10(temperature_input);

    assert(temperature >= temperature_min && temperature <= temperature_max);

    const size_t tem_idx = pick_temperature_bin(temperature);

    return interpolate_table_at_temperature(temperature, tem_idx);
  }

  __device__ double get_cooling_table_at_temperature(const double temperature) const
  {
    return get_table_at_temperature(d_cooling_function_table, temperature);
  }

  __device__ double calculate_temperature_from_pressure_and_density(const double pressure, const double density) const
  {
    return 2 * MEAN_PARTICLE_COUNT * PROTON_MASS * pressure / (3 * BOLTZMANN_CONSTANT * density * GAMMA_MINUS1);
  }

  __device__ double calculate_energy_from_temperature_and_density(const double temperature, const double density) const
  {
    return 3 * BOLTZMANN_CONSTANT * density * temperature / (2 * MEAN_PARTICLE_COUNT * PROTON_MASS);
  }

  __device__ double brents_fun(double lower, double upper, double tol, unsigned int max_iter, double current_energy, double density,
                               double time_step_physical_units) const
  {
    double a = lower;
    double b = upper;

    double fa = implicit_solver_function(current_energy, density, time_step_physical_units, a);
    double fb = implicit_solver_function(current_energy, density, time_step_physical_units, b);
    double fs = 0;  // initialize
    // cout << "[BRENTS_FUN] upper bound: " << upper << endl;
    // cout << "[BRENTS_FUN] lower bound: " << lower << endl;

    // dbl_compare_t<double> dbl_compare;
    // dbl_compare_init_num_digits(&dbl_compare, 10);

    if(!(fa * fb < 0))
      {
        // cout << "Signs of f(lower_bound) and f(upper_bound) must be opposites, returning -11"
        //      << endl;  // throws exception if root isn't bracketed
        return -11;
      }

    if(fabs(fa) < fabs(b))
      {
        double temp = a;
        a           = b;
        b           = temp;

        temp = fa;
        fa   = fb;
        fb   = temp;
      }

    double c   = a;      // c now equals the largest magnitude of the lower and upper bounds
    double fc  = fa;     // precompute function evalutation for point c by assigning it the same value as fa
    bool mflag = true;   // boolean flag used to evaluate if statement later on
    double s   = lower;  // Our Root that will be returned
    double d   = 0;      // Only used if mflag is unset (mflag == false)

    for(unsigned int iter = 1; iter < max_iter; ++iter)
      {
        // stop if converged on root or error is less than tolerance
        if(fabs(b - a) < tol)
          {
            // cout << "[BRENTS_FUN] After " << iter << " iterations the root is: " << s << endl;
            // cout << "[BRENTS_FUN] upper bound: " << upper << endl;
            // cout << "[BRENTS_FUN] lower bound: " << lower << endl;

            return s;
          }

        if(fa != fc && fb != fc)
          {
            // use inverse quadratic interopolation
            s = (a * fb * fc / ((fa - fb) * (fa - fc))) + (b * fa * fc / ((fb - fa) * (fb - fc))) +
                (c * fa * fb / ((fc - fa) * (fc - fb)));
          }
        else
          {
            // secant method
            s = b - fb * (b - a) / (fb - fa);
          }

        // checks to see whether we can use the faster converging quadratic && secant methods or if we need to use bisection
        if(((s < (3 * a + b) * 0.25) || (s > b)) || (mflag && (fabs(s - b) >= (fabs(b - c) * 0.5))) ||
           (!mflag && (fabs(s - b) >= (fabs(c - d) * 0.5))) || (mflag && (fabs(b - c) < tol)) || (!mflag && (fabs(c - d) < tol)))
          {
            // bisection method
            s = (a + b) * 0.5;

            mflag = true;
          }
        else
          {
            mflag = false;
          }

        fs = implicit_solver_function(current_energy, density, time_step_physical_units, s);  // calculate fs
        d  = c;   // first time d is being used (wasnt used on first iteration because mflag was set)
        c  = b;   // set c equal to upper bound
        fc = fb;  // set f(c) = f(b)

        if(fa * fs < 0)  // fa and fs have opposite signs
          {
            b  = s;
            fb = fs;  // set f(b) = f(s)
          }
        else
          {
            a  = s;
            fa = fs;  // set f(a) = f(s)
          }

        if(fabs(fa) < fabs(fb))  // if magnitude of fa is less than magnitude of fb
          {
            double temp = a;
            a           = b;
            b           = temp;

            temp = fa;
            fa   = fb;
            fb   = temp;
          }

      }  // end for

    // printf("The solution does not converge or iterations are not sufficient\n");
    return -1;
  }  // end brents_fun

  __device__ double implicit_solver_function(const double energy_n, const double density, const double time_step,
                                             const double energy_n1) const
  {
    const double pressure_n1           = GAMMA_MINUS1 * energy_n1;
    const auto hydrogen_number_density = density / PROTON_MASS;

    const double temperature = calculate_temperature_from_pressure_and_density(pressure_n1, density);
    const double physical_cooling_rate =
        get_cooling_table_at_temperature(temperature) * hydrogen_number_density * hydrogen_number_density;  // in erg cm^-3 s^-1
    const auto to_return = energy_n1 - energy_n + physical_cooling_rate * time_step;

    return to_return;
  }

  void init() {}

  void set_cooling_function_table(double* cooling_function_table_input) { cooling_function_table = cooling_function_table_input; }

  void set_temperature_bins(double* cooling_function_temperature_bins_input)
  {
    temperature_bins = cooling_function_temperature_bins_input;
  }
};

class CoolingFunctionTableGpuData : public CoolingFunctionTableGpu
{
 public:
  double cooling_function_table[temperature_bins_count];
  double temperature_bins[temperature_bins_count];

  CoolingFunctionTableGpuData() = default;

  void print_temperature_bins() const
  {
    printf("Printing temperature bins...\n");
    for(size_t i = 0; i < temperature_bins_count; i++)
      {
        printf("temperature_bins[%lu]: %e\n", i, temperature_bins[i]);
      }
  }

  void print_cooling_function_table() const
  {
    printf("Printing cooling function table...\n");
    for(size_t i = 0; i < temperature_bins_count; i++)
      {
        printf("[print_cooling_function_table] cooling_function_table[%lu]: %e\n", i, cooling_function_table[i]);
      }
  }

  void load_temperature_bins(const string fpath, const size_t bins_count)
  {
    const auto bins_data = read_n_elements_from_binary_into_vector<double>(fpath, bins_count);

    assert(bins_data.size() == bins_count);

    for(size_t i = 0; i < bins_count; i++)
      {
        temperature_bins[i] = bins_data[i];
      }
  }

  void load_cooling_function_table(const string fpath)
  {
    const auto file_content = read_n_elements_from_binary_into_vector<double>(fpath, 41 * 176);
    assert(file_content.size() == 41 * 176);

    constexpr size_t rho_idx = 40;
    for(size_t tem_idx = 0; tem_idx < 176; tem_idx++)
      {
        cooling_function_table[tem_idx] = file_content.at(tem_idx * 41 + rho_idx);
      }
  }

  void init(const string cooling_function_table_fpath, const string temperature_bins_fpath)
  {
    printf("Initializing cooling function table...\n");
    load_temperature_bins(temperature_bins_fpath, temperature_bins_count);

    load_cooling_function_table(cooling_function_table_fpath);
  }
};

#endif
