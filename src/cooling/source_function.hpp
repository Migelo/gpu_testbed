#pragma once
#include "runrun.h"

#include <vector>

#include "../data/data_types.h"
#include "../data/dg_data.hpp"

state convert_state_from_code_to_physical_units(const state &state_code_units);

void source_function_cooling_serial(dg_data_t *dg, const uint64_t c, const uint64_t qpoint_in_cell, const double *fields,
                                    const state &st, vector<double> &dwdt);

void measure_box_temperature(dg_data_t *dg);

state convert_state_from_code_to_physical_units(const state &state_code_units);

void enforce_temperature_floor(const dg_data_t *dg, double *w);

#ifdef GPU
void measure_box_temperature_gpu(dg_data_t *dg, dg_data_for_gpu *d_dggpu);
__device__ void dg_add_in_source_cooling(int c, int q, double *fields, const dg_data_for_gpu *d_dggpu, state &st, double *dw_shared,
                                         const int dw_shared_index);
void enforce_temperature_floor_gpu(dg_data_t *dg, double *w, dg_data_for_gpu *d_dggpu);
#endif  // GPU
