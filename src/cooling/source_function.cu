#include "runrun.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/pair.h>

#include "../bisection/bisection.hpp"
#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../compute_cuda/dg_limiting_cuda.h"
#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../cooling/source_function.hpp"
#include "../riemann/riemann_hllc.hpp"
#include "../units/units.h"
#include "../utils/dbl_compare.hpp"
#include "../utils/dg_utils.hpp"

using ::std::max;

#ifdef COOLING

void measure_box_temperature(dg_data_t *dg)
{
  double accumulated_temperature = 0;
  double accumulated_mass        = 0;
  for(decltype(Nc) c = 0; c < Nc; c++)  // loop over local cells
    {
      double fields[NF];

      get_average_fields_of_cell(fields, dg->w, c);

      state code_state;
      get_primitive_state_from_conserved_fields(fields, code_state, &All.ViscParam);
      const state physical_state = convert_state_from_code_to_physical_units(code_state);

      const double rho         = physical_state.rho;
      const auto ptot          = physical_state.press;
      const double temperature = dg->cooling_function_table.calculate_temperature_from_pressure_and_density(ptot, rho);
      const double cell_mass   = dg->getCellVolume() * rho;
      accumulated_mass += cell_mass;
      accumulated_temperature += temperature * cell_mass;
    }  // for every cell
  MPI_Allreduce(MPI_IN_PLACE, &accumulated_temperature, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &accumulated_mass, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  const double average_temperature = accumulated_temperature / accumulated_mass;
  mpi_printf("[AVERAGE_TEMPERATURE] %e %e\n", dg->Time, average_temperature);

  // open a file called temperature.txt and append the current time and average temperature
  if(ThisTask == 0)
    {
      string file_open_mode = "a";
      if(dg->Time == 0)
        {
          file_open_mode = "w";
        }
      FILE *fp = fopen("data/temperature.txt", file_open_mode.c_str());
      fprintf(fp, "%e %e\n", dg->Time, average_temperature);
      fclose(fp);
    }
}

#ifdef GPU

__global__ void measure_box_temperature_kernel(const uint Nc, const dg_data_for_gpu *d_dggpu, const double *w, double *temperatures,
                                               double *masses)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < Nc)
    {
      double fields[NF];

      get_average_fields_of_cell(fields, w, c);

      state code_state;
      get_primitive_state_from_conserved_fields(fields, code_state, &d_dggpu->ViscParam);
      const state physical_state = convert_state_from_code_to_physical_units(code_state);

      const auto rho         = physical_state.rho;
      const auto ptot        = physical_state.press;
      const auto temperature = d_dggpu->cooling_function_table.calculate_temperature_from_pressure_and_density(ptot, rho);
      const auto cell_mass   = d_dggpu->cellVolume * rho;
      masses[c]              = cell_mass;
      temperatures[c]        = temperature * cell_mass;
    }  // for every cell
}

void measure_box_temperature_gpu(dg_data_t *dg, dg_data_for_gpu *d_dggpu)
{
  float time, time_max;
  cudaEvent_t startEvent, stopEvent;
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  double *d_temperature, *d_mass;
  myCudaMalloc(&d_temperature, Nc * sizeof(double));
  myCudaMalloc(&d_mass, Nc * sizeof(double));
  measure_box_temperature_kernel<<<(Nc + 255) / 256, 256>>>(Nc, d_dggpu, dg->d_w, d_temperature, d_mass);

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  // double accumulated_temperature = sum_array(Nc, d_temperature);
  // double accumulated_mass        = sum_array(Nc, d_mass);

  thrust::device_ptr<double> d_temperature_ptr(d_temperature);
  auto accumulated_temperature = thrust::reduce(d_temperature_ptr, d_temperature_ptr + Nc);

  thrust::device_ptr<double> d_mass_ptr(d_mass);
  auto accumulated_mass = thrust::reduce(d_mass_ptr, d_mass_ptr + Nc);

  MPI_Allreduce(MPI_IN_PLACE, &accumulated_temperature, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &accumulated_mass, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  const double average_temperature = accumulated_temperature / accumulated_mass;
  mpi_printf("[AVERAGE_TEMPERATURE] %e %e\n", dg->Time, average_temperature);

  if(ThisTask == 0)
    {
      string file_open_mode = "a";
      if(dg->Time == 0)
        {
          file_open_mode = "w";
        }
      FILE *fp = fopen("data/temperature.txt", file_open_mode.c_str());
      fprintf(fp, "%e %e\n", dg->Time, average_temperature);
      fclose(fp);
    }
  myCudaFree(d_temperature, Nc * sizeof(double));
  myCudaFree(d_mass, Nc * sizeof(double));
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[measure_box_temperature_kernel_time]: %f ms\n", time);
}

__device__ void dg_add_in_source_cooling(int c, int q, double *fields, const dg_data_for_gpu *d_dggpu, state &st, double *dw_shared,
                                         const int dw_shared_index)
{
  // only 3D
  static_assert(ND == 3);

  if(d_dggpu->Time <= d_dggpu->CoolingTimeDelay)
    {
      return;
    }

  const auto physical_state = convert_state_from_code_to_physical_units(st);

  // const auto density_code_units = st.rho;
  const auto pressure_code_units       = st.press;
  const auto current_energy_code_units = pressure_code_units / GAMMA_MINUS1;

  const auto density  = physical_state.rho;
  const auto pressure = physical_state.press;

  const double current_energy = pressure / GAMMA_MINUS1;
  const double current_temperature =
      d_dggpu->cooling_function_table.calculate_temperature_from_pressure_and_density(current_energy * GAMMA_MINUS1, density);
  const double minimum_energy =
      d_dggpu->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, density);

  const double time_step_code_units     = d_dggpu->TimeStep * d_ButcherTab_b[d_dggpu->current_rk_stage];
  const double time_step_physical_units = time_step_code_units / TIME_FACTOR;

  // try explicit solver
  bool use_explicit_solver = true;
  {
    const auto hydrogen_number_density = density / PROTON_MASS;

    const double temperature = current_temperature;

    if(threadIdx.x == 0 && blockIdx.x == 0)
      printf("[COOLING] temperature = %1.2e\n", temperature);
    const double cooling_rate_physical_units = -d_dggpu->cooling_function_table.get_cooling_table_at_temperature(temperature) *
                                               hydrogen_number_density * hydrogen_number_density;  // in erg cm^-3 s^-1
    const double cooled_away_energy = cooling_rate_physical_units * time_step_physical_units;

    if(abs(cooled_away_energy) > 0.05 * current_energy)
      {
        use_explicit_solver = false;
      }

    if(threadIdx.x == 0 && blockIdx.x == 0)
      printf(
          "[COOLING2] cooling_rate_physical_units = %1.2e, cooled_away_energy = %1.2e, current_energy = %1.2e, d_dggpu->Time = "
          "%1.2e use_explicit_solver = %d\n",
          cooling_rate_physical_units, cooled_away_energy, current_energy, d_dggpu->Time, use_explicit_solver);

    if(use_explicit_solver)
      {
        if(threadIdx.x == 0 && blockIdx.x == 0)
          printf("[COOLING] using explicit solver, current_energy = %1.2e, cooled_away_energy = %1.2e\n", current_energy,
                 cooled_away_energy);
        // dwdt needs energy rate in erg cm^-3 s^-1 in code units
        // const double cooling_rate_physical_units = (implicit_energy - current_energy) / time_step_physical_units;
        const double cooling_rate_code_units =
            cooling_rate_physical_units * ENERGY_FACTOR / TIME_FACTOR / powConstexpr(LENGTH_FACTOR, 3);

        const auto fac = calculate_qpoint_integration_factor(d_dggpu->cellVolume);
        const auto f   = ND + 1;

        for(decltype(ND) b = 0; b < NB; b++)
          {
            dw_shared[dw_shared_idx_no_reduction(b, f, threadIdx.x)] +=
                fac * cooling_rate_code_units * d_dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)] * d_qpoint_weights_internal[q];
          }

        return;
      }
  }
  if(threadIdx.x == 0 && blockIdx.x == 0)
    printf("[COOLING] using implicit solver\n");

  // auto implicit_solver_function_with_fixed_parameters = [&, current_energy, density,
  //                                                        time_step_physical_units](double energy_next) -> double {
  //   return d_dggpu->cooling_function_table.implicit_solver_function(current_energy, density, time_step_physical_units, energy_next);
  // };

  if(current_temperature <= TEMPERATURE_FLOOR * (1 + 1e-4))
    {
      // if(DEBUG_VAR)
      //   {
      //     gpu_printf("implicit solver starting at or below the temperature floor returning\n");
      //   }
      return;
    }

  bool implicit_solver_finished = false;
  double implicit_energy;
  double lower_bound = current_energy * .1;
  double upper_bound = current_energy;
  int niter          = 0;
  double found_temperature;

  if(threadIdx.x == 0 && blockIdx.x == 0)
    printf("[IMPLICITBOUNDS] lower_bound = %1.2e, upper_bound = %1.2e\n", lower_bound, upper_bound);
  while((!implicit_solver_finished) && (niter < 10))
    {
      implicit_energy = d_dggpu->cooling_function_table.brents_fun(lower_bound, upper_bound, minimum_energy / 1e4, 1000,
                                                                   current_energy, density, time_step_physical_units);

      if(threadIdx.x == 0 && blockIdx.x == 0)
        printf("[IMPLICIT] niter = %d, current_energy = %1.2e, implicit_energy = %1.2e\n", niter, current_energy, implicit_energy);
      if(implicit_energy < -10)
        {
          lower_bound = fmax(lower_bound * 0.5, minimum_energy);
        }
      if(implicit_energy > 0)
        {
          implicit_solver_finished = true;
          found_temperature =
              d_dggpu->cooling_function_table.calculate_temperature_from_pressure_and_density(implicit_energy * GAMMA_MINUS1, density);
        }
      niter++;
    }

  if(DEBUG_VAR)
    {
      if(threadIdx.x == 0 && blockIdx.x == 0)
        printf("[IMPLICIT] implicit solver found T=%1.2e after Niter=%d\n", found_temperature, niter);
    }

  if(!implicit_solver_finished)
    {
      if(DEBUG_VAR)
        {
          if(threadIdx.x == 0 && blockIdx.x == 0)
            printf(
                "[IMPLICIT] implicit solver failed to find a new temperature, assuming we are at the temperature floor of T=%1.2eK\n",
                TEMPERATURE_FLOOR);
        }

      found_temperature = TEMPERATURE_FLOOR;
      implicit_energy   = d_dggpu->cooling_function_table.calculate_energy_from_temperature_and_density(found_temperature, density);
    }

  if(found_temperature < TEMPERATURE_FLOOR)
    {
      if(DEBUG_VAR)
        {
          // if(threadIdx.x == 0 && blockIdx.x == 0)
          printf("[IMPLICIT] implicit solver found T=%1.2eK, which is below the temperature floor of %1.2eK, fixing\n",
                 found_temperature, TEMPERATURE_FLOOR);
        }
      implicit_energy = d_dggpu->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, density);
    }

  // dwdt needs energy rate in erg cm^-3 s^-1 in code units
  const double cooling_rate_physical_units = (implicit_energy - current_energy) / time_step_physical_units;
  if(threadIdx.x == 0 && blockIdx.x == 0)
    printf(
        "[IMPLICIT] cooling_rate_physical_units = %1.2e, implicit_energy = %1.2e, current_energy = %1.2e, time_step_physical_units = "
        "%1.2e\n",
        cooling_rate_physical_units, implicit_energy, current_energy, time_step_physical_units);
  const double cooling_rate_code_units = cooling_rate_physical_units * ENERGY_FACTOR / TIME_FACTOR / powConstexpr(LENGTH_FACTOR, 3);
  // printf contents of the line above
  printf("[IMPLICIT] cooling_rate_code_units = %1.2e, time_step_physical_units = %1.2e\n", cooling_rate_code_units,
         time_step_physical_units);
  if(threadIdx.x == 0 && blockIdx.x == 0)
    printf(
        "[IMPLICIT] implicit_energy = %1.2e, current_energy_physical = %1.2e, current_energy_code = %1.2e, cooling_rate_physical = "
        "%1.2e, cooling_rate_code = "
        "%1.2e energy - cooled_away = %1.2e, implicit_temperature = %1.2e\n",
        implicit_energy, current_energy, current_energy_code_units, cooling_rate_physical_units, cooling_rate_code_units,
        (current_energy - cooling_rate_physical_units * time_step_physical_units),
        d_dggpu->cooling_function_table.calculate_temperature_from_pressure_and_density(
            (current_energy - cooling_rate_physical_units * time_step_physical_units) * GAMMA_MINUS1, density));

  const auto fac   = calculate_qpoint_integration_factor(d_dggpu->cellVolume);
  constexpr auto f = ND + 1;

  for(decltype(ND) b = 0; b < NB; b++)
    {
      dw_shared[dw_shared_idx_no_reduction(b, f, dw_shared_index)] +=
          fac * cooling_rate_code_units * d_dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)] * d_qpoint_weights_internal[q];
    }

  return;
}

#endif  // GPU

void source_function_cooling_serial(dg_data_t *dg, const uint64_t c, const uint64_t q, const double *fields, const state &st,
                                    vector<double> &dwdt)
{
  // only 3D
  static_assert(ND == 3);

  if(dg->Time <= All.CoolingTimeDelay)
    {
      return;
    }

  const auto physical_state = convert_state_from_code_to_physical_units(st);

  const auto density  = physical_state.rho;
  const auto pressure = physical_state.press;

  const double current_energy = pressure / GAMMA_MINUS1;
  const double current_temperature =
      dg->cooling_function_table.calculate_temperature_from_pressure_and_density(current_energy * GAMMA_MINUS1, density);
  const double minimum_energy = dg->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, density);

  const double time_step_code_units     = dg->TimeStep * dg->ButcherTab_b[dg->current_rk_stage];
  const double time_step_physical_units = time_step_code_units / TIME_FACTOR;

  // try explicit solver
  bool use_explicit_solver = true;
  {
    const auto hydrogen_number_density = density / PROTON_MASS;

    const double temperature = current_temperature;
    const double cooling_rate_physical_units =
        -dg->cooling_function_table.get_cooling_table_at_temperature_and_density(temperature, density) * hydrogen_number_density *
        hydrogen_number_density;  // in erg cm^-3 s^-1
    const double cooled_away_energy = cooling_rate_physical_units * time_step_physical_units;

    if(c == 0 && q == 0)
      printf("[COOLING2] cooling_rate_physical_units = %1.2e, cooled_away_energy = %1.2e, current_energy = %1.2e, dg->Time = %1.2e\n",
             cooling_rate_physical_units, cooled_away_energy, current_energy, dg->Time);

    if(abs(cooled_away_energy) > 0.05 * current_energy)
      {
        use_explicit_solver = false;
      }

    if(use_explicit_solver)
      {
        // dwdt needs energy rate in erg cm^-3 s^-1 in code units
        // const double cooling_rate_physical_units = (implicit_energy - current_energy) / time_step_physical_units;
        const double cooling_rate_code_units =
            cooling_rate_physical_units * ENERGY_FACTOR / TIME_FACTOR / powConstexpr(LENGTH_FACTOR, 3);

        const auto fac = calculate_qpoint_integration_factor(dg->getCellVolume());
        const auto f   = ND + 1;

        for(decltype(ND) b = 0; b < NB; b++)
          {
            dwdt[w_idx_internal(b, f, c)] +=
                fac * cooling_rate_code_units * dg->bfunc_internal[bfunc_internal_idx(q, b)] * dg->qpoint_weights_internal[q];
          }

        return;
      }
  }

  auto implicit_solver_function_with_fixed_parameters = [&, current_energy, density,
                                                         time_step_physical_units](double energy_next) -> double {
    return dg->cooling_function_table.implicit_solver_function(current_energy, density, time_step_physical_units, energy_next);
  };

  if(current_temperature <= TEMPERATURE_FLOOR * (1 + 1e-4))
    {
      if(DEBUG_VAR)
        {
          mpi_printf("implicit solver starting at or below the temperature floor returning\n");
        }
      return;
    }

  bool implicit_solver_finished = false;
  double implicit_energy;
  double lower_bound = current_energy * .5;
  double upper_bound = current_energy;
  int niter          = 0;
  double found_temperature;

  while((!implicit_solver_finished) && (niter < 10))
    {
      implicit_energy =
          brents_fun(implicit_solver_function_with_fixed_parameters, lower_bound, upper_bound, minimum_energy / 1e4, 1000);
      if(implicit_energy < -10)
        {
          lower_bound = std::max(lower_bound * 0.5, minimum_energy);
        }
      if(implicit_energy > 0)
        {
          implicit_solver_finished = true;
          found_temperature =
              dg->cooling_function_table.calculate_temperature_from_pressure_and_density(implicit_energy * GAMMA_MINUS1, density);
        }
      niter++;
    }

  if(DEBUG_VAR)
    {
      mpi_printf("implicit solver stopping after Niter=%d\n", niter);
    }

  if(!implicit_solver_finished)
    {
      if(DEBUG_VAR)
        {
          mpi_printf("implicit solver failed to find a new temperature, assuming we are at the temperature floor of T=%1.2eK\n",
                     TEMPERATURE_FLOOR);
        }

      found_temperature = TEMPERATURE_FLOOR;
      implicit_energy   = dg->cooling_function_table.calculate_energy_from_temperature_and_density(found_temperature, density);
    }

  if(found_temperature < TEMPERATURE_FLOOR)
    {
      if(DEBUG_VAR)
        {
          mpi_printf("implicit solver found T=%1.2eK, which is below the temperature floor of %1.2eK, fixing\n", found_temperature,
                     TEMPERATURE_FLOOR);
        }
      implicit_energy = dg->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, density);
    }

  // dwdt needs energy rate in erg cm^-3 s^-1 in code units
  const double cooling_rate_physical_units = (implicit_energy - current_energy) / time_step_physical_units;
  const double cooling_rate_code_units = cooling_rate_physical_units * ENERGY_FACTOR / TIME_FACTOR / powConstexpr(LENGTH_FACTOR, 3);

  const auto fac = calculate_qpoint_integration_factor(dg->getCellVolume());
  const auto f   = ND + 1;

  for(decltype(ND) b = 0; b < NB; b++)
    {
      dwdt[w_idx_internal(b, f, c)] +=
          fac * cooling_rate_code_units * dg->bfunc_internal[bfunc_internal_idx(q, b)] * dg->qpoint_weights_internal[q];
    }

  return;
}

void enforce_temperature_floor(const dg_data_t *dg, double *w)
{
  constexpr auto f = ND + 1;
  for(uint64_t c = 0; c < Nc; c++)
    {
      vector<double> fields(NF, 0);
      get_average_fields_of_cell(fields.data(), w, c);

      state code_state;
      get_primitive_state_from_conserved_fields(fields.data(), code_state, &All.ViscParam);
      const state physical_state = convert_state_from_code_to_physical_units(code_state);

      const auto rho_code = code_state.rho;

      const auto rho_physical  = physical_state.rho;
      const auto ptot_physical = physical_state.press;
      const auto temperature = dg->cooling_function_table.calculate_temperature_from_pressure_and_density(ptot_physical, rho_physical);

      // const bool negative_energy = (w[w_idx_internal(0, f, c)] < 0);
      // if(negative_energy)
      //   {
      //     printf("[enforce_temperature_floor] negative energy c=%lu T=%1.2eK\n", c, temperature);
      //   }

      if(temperature < TEMPERATURE_FLOOR)
        {
          // mpi_printf("[enforce_temperature_floor] triggered!\n");
          const auto p2_code = calculate_momenta_squared_from_fields(fields.data());

          const double rho_u_code =
              dg->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, rho_physical) *
              ENERGY_FACTOR / powConstexpr(LENGTH_FACTOR, 3);
          w[w_idx_internal(0, f, c)] = rho_u_code + 0.5 * p2_code / rho_code;
          zero_out_higher_orders_of_field_in_cell(dg->w, f, c, &All.ViscParam);
        }
    }
}

#ifdef GPU
__global__ void enforce_temperature_floor_kernel(const uint Nc, const dg_data_for_gpu *d_dggpu, double *w)
{
  const uint64_t c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < Nc)
    {
      constexpr auto f = ND + 1;
      double fields[NF];
      for(int i = 0; i < NF; i++)
        {
          fields[i] = 0;
        }
      get_average_fields_of_cell(fields, w, c);

      state code_state;
      get_primitive_state_from_conserved_fields(fields, code_state, &d_dggpu->ViscParam);
      const state physical_state = convert_state_from_code_to_physical_units(code_state);

      const auto rho_code = code_state.rho;

      const auto rho_physical  = physical_state.rho;
      const auto ptot_physical = physical_state.press;
      const auto temperature =
          d_dggpu->cooling_function_table.calculate_temperature_from_pressure_and_density(ptot_physical, rho_physical);

      if(temperature < TEMPERATURE_FLOOR)
        {
          // mpi_printf("[enforce_temperature_floor] triggered!\n");
          const auto p2_code = calculate_momenta_squared_from_fields(fields);

          const double rho_u_code =
              d_dggpu->cooling_function_table.calculate_energy_from_temperature_and_density(TEMPERATURE_FLOOR, rho_physical) *
              ENERGY_FACTOR / powConstexpr(LENGTH_FACTOR, 3);
          w[w_idx_internal(0, f, c)] = rho_u_code + 0.5 * p2_code / rho_code;
          zero_out_higher_orders_of_field_in_cell(w, f, c, &d_dggpu->ViscParam);
        }
    }
}

void enforce_temperature_floor_gpu(dg_data_t *dg, double *w, dg_data_for_gpu *d_dggpu)
{
  float time, time_max;
  cudaEvent_t startEvent, stopEvent;
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  enforce_temperature_floor_kernel<<<(Nc + 255) / 256, 256>>>(Nc, d_dggpu, w);

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[enforce_temperature_floor_kernel_time]: %f ms\n", time);
}
#endif  // GPU
#endif  // COOLING
