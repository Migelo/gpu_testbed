#pragma once

#include <fenv.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/time.h>

#include <cassert>
#include <iostream>
#include <list>
#include <string>
#include <vector>

#include "../data/dg_data.hpp"

using ::std::cout;
using ::std::endl;
using ::std::list;
using ::std::string;
using ::std::vector;

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#endif

template <typename T>
void dg_utils_print_first_n(vector<T> vec, int32_t n, char const* endline)
{
  cout << "printing first " << n << " elements" << endl;
  for(int32_t i = 0; i < n; i++)
    cout << vec.at(i) << endline;
  cout << endl;
}

template <typename T>
void dg_utils_print_last_n(vector<T> vec, int32_t n, char const* endline)
{
  cout << "printing last " << n << " elements" << endl;
  for(uint32_t i = vec.size() - n; i < vec.size(); i++)
    cout << vec.at(i) << endline;
  cout << endl;
}

template <typename T>
void dg_utils_print_from_to(vector<T> vec, int32_t n0, int32_t n1, char const* endline)
{
  cout << "printing elements from " << n0 << " to " << n1 << endl;
  for(int32_t i = n0; i < n1; i++)
    cout << vec.at(i) << endline;
  cout << endl;
}

template <typename T>
constexpr bool dg_utils_is_perfect_square(T x)
{
  // Find floating point value of
  // square root of x.
  double sr = sqrt(x);

  // If square root is an integer
  return ((sr - floor(sr)) < 1e-12);
}

template <typename T>
constexpr bool dg_utils_is_perfect_cube(T x)
{
  // Find floating point value of
  // square root of x.
  double sr = cbrt(x);

  // If square root is an integer
  return ((sr - floor(sr)) < 1e-12);
}

class NotImplemented : public std::logic_error
{
 private:
  std::string _text;

  NotImplemented(const char* message, const char* function) : std::logic_error("Not Implemented")
  {
    _text = message;
    _text += " : ";
    _text += function;
  }

 public:
  NotImplemented() : NotImplemented("Not Implememented", __FUNCTION__) {}

  explicit NotImplemented(const char* message) : NotImplemented(message, __FUNCTION__) {}

  virtual const char* what() const throw() { return _text.c_str(); }
};

class ObsoleteFunction : public std::logic_error
{
 private:
  std::string _text;

  ObsoleteFunction(const char* message, const char* function) : std::logic_error("Obsolete Function")
  {
    _text = message;
    _text += " : ";
    _text += function;
  }

 public:
  ObsoleteFunction() : ObsoleteFunction("Obsolete Function", __FUNCTION__) {}

  explicit ObsoleteFunction(const char* message) : ObsoleteFunction(message, __FUNCTION__) {}

  virtual const char* what() const throw() { return _text.c_str(); }
};

/*
static void enable_core_dumps_and_fpu_exceptions(void)
{
#if defined(__linux__)
  // enable floating point exceptions
  extern int feenableexcept(int __excepts);
  feenableexcept(FE_DIVBYZERO | FE_INVALID);
#endif

  // set core-dump size to infinity
  rlimit rlim;
  getrlimit(RLIMIT_CORE, &rlim);
  rlim.rlim_cur = RLIM_INFINITY;
  setrlimit(RLIMIT_CORE, &rlim);

  signal(SIGSEGV, SIG_DFL);
  signal(SIGBUS, SIG_DFL);
  signal(SIGFPE, SIG_DFL);
  signal(SIGINT, SIG_DFL);
}
*/

template <unsigned int p>
int constexpr intPower(const int x)
{
  if constexpr(p == 0)
    return 1;
  if constexpr(p == 1)
    return x;

  int tmp = intPower<p / 2>(x);
  if constexpr((p % 2) == 0)
    {
      return tmp * tmp;
    }
  return x * tmp * tmp;
}

__host__ __device__ constexpr double powConstexpr(double x, int exp)
{
  int sign = 1;
  if(exp < 0)
    {
      sign = -1;
      exp  = -exp;
    }
  if(exp == 0)
    return x < 0 ? -1.0 : 1.0;
  double ret = x;
  while(--exp)
    ret *= x;
  return sign > 0 ? ret : 1.0 / ret;
}

string sizeof_fmt(double num);

void sigterm_handler(int signal);
