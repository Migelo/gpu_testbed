// Copyright 2020 Miha Cernetic
#ifndef DBL_COMPARE_HPP_
#define DBL_COMPARE_HPP_

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#endif

#include <vector>

using ::std::vector;

template <typename T>
struct dbl_compare_t
{
  T abs_tol, rel_tol;
};

template <typename T>
__host__ __device__ void dbl_compare_init(dbl_compare_t<T> *cmp, T abs_tol, T rel_tol);

template <typename T>
__host__ __device__ void dbl_compare_init_num_digits(dbl_compare_t<T> *cmp, int num_digits);

template <typename T>
__host__ __device__ bool dbl_compare_values(dbl_compare_t<T> *cmp, T a, T b);

template <typename T>
bool dbl_compare_results(dbl_compare_t<T> *cmp, vector<T> results_one, vector<T> results_two, const char *results_one_description,
                         const char *results_two_description);
#endif  // DBL_COMPARE_HPP_
