
#include "runrun.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "../data/allvars.h"
#include "../data/macros.h"
#include "../utils/misc.h"

/*! \brief  Divides N elements evenly on pieces chunks, writes in first and
 *          count arrays.
 *
 * \param[in] N Number of elements.
 * \param[in] pieces Number of chunks.
 * \param[in] index Index of piece that is needed as output.
 * \param[out] first Index of first element of piece number 'index'.
 * \param[out] count Number of elements of piece number 'index'.
 *
 * \return void
 */
void subdivide_evenly(const int N, const int pieces, const int index, int *const first, int *const count)
{
  const int avg              = (N - 1) / pieces + 1;
  const int exc              = pieces * avg - N;
  const int indexlastsection = pieces - exc;

  if(index < indexlastsection)
    {
      *first = index * avg;
      *count = avg;
    }
  else
    {
      *first = index * avg - (index - indexlastsection);
      *count = avg - 1;
    }
}

void *mymalloc_fullinfo(const char *const varname, const size_t n, const char *const func, const char *const file, const int line)
{
  void *p = malloc(n);
  if(!p)
    Terminate("allocation of '%s' failed", varname);

  return p;
}

void myfree(void *ptr) { free(ptr); }

/*! \brief A wrapper around MPI_Alltoallv that can deal with data in
 *         individual sends that are very big.
 *
 *  \param[in] sendb Starting address of send buffer.
 *  \param[in] sendcounts Integer array equal to the group size specifying the
 *             number of elements to send to each processor.
 *  \param[in] sdispls Integer array (of length group size). Entry j specifies
 *             the displacement (relative to sendbuf) from which to take the
 *             outgoing data destined for process j.
 *  \param[out] recvb Starting address of receive buffer.
 *  \param[in] recvcounts Integer array equal to the group size specifying the
 *             maximum number of elements that can be received from each
 *             processor.
 *  \param[in] rdispls Integer array (of length group size). Entry i specifies
 *             the displacement (relative to recvbuf at which to place the
 *             incoming data from process i.
 *  \param[in] len Size of single element in send array.
 *  \param[in] big_flag Flag if cummunication of large data. If not, the normal
 *             MPI_Alltoallv function is used.
 *  \param[in] comm MPI communicator.
 *
 *  \return void
 */
void myMPI_Alltoallv(void *sendb, size_t *sendcounts, size_t *sdispls, void *recvb, size_t *recvcounts, size_t *rdispls, int len,
                     int big_flag, MPI_Comm comm)
{
  char *sendbuf = (char *)sendb;
  char *recvbuf = (char *)recvb;

  if(big_flag == 0)
    {
      int ntask;
      MPI_Comm_size(comm, &ntask);

      int *scount = (int *)mymalloc("scount", ntask * sizeof(int));
      int *rcount = (int *)mymalloc("rcount", ntask * sizeof(int));
      int *soff   = (int *)mymalloc("soff", ntask * sizeof(int));
      int *roff   = (int *)mymalloc("roff", ntask * sizeof(int));

      for(int i = 0; i < ntask; i++)
        {
          scount[i] = sendcounts[i] * len;
          rcount[i] = recvcounts[i] * len;
          soff[i]   = sdispls[i] * len;
          roff[i]   = rdispls[i] * len;
        }

      MPI_Alltoallv(sendbuf, scount, soff, MPI_BYTE, recvbuf, rcount, roff, MPI_BYTE, comm);

      myfree(roff);
      myfree(soff);
      myfree(rcount);
      myfree(scount);
    }
  else
    {
      /* here we definitely have some large messages. We default to the
       * pair-wise protocoll, which should be most robust anyway.
       */

      int ntask, thistask, ptask;
      MPI_Comm_size(comm, &ntask);
      MPI_Comm_rank(comm, &thistask);
      for(ptask = 0; ntask > (1 << ptask); ptask++)
        ;

      for(int ngrp = 0; ngrp < (1 << ptask); ngrp++)
        {
          int target = thistask ^ ngrp;

          if(target < ntask)
            {
              if(sendcounts[target] > 0 || recvcounts[target] > 0)
                myMPI_Sendrecv(sendbuf + sdispls[target] * len, sendcounts[target] * len, MPI_BYTE, target, TAG_PDATA + ngrp,
                               recvbuf + rdispls[target] * len, recvcounts[target] * len, MPI_BYTE, target, TAG_PDATA + ngrp, comm,
                               MPI_STATUS_IGNORE);
            }
        }
    }
}

/*! \brief Self-made sendrecv function with limiter to the number of elements
 *         that can be sent in one go.
 *
 *  If the total message is longer, multiple MPI_Sendrecv calls are executed
 *  until the entire message has been communicated.
 *
 *  \param[in] sendb Initial address of send buffer.
 *  \param[in] sendcount Number of elements in send buffer.
 *  \param[in] sendtype Type of elements in send buffer (handle).
 *  \param[in] dest Rank of destination.
 *  \param[in] sendtag Send tag.
 *  \param[out] recvb Initial address of receive buffer.
 *  \param[in] recvcount Number of elements in receive buffer.
 *  \param[in] recvtype Type of elements in receive buffer (handle).
 *  \param[in] source Rank of source.
 *  \param[in] recvtag Receive tag.
 *  \param[in] comm MPI communicator.
 *  \param[out] status Status, referring to receive operation.
 *
 *  \return 0
 */
int myMPI_Sendrecv(void *sendb, size_t sendcount, MPI_Datatype sendtype, int dest, int sendtag, void *recvb, size_t recvcount,
                   MPI_Datatype recvtype, int source, int recvtag, MPI_Comm comm, MPI_Status *status)
{
  int iter      = 0, size_sendtype, size_recvtype, send_now, recv_now;
  char *sendbuf = (char *)sendb;
  char *recvbuf = (char *)recvb;

  if(dest != source)
    Terminate("dest != source");

  MPI_Type_size(sendtype, &size_sendtype);
  MPI_Type_size(recvtype, &size_recvtype);

  int thistask;
  MPI_Comm_rank(comm, &thistask);

  if(dest == thistask)
    {
      memcpy(recvbuf, sendbuf, recvcount * size_recvtype);
      return 0;
    }

  size_t count_limit = MPI_MESSAGE_SIZELIMIT_IN_BYTES / size_sendtype;

  while(sendcount > 0 || recvcount > 0)
    {
      if(sendcount > count_limit)
        {
          send_now = count_limit;
          /*
             if(iter == 0)
             {
             printf("Imposing size limit on MPI_Sendrecv() on task=%d (send of size=%lld)\n", ThisTask, (long long) sendcount *
             size_sendtype); myflush(stdout);
             }
           */
          iter++;
        }
      else
        send_now = sendcount;

      if(recvcount > count_limit)
        recv_now = count_limit;
      else
        recv_now = recvcount;

      MPI_Sendrecv(sendbuf, send_now, sendtype, dest, sendtag, recvbuf, recv_now, recvtype, source, recvtag, comm, status);

      sendcount -= send_now;
      recvcount -= recv_now;

      sendbuf += send_now * size_sendtype;
      recvbuf += recv_now * size_recvtype;
    }

  return 0;
}

void myMPI_exchange_of_interface_data(const void *right_states_to_send, void *left_states_to_recv, int leftTask,
                                      const void *left_states_to_send, void *right_states_to_recv, int rightTask, int count)
{
  if(ThisTask & 1)  // uneven ranks do this sequence
    {
      MPI_Sendrecv(right_states_to_send, count, MPI_BYTE, leftTask, TAG_LEFT, left_states_to_recv, count, MPI_BYTE, leftTask, TAG_LEFT,
                   MyComm, MPI_STATUS_IGNORE);
      MPI_Sendrecv(left_states_to_send, count, MPI_BYTE, rightTask, TAG_RIGHT, right_states_to_recv, count, MPI_BYTE, rightTask,
                   TAG_RIGHT, MyComm, MPI_STATUS_IGNORE);
    }
  else  // even ranks do the reverse sequence.
    {
      if(rightTask & 1)  // but: if the total number of ranks is uneven, we have to skip the connection between the last rank and
                         // the first one (which are *both* even)
        MPI_Sendrecv(left_states_to_send, count, MPI_BYTE, rightTask, TAG_LEFT, right_states_to_recv, count, MPI_BYTE, rightTask,
                     TAG_LEFT, MyComm, MPI_STATUS_IGNORE);

      if(leftTask & 1)
        MPI_Sendrecv(right_states_to_send, count, MPI_BYTE, leftTask, TAG_RIGHT, left_states_to_recv, count, MPI_BYTE, leftTask,
                     TAG_RIGHT, MyComm, MPI_STATUS_IGNORE);
    }

  // if the total number of ranks is uneven, we have then still have one extra connection between the first and last ranks to do
  if(NTask & 1)
    {
      if(NTask == 1)
        {
          memcpy(left_states_to_recv, left_states_to_send, count);
          memcpy(right_states_to_recv, right_states_to_send, count);
        }
      else
        {
          if(ThisTask == 0)
            {
              MPI_Sendrecv(right_states_to_send, count, MPI_BYTE, leftTask, TAG_LEFT, left_states_to_recv, count, MPI_BYTE, leftTask,
                           TAG_LEFT, MyComm, MPI_STATUS_IGNORE);
            }
          if(ThisTask == NTask - 1)
            {
              MPI_Sendrecv(left_states_to_send, count, MPI_BYTE, rightTask, TAG_LEFT, right_states_to_recv, count, MPI_BYTE, rightTask,
                           TAG_LEFT, MyComm, MPI_STATUS_IGNORE);
            }
        }
    }
}
