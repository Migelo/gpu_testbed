// Copyright 2023 Miha Cernetic

#include <iostream>
#include <list>
#include <string>
#include <vector>

#include "dg_utils.hpp"

using ::std::cout;
using ::std::endl;
using ::std::list;
using ::std::string;

string sizeof_fmt(double num)
{
  // by Fred Cirera,  https://stackoverflow.com/a/1094933/1870254, modified
  constexpr char suffix[]     = "B";
  list<const char *> prefixes = {"", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"};
  char buff[100];
  for(const char *prefix : prefixes)
    {
      if(abs(num) < 1024.0)
        {
          sprintf(buff, "%3.1f %s%s", num, prefix, suffix);
          string return_string = buff;
          return return_string;
        }
      num /= 1024.0;
    }
  sprintf(buff, "%.1f Yi%s\n", num, suffix);
  string return_string = buff;
  return return_string;
}
