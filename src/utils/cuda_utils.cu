
#include "runrun.h"

#ifdef GPU

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "../data/macros.h"
#include "../utils/cuda_utils.h"

extern "C"
{
  void gpuAssert(cudaError_t code, const char *file, int line)
  {
    if(code != cudaSuccess)

      Terminate("GPUassert: %s %s:%d\n", cudaGetErrorString(code), file, line);
  }
}

void dg_data_cuda_timer_start(cudaEvent_t *start, cudaEvent_t *stop)
{
  /*
  gpuErrchk(cudaEventCreate(start));
  gpuErrchk(cudaEventCreate(stop));
  gpuErrchk(cudaEventRecord(*start));
  */
}

void dg_data_cuda_timer_stop(cudaEvent_t start, cudaEvent_t stop, float *time)
{
  /*
  gpuErrchk(cudaEventRecord(stop));
  gpuErrchk(cudaEventSynchronize(stop));
  gpuErrchk(cudaEventElapsedTime(time, start, stop));
  */
}

#endif
