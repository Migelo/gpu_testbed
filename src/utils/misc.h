#ifndef MISC_H_
#define MISC_H_

#include <mpi.h>

#define MPI_MESSAGE_SIZELIMIT_IN_BYTES 100000000
#define TAG_PDATA 10

#define mymalloc(varname, n) mymalloc_fullinfo(varname, n, __func__, __FILE__, __LINE__)

void *mymalloc_fullinfo(const char *const varname, const size_t n, const char *const func, const char *const file, const int line);
void myfree(void *ptr);

void subdivide_evenly(const int N, const int pieces, const int index, int *const first, int *const count);

void myMPI_Alltoallv(void *sendb, size_t *sendcounts, size_t *sdispls, void *recvb, size_t *recvcounts, size_t *rdispls, int len,
                     int big_flag, MPI_Comm comm);

int myMPI_Sendrecv(void *sendb, size_t sendcount, MPI_Datatype sendtype, int dest, int sendtag, void *recvb, size_t recvcount,
                   MPI_Datatype recvtype, int source, int recvtag, MPI_Comm comm, MPI_Status *status);

void myMPI_exchange_of_interface_data(const void *right_states_to_send, void *left_states_to_recv, int leftTask,
                                      const void *left_states_to_send, void *right_states_to_recv, int rightTask, int count);
#endif
