// Copyright 2020 Miha Cernetic
#ifndef CUDA_UTILS_H_
#define CUDA_UTILS_H_

#ifdef __cplusplus

extern "C"
{
#endif  // __cplusplus

#include "runrun.h"

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#endif

#ifdef GPU

#define gpuErrchk(ans)                    \
  {                                       \
    gpuAssert((ans), __FILE__, __LINE__); \
  }
  void gpuAssert(cudaError_t code, const char *file, int line);

  void dg_data_cuda_timer_start(cudaEvent_t *start, cudaEvent_t *stop);

  void dg_data_cuda_timer_stop(cudaEvent_t start, cudaEvent_t stop, float *time);

#endif

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // CUDA_UTILS_H_
