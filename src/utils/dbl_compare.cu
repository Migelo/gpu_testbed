// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <cassert>

#include "../utils/dbl_compare.hpp"

// using ::std::vector;

template <typename T>
__host__ __device__ void dbl_compare_init(dbl_compare_t<T> *cmp, T abs_tol, T rel_tol)
{
  assert(abs_tol >= 0.0);
  assert(rel_tol >= 0.0);
  cmp->abs_tol = abs_tol;
  cmp->rel_tol = rel_tol;
}

template <typename T>
__host__ __device__ void dbl_compare_init_num_digits(dbl_compare_t<T> *cmp, int num_digits)
{
  const T eps = pow(10.0, -num_digits);
  dbl_compare_init(cmp, eps, eps);
}

template <typename T>
__host__ __device__ bool dbl_compare_values(dbl_compare_t<T> *cmp, T a, T b)
{
  return fabs(a - b) <= (cmp->abs_tol + cmp->rel_tol * (fabs(a) + fabs(b)));
}

template void dbl_compare_init_num_digits(dbl_compare_t<double> *cmp, int num_digits);
template void dbl_compare_init(dbl_compare_t<double> *cmp, double abs_tol, double rel_tol);
template bool dbl_compare_values(dbl_compare_t<double> *cmp, double a, double b);

template void dbl_compare_init_num_digits(dbl_compare_t<float> *cmp, int num_digits);
template void dbl_compare_init(dbl_compare_t<float> *cmp, float abs_tol, float rel_tol);
template bool dbl_compare_values(dbl_compare_t<float> *cmp, float a, float b);
