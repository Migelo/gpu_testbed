// Copyright 2021 Miha Cernetic
#pragma once

#include "runrun.h"

#include "../data/dg_data.hpp"
#include "../data/dg_params.h"

#ifdef GPU
#include <cuda_runtime.h>

void dg_data_setup_gpu(dg_data_t *dg);
void dg_data_setup_gpu_memory_usage(dg_data_t *dg);

__constant__ extern double d_qpoint_location[DG_NUM_QPOINTS_1D];
__constant__ extern double d_qpoint_weights_internal[DG_NUM_INNER_QPOINTS];
__constant__ extern double d_qpoint_weights_external[DG_NUM_OUTER_QPOINTS];
__constant__ extern double d_ButcherTab_a[RK_STAGES * RK_STAGES];
__constant__ extern double d_ButcherTab_b[RK_STAGES];
__constant__ extern double d_ButcherTab_c[RK_STAGES];
#ifdef COOLING
__constant__ extern double d_cooling_function_table[176];
__constant__ extern double d_cooling_function_temperature_bins[176];
#endif  // COOLING

#endif
