// Copyright 2020 Miha Cernetic
#ifndef DG_LIMITING_CUDA_H_
#define DG_LIMITING_CUDA_H_

#include "runrun.h"

#include "./dg_compute_cuda.cuh"
#ifdef GPU
// #include <cuda_runtime.h>

void positivity_limiter_cuda(dg_data_t *dg, double *d_w, dg_data_for_gpu &dggpu);
__global__ void fill_mean_cell_values(int numw, double *w, double *leftw_to_send, double *rightw_to_send, int celloffset);
__global__ void positivity_limiter_kernel(int Nc, int Nslabs, double *w, int *Nlimited_rho, int *Nlimited_p,
                                          double *LimiterInjectedMass, double *LimiterInjectedEnergy, const dg_data_for_gpu dggpu);
#endif  // GPU

#endif
