
#include "runrun.h"

#ifdef GPU

#include "../compute_cuda/device_variables.cuh"
#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../compute_general/compute_general.cuh"
#include "../riemann/riemann_hllc.hpp"

double sum_array(int Nc, double *in) /* sums up the device array in[] of length Nc. The input array's contents are destroyed */
{
  int blocks = (Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;

  double *out;
  myCudaMalloc(&out, blocks * sizeof(double));

  /* first, do a parallel reduce over each block */
  reduce_summation<<<blocks, THREADS_PER_BLOCK>>>(Nc, in, out);

  /* now reduce the results of the blocks */
  while(blocks > 1)
    {
      gpuErrchk(cudaMemcpy(in, out, blocks * sizeof(double), cudaMemcpyDeviceToDevice));

      reduce_summation<<<(blocks + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(blocks, in, out);

      blocks = (blocks + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    }

  double result;
  gpuErrchk(cudaMemcpy(&result, out, sizeof(double), cudaMemcpyDeviceToHost));

  myCudaFree(out, (Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK * sizeof(double));

  return result;
}

__global__ void reduce_summation(int nelem, double *in, double *out)
{
  __shared__ double sdata[THREADS_PER_BLOCK];

  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;

  // each thread loads one element from global to shared mem
  unsigned int tid = threadIdx.x;

  if(i < nelem)
    sdata[tid] = in[i];
  else
    sdata[tid] = 0;

  __syncthreads();

  for(unsigned int s = blockDim.x / 2; s > 0; s >>= 1)
    {
      if(tid < s)
        {
          sdata[tid] += sdata[tid + s];
        }
      __syncthreads();
    }

  // write result for this block to output memory
  if(tid == 0)
    out[blockIdx.x] = sdata[0];
}

__global__ void multiply_butchertab_a(uint64_t n, int s, double dt, double *w, double *dwdt)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    {
      double temp = 0;
      for(int m = 0; m < s; m++)
        {
          temp += d_ButcherTab_a[s * RK_STAGES + m] * dt * dwdt[n * m + i];
        }
      w[i] += temp;
    }
}

__global__ void multiply_butchertab_b(uint64_t n, int s, double dt, double *w, double *dwdt)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    {
      double temp = 0;
      for(int m = 0; m < s; m++)
        {
          temp += d_ButcherTab_b[m] * dt * dwdt[n * m + i];
        }
      w[i] += temp;
    }
}

__global__ void multiply_butchertab_c(uint64_t n, int s, double dt, double *w, double *dwdt)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    {
      double temp = 0;
      for(int m = 0; m < s; m++)
        {
          temp += d_ButcherTab_c[m] * dt * dwdt[n * m + i];
        }
      w[i] += temp;
    }
}

__global__ void copy_array(uint64_t n, double *src, double *dst)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;

  if(i < n)
    {
      dst[i] = src[i];
    }
}
__global__ void copy_array_scaled(uint64_t n, const double factor, double *src, double *dst)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    {
      dst[i] = src[i] * factor;
    }
}

__global__ void add_to_array(uint64_t n, const double addition, double *arr)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    arr[i] += arr[i] + addition;
}

__global__ void scale_and_add_to_array(uint64_t n, const double factor, const double addition, double *src, double *dst)
{
  uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < n)
    {
      dst[i] = src[i] * factor + addition;
    }
}

__global__ void test_kernel_bandwidth(uint64_t n, double *w)
{
  const uint64_t i = blockIdx.x * blockDim.x + threadIdx.x;

  w[i] *= 1.0596;
}

__global__ void multiply_weights(uint64_t N, double fac, double *dwdt)
{
  const int i = blockIdx.x * blockDim.x + threadIdx.x;
  if(i < N)
    dwdt[i] *= fac;
}

__global__ void check_cell_for_trouble_kernel(double *w, double *w_temp, dg_data_for_gpu dggpu, char *troubled_array)
{
  const uint64_t c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < dggpu.Nc)
    {
      const auto trouble = check_cell_for_trouble(dggpu.d_qpoint_weights_positivity, w_temp, c, &dggpu.ViscParam);
      if(trouble)
        {
          if(!troubled_array[c])
            {
              troubled_array[c] = true;
              atomicExch(&d_trouble_detected, 1);
              zero_out_higher_orders_in_cell(w, c, &dggpu.ViscParam);
            }
          else
            {
              zero_out_higher_orders_in_cell(w_temp, c, &dggpu.ViscParam);
            }
        }
    }
}

#ifdef USE_PRIMITIVE_PROJECTION
__global__ void project_to_primitives_kernel(double *w, dg_data_for_gpu *dggpu)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < dggpu->Nc)
    {
      project_cell_to_primitives(c, w, dggpu->d_w_prim, dggpu->d_bfunc_internal, d_qpoint_weights_internal, &dggpu->ViscParam);
    }
}
#endif

__global__ void setTroubleDetected(int value) { d_trouble_detected = value; }

#endif  // GPU
