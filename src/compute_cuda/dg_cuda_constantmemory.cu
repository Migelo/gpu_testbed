// Copyright 2021 Miha Cernetic

#include "runrun.h"

#ifdef GPU

#include "./dg_cuda_constantmemory.cuh"

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"

__constant__ double d_qpoint_location[DG_NUM_QPOINTS_1D];
__constant__ double d_qpoint_weights_internal[DG_NUM_INNER_QPOINTS];
__constant__ double d_qpoint_weights_external[DG_NUM_OUTER_QPOINTS];
__constant__ double d_ButcherTab_a[RK_STAGES * RK_STAGES];
__constant__ double d_ButcherTab_b[RK_STAGES];
__constant__ double d_ButcherTab_c[RK_STAGES];
#ifdef COOLING
__constant__ double d_cooling_function_table[176];
__constant__ double d_cooling_function_temperature_bins[176];
#endif  // COOLING

void dg_data_setup_gpu(dg_data_t *dg)
{
  size_t free_mem, total_mem;
  float time = 0;

  cudaEvent_t start = 0, stop = 0, total_start = 0, total_stop = 0;
  dg_data_cuda_timer_start(&start, &stop);
  dg_data_cuda_timer_start(&total_start, &total_stop);

  cudaSetDevice(LocalDeviceRank);
  cudaMemGetInfo(&free_mem, &total_mem);
  gpu_printf("GPU memory before allocations: %g / %g MB\n", (total_mem - free_mem) / 1.e6, total_mem / 1.e6);

  // allocate memory on the device
  myCudaMalloc(&dg->d_w, dg->N_w * sizeof(double));
  myCudaMalloc(&dg->d_bfunc_internal, dg->bfunc_internal.size() * sizeof(double));
  myCudaMalloc(&dg->d_bfunc_external, dg->bfunc_external.size() * sizeof(double));
  myCudaMalloc(&dg->d_bfunc_internal_diff, dg->bfunc_internal_diff.size() * sizeof(double));
  myCudaMalloc(&dg->d_qpoint_weights_positivity, dg->qpoint_weights_positivity.size() * sizeof(double));
  myCudaMalloc(&dg->d_bfunc_mid, dg->bfunc_mid.size() * sizeof(double));
  myCudaMalloc(&dg->d_bfunc_mid_diff, dg->bfunc_mid_diff.size() * sizeof(double));

#ifdef VISCOSITY
  for(uint d = 0; d < ND; d++)
    {
      myCudaMalloc(&dg->d_bfunc_project_list[d], dg->bfunc_project_list[d].size() * sizeof(int));
      myCudaMalloc(&dg->d_bfunc_project_value_left[d], dg->bfunc_project_value_left[d].size() * sizeof(double));
      myCudaMalloc(&dg->d_bfunc_project_value_right[d], dg->bfunc_project_value_right[d].size() * sizeof(double));
    }

  myCudaMalloc(&dg->d_bfunc_project_count, ND * NB_INCREASED * sizeof(int));
  myCudaMalloc(&dg->d_bfunc_project_first, ND * NB_INCREASED * sizeof(int));
#endif

#ifdef ART_VISCOSITY
  myCudaMalloc(&dg->d_smthness, Nc * sizeof(double));
  gpuErrchk(cudaMemset(dg->d_smthness, 0, Nc * sizeof(double)));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  myCudaMalloc(&dg->d_smthrate, Nc * sizeof(double));
  gpuErrchk(cudaMemset(dg->d_smthrate, 0, Nc * sizeof(double)));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
#endif

  cudaMemGetInfo(&free_mem, &total_mem);
  gpu_printf("GPU memory after main allocations: %g / %g MB\n", (total_mem - free_mem) / 1e6, total_mem / 1e6);

  // dg_data_cuda_timer_stop(start, stop, &time);
  gpu_printf("Time to initialize memory on the device:  %3.1f ms \n", time);

  // copy the data to device
  dg_data_cuda_timer_start(&start, &stop);
  dg_data_copy_array_to_gpu(dg->bfunc_internal.size(), dg->bfunc_internal.data(), dg->d_bfunc_internal);

  dg_data_copy_array_to_gpu(dg->bfunc_external.size(), dg->bfunc_external.data(), dg->d_bfunc_external);

  dg_data_copy_array_to_gpu(dg->bfunc_internal_diff.size(), dg->bfunc_internal_diff.data(), dg->d_bfunc_internal_diff);

  dg_data_copy_array_to_gpu(dg->qpoint_weights_positivity.size(), dg->qpoint_weights_positivity.data(),
                            dg->d_qpoint_weights_positivity);

  dg_data_copy_array_to_gpu(dg->bfunc_mid.size(), dg->bfunc_mid.data(), dg->d_bfunc_mid);

  dg_data_copy_array_to_gpu(dg->bfunc_mid_diff.size(), dg->bfunc_mid_diff.data(), dg->d_bfunc_mid_diff);

#ifdef VISCOSITY
  for(uint d = 0; d < ND; d++)
    {
      dg_data_copy_array_to_gpu(dg->bfunc_project_list[d].size(), dg->bfunc_project_list[d].data(), dg->d_bfunc_project_list[d]);
      dg_data_copy_array_to_gpu(dg->bfunc_project_value_left[d].size(), dg->bfunc_project_value_left[d].data(),
                                dg->d_bfunc_project_value_left[d]);
      dg_data_copy_array_to_gpu(dg->bfunc_project_value_right[d].size(), dg->bfunc_project_value_right[d].data(),
                                dg->d_bfunc_project_value_right[d]);
    }

  dg_data_copy_array_to_gpu(ND * NB_INCREASED, dg->bfunc_project_count[0], (*dg->d_bfunc_project_count)[0]);
  dg_data_copy_array_to_gpu(ND * NB_INCREASED, dg->bfunc_project_first[0], (*dg->d_bfunc_project_first)[0]);
#endif

  size_t constant_cache_size = 0;

  gpuErrchk(cudaMemcpyToSymbol(d_qpoint_location, dg->qpoint_location.data(), dg->qpoint_location.size() * sizeof(double), 0,
                               cudaMemcpyHostToDevice));
  constant_cache_size += dg->qpoint_location.size();
  gpuErrchk(cudaMemcpyToSymbol(d_qpoint_weights_internal, dg->qpoint_weights_internal.data(),
                               dg->qpoint_weights_internal.size() * sizeof(double), 0, cudaMemcpyHostToDevice));
  constant_cache_size += dg->qpoint_weights_internal.size();
  gpuErrchk(cudaMemcpyToSymbol(d_qpoint_weights_external, dg->qpoint_weights_external.data(),
                               dg->qpoint_weights_external.size() * sizeof(double), 0, cudaMemcpyHostToDevice));
  constant_cache_size += dg->qpoint_weights_external.size();
  gpuErrchk(cudaMemcpyToSymbol(d_ButcherTab_a, dg->ButcherTab_a, RK_STAGES * RK_STAGES * sizeof(double), 0, cudaMemcpyHostToDevice));
  constant_cache_size += RK_STAGES * RK_STAGES;
  gpuErrchk(cudaMemcpyToSymbol(d_ButcherTab_b, dg->ButcherTab_b, RK_STAGES * sizeof(double), 0, cudaMemcpyHostToDevice));
  constant_cache_size += RK_STAGES;
  gpuErrchk(cudaMemcpyToSymbol(d_ButcherTab_c, dg->ButcherTab_c, RK_STAGES * sizeof(double), 0, cudaMemcpyHostToDevice));
  constant_cache_size += RK_STAGES;
#ifdef COOLING
  gpuErrchk(cudaMemcpyToSymbol(d_cooling_function_table, dg->cooling_function_table_gpu_data.cooling_function_table,
                               dg->cooling_function_table_gpu_data.temperature_bins_count * sizeof(double), 0,
                               cudaMemcpyHostToDevice));
  constant_cache_size += dg->cooling_function_table_gpu_data.temperature_bins_count;
  gpuErrchk(cudaMemcpyToSymbol(d_cooling_function_temperature_bins, dg->cooling_function_table_gpu_data.temperature_bins,
                               dg->cooling_function_table_gpu_data.temperature_bins_count * sizeof(double), 0,
                               cudaMemcpyHostToDevice));
  constant_cache_size += dg->cooling_function_table_gpu_data.temperature_bins_count;

  dg->cooling_function_table_gpu.set_cooling_function_table(d_cooling_function_table);
  dg->cooling_function_table_gpu.set_temperature_bins(d_cooling_function_temperature_bins);
#endif  // COOLING
  gpu_printf("Constant cache used: %lu B\n", constant_cache_size * sizeof(double));

  dg_data_cuda_timer_stop(start, stop, &time);
  gpu_printf("done!\n");
  gpu_printf("Time to copy data to device:  %3.1f ms, %3.0f MB/s\n", time,
             static_cast<double>(dg->bfunc_internal.size() + dg->bfunc_external.size() + dg->qpoint_location.size() +
                                 dg->qpoint_weights.size() + dg->bfunc_internal_diff.size()) *
                 static_cast<double>(sizeof(double)) * 1000. / 1024. / 1024. / time);

  // dg_data_copy_array_to_gpu(dg->N_w, dg->w.data(), dg->d_w);
  gpuErrchk(cudaMemcpy(dg->d_w, dg->w, dg->N_w * sizeof(double), cudaMemcpyHostToDevice));
#ifdef ART_VISCOSITY
  gpuErrchk(cudaMemcpy(dg->d_smthness, dg->smthness, Nc * sizeof(double), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(dg->d_smthrate, dg->smthrate, Nc * sizeof(double), cudaMemcpyHostToDevice));
#endif
}

void dg_data_setup_gpu_memory_usage(dg_data_t *dg)
{
  // the data used for time stepping
  double memory_usage = 1. * NB * NF * Nc * (RK_STAGES + 2);

  // if we're doing reduction account for that as well
#if((NB * NF * DG_NUM_INNER_QPOINTS * 8) > 166912)
  memory_usage += NB * NF * DG_NUM_INNER_QPOINTS * Nc;
#endif  // ((NB * NF * DG_NUM_INNER_QPOINTS * 8) > 166912)

  // data usage for limiting
  int numw = DG_NUM_CELLS_1D * NF;
#if(ND == 3)
  numw *= DG_NUM_CELLS_1D;
#endif
  memory_usage += 2 * numw;

  // surface fluxes
  int weights_on_plane = DG_NUM_CELLS_1D * NB * NF;
#if(ND == 3)
  weights_on_plane *= DG_NUM_CELLS_1D;
#endif  // (ND == 3)
  memory_usage += weights_on_plane;

// various kernels for turbulence driving
#ifdef TURBULENCE
  memory_usage += Nc;
  memory_usage += 10 * dg->StNModes;
#endif  // TURBULENCE

#if defined(USE_PRIMITIVE_PROJECTION) && !defined(DO_NOT_SAVE_W_PRIM)
  memory_usage += 1. * NB * NF * Nc;
#endif
  // memory usage in bytes
  memory_usage *= sizeof(double);

  // compare the predicted data usage to the free memory available
  size_t free_mem, total_mem;
  cudaSetDevice(LocalDeviceRank);
  cudaMemGetInfo(&free_mem, &total_mem);
  gpu_printf("Available memory: %lu MB\nPredicted memory usage: %5.1f MB\n", free_mem / 1024 / 1024, memory_usage / 1024 / 1024);
  if((double)free_mem < memory_usage)
    printf("WARNING: predicted memory usage is greater than available memory!\n");
}

#endif  // GPU
