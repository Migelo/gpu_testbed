// Copyright 2020 Miha Cernetic
#pragma once
#include "runrun.h"

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#endif

#include "../data/allvars.h"
#include "../data/data_types.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../tests/dg_geometry.hpp"
#include "../utils/cuda_utils.h"

constexpr uint64_t THREADS_PER_BLOCK = 256;

struct dg_data_for_gpu
{
  uint64_t Nc;
  uint64_t N_w;
  double cellSize;
  double faceArea;
  double cellVolume;
  double boxSize;
  double fac_area;
  double Time;
  double TimeStep;
  int current_rk_stage;
  double *d_bfunc_internal;
  double *d_bfunc_external;
  double *d_bfunc_internal_diff;
  double *d_qpoint_weights_positivity;
  double *d_bfunc_mid;
  double *d_bfunc_mid_diff;

  int FirstSlabThisTask;

  char trouble_detected;

#ifdef VISCOSITY
  int (*d_bfunc_project_count)[ND][NB_INCREASED];
  int (*d_bfunc_project_first)[ND][NB_INCREASED];

  int *d_bfunc_project_list[ND];
  double *d_bfunc_project_value_left[ND];
  double *d_bfunc_project_value_right[ND];
#endif

  double fac_cellsize;
#ifdef VISCOSITY
  double fac_cellsize_axis;
#endif  // VISCOSITY

#ifdef TURBULENCE
  int StNModes;
  double *d_StMode;
  double *d_StAmpl;
  double *d_StAka;
  double *d_StAkb;
  double StAmplFacStSolWeightNorm;
#ifdef DECAYING_TURBULENCE
  double StopDrivingAfterTime;
#endif  // DECAYING_TURBULENCE

#endif

  visc_params ViscParam;

#ifdef ART_VISCOSITY
  double *d_smthness;
  double *d_smthrate;
#endif

#ifdef COOLING
  double CoolingTimeDelay;
  CoolingFunctionTableGpu cooling_function_table;
#endif  // COOLING

#ifdef USE_PRIMITIVE_PROJECTION
  double *d_w_prim;
#endif  // USE_PRIMITIVE_PROJECTION
};

__host__ __device__ __forceinline__ uint64_t w_idx_internal(uint64_t b, uint64_t f, uint64_t c) { return b + f * NB + c * NB * NF; }

// double sum_array(int Nc, double *in);
void dg_compute_cuda(dg_data_t *dg, dg_data_for_gpu *d_dggpu, double *d_w, double *d_dwdt, char *d_troubled);
double getMaxTistepCpu(dg_data_t *dg);
double getMaxTistepUsingAllQpointsCpu(dg_data_t *dg);

void dg_reset_temperature_cuda(dg_data_t *dg, const dg_data_for_gpu *d_dggpu);
void dg_store_driving_energy_cuda(dg_data_t *dg, const dg_data_for_gpu *d_dggpu, int flag);

#ifdef TURBULENCE
__global__ void dg_store_driving_energy_gpu(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, double *w, double *EgyDrive, int flag);
__global__ void dg_reset_temperature_gpu(const uint64_t Nc, const dg_data_for_gpu *dggpu, double *w, double *EgyDiss);
__global__ void dg_reset_temperature_gpu_old(const uint64_t Nc, const dg_data_for_gpu *dggpu, double *w, double *EgyDiss);
#endif  //  TURBULENCE

// __host__ __device__ __forceinline__ uint64_t dw_shared_idx(const uint64_t b, const uint64_t f, const uint64_t q, const uint64_t c)
// {
//   return b + NB * (f + NF * (q + DG_NUM_INNER_QPOINTS * c));
// }

__global__ void fill_mean_cell_values(int numw, double *w, double *leftw_to_send, double *rightw_to_send, int celloffset);

__host__ __device__ __forceinline__ int32_t dw_shared_idx(int32_t b, int32_t f) { return b + NB * f; }

__host__ __device__ __forceinline__ uint64_t dw_shared_idx_no_reduction(uint64_t b, uint64_t f, uint64_t q)
{
  return b + NB * (f + NF * q);
}

__host__ __device__ __forceinline__ uint32_t bfunc_internal_idx(const uint32_t q, const uint32_t b)
{
  return q + DG_NUM_INNER_QPOINTS * b;
}

__host__ __device__ __forceinline__ uint32_t bfunc_external_side_idx(const uint32_t q, const uint32_t b, const int axis)
{
  return q + DG_NUM_OUTER_QPOINTS * b + NB * DG_NUM_OUTER_QPOINTS * axis;
}

__host__ __device__ __forceinline__ uint32_t bfunc_mid_idx(const uint32_t q, const uint32_t b, const int axis)
{
  return q + DG_NUM_OUTER_QPOINTS * b + NB_INCREASED * DG_NUM_OUTER_QPOINTS * axis;
}

__host__ __device__ __forceinline__ uint32_t bfunc_mid_diff_idx(const uint32_t q, const uint32_t b, const int diff_dir, const int axis)
{
  return q * (NB_INCREASED * ND * ND) + b * (ND * ND) + diff_dir * ND + axis;
}

__host__ __device__ __forceinline__ uint32_t bfunc_external_diff_side_idx(const uint32_t q, const uint32_t b, const int diff_dir,
                                                                          const int axis)
{
  return q * (NB * ND * (2 * ND)) + b * (ND * (2 * ND)) + diff_dir * (2 * ND) + axis;
}

__host__ __device__ __forceinline__ uint64_t bfunc_diff_internal_idx(uint64_t q, uint64_t b, uint64_t d)
{
  return q + DG_NUM_INNER_QPOINTS * b + DG_NUM_INNER_QPOINTS * NB * d;
}

__global__ void reduce_summation(int nelem, double *in, double *out);

__global__ void process_cell_calculate_no_reduction(const dg_data_for_gpu *d_dggpu, double *w, double *dwdt);

__global__ void calculate_internal_fluxes(const dg_data_for_gpu *d_dggpu, double *w, double *dwdt);

__global__ void compute_surface_fluxes(const dg_data_for_gpu *d_dggpu, int axis, int oddeven, int Nslabs, double *w, double *dwdt,
                                       fields_face *right_states_to_recv, fields_face *left_states_to_recv,
                                       slopes *right_slopes_to_recv, slopes *left_slopes_to_recv, char *troubled);

__global__ void compute_surface_fluxes_qpoint_for_loop(const dg_data_for_gpu *d_dggpu, int axis, int oddeven, int Nslabs, double *w,
                                                       double *dwdt, fields_face *right_states_to_recv,
                                                       fields_face *left_states_to_recv, slopes *right_slopes_to_recv,
                                                       slopes *left_slopes_to_recv, char *troubled, const int64_t max_c);

__global__ void extract_right_and_left_states(const dg_data_for_gpu *d_dggpu, int numstates, int Nslabs, double *d_w,
                                              fields_face *d_right_states_to_send, fields_face *d_left_states_to_send);

__global__ void compute_right_and_left_slopes(const dg_data_for_gpu *d_dggpu, int numstates, int Nslabs, double *d_w,
                                              slopes *d_right_slopes_to_send, slopes *d_left_states_to_send,
                                              fields_face *right_states_to_recv, fields_face *left_states_to_recv);

__device__ void dg_add_in_source_turbulence(int c, int qpoint_in_cell, double *fields, const dg_data_for_gpu *d_dggpu, state &st,
                                            double *dw_shared, const int dw_shared_index);

__device__ void dg_add_in_source_alpha(int c, int qpoint_in_cell, double *fields, double *grad_fields, const dg_data_for_gpu *d_dggpu,
                                       state &st, double *dw_shared);

__device__ void dg_add_in_source_gravity(int c, int qpoint_in_cell, double *fields, const dg_data_for_gpu *d_dggpu, state &st,
                                         double *dw_shared, const int dw_shared_index);

__global__ void copy_array(uint64_t n, double *src, double *dst);

void dg_advance_timestep_cuda(dg_data_t *dg, double dt);

double getMaxTistepGpu(dg_data_t *dg);

#ifdef GPU

template <typename T>
void dg_data_copy_array_to_gpu(const size_t n_elements, T *src, T *dst)
{
  // a templated function to copy array of any type to the gpu
  // n_elements - number of elements to copy
  // src - pointer to the host array
  // dst - pointer to the device array

  cudaEvent_t start = 0, stop = 0;
  float time = 0;
  size_t free_mem, total_mem, src_size;

  src_size = n_elements * sizeof(T);

  gpu_printf("Copying %lu elements to gpu..\n", n_elements);

  cudaMemGetInfo(&free_mem, &total_mem);
  if(free_mem < src_size)
    {
      Terminate(
          "Unable to copy to the GPU -- ran out of memory.\n"
          "Free memory left on device %lu MB, you are trying to copy %lu MB.\n",
          free_mem / 1024 / 1024, src_size / 1024 / 1024);
    }

  dg_data_cuda_timer_start(&start, &stop);

  gpuErrchk(cudaMemcpy(dst, src, src_size, cudaMemcpyHostToDevice));

  dg_data_cuda_timer_stop(start, stop, &time);
  gpu_printf("Time to copy to device:  %3.1f ms, %3.0f MB/s\n", time, static_cast<double>(src_size) * 1000. / 1024. / 1024. / time);
}

template <typename T>
void dg_data_copy_array_from_gpu(uint64_t n_elements, T *src, T *dst)
{
  // a templated function to copy array of any type from the gpu
  // n_elements - number of elements to copy
  // src - pointer to the device array
  // dst - pointer to the host array

  cudaEvent_t start = 0, stop = 0;
  float time = 0;

  const size_t src_size = n_elements * sizeof(T);

  gpu_printf("Copying %lu elements from gpu..\n", n_elements);

  dg_data_cuda_timer_start(&start, &stop);

  gpuErrchk(cudaMemcpy(dst, src, src_size, cudaMemcpyDeviceToHost));

  dg_data_cuda_timer_stop(start, stop, &time);
  gpu_printf("Time to copy from device:  %3.1f ms, %3.0f MB/s\n", time, static_cast<double>(src_size) * 1000. / 1024. / 1024. / time);
}
#endif  // GPU

__host__ __device__ __forceinline__ void get_conserved_fields_from_weights(const double *w, const double *bfunc_internal,
                                                                           double *fields, const int c, const int q)
{
  for(unsigned long f = 0; f < NF; f++)
    {
      fields[f] = 0;

      for(unsigned long b = 0; b < NB; b++)
        fields[f] += w[w_idx_internal(b, f, c)] * bfunc_internal[bfunc_internal_idx(q, b)];
    }
}

#ifdef ART_VISCOSITY
__global__ void smoothness_indicator_kernel(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, const double *w);
__global__ void smoothrate_indicator_kernel(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, const double *w, const double *dwdt);
__global__ void single_shock_clear(const uint64_t Nc, double *dwdt, int ThisTask, int NTask);
#endif  // ART_VISCOSITY

void check_cell_for_trouble_gpu(double *d_wtemp, dg_data_t *dg, dg_data_for_gpu dggpu, char *d_troubled);

void project_to_primitives_gpu(dg_data_t *dg, double *d_w, dg_data_for_gpu *d_dggpu);
