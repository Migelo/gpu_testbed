#include "runrun.h"

#include "./dg_cuda_memory.cuh"

#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../utils/cuda_utils.h"

using ::std::string;

#ifdef GPU

void myCudaFree_(void* devPtr, string name, size_t size, string file, int line)
{
  All.GpuMemoryAllocation -= size;
  memory_allocation deallocation;
  deallocation.name = "";
  deallocation.size = 0;
  deallocation.file = "";
  deallocation.line = 0;

  bool found = false;
  for(int i = 0; i < 100; i++)
    {
      if((All.memory_allocations[i].name == name) && (All.memory_allocations[i].size == size))
        {
          found                     = true;
          All.memory_allocations[i] = deallocation;
#ifdef DEBUG
          mpi_printf("deallocating %s for %s at %s:%d\n", name.c_str(), sizeof_fmt(size).c_str(), file.c_str(), line);
#endif  // DEBUG
          gpuErrchk(cudaFree(devPtr));
          break;
        }
    }
  if(!found)
    {
      dg_cuda_memory_writeout_allocations();
      Terminate("tried to deallocate unexisting %s with size %lu at %s:%d\ncheck memory_allocations.txt file\n",
                deallocation.name.c_str(), deallocation.size, deallocation.file.c_str(), deallocation.line);
    }
}

void dg_cuda_memory_writeout_allocations()
{
  FILE* fd;
  fd = fopen("memory_allocations.txt", "w");
  for(int i = 0; i < 100; i++)
    {
      auto allocation = All.memory_allocations[i];
      fprintf(fd, "%s size=%lu in %s:%d\n", allocation.name.c_str(), allocation.size, allocation.file.c_str(), allocation.line);
    }
  fclose(fd);
}

#endif  // GPU
