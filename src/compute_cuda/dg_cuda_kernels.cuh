#pragma once
#include "runrun.h"

#ifdef GPU

#include "../compute_cuda/dg_compute_cuda.cuh"

__global__ void multiply_butchertab_a(uint64_t n, int s, double dt, double *w, double *dwdt);
__global__ void multiply_butchertab_b(uint64_t n, int s, double dt, double *w, double *dwdt);
__global__ void multiply_butchertab_c(uint64_t n, int s, double dt, double *w, double *dwdt);
__global__ void copy_array(uint64_t n, double *src, double *dst);
__global__ void copy_array_scaled(uint64_t n, const double factor, double *src, double *dst);
__global__ void add_to_array(uint64_t n, const double addition, double *arr);
__global__ void scale_and_add_to_array(uint64_t n, const double factor, const double addition, double *src, double *dst);
__global__ void test_kernel_bandwidth(uint64_t n, double *w);
__global__ void multiply_weights(uint64_t N, double fac, double *dwdt);
__global__ void check_cell_for_trouble_kernel(double *w, double *w_temp, dg_data_for_gpu dggpu, char *troubled_array);
__global__ void project_to_primitives_kernel(double *w, dg_data_for_gpu *dggpu);
__global__ void setTroubleDetected(int value);
#endif  // GPU
