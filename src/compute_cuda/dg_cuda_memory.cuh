#ifndef DG_CUDA_MEMORY_CUH_
#define DG_CUDA_MEMORY_CUH_

#include "runrun.h"

#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../utils/cuda_utils.h"
#include "../utils/dg_utils.hpp"

#ifdef GPU

// void myCudaFree(void* devPtr, string name, size_t size, string file, int line);

void myCudaFree_(void* devPtr, string name, size_t size, string file, int line);
void dg_cuda_memory_writeout_allocations();

template <typename T>
void myCudaMalloc_(T** devPtr, string varName, size_t size, string file, int line)
{
  assert(size > 0);
  All.GpuMemoryAllocation += size;
  memory_allocation allocation;
  allocation.name = varName.substr(1, varName.length());
  allocation.size = size;
  allocation.file = file;
  allocation.line = line;
  mpi_printf("allocating %s for %s at %s:%d\n", varName.c_str(), sizeof_fmt((double)size).c_str(), file.c_str(), line);
  gpuErrchk(cudaMalloc(devPtr, size));

  for(int i = 0; i < 100; i++)
    {
      if(All.memory_allocations[i].size == 0)
        {
          All.memory_allocations[i] = allocation;
#ifdef DEBUG
          mpi_printf("allocating: %s size: %lu\n", varName.c_str(), allocation.size);
#endif  // DEBUG
          break;
        }
      else if(i == 99)
        {
          dg_cuda_memory_writeout_allocations();
          Terminate("too many allocations exist, exiting\ninspect memory_allocations.txt\n");
        }
    }
}
#define myCudaMalloc(devPtr, size) myCudaMalloc_(devPtr, GET_VARIABLE_NAME(devPtr), size, __FILE__, __LINE__)

#define myCudaFree(devPtr, size) myCudaFree_(devPtr, GET_VARIABLE_NAME(devPtr), size, __FILE__, __LINE__)

#endif  // GPU

#endif  // DG_CUDA_MEMORY_CUH_
