#include "runrun.h"

#ifdef GPU

#include <assert.h>
#include <cuda.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <random>
#include <vector>

#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/pair.h>

#include "../compute_cuda/device_variables.cuh"
#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../compute_cuda/dg_cuda_kernels.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../compute_cuda/dg_limiting_cuda.h"
#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../cooling/source_function.hpp"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../io/dg_io.hpp"
#include "../riemann/riemann_hllc.hpp"
#include "../tests/dg_geometry.hpp"
#include "../turbulence/dg_ab_turb.hpp"
#include "../units/units.h"
#include "../utils/cuda_utils.h"

__device__ int d_trouble_detected;

// to compute results with CUDA:
void dg_advance_timestep_cuda(dg_data_t *dg, double dt)
{
  // variables for timing
  float time, time_max;
  cudaEvent_t startEvent, stopEvent;

  double *d_wtemp, *d_dwdt;

  myCudaMalloc(&d_wtemp, dg->N_w * sizeof(double));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  myCudaMalloc(&d_dwdt, dg->N_w * RK_STAGES * sizeof(double));
  gpuErrchk(cudaMemset(d_dwdt, 0, dg->N_w * RK_STAGES * sizeof(double)));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

#if defined(USE_PRIMITIVE_PROJECTION) && defined(GPU) && !defined(DO_NOT_SAVE_W_PRIM)
  myCudaMalloc(&dg->d_w_prim, dg->N_w * sizeof(double));
  gpuErrchk(cudaMemset(dg->d_w_prim, 0, dg->N_w * sizeof(double)));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
#endif  // USE_PRIMITIVE_PROJECTION GPU

  // set up auxiliary data we need on the GPU
  dg_data_for_gpu dggpu;
  {
    dggpu.Nc                          = Nc;
    dggpu.N_w                         = dg->N_w;
    dggpu.cellSize                    = dg->getCellSize();
    dggpu.faceArea                    = dg->getFaceArea();
    dggpu.cellVolume                  = dg->getCellVolume();
    dggpu.boxSize                     = dg->getBoxSize();
    dggpu.Time                        = dg->Time;
    dggpu.TimeStep                    = dg->TimeStep;
    dggpu.current_rk_stage            = dg->current_rk_stage;
    dggpu.d_bfunc_internal            = dg->d_bfunc_internal;
    dggpu.d_bfunc_external            = dg->d_bfunc_external;
    dggpu.d_bfunc_internal_diff       = dg->d_bfunc_internal_diff;
    dggpu.d_qpoint_weights_positivity = dg->d_qpoint_weights_positivity;
    dggpu.d_bfunc_mid                 = dg->d_bfunc_mid;
    dggpu.d_bfunc_mid_diff            = dg->d_bfunc_mid_diff;
    dggpu.FirstSlabThisTask           = FirstSlabThisTask;

#ifdef VISCOSITY
#ifdef ART_VISCOSITY
    dggpu.d_smthness = dg->d_smthness;
    dggpu.d_smthrate = dg->d_smthrate;
#endif

    for(long unsigned d = 0; d < ND; d++)
      {
        dggpu.d_bfunc_project_list[d]        = dg->d_bfunc_project_list[d];
        dggpu.d_bfunc_project_value_left[d]  = dg->d_bfunc_project_value_left[d];
        dggpu.d_bfunc_project_value_right[d] = dg->d_bfunc_project_value_right[d];
      }

    dggpu.d_bfunc_project_count = dg->d_bfunc_project_count;
    dggpu.d_bfunc_project_first = dg->d_bfunc_project_first;
#endif

    dggpu.fac_cellsize = 2.0 / dg->getCellSize();
#ifdef VISCOSITY
    dggpu.fac_cellsize_axis = 2.0 / dg->getCellSize() / (2 * dg->legendre_overlapp_eta);
#endif  // VISCOSITY
    dggpu.ViscParam = All.ViscParam;

#ifdef TURBULENCE
    dggpu.StNModes                 = dg->StNModes;
    dggpu.d_StMode                 = dg->d_StMode;
    dggpu.d_StAmpl                 = dg->d_StAmpl;
    dggpu.d_StAka                  = dg->d_StAka;
    dggpu.d_StAkb                  = dg->d_StAkb;
    dggpu.StAmplFacStSolWeightNorm = All.StAmplFac * dg->StSolWeightNorm;
#ifdef DECAYING_TURBULENCE
    dggpu.StopDrivingAfterTime = All.StopDrivingAfterTime;
#endif  // DECAYING_TURBULENCE
#endif  // TURBULENCE

#if(ND == 1)
    dggpu.fac_area = dggpu.faceArea;
#endif
#if(ND == 2)
    dggpu.fac_area = 0.5 * dggpu.faceArea;
#endif
#if(ND == 3)
    dggpu.fac_area = 0.25 * dggpu.faceArea;
#endif

#ifdef COOLING
    dggpu.CoolingTimeDelay = All.CoolingTimeDelay;

    dggpu.cooling_function_table = dg->cooling_function_table_gpu;
#endif  // COOLING

#if defined(USE_PRIMITIVE_PROJECTION) && !defined(DO_NOT_SAVE_W_PRIM)
    dggpu.d_w_prim = dg->d_w_prim;
#endif  // USE_PRIMITIVE_PROJECTION
  }

  // push the dggpu structure to the GPU
  dg_data_for_gpu *d_dggpu;
  myCudaMalloc(&d_dggpu, sizeof(dg_data_for_gpu));
  gpuErrchk(cudaMemcpy(d_dggpu, &dggpu, sizeof(dg_data_for_gpu), cudaMemcpyHostToDevice));

  gpu_printf("Starting a CUDA DG timestep\n");

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  char *d_troubled;
  myCudaMalloc(&d_troubled, Nc * sizeof(char));
  gpuErrchk(cudaMemset(d_troubled, 0, Nc * sizeof(char)));
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  int n_troubled_cells                = 0;
  int n_troubled_cells_total          = 0;
  int previous_n_troubled_cells_total = 0;

  int timestep_reduction_count = 0;

#ifdef COOLING
  measure_box_temperature_gpu(dg, d_dggpu);
#endif  // COOLING

  do
    {
      int trouble_detected = 0;
      setTroubleDetected<<<1, 1>>>(0);
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());

      for(int s = 0; s < RK_STAGES; s++)
        {
          dg->current_rk_stage   = s;
          dggpu.current_rk_stage = s;
#ifdef GPU
          cudaMemcpy(&(d_dggpu->current_rk_stage), &s, sizeof(int), cudaMemcpyHostToDevice);
#endif

          gpu_printf("s = %d | RK_STAGES=%d\n", s, RK_STAGES);

          // we copy the d_w array contents to d_wtemp to be used for intermediate RK steps
          gpuErrchk(cudaEventCreate(&startEvent));
          gpuErrchk(cudaEventCreate(&stopEvent));
          gpuErrchk(cudaEventRecord(startEvent, 0));
          // copy_array<<<(dg->N_w + 255) / 256, 256>>>(dg->N_w, dg->d_w, d_wtemp);
          gpuErrchk(cudaMemcpy(d_wtemp, dg->d_w, dg->N_w * sizeof(double), cudaMemcpyDeviceToDevice));
          gpuErrchk(cudaPeekAtLastError());
          gpuErrchk(cudaDeviceSynchronize());
          gpuErrchk(cudaEventRecord(stopEvent, 0));
          gpuErrchk(cudaEventSynchronize(stopEvent));
          gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
          MPI_Reduce(&time_max, &time, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
          mpi_printf("[copy_array_time]: %f ms\n", time);

          if(s != 0)
            {
              // multiply with ButcherTab_a
              // w_temp[i] += (dg->ButcherTab_a[s][m] * dt) * dWdt[m][i];
              gpuErrchk(cudaEventCreate(&startEvent));
              gpuErrchk(cudaEventCreate(&stopEvent));
              gpuErrchk(cudaEventRecord(startEvent, 0));
              multiply_butchertab_a<<<(dg->N_w + 255) / 256, 256>>>(dg->N_w, s, dt, d_wtemp, d_dwdt);
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());

              gpuErrchk(cudaEventRecord(stopEvent, 0));
              gpuErrchk(cudaEventSynchronize(stopEvent));
              gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
              MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
              mpi_printf("[multiply_butchertab_a_time]: %f ms\n", time);

              // first, let's check whether the current intermediate state would be valid
              // trouble_detected =
              check_cell_for_trouble_gpu(d_wtemp, dg, dggpu, d_troubled);
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());

              // using thrust sum up the array
              thrust::device_ptr<char> d_troubled_ptr(d_troubled);
              n_troubled_cells = thrust::reduce(d_troubled_ptr, d_troubled_ptr + Nc, (int)0);

              MPI_Allreduce(MPI_IN_PLACE, &n_troubled_cells, 1, MPI_INT, MPI_SUM, MyComm);

              gpuErrchk(cudaMemcpyFromSymbol(&trouble_detected, d_trouble_detected, sizeof(int)));
              MPI_Allreduce(MPI_IN_PLACE, &trouble_detected, 1, MPI_INT, MPI_MAX, MyComm);

              if(trouble_detected)  // we break in this case and will abandon this step
                {
                  mpi_printf("[check_cell_for_trouble_gpu] detected trouble: %d times, breaking at the start\n", n_troubled_cells);
                  break;
                }
            }

#ifdef COOLING
          enforce_temperature_floor_gpu(dg, d_wtemp, d_dggpu);
#endif  // COOLING

          // now do main computation of the right-hand-side
          dg_compute_cuda(dg, d_dggpu, d_wtemp, d_dwdt + (dg->N_w * s), d_troubled);

#ifdef ART_VISCOSITY
          if(s == RK_STAGES - 1)
            {
              gpuErrchk(cudaEventCreate(&startEvent));
              gpuErrchk(cudaEventCreate(&stopEvent));
              gpuErrchk(cudaEventRecord(startEvent, 0));

              smoothness_indicator_kernel<<<(Nc + 255) / 256, 256>>>(Nc, d_dggpu, d_wtemp);
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());
              smoothrate_indicator_kernel<<<(Nc + 255) / 256, 256>>>(Nc, d_dggpu, d_wtemp, d_dwdt + (dg->N_w * s));
              gpuErrchk(cudaPeekAtLastError());
              gpuErrchk(cudaDeviceSynchronize());

              gpuErrchk(cudaEventRecord(stopEvent, 0));
              gpuErrchk(cudaEventSynchronize(stopEvent));
              gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
              MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
              mpi_printf("[smoothness_indicator_kernel_time]: %f ms\n", time);
            }
#endif  // ART_VISCOSITY
        }

      if(trouble_detected == false)
        {
          // we are OK so far, determine the final weights
          // first copy old weights to d_wtemp
          gpuErrchk(cudaEventCreate(&startEvent));
          gpuErrchk(cudaEventCreate(&stopEvent));
          gpuErrchk(cudaEventRecord(startEvent, 0));
          gpuErrchk(cudaMemcpy(d_wtemp, dg->d_w, dg->N_w * sizeof(double), cudaMemcpyDeviceToDevice));
          gpuErrchk(cudaPeekAtLastError());
          gpuErrchk(cudaDeviceSynchronize());
          gpuErrchk(cudaEventRecord(stopEvent, 0));
          gpuErrchk(cudaEventSynchronize(stopEvent));
          gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
          MPI_Reduce(&time_max, &time, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
          mpi_printf("[copy_array_time]: %f ms\n", time);
          // then perform the multiply_butchertab_b

          // we are OK so far, determine the final weights
          // ButcherTab_b updates
          // dg->w[i] += (dg->ButcherTab_b[s] * dt) * dWdt[s][i];
          gpuErrchk(cudaEventCreate(&startEvent));
          gpuErrchk(cudaEventCreate(&stopEvent));
          gpuErrchk(cudaEventRecord(startEvent, 0));
          multiply_butchertab_b<<<(dg->N_w + 255) / 256, 256>>>(dg->N_w, RK_STAGES, dt, d_wtemp, d_dwdt);
          gpuErrchk(cudaPeekAtLastError());
          gpuErrchk(cudaDeviceSynchronize());
          gpuErrchk(cudaEventRecord(stopEvent, 0));
          gpuErrchk(cudaEventSynchronize(stopEvent));
          gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
          MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
          mpi_printf("[multiply_butchertab_b_time]: %f ms\n", time);

          // check whether the final state would run into problems
          check_cell_for_trouble_gpu(d_wtemp, dg, dggpu, d_troubled);
          gpuErrchk(cudaPeekAtLastError());
          gpuErrchk(cudaDeviceSynchronize());

          // using thrust sum up the array
          thrust::device_ptr<char> d_troubled_ptr(d_troubled);
          n_troubled_cells = thrust::reduce(d_troubled_ptr, d_troubled_ptr + Nc, (int)0);

          gpuErrchk(cudaMemcpyFromSymbol(&trouble_detected, d_trouble_detected, sizeof(int)));
        }
      MPI_Allreduce(MPI_IN_PLACE, &trouble_detected, 1, MPI_INT, MPI_MAX, MyComm);
      MPI_Allreduce(&n_troubled_cells, &n_troubled_cells_total, 1, MPI_INT, MPI_SUM, MyComm);

      if(trouble_detected == false)
        {
#ifdef TURBULENCE
          dg_store_driving_energy_cuda(dg, d_dggpu, 0);
#endif  // TURBULENCE

          gpuErrchk(cudaEventCreate(&startEvent));
          gpuErrchk(cudaEventCreate(&stopEvent));
          gpuErrchk(cudaEventRecord(startEvent, 0));
          gpuErrchk(cudaMemcpy(dg->d_w, d_wtemp, dg->N_w * sizeof(double), cudaMemcpyDeviceToDevice));
          gpuErrchk(cudaPeekAtLastError());
          gpuErrchk(cudaDeviceSynchronize());
          gpuErrchk(cudaEventRecord(stopEvent, 0));
          gpuErrchk(cudaEventSynchronize(stopEvent));
          gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
          MPI_Reduce(&time_max, &time, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
          mpi_printf("[copy_array_time]: %f ms\n", time);

#ifdef TURBULENCE
          dg_store_driving_energy_cuda(dg, d_dggpu, 1);
          dg_reset_temperature_cuda(dg, d_dggpu);
#endif  // TURBULENCE
          break;
        }
      else
        {
          timestep_reduction_count++;

          if(n_troubled_cells_total == previous_n_troubled_cells_total)
            {
              // continue - no new troubled cells found, so let's try to continue the step again with half the step size

              dt /= 2;
              dg->TimeStep /= 2;
              dggpu.TimeStep = dg->TimeStep;
              gpuErrchk(cudaMemcpy(&(d_dggpu->TimeStep), &(dg->TimeStep), sizeof(double), cudaMemcpyHostToDevice));
            }
        }

      if(timestep_reduction_count > 100)
        {
          Terminate("Iteration count exceeded. Something is probably wrong\n");
        }

      previous_n_troubled_cells_total = n_troubled_cells_total;
      mpi_printf("REPEAT needed (%d iteration, %d troubled cells, frac=%g) timestep %g\n", timestep_reduction_count,
                 n_troubled_cells_total, (double)n_troubled_cells_total / pow(DG_NUM_CELLS_1D, ND), dt);
    }
  while(true);

#ifdef COOLING
  enforce_temperature_floor_gpu(dg, dg->d_w, d_dggpu);
#endif  // COOLING

  myCudaFree(d_dggpu, sizeof(dg_data_for_gpu));
  myCudaFree(d_dwdt, dg->N_w * RK_STAGES * sizeof(double));
  myCudaFree(d_wtemp, dg->N_w * sizeof(double));
  myCudaFree(d_troubled, Nc * sizeof(char));
#if defined(USE_PRIMITIVE_PROJECTION) && defined(GPU) && !defined(DO_NOT_SAVE_W_PRIM)
  myCudaFree(dg->d_w_prim, dg->N_w * sizeof(double));
#endif  // USE_PRIMITIVE_PROJECTION GPU
}

void dg_compute_cuda(dg_data_t *dg, dg_data_for_gpu *d_dggpu, double *d_w, double *d_dwdt, char *d_troubled)
{
#if defined(USE_PRIMITIVE_PROJECTION) && !defined(DO_NOT_SAVE_W_PRIM)
  project_to_primitives_gpu(dg, d_w, d_dggpu);
#endif  // USE_PRIMITIVE_PROJECTION

  // events for timing
  cudaEvent_t startEvent, stopEvent;
  float time, time_max;

  // clear derivative
  gpuErrchk(cudaMemset(d_dwdt, 0, dg->N_w * sizeof(double)));

  const double fac_vol = 1.0 / dg->getCellVolume();

  // ------------------------------------------------------------------------------------------------------------------------------------
  // first, compute the states at the quadpoints on the right side of the leftmost slab interface
  // ------------------------------------------------------------------------------------------------------------------------------------

  constexpr int states_on_plane = cell_stride_x * DG_NUM_OUTER_QPOINTS;

  fields_face *d_right_states_to_send;
  myCudaMalloc(&d_right_states_to_send, states_on_plane * sizeof(fields_face));
  fields_face *d_left_states_to_send;
  myCudaMalloc(&d_left_states_to_send, states_on_plane * sizeof(fields_face));
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  extract_right_and_left_states<<<(states_on_plane + 255) / 256, 256>>>(d_dggpu, states_on_plane, Nslabs, d_w, d_right_states_to_send,
                                                                        d_left_states_to_send);

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[extract_right_and_left_states_time]: %f ms\n", time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // copy right_states_to_send from the gpu
  // ----------------------------------------------------------------------------------------------------------------------------

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  gpuErrchk(
      cudaMemcpy(dg->right_states_to_send, d_right_states_to_send, states_on_plane * sizeof(fields_face), cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(dg->left_states_to_send, d_left_states_to_send, states_on_plane * sizeof(fields_face), cudaMemcpyDeviceToHost));

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[cudaMemcpy_right_and_left_states_to_send_time]: %f ms, %f GB/s\n", time,
             2.0 * states_on_plane * sizeof(fields_face) / time / 1e6);

  // ----------------------------------------------------------------------------------------------------------------------------
  // initiate MPI transfer of the plane of states
  // ----------------------------------------------------------------------------------------------------------------------------
  cudaEvent_t startEventAsync, stopEventAsync;
  gpuErrchk(cudaEventCreate(&startEventAsync));
  gpuErrchk(cudaEventCreate(&stopEventAsync));
  gpuErrchk(cudaEventRecord(startEventAsync, 0));
  int leftTask  = ThisTask > 0 ? ThisTask - 1 : NTask - 1;
  int rightTask = ThisTask < NTask - 1 ? ThisTask + 1 : 0;

  MPI_Request requests[4];

  MPI_Isend(dg->right_states_to_send, states_on_plane * sizeof(fields_face), MPI_BYTE, leftTask, TAG_RIGHT, MyComm, &requests[0]);
  MPI_Irecv(dg->right_states_to_recv, states_on_plane * sizeof(fields_face), MPI_BYTE, rightTask, TAG_RIGHT, MyComm, &requests[1]);
  MPI_Isend(dg->left_states_to_send, states_on_plane * sizeof(fields_face), MPI_BYTE, rightTask, TAG_LEFT, MyComm, &requests[2]);
  MPI_Irecv(dg->left_states_to_recv, states_on_plane * sizeof(fields_face), MPI_BYTE, leftTask, TAG_LEFT, MyComm, &requests[3]);

  // ----------------------------------------------------------------------------------------------------------------------------
  // next, compute the contribution from the internal volume integral
  // ----------------------------------------------------------------------------------------------------------------------------
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

// process_cell internal fluxes kernel
#define USE_SLOW_INTERNAL_FLUX false
#if(USE_SLOW_INTERNAL_FLUX)
  mpi_printf("calculate_internal_fluxes block size = %d\n", INTERNAL_KERNEL_BLOCK_SIZE);
  cudaFuncSetAttribute(calculate_internal_fluxes, cudaFuncAttributeMaxDynamicSharedMemorySize,
                       (NB * NF * INTERNAL_KERNEL_BLOCK_SIZE * 8));
  calculate_internal_fluxes<<<Nc, INTERNAL_KERNEL_BLOCK_SIZE, NB * NF * INTERNAL_KERNEL_BLOCK_SIZE * 8>>>(d_dggpu, d_w, d_dwdt);
#else   //(DEGREE_K > 1)
  constexpr uint64_t THREADS_PER_BLOCK = 32;
  mpi_printf("calculate_internal_fluxes block size = %d\n", THREADS_PER_BLOCK);
  calculate_internal_fluxes<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(d_dggpu, d_w, d_dwdt);
#endif  // (DEGREE_K > 1)
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[internal_volume_integral_time]: %f ms\n", time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // check that the MPI transfers have finished
  // ----------------------------------------------------------------------------------------------------------------------------
  MPI_Waitall(4, requests, MPI_STATUSES_IGNORE);
  gpuErrchk(cudaEventRecord(stopEventAsync, 0));
  gpuErrchk(cudaEventSynchronize(stopEventAsync));
  gpuErrchk(cudaEventElapsedTime(&time, startEventAsync, stopEventAsync));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[MPI_right_and_left_states_to_send_recv_time]: %f ms, %f GB/s\n", time,
             2.0 * states_on_plane * sizeof(fields_face) / 1e6 / time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // copy right and left states to the gpu
  // ----------------------------------------------------------------------------------------------------------------------------
  fields_face *d_right_states_to_recv = d_right_states_to_send;  // we can recycle this buffer here
  fields_face *d_left_states_to_recv  = d_left_states_to_send;   // we can recycle this buffer here
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  gpuErrchk(
      cudaMemcpy(d_right_states_to_recv, dg->right_states_to_recv, states_on_plane * sizeof(fields_face), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(d_left_states_to_recv, dg->left_states_to_recv, states_on_plane * sizeof(fields_face), cudaMemcpyHostToDevice));
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[cudaMemcpy_right_states_to_recv_time]: %f ms, %3.1f GB/s\n", time,
             2.0 * states_on_plane * sizeof(fields_face) / 1e6 / time);

#ifdef FINITE_VOLUME

  // ----------------------------------------------------------------------------------------------------------------------------
  // compute slopes on leftmost and rightmost slabs
  // ----------------------------------------------------------------------------------------------------------------------------
  slopes *d_right_slopes_to_send;
  myCudaMalloc(&d_right_slopes_to_send, cell_stride_x * sizeof(slopes));
  slopes *d_left_slopes_to_send;
  myCudaMalloc(&d_left_slopes_to_send, cell_stride_x * sizeof(slopes));
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  // execute slope computation kernel for surface planes
  compute_right_and_left_slopes<<<(cell_stride_x + 255) / 256, 256>>>(d_dggpu, states_on_plane, Nslabs, d_w, d_right_slopes_to_send,
                                                                      d_left_slopes_to_send, d_right_states_to_recv,
                                                                      d_left_states_to_recv);

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[compute_right_and_left_slopes time]: %f ms\n", time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // fetch right and left slopes from the gpu
  // ----------------------------------------------------------------------------------------------------------------------------

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  gpuErrchk(cudaMemcpy(dg->right_slopes_to_send, d_right_slopes_to_send, cell_stride_x * sizeof(slopes), cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy(dg->left_slopes_to_send, d_left_slopes_to_send, cell_stride_x * sizeof(slopes), cudaMemcpyDeviceToHost));

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[cudaMemcpy_right_and_left_slopes_to_send_time]: %f ms, %f GB/s\n", time,
             2.0 * cell_stride_x * sizeof(slopes) / time / 1e6);

  // ----------------------------------------------------------------------------------------------------------------------------
  // initiate MPI transfer of the plane of slopes
  // ----------------------------------------------------------------------------------------------------------------------------
  gpuErrchk(cudaEventCreate(&startEventAsync));
  gpuErrchk(cudaEventCreate(&stopEventAsync));
  gpuErrchk(cudaEventRecord(startEventAsync, 0));

  MPI_Isend(dg->right_slopes_to_send, cell_stride_x * sizeof(slopes), MPI_BYTE, leftTask, TAG_RIGHT, MyComm, &requests[0]);
  MPI_Irecv(dg->right_slopes_to_recv, cell_stride_x * sizeof(slopes), MPI_BYTE, rightTask, TAG_RIGHT, MyComm, &requests[1]);
  MPI_Isend(dg->left_slopes_to_send, cell_stride_x * sizeof(slopes), MPI_BYTE, rightTask, TAG_LEFT, MyComm, &requests[2]);
  MPI_Irecv(dg->left_slopes_to_recv, cell_stride_x * sizeof(slopes), MPI_BYTE, leftTask, TAG_LEFT, MyComm, &requests[3]);

  MPI_Waitall(4, requests, MPI_STATUSES_IGNORE);
  gpuErrchk(cudaEventRecord(stopEventAsync, 0));
  gpuErrchk(cudaEventSynchronize(stopEventAsync));
  gpuErrchk(cudaEventElapsedTime(&time, startEventAsync, stopEventAsync));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[MPI_right_and_left_slopes_to_send_recv_time]: %f ms, %f GB/s\n", time,
             2.0 * cell_stride_x * sizeof(slopes) / 1e6 / time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // copy right and left slopes to the gpu
  // ----------------------------------------------------------------------------------------------------------------------------
  slopes *d_right_slopes_to_recv = d_right_slopes_to_send;  // we can recycle this buffer here
  slopes *d_left_slopes_to_recv  = d_left_slopes_to_send;   // we can recycle this buffer here
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  gpuErrchk(cudaMemcpy(d_right_slopes_to_recv, dg->right_slopes_to_recv, cell_stride_x * sizeof(slopes), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(d_left_slopes_to_recv, dg->left_slopes_to_recv, cell_stride_x * sizeof(slopes), cudaMemcpyHostToDevice));
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[cudaMemcpy_right_and_left_slopes_to_recv_time]: %f ms, %3.1f GB/s\n", time,
             2.0 * cell_stride_x * sizeof(slopes) / 1e6 / time);
#else
  slopes *d_right_slopes_to_recv = NULL;
  slopes *d_left_slopes_to_recv  = NULL;
#endif

  // ----------------------------------------------------------------------------------------------------------------------------
  // now let's compute the surface fluxes
  // ----------------------------------------------------------------------------------------------------------------------------
  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

#define USE_SLOW_EXTERNAL_FLUX false
  {
#if(USE_SLOW_EXTERNAL_FLUX)
    mpi_printf("executing surface flux kernel %d times with %d blocks of %d threads\n", (int)2 * ND, (int)Nc / 2,
               (int)EXTERNAL_KERNEL_BLOCK_SIZE);
#else   // USE_SLOW_EXTERNAL_FLUX
    const auto blocks                    = Nc / 2;
    constexpr uint64_t THREADS_PER_BLOCK = 32;
    mpi_printf("executing surface flux kernel %d times with %d blocks of %d threads\n", (int)2 * ND,
               (int)((blocks + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK), (int)THREADS_PER_BLOCK);
#endif  // USE_SLOW_EXTERNAL_FLUX
  }

  for(decltype(ND) axis = 0; axis < ND; axis++)
    for(int oddeven = 0; oddeven < 2; oddeven++)
      {
        mpi_printf("axis=%d oddeven=%d cell_stride_x=%d\n", axis, oddeven, cell_stride_x);
        auto blocks = Nc / 2;
        if(axis == 0 && oddeven == 0)
          blocks += cell_stride_x;

#if(USE_SLOW_EXTERNAL_FLUX)
        cudaFuncSetAttribute(compute_surface_fluxes, cudaFuncAttributeMaxDynamicSharedMemorySize,
                             (NB * NF * EXTERNAL_KERNEL_BLOCK_SIZE * 2 * 8));
        compute_surface_fluxes<<<blocks, EXTERNAL_KERNEL_BLOCK_SIZE, NB * NF * EXTERNAL_KERNEL_BLOCK_SIZE * 2 * 8>>>(
            d_dggpu, axis, oddeven, Nslabs, d_w, d_dwdt, d_right_states_to_recv, d_left_states_to_recv, d_right_slopes_to_recv,
            d_left_slopes_to_recv, d_troubled);
#else   // USE_SLOW_EXTERNAL_FLUX
        constexpr uint64_t THREADS_PER_BLOCK = 32;
        compute_surface_fluxes_qpoint_for_loop<<<(blocks + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(
            d_dggpu, axis, oddeven, Nslabs, d_w, d_dwdt, d_right_states_to_recv, d_left_states_to_recv, d_right_slopes_to_recv,
            d_left_slopes_to_recv, d_troubled, blocks);
#endif  // USE_SLOW_EXTERNAL_FLUX

        gpuErrchk(cudaPeekAtLastError());
        gpuErrchk(cudaDeviceSynchronize());
      }
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));

  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[compute_surface_fluxes_time]: %f ms\n", time);

  // ----------------------------------------------------------------------------------------------------------------------------
  // we are basically done at this point, free auxiliary storage
  // ----------------------------------------------------------------------------------------------------------------------------

#ifdef FINITE_VOLUME
  myCudaFree(d_left_slopes_to_send, cell_stride_x * sizeof(slopes));
  myCudaFree(d_right_slopes_to_send, cell_stride_x * sizeof(slopes));
#endif
  myCudaFree(d_left_states_to_send, states_on_plane * sizeof(fields_face));
  myCudaFree(d_right_states_to_send, states_on_plane * sizeof(fields_face));

  // ----------------------------------------------------------------------------------------------------------------------------
  // normalize result
  // ----------------------------------------------------------------------------------------------------------------------------

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  multiply_weights<<<(dg->N_w + 255) / 256, 256>>>(dg->N_w, fac_vol, d_dwdt);

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[multiply_weights_time]: %f ms\n", time);

#if defined(SINGLE_SHOCK)
  single_shock_clear<<<(dg->N_w + 255) / 256, 256>>>(Nc, d_dwdt, ThisTask, NTask);
#endif
}

#ifdef FINITE_VOLUME
__global__ void compute_right_and_left_slopes(const dg_data_for_gpu *d_dggpu, int numstates, int Nslabs, double *d_w,
                                              slopes *d_right_slopes_to_send, slopes *d_left_slopes_to_send,
                                              fields_face *right_states_to_recv, fields_face *left_states_to_recv)
{
  const auto i = threadIdx.x + blockDim.x * blockIdx.x;

  if(i < numstates)
    {
      for(int phase = 0; phase < 2; phase++)
        {
          int x = (phase == 0) ? 0 : Nslabs - 1;

          int cell         = i + cell_stride_x * x;
          const int cell_L = cell - cell_stride_x;
          const int cell_R = cell + cell_stride_x;

          double fields_L[NF];
          double fields[NF];
          double fields_R[NF];

          // note: for FV, we have NB=1 and the bfunc-values are just 1, thus no need to loop over the basis functions
          for(int f = 0; f < NF; f++)
            {
              fields_L[f] = (x > 0) ? d_w[w_idx_internal(0, f, cell_L)] : left_states_to_recv[i].fields[f];
              fields[f]   = d_w[w_idx_internal(0, f, cell)];
              fields_R[f] = (x < Nslabs - 1) ? d_w[w_idx_internal(0, f, cell_R)] : right_states_to_recv[i].fields[f];
            }

          state st_L;
          get_primitive_state_from_conserved_fields(fields_L, st_L, &d_dggpu->ViscParam);
          state st;
          get_primitive_state_from_conserved_fields(fields, st, &d_dggpu->ViscParam);
          state st_R;
          get_primitive_state_from_conserved_fields(fields_R, st_R, &d_dggpu->ViscParam);

          slopes &sl = (phase == 0) ? d_right_slopes_to_send[i] : d_left_slopes_to_send[i];
          do_slope_limiting(st_L, st, st_R, 0, sl);
        }
    }
}
#endif

#if(USE_SLOW_INTERNAL_FLUX)
__global__ void calculate_internal_fluxes(const dg_data_for_gpu *d_dggpu, double *w, double *dwdt)
{
  // 1. weights to conserved variables
  // 2. conserved variables to flux
  // 3. flux to update weights

  // blockDim.x = INTERNAL_KERNEL_BLOCK_SIZE
  // blockIdx.x = 0...ncells

  extern __shared__ double dw_shared[];

  for(int i = threadIdx.x; i < NF * NB * INTERNAL_KERNEL_BLOCK_SIZE; i += INTERNAL_KERNEL_BLOCK_SIZE)
    dw_shared[i] = 0;
  __syncthreads();

  const int c = blockIdx.x;

  for(int q = threadIdx.x; q < DG_NUM_INNER_QPOINTS; q += INTERNAL_KERNEL_BLOCK_SIZE)
    {
      double fields[NF];  // storage for conserved variables

      for(int i = 0; i < NF; i++)
        fields[i] = 0;

      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) b = 0; b < NB; b++)
          fields[f] += w[w_idx_internal(b, f, c)] * d_dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)];

      // calculate primitive variables
      state st;
      get_primitive_state_from_conserved_fields(fields, st, &d_dggpu->ViscParam);

#ifdef RICHTMYER_VISCOSITY
      // evaluate velocity divergence
      double divvel = 0;
      for(int d = 0; d < ND; d++)
        {
          double dgrad[1 + ND] = {0};
          for(int f = 0; f < 1 + ND; f++)
            for(int b = 0; b < NB; b++)
              dgrad[f] += d_dggpu->fac_cellsize * w[w_idx_internal(b, f, c)] *
                          d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];

          divvel += (dgrad[1 + d] - st.s[1 + d] * dgrad[0]) / st.rho;
        }

      if(divvel < 0)
        {
          double csnd = sqrt(GAMMA * st.press / st.rho);

          double press = d_dggpu->ViscParam.ArtViscRichtmyer * st.rho * pow(d_dggpu->ViscParam.Resolution * divvel, 2) +
                         d_dggpu->ViscParam.ArtViscLandshoff * st.rho * fabs(d_dggpu->ViscParam.Resolution * divvel) * csnd;

          double press_max = 0.5 * st.rho * pow(d_dggpu->ViscParam.Resolution, 2) * fabs(divvel) / d_dggpu->TimeStep;

          if(press > press_max)
            press = press_max;

          st.press += press;
        }
#endif

      // calculate full flux
      double flux[ND * NF];
      get_flux_from_primitive_state_and_conserved_fields(fields, st, flux);

#ifdef VISCOSITY
      // evaluate gradient of conserved states
      double grad_fields[NF * ND];

      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) d = 0; d < ND; d++)
          {
            grad_fields[f * ND + d] = 0;

            for(decltype(ND) b = 0; b < NB; b++)
              grad_fields[f * ND + d] += d_dggpu->fac_cellsize * w[w_idx_internal(b, f, c)] *
                                         d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];
          }

      // calculate viscous flux internally
      double viscous_flux[NF * ND];
      get_viscous_flux_from_conserved_fields_and_their_gradients(fields, grad_fields, viscous_flux, &d_dggpu->ViscParam);

      // subtract viscous flux from regular flux
      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) d = 0; d < ND; d++)
          flux[f + d * NF] -= viscous_flux[f * ND + d];
#endif  // VISCOSITY

#pragma unroll
      for(decltype(ND) f = 0; f < NF; f++)
        {
          for(decltype(ND) b = 0; b < NB; b++)
            {
              double temp = 0.;
              for(decltype(ND) d = 0; d < ND; d++)
                temp +=
                    flux[f + d * NF] * d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)] * d_qpoint_weights_internal[q];

              dw_shared[dw_shared_idx_no_reduction(b, f, threadIdx.x)] += temp * d_dggpu->fac_area;
            }
        }

#ifdef TURBULENCE
      dg_add_in_source_turbulence(c, q, fields, d_dggpu, st, dw_shared, threadIdx.x);
#endif  // TURBULENCE

#ifdef ART_VISCOSITY
      dg_add_in_source_alpha(c, q, fields, grad_fields, d_dggpu, st, dw_shared);
#endif  // ART_VISCOSITY

#ifdef GRAVITY_WAVES
      dg_add_in_source_gravity(c, q, fields, d_dggpu, st, dw_shared);
#endif  // GRAVITY_WAVES

#ifdef COOLING
      dg_add_in_source_cooling(c, q, fields, d_dggpu, st, dw_shared);
#endif  // COOLING
    }
  // }
  //  sync threads for this cell and then sum the results for the different quad-points,
  //  but now the threads parallelize over the list of weights for the cell (i.e. basis functions and fields)
  __syncthreads();

  for(int i = threadIdx.x; i < NB * NF; i += INTERNAL_KERNEL_BLOCK_SIZE)
    {
      const int f = i / NB;
      const int b = i % NB;

      double temp = 0;
      for(int qpoint_in_cell = 0; qpoint_in_cell < INTERNAL_KERNEL_BLOCK_SIZE; qpoint_in_cell++)
        temp += dw_shared[dw_shared_idx_no_reduction(b, f, qpoint_in_cell)];

      dwdt[w_idx_internal(b, f, c)] += temp;
    }
}
#else  // USE_SLOW_INTERNAL_FLUX
__global__ void calculate_internal_fluxes(const dg_data_for_gpu *d_dggpu, double *w, double *dwdt)
{
  // 1. weights to conserved variables
  // 2. conserved variables to flux
  // 3. flux to update weights

  // blockDim.x = INTERNAL_KERNEL_BLOCK_SIZE
  // blockIdx.x = 0...ncells
  const auto c = threadIdx.x + blockDim.x * blockIdx.x;
  if(c >= d_dggpu->Nc)
    return;

  double dw_shared[NB * NF] = {0};

  for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
    {
      double fields[NF] = {0};  // storage for conserved variables

      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) b = 0; b < NB; b++)
          fields[f] += w[w_idx_internal(b, f, c)] * d_dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)];

      // calculate primitive variables
      state st;
      get_primitive_state_from_conserved_fields(fields, st, &d_dggpu->ViscParam);

#ifdef RICHTMYER_VISCOSITY
      // evaluate velocity divergence
      double divvel = 0;
      for(int d = 0; d < ND; d++)
        {
          double dgrad[1 + ND] = {0};
          for(int f = 0; f < 1 + ND; f++)
            for(int b = 0; b < NB; b++)
              dgrad[f] += d_dggpu->fac_cellsize * w[w_idx_internal(b, f, c)] *
                          d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];

          divvel += (dgrad[1 + d] - st.s[1 + d] * dgrad[0]) / st.rho;
        }

      if(divvel < 0)
        {
          double csnd = sqrt(GAMMA * st.press / st.rho);

          double press = d_dggpu->ViscParam.ArtViscRichtmyer * st.rho * pow(d_dggpu->ViscParam.Resolution * divvel, 2) +
                         d_dggpu->ViscParam.ArtViscLandshoff * st.rho * fabs(d_dggpu->ViscParam.Resolution * divvel) * csnd;

          double press_max = 0.5 * st.rho * pow(d_dggpu->ViscParam.Resolution, 2) * fabs(divvel) / d_dggpu->TimeStep;

          if(press > press_max)
            press = press_max;

          st.press += press;
        }
#endif

      // calculate full flux
      double flux[ND * NF] = {0};
      get_flux_from_primitive_state_and_conserved_fields(fields, st, flux);

#ifdef VISCOSITY
      // evaluate gradient of conserved states
      double grad_fields[NF * ND];

      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) d = 0; d < ND; d++)
          {
            grad_fields[f * ND + d] = 0;

            for(decltype(ND) b = 0; b < NB; b++)
              grad_fields[f * ND + d] += d_dggpu->fac_cellsize * w[w_idx_internal(b, f, c)] *
                                         d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];
          }

      // calculate viscous flux internally
      double viscous_flux[NF * ND];
      get_viscous_flux_from_conserved_fields_and_their_gradients(fields, grad_fields, viscous_flux, &d_dggpu->ViscParam);

      // subtract viscous flux from regular flux
      for(decltype(ND) f = 0; f < NF; f++)
        for(decltype(ND) d = 0; d < ND; d++)
          flux[f + d * NF] -= viscous_flux[f * ND + d];
#endif  // VISCOSITY

#pragma unroll
      for(decltype(ND) f = 0; f < NF; f++)
        {
          for(decltype(ND) b = 0; b < NB; b++)
            {
              double temp = 0.;
              for(decltype(ND) d = 0; d < ND; d++)
                temp +=
                    flux[f + d * NF] * d_dggpu->d_bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)] * d_qpoint_weights_internal[q];

              dw_shared[dw_shared_idx(b, f)] += temp * d_dggpu->fac_area;
            }
        }

#ifdef TURBULENCE
      dg_add_in_source_turbulence(c, q, fields, d_dggpu, st, dw_shared, 0);
#endif  // TURBULENCE

#ifdef ART_VISCOSITY
      assert(0);  // not implemented
      dg_add_in_source_alpha(c, q, fields, grad_fields, d_dggpu, st, dw_shared);
#endif  // ART_VISCOSITY

#ifdef GRAVITY_WAVES
      dg_add_in_source_gravity(c, q, fields, d_dggpu, st, dw_shared, 0);
#endif  // GRAVITY_WAVES

#ifdef COOLING
      dg_add_in_source_cooling(c, q, fields, d_dggpu, st, dw_shared, 0);
#endif  // COOLING
    }

  const auto starting_index = w_idx_internal(0, 0, c);
  for(int i = 0; i < NB * NF; i++)
    {
      dwdt[starting_index + i] += dw_shared[i];
    }
}
#endif  // USE_SLOW_INTERNAL_FLUX

#ifdef ART_VISCOSITY

__device__ void dg_add_in_source_alpha(int c, int qpoint_in_cell, double *fields, double *grad_fields, const dg_data_for_gpu *d_dggpu,
                                       state &st, double *dw_shared)
{
  double source = 0;  // storage for source function

  // get matrix of velocity derivatives
  double vel[ND], dvel[ND][ND], u, du[ND], dye, ddye[ND];
  get_primitive_variable_gradients(fields, grad_fields, vel, dvel, u, du, dye, ddye);

  // get velocity divergence
  double divvel = 0;
  for(int i = 0; i < ND; i++)
    divvel += dvel[i][i];

  if(divvel < 0)
    source += -d_dggpu->ViscParam.ArtViscShockGrowth * divvel;

#ifndef ISOTHERM_EQS
  double cs = 0;
  if(st.press > 0 && st.rho > 0)
    cs = sqrt(GAMMA * st.press / st.rho);
#else
  double cs = d_dggpu->IsoSoundSpeed;
#endif

  double res = d_dggpu->ViscParam.Resolution;

  double tau = d_dggpu->ViscParam.ArtViscDecay * res / cs;

  source += -fields[ALPHA_FIELD] / tau;

#define S0 0.01
  const double sonset = (S0 / (DG_ORDER * DG_ORDER)) * d_dggpu->ViscParam.ArtViscWiggleOnset;

  if(d_dggpu->d_smthness[c] > sonset && d_dggpu->d_smthrate[c] > 0)
    source += d_dggpu->ViscParam.ArtViscWiggleGrowth * d_dggpu->d_smthrate[c];

#if(ND == 1)
  const double fac = 0.5 * d_dggpu->cellVolume;
#endif
#if(ND == 2)
  const double fac = 0.25 * d_dggpu->cellVolume;
#endif
#if(ND == 3)
  const double fac = 0.125 * d_dggpu->cellVolume;
#endif

  // update dw/dt of the weights with the source term
  int f = ALPHA_FIELD;

  for(decltype(ND) b = 0; b < NB; b++)
    {
      dw_shared[dw_shared_idx_no_reduction(b, f, threadIdx.x)] +=
          fac * source * d_dggpu->d_bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] * d_qpoint_weights_internal[qpoint_in_cell];
    }
}
#endif

#ifdef TURBULENCE

__device__ void dg_add_in_source_turbulence(int c, int qpoint_in_cell, double *fields, const dg_data_for_gpu *d_dggpu, state &st,
                                            double *dw_shared, const int dw_shared_index)
{
#ifdef DECAYING_TURBULENCE
  if(d_dggpu->Time > d_dggpu->StopDrivingAfterTime)
    return;
#endif  //  DECAYING_TURBULENCE
  // compute force density at the desired point
  double fx = 0;
  double fy = 0;
#ifdef THREEDIMS
  double fz = 0;
#endif  // THREEDIMS

  int cell_offset = d_dggpu->FirstSlabThisTask * DG_NUM_CELLS_1D;
#if(ND == 3)
  cell_offset *= DG_NUM_CELLS_1D;
#endif

  // get coordinates of cell center
  const xyz cell_center = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);

  double xx = d_dggpu->cellSize * (cell_center.x + .5);
  double yy = d_dggpu->cellSize * (cell_center.y + .5);
#ifdef THREEDIMS
  double zz = d_dggpu->cellSize * (cell_center.z + .5);
#endif  // THREEDIMS

  // add qpoint coordinates
  const xyz qxyz = dg_geometry_idx_to_cell(qpoint_in_cell, DG_NUM_QPOINTS_1D);

  xx += d_qpoint_location[qxyz.x] / 2 * d_dggpu->cellSize;
  yy += d_qpoint_location[qxyz.y] / 2 * d_dggpu->cellSize;
#ifdef THREEDIMS
  zz += d_qpoint_location[qxyz.z] / 2 * d_dggpu->cellSize;
#endif  // THREEDIMS

  for(int m = 0; m < d_dggpu->StNModes; m++)  // calc force
    {
      const double kxx = d_dggpu->d_StMode[3 * m + 0] * xx;
      const double kyy = d_dggpu->d_StMode[3 * m + 1] * yy;
      double kzz       = 0;
#ifdef THREEDIMS
      kzz = d_dggpu->d_StMode[3 * m + 2] * zz;
#endif  // THREEDIMS

      const double kdotx = kxx + kyy + kzz;
      const double ampl  = d_dggpu->d_StAmpl[m];

      const double realt = cos(kdotx);
      const double imagt = sin(kdotx);

      fx += ampl * (d_dggpu->d_StAka[3 * m + 0] * realt - d_dggpu->d_StAkb[3 * m + 0] * imagt);
      fy += ampl * (d_dggpu->d_StAka[3 * m + 1] * realt - d_dggpu->d_StAkb[3 * m + 1] * imagt);
#ifdef THREEDIMS
      fz += ampl * (d_dggpu->d_StAka[3 * m + 2] * realt - d_dggpu->d_StAkb[3 * m + 2] * imagt);
#endif  // THREEDIMS
    }

  fx *= 2. * d_dggpu->StAmplFacStSolWeightNorm * st.rho;
  fy *= 2. * d_dggpu->StAmplFacStSolWeightNorm * st.rho;
#ifdef THREEDIMS
  fz *= 2. * d_dggpu->StAmplFacStSolWeightNorm * st.rho;
#endif  // THREEDIMS

  double source[NF];  // storage for source function
  for(int f = 0; f < NF; f++)
    source[f] = 0;

#if(ND == 3)
  source[0] = 0;
  source[1] = fx;
  source[2] = fy;
  source[3] = fz;
#ifndef ISOTHERM_EQS
  source[4] = st.velx * fx + st.vely * fy + st.velz * fz;  // work term
#endif
#endif  // (ND == 3)
#if(ND == 2)
  source[0] = 0;
  source[1] = fx;
  source[2] = fy;
#ifndef ISOTHERM_EQS
  source[3] = st.velx * fx + st.vely * fy;  // work term
#endif
#endif  // (ND == 2)

#if(ND == 1)
  const double fac = 0.5 * d_dggpu->cellVolume;
#endif
#if(ND == 2)
  const double fac = 0.25 * d_dggpu->cellVolume;
#endif
#if(ND == 3)
  const double fac = 0.125 * d_dggpu->cellVolume;
#endif

// update dw/dt of the weights with the source term
#pragma unroll
  for(int f = 1; f < NF; f++)
    {
      for(decltype(ND) b = 0; b < NB; b++)
        {
          const double temp = fac * source[f] * d_dggpu->d_bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] *
                              d_qpoint_weights_internal[qpoint_in_cell];
          dw_shared[b + NB * f + NB * NF * dw_shared_index] += temp;
        }
    }
}

#endif  // TURBULENCE

#if(USE_SLOW_EXTERNAL_FLUX)
__global__ void compute_surface_fluxes(const dg_data_for_gpu *d_dggpu, int axis, int oddeven, int Nslabs, double *w, double *dwdt,
                                       fields_face *right_states_to_recv, fields_face *left_states_to_recv,
                                       slopes *right_slopes_to_recv, slopes *left_slopes_to_recv, char *troubled)
{
  // threadIdx.x -  outer quad point
  // blockIdx.x  -  cell id of red-black order

  extern __shared__ double dw_shared_left[];
  double *dw_shared_right = dw_shared_left + (EXTERNAL_KERNEL_BLOCK_SIZE * NB * NF);

  for(int i = threadIdx.x; i < NF * NB * EXTERNAL_KERNEL_BLOCK_SIZE * 2; i += EXTERNAL_KERNEL_BLOCK_SIZE)
    dw_shared_left[i] = 0;
  __syncthreads();

#if(ND == 3)
  int x, y, z;

  if(axis == 0)
    {
      x = blockIdx.x / (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D);
      y = (blockIdx.x - x * (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      z = blockIdx.x - x * (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D) - y * DG_NUM_CELLS_1D;

      x *= 2;
      x += oddeven;
      x -= 1;
    }
  else if(axis == 1)
    {
      y = blockIdx.x / (Nslabs * DG_NUM_CELLS_1D);
      x = (blockIdx.x - y * (Nslabs * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      z = blockIdx.x - y * (DG_NUM_CELLS_1D * Nslabs) - x * DG_NUM_CELLS_1D;

      y *= 2;
      y += oddeven;
    }
  else
    {
      z = blockIdx.x / (Nslabs * DG_NUM_CELLS_1D);
      x = (blockIdx.x - z * (Nslabs * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      y = blockIdx.x - z * (DG_NUM_CELLS_1D * Nslabs) - x * DG_NUM_CELLS_1D;

      z *= 2;
      z += oddeven;
    }

  // get cell coordinates

  const int cell_L = cell_stride_x * x + cell_stride_y * y + z;

#endif  // (ND == 3)
#if(ND == 2)
  int x, y;

  if(axis == 0)
    {
      x = blockIdx.x / (DG_NUM_CELLS_1D);
      y = blockIdx.x - x * DG_NUM_CELLS_1D;

      x *= 2;
      x += oddeven;
      x -= 1;
    }
  else
    {
      y = blockIdx.x / Nslabs;
      x = blockIdx.x - y * Nslabs;

      y *= 2;
      y += oddeven;
    }

  const int cell_L = cell_stride_x * x + y;

#endif  // (ND == 2)
#if(ND == 1)

  int x;

  x = blockIdx.x;

  x *= 2;
  x += oddeven;
  x -= 1;

  const int cell_L = x;

#endif  // (ND == 1)

  int xx = x;
  if(axis == 0)
    {
      xx++;
    }

  int cell_R = cell_stride_x * xx;

#if(ND >= 2)
  int yy = y;
  if(axis == 1)
    {
      yy++;
      if(yy >= DG_NUM_CELLS_1D)
        yy = 0;
    }
  cell_R += cell_stride_y * yy;
#endif
#if(ND == 3)
  int zz = z;
  if(axis == 2)
    {
      zz++;
      if(zz >= DG_NUM_CELLS_1D)
        zz = 0;
    }
  cell_R += zz;
#endif  // (ND == 3)

#ifdef FINITE_VOLUME
  int xm = x;
  if(axis == 0)
    xm--;

  int xp = xx;
  if(axis == 0)
    xp++;

  int cell_LL = cell_stride_x * xm;
  int cell_RR = cell_stride_x * xp;

#if(ND >= 2)
  int ym = y;
  int yp = yy;
  if(axis == 1)
    {
      ym--;
      if(ym < 0)
        ym += NUM_CELLS_Y;
      yp++;
      if(yp >= NUM_CELLS_Y)
        yp = 0;
    }
  cell_LL += cell_stride_y * ym;
  cell_RR += cell_stride_y * yp;
#endif
#if(ND == 3)
  int zm = z;
  int zp = zz;
  if(axis == 2)
    {
      zm--;
      zp++;
      if(zm < 0)
        zm += NUM_CELLS_Z;
      if(zp >= NUM_CELLS_Z)
        zp = 0;
    }
  cell_LL += zm;
  cell_RR += zp;
#endif

#if(ND == 3)
  const int st_index = (DG_NUM_CELLS_1D * y + z);
#endif
#if(ND == 2)
  const int st_index = y;
#endif
#if(ND == 1)
  const int st_index = 0;
#endif

#endif  // FINITE_VOLUME

#ifdef DO_NOT_SAVE_W_PRIM
  double w_prim_L[NF * NB] = {0};
  double w_prim_R[NF * NB] = {0};
  project_single_cell_to_primitives(cell_L, w, w_prim_L, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal, &d_dggpu->ViscParam);
  project_single_cell_to_primitives(cell_R, w, w_prim_R, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal, &d_dggpu->ViscParam);
#endif  // DO_NOT_SAVE_W_PRIM

  for(int qpoint_on_face = threadIdx.x; qpoint_on_face < DG_NUM_OUTER_QPOINTS; qpoint_on_face += EXTERNAL_KERNEL_BLOCK_SIZE)
    {
      double fields_L[NF] = {0};
      double fields_R[NF] = {0};
      state st_L{};
      state st_R{};

#ifdef FINITE_VOLUME
      slopes slope_L;
      slopes slope_R;
      char troubled_L;
      char troubled_R;
#endif
#ifdef VISCOSITY
      double fields_surface_L[NF] = {0};
      double fields_surface_R[NF] = {0};

      double grad_fields_surface_L[NF * ND] = {0};
      double grad_fields_surface_R[NF * ND] = {0};
#endif

      if(x >= 0)
        {
#ifdef FINITE_VOLUME
          troubled_L           = troubled[cell_L];
          double fields_Lm[NF] = {0};
          double fields_Lp[NF] = {0};
#endif

#pragma unroll
          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef USE_PRIMITIVE_PROJECTION
#ifdef DO_NOT_SAVE_W_PRIM
                  fields_L[f] +=
                      w_prim_L[w_idx_internal(b, f, 0)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#else   // DO_NOT_SAVE_W_PRIM
                  fields_L[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_L)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#endif  // DO_NOT_SAVE_W_PRIM
#else   // USE_PRIMITIVE_PROJECTION
                  fields_L[f] +=
                      w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#endif  // USE_PRIMITIVE_PROJECTION

#ifdef FINITE_VOLUME
                  // note: for finite volume, NB=1, and the bfunc is just 1
                  fields_Lm[f] += (axis != 0 || x > 0) ? w[w_idx_internal(0, f, cell_LL)] : left_states_to_recv[st_index].fields[f];
                  fields_Lp[f] +=
                      (axis != 0 || x < Nslabs - 1) ? w[w_idx_internal(0, f, cell_R)] : right_states_to_recv[st_index].fields[f];
#endif  // FINITE_VOLUME
                }

#ifdef VISCOSITY
              for(decltype(ND) l = 0; l < NB_INCREASED; l++)
                {
                  double wg = 0;

                  for(int i = 0; i < (*d_dggpu->d_bfunc_project_count)[axis][l]; i++)
                    {
                      int index = (*d_dggpu->d_bfunc_project_first)[axis][l] + i;
                      int b     = d_dggpu->d_bfunc_project_list[axis][index];

                      wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_project_value_left[axis][index];
                    }

                  fields_surface_L[f] += wg * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, axis)];

                  for(decltype(ND) d = 0; d < ND; d++)
                    grad_fields_surface_L[f * ND + d] +=
                        wg * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, axis)];
                }

              for(decltype(ND) d = 0; d < ND; d++)
                if(d == axis)
                  grad_fields_surface_L[f * ND + d] *= d_dggpu->fac_cellsize_axis;
                else
                  grad_fields_surface_L[f * ND + d] *= d_dggpu->fac_cellsize;
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_L, st_L, &d_dggpu->ViscParam);
#endif
#ifdef FINITE_VOLUME
          state st_Lm;
          get_primitive_state_from_conserved_fields(fields_Lm, st_Lm, &d_dggpu->ViscParam);
          state st_Lp;
          get_primitive_state_from_conserved_fields(fields_Lp, st_Lp, &d_dggpu->ViscParam);

          do_slope_limiting(st_Lm, st_L, st_Lp, axis, slope_L);
#endif
        }
      else
        {
#if(ND == 3)
          const int st_count = (DG_NUM_CELLS_1D * y + z) * DG_NUM_OUTER_QPOINTS + threadIdx.x;
#endif
#if(ND == 2)
          const int st_count = y * DG_NUM_OUTER_QPOINTS + threadIdx.x;
#endif
#if(ND == 1)
          const int st_count = threadIdx.x;
#endif
#ifdef FINITE_VOLUME
          slope_L    = left_slopes_to_recv[st_count];
          troubled_L = left_states_to_recv[st_count].troubled;
#endif
          for(decltype(ND) f = 0; f < NF; f++)
            {
              fields_L[f] = left_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
              fields_surface_L[f] = left_states_to_recv[st_count].fields_surface[f];

              for(decltype(ND) d = 0; d < ND; d++)
                grad_fields_surface_L[f * ND + d] = left_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_L, st_L, &d_dggpu->ViscParam);
#endif
        }

      if(xx < Nslabs)
        {
#ifdef FINITE_VOLUME
          troubled_R           = troubled[cell_R];
          double fields_Rm[NF] = {0};
          double fields_Rp[NF] = {0};
#endif

#pragma unroll
          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef USE_PRIMITIVE_PROJECTION
#ifdef DO_NOT_SAVE_W_PRIM
                  fields_R[f] += w_prim_R[w_idx_internal(b, f, 0)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#else   // DO_NOT_SAVE_W_PRIM

                  fields_R[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_R)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#endif  // DO_NOT_SAVE_W_PRIM
#else   // USE_PRIMITIVE_PROJECTION
                  fields_R[f] += w[w_idx_internal(b, f, cell_R)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#endif
#ifdef FINITE_VOLUME
                  fields_Rm[f] += (axis != 0 || x >= 0) ? w[w_idx_internal(0, f, cell_L)] : left_states_to_recv[st_index].fields[f];
                  fields_Rp[f] +=
                      (axis != 0 || xx < Nslabs - 1) ? w[w_idx_internal(0, f, cell_RR)] : right_states_to_recv[st_index].fields[f];
#endif
                }

#ifdef VISCOSITY
              for(decltype(ND) l = 0; l < NB_INCREASED; l++)
                {
                  double wg = 0;

                  for(int i = 0; i < (*d_dggpu->d_bfunc_project_count)[axis][l]; i++)
                    {
                      int index = (*d_dggpu->d_bfunc_project_first)[axis][l] + i;
                      int b     = d_dggpu->d_bfunc_project_list[axis][index];

                      wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_R)] * d_dggpu->d_bfunc_project_value_right[axis][index];
                    }

                  fields_surface_R[f] += wg * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, axis)];

                  for(decltype(ND) d = 0; d < ND; d++)
                    grad_fields_surface_R[f * ND + d] +=
                        wg * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, axis)];
                }

              for(decltype(ND) d = 0; d < ND; d++)
                if(d == axis)
                  grad_fields_surface_R[f * ND + d] *= d_dggpu->fac_cellsize_axis;
                else
                  grad_fields_surface_R[f * ND + d] *= d_dggpu->fac_cellsize;
#endif  // VISCOSITY
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_R, st_R, &d_dggpu->ViscParam);
#endif
#ifdef FINITE_VOLUME
          state st_Rm;
          get_primitive_state_from_conserved_fields(fields_Rm, st_Rm, &d_dggpu->ViscParam);
          state st_Rp;
          get_primitive_state_from_conserved_fields(fields_Rp, st_Rp, &d_dggpu->ViscParam);

          do_slope_limiting(st_Rm, st_R, st_Rp, axis, slope_R);
#endif
        }
      else
        {
#if(ND == 3)
          const int st_count = (DG_NUM_CELLS_1D * y + z) * DG_NUM_OUTER_QPOINTS + threadIdx.x;
#endif
#if(ND == 2)
          const int st_count = y * DG_NUM_OUTER_QPOINTS + threadIdx.x;
#endif
#if(ND == 1)
          const int st_count = threadIdx.x;
#endif
#ifdef FINITE_VOLUME
          slope_R    = right_slopes_to_recv[st_count];
          troubled_R = right_states_to_recv[st_count].troubled;
#endif
          for(decltype(ND) f = 0; f < NF; f++)
            {
              fields_R[f] = right_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
              fields_surface_R[f] = right_states_to_recv[st_count].fields_surface[f];

              for(decltype(ND) d = 0; d < ND; d++)
                grad_fields_surface_R[f * ND + d] = right_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_R, st_R, &d_dggpu->ViscParam);
#endif
        }

#ifdef USE_PRIMITIVE_PROJECTION
      for(int f = 0; f < NF; f++)
        {
          st_L.s[f] = fields_L[f];
          st_R.s[f] = fields_R[f];
        }
#endif
#ifdef FINITE_VOLUME
      if(troubled_L == false && troubled_R == false)
        {
          st_L.rho += 0.5 * slope_L.rho;
          st_R.rho -= 0.5 * slope_R.rho;
          st_L.velx += 0.5 * slope_L.velx;
          st_R.velx -= 0.5 * slope_R.velx;
#if(ND >= 2)
          st_L.vely += 0.5 * slope_L.vely;
          st_R.vely -= 0.5 * slope_R.vely;
#endif
#if(ND >= 3)
          st_L.velz += 0.5 * slope_L.velz;
          st_R.velz -= 0.5 * slope_R.velz;
#endif
#ifndef ISOTHERM_EQS
          st_L.press += 0.5 * slope_L.press;
          st_R.press -= 0.5 * slope_R.press;
#endif
#ifdef ENABLE_DYE
          st_L.dye += 0.5 * slope_L.dye;
          st_R.dye -= 0.5 * slope_R.dye;
#endif
        }
#endif

#if(ND >= 2)
      if(axis == 1)
        {
          double vtmp;
          /* swap x and y velocities */
          vtmp      = st_L.velx;
          st_L.velx = st_L.vely;
          st_L.vely = vtmp;

          vtmp      = st_R.velx;
          st_R.velx = st_R.vely;
          st_R.vely = vtmp;
        }
#endif
#if(ND == 3)
      if(axis == 2)
        {
          double vtmp;
          /* swap x and z velocities */
          vtmp      = st_L.velx;
          st_L.velx = st_L.velz;
          st_L.velz = vtmp;

          vtmp      = st_R.velx;
          st_R.velx = st_R.velz;
          st_R.velz = vtmp;
        }
#endif

      if(st_L.press < 0 || st_R.press < 0)
        {
          printf("axis=%d cell_L=%d  cell_R=%d  st_L.press=%g st_R.press=%g\n", axis, cell_L, cell_R, st_L.press, st_R.press);
          assert(0);
        }

      state sf;  // primitive state on face
      fluxes flx;

      // note: riemann solver assumes that the states are separated in x-direction
      godunov_flux_3d(st_L, st_R, sf, flx, &d_dggpu->ViscParam);

#if(ND >= 2)
      if(axis == 1)
        {
          /* swap computed x and y velocities and momentum fluxes */
          const double vtemp = sf.velx;
          sf.velx            = sf.vely;
          sf.vely            = vtemp;

          const double flxtemp = flx.field_flx[1];
          flx.field_flx[1]     = flx.field_flx[2];
          flx.field_flx[2]     = flxtemp;
        }
#endif
#if(ND == 3)
      else if(axis == 2)
        {
          /* swap x and z velocities and momentum fluxes */
          const double vtemp = sf.velx;
          sf.velx            = sf.velz;
          sf.velz            = vtemp;

          const double flxtemp = flx.field_flx[1];
          flx.field_flx[1]     = flx.field_flx[3];
          flx.field_flx[3]     = flxtemp;
        }
#endif

#ifdef VISCOSITY
      double full_viscous_flux[NF * ND];
      double fields_surface[NF], grad_fields_surface[NF * ND];

      for(decltype(ND) f = 0; f < NF; f++)
        {
          fields_surface[f] = fields_surface_L[f] + fields_surface_R[f];
          for(decltype(ND) d = 0; d < ND; d++)
            grad_fields_surface[f * ND + d] = grad_fields_surface_L[f * ND + d] + grad_fields_surface_R[f * ND + d];
        }

      // calculate viscous flux
      get_viscous_flux_from_conserved_fields_and_their_gradients(fields_surface, grad_fields_surface, full_viscous_flux,
                                                                 &d_dggpu->ViscParam);

      // correct the surface flux with the viscous flux
      for(decltype(ND) f = 0; f < NF; f++)
        flx.field_flx[f] -= full_viscous_flux[f * ND + axis];
#endif  // VISCOSITY

      // update weight changes
      for(decltype(ND) b = 0; b < NB; b++)
        {
          double fac_L = d_dggpu->faceArea * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)] *
                         d_qpoint_weights_external[qpoint_on_face];

#if(ND == 2)
          fac_L *= 0.5;
#endif
#if(ND == 3)
          fac_L *= 0.25;
#endif

          double fac_R = d_dggpu->faceArea * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, ND + axis)] *
                         d_qpoint_weights_external[qpoint_on_face];
#if(ND == 2)
          fac_R *= 0.5;
#endif
#if(ND == 3)
          fac_R *= 0.25;
#endif

          for(decltype(ND) f = 0; f < NF; f++)
            dw_shared_left[threadIdx.x * NB * NF + dw_shared_idx(b, f)] = -fac_L * flx.field_flx[f];

          for(decltype(ND) f = 0; f < NF; f++)
            dw_shared_right[threadIdx.x * NB * NF + dw_shared_idx(b, f)] = fac_R * flx.field_flx[f];
        }
    }
  //  sync threads for this face and then sum the results for the different quad-points,
  //  but now the threads parallelize over the list of weights for the cells (i.e. basis functions and fields)
  __syncthreads();

  for(int i = threadIdx.x; i < NB * NF; i += EXTERNAL_KERNEL_BLOCK_SIZE)
    {
      const int f = i / NB;
      const int b = i % NB;

      double temp_L = 0;
      double temp_R = 0;
      for(int qpoint_on_face = 0; qpoint_on_face < EXTERNAL_KERNEL_BLOCK_SIZE; qpoint_on_face++)
        {
          temp_L += dw_shared_left[qpoint_on_face * NB * NF + dw_shared_idx(b, f)];
          temp_R += dw_shared_right[qpoint_on_face * NB * NF + dw_shared_idx(b, f)];
        }

      if(x >= 0 && x < Nslabs)
        dwdt[w_idx_internal(b, f, cell_L)] += temp_L;

      if(xx >= 0 && xx < Nslabs)
        dwdt[w_idx_internal(b, f, cell_R)] += temp_R;
    }
}
#else  // USE_SLOW_EXTERNAL_FLUX
__global__ void compute_surface_fluxes_qpoint_for_loop(const dg_data_for_gpu *d_dggpu, int axis, int oddeven, int Nslabs, double *w,
                                                       double *dwdt, fields_face *right_states_to_recv,
                                                       fields_face *left_states_to_recv, slopes *right_slopes_to_recv,
                                                       slopes *left_slopes_to_recv, char *troubled, const int64_t max_c)
{
  // c  -  cell id of red-black order
  const int64_t c = threadIdx.x + blockDim.x * blockIdx.x;
  if(c >= max_c)
    return;

  double dw_shared_left[NB * NF]  = {0};
  double dw_shared_right[NB * NF] = {0};

#if(ND == 3)
  int x, y, z;

  if(axis == 0)
    {
      x = c / (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D);
      y = (c - x * (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      z = c - x * (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D) - y * DG_NUM_CELLS_1D;

      x *= 2;
      x += oddeven;
      x -= 1;
    }
  else if(axis == 1)
    {
      y = c / (Nslabs * DG_NUM_CELLS_1D);
      x = (c - y * (Nslabs * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      z = c - y * (DG_NUM_CELLS_1D * Nslabs) - x * DG_NUM_CELLS_1D;

      y *= 2;
      y += oddeven;
    }
  else
    {
      z = c / (Nslabs * DG_NUM_CELLS_1D);
      x = (c - z * (Nslabs * DG_NUM_CELLS_1D)) / DG_NUM_CELLS_1D;
      y = c - z * (DG_NUM_CELLS_1D * Nslabs) - x * DG_NUM_CELLS_1D;

      z *= 2;
      z += oddeven;
    }

  // get cell coordinates

  const int cell_L = cell_stride_x * x + cell_stride_y * y + z;

#endif  // (ND == 3)
#if(ND == 2)
  int x, y;

  if(axis == 0)
    {
      x = c / (DG_NUM_CELLS_1D);
      y = c - x * DG_NUM_CELLS_1D;

      x *= 2;
      x += oddeven;
      x -= 1;
    }
  else
    {
      y = c / Nslabs;
      x = c - y * Nslabs;

      y *= 2;
      y += oddeven;
    }

  const int cell_L = cell_stride_x * x + y;

#endif  // (ND == 2)
#if(ND == 1)

  int x;

  x = c;

  x *= 2;
  x += oddeven;
  x -= 1;

  const int cell_L = x;

#endif  // (ND == 1)

  int xx = x;
  if(axis == 0)
    {
      xx++;
    }

  int cell_R = cell_stride_x * xx;

#if(ND >= 2)
  int yy = y;
  if(axis == 1)
    {
      yy++;
      if(yy >= DG_NUM_CELLS_1D)
        yy = 0;
    }
  cell_R += cell_stride_y * yy;
#endif
#if(ND == 3)
  int zz = z;
  if(axis == 2)
    {
      zz++;
      if(zz >= DG_NUM_CELLS_1D)
        zz = 0;
    }
  cell_R += zz;
#endif  // (ND == 3)

#ifdef FINITE_VOLUME
  int xm = x;
  if(axis == 0)
    xm--;

  int xp = xx;
  if(axis == 0)
    xp++;

  int cell_LL = cell_stride_x * xm;
  int cell_RR = cell_stride_x * xp;

#if(ND >= 2)
  int ym = y;
  int yp = yy;
  if(axis == 1)
    {
      ym--;
      if(ym < 0)
        ym += NUM_CELLS_Y;
      yp++;
      if(yp >= NUM_CELLS_Y)
        yp = 0;
    }
  cell_LL += cell_stride_y * ym;
  cell_RR += cell_stride_y * yp;
#endif
#if(ND == 3)
  int zm = z;
  int zp = zz;
  if(axis == 2)
    {
      zm--;
      zp++;
      if(zm < 0)
        zm += NUM_CELLS_Z;
      if(zp >= NUM_CELLS_Z)
        zp = 0;
    }
  cell_LL += zm;
  cell_RR += zp;
#endif

#if(ND == 3)
  const int st_index = (DG_NUM_CELLS_1D * y + z);
#endif
#if(ND == 2)
  const int st_index = y;
#endif
#if(ND == 1)
  const int st_index = 0;
#endif

#endif  // FINITE_VOLUME

#ifdef DO_NOT_SAVE_W_PRIM
  double w_prim_L[NF * NB] = {0};
  double w_prim_R[NF * NB] = {0};
  if(x >= 0)
    project_single_cell_to_primitives(cell_L, w, w_prim_L, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal, &d_dggpu->ViscParam);
  if(xx < Nslabs)
    project_single_cell_to_primitives(cell_R, w, w_prim_R, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal, &d_dggpu->ViscParam);
#endif  // DO_NOT_SAVE_W_PRIM

  for(int qpoint_on_face = 0; qpoint_on_face < DG_NUM_OUTER_QPOINTS; qpoint_on_face++)
    {
      double fields_L[NF] = {0};
      double fields_R[NF] = {0};
      state st_L{};
      state st_R{};

#ifdef FINITE_VOLUME
      slopes slope_L;
      slopes slope_R;
      char troubled_L;
      char troubled_R;
#endif
#ifdef VISCOSITY
      double fields_surface_L[NF] = {0};
      double fields_surface_R[NF] = {0};

      double grad_fields_surface_L[NF * ND] = {0};
      double grad_fields_surface_R[NF * ND] = {0};
#endif

      if(x >= 0)
        {
#ifdef FINITE_VOLUME
          troubled_L           = troubled[cell_L];
          double fields_Lm[NF] = {0};
          double fields_Lp[NF] = {0};
#endif

#pragma unroll
          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef USE_PRIMITIVE_PROJECTION
#ifdef DO_NOT_SAVE_W_PRIM
                  fields_L[f] +=
                      w_prim_L[w_idx_internal(b, f, 0)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#else   // DO_NOT_SAVE_W_PRIM
                  fields_L[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_L)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#endif  // DO_NOT_SAVE_W_PRIM
#else   // USE_PRIMITIVE_PROJECTION
                  fields_L[f] +=
                      w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)];
#endif  // USE_PRIMITIVE_PROJECTION

#ifdef FINITE_VOLUME
                  // note: for finite volume, NB=1, and the bfunc is just 1
                  fields_Lm[f] += (axis != 0 || x > 0) ? w[w_idx_internal(0, f, cell_LL)] : left_states_to_recv[st_index].fields[f];
                  fields_Lp[f] +=
                      (axis != 0 || x < Nslabs - 1) ? w[w_idx_internal(0, f, cell_R)] : right_states_to_recv[st_index].fields[f];
#endif  // FINITE_VOLUME
                }

#ifdef VISCOSITY
              for(decltype(ND) l = 0; l < NB_INCREASED; l++)
                {
                  double wg = 0;

                  for(int i = 0; i < (*d_dggpu->d_bfunc_project_count)[axis][l]; i++)
                    {
                      int index = (*d_dggpu->d_bfunc_project_first)[axis][l] + i;
                      int b     = d_dggpu->d_bfunc_project_list[axis][index];

                      wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_project_value_left[axis][index];
                    }

                  fields_surface_L[f] += wg * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, axis)];

                  for(decltype(ND) d = 0; d < ND; d++)
                    grad_fields_surface_L[f * ND + d] +=
                        wg * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, axis)];
                }

              for(decltype(ND) d = 0; d < ND; d++)
                if(d == axis)
                  grad_fields_surface_L[f * ND + d] *= d_dggpu->fac_cellsize_axis;
                else
                  grad_fields_surface_L[f * ND + d] *= d_dggpu->fac_cellsize;
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_L, st_L, &d_dggpu->ViscParam);
#endif
#ifdef FINITE_VOLUME
          state st_Lm;
          get_primitive_state_from_conserved_fields(fields_Lm, st_Lm, &d_dggpu->ViscParam);
          state st_Lp;
          get_primitive_state_from_conserved_fields(fields_Lp, st_Lp, &d_dggpu->ViscParam);

          do_slope_limiting(st_Lm, st_L, st_Lp, axis, slope_L);
#endif
        }
      else
        {
#if(ND == 3)
          const int st_count = (DG_NUM_CELLS_1D * y + z) * DG_NUM_OUTER_QPOINTS + qpoint_on_face;
#endif
#if(ND == 2)
          const int st_count = y * DG_NUM_OUTER_QPOINTS + qpoint_on_face;
#endif
#if(ND == 1)
          const int st_count = qpoint_on_face;
#endif
#ifdef FINITE_VOLUME
          slope_L    = left_slopes_to_recv[st_count];
          troubled_L = left_states_to_recv[st_count].troubled;
#endif
          for(decltype(ND) f = 0; f < NF; f++)
            {
              fields_L[f] = left_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
              fields_surface_L[f] = left_states_to_recv[st_count].fields_surface[f];

              for(decltype(ND) d = 0; d < ND; d++)
                grad_fields_surface_L[f * ND + d] = left_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_L, st_L, &d_dggpu->ViscParam);
#endif
        }

      if(xx < Nslabs)
        {
#ifdef FINITE_VOLUME
          troubled_R           = troubled[cell_R];
          double fields_Rm[NF] = {0};
          double fields_Rp[NF] = {0};
#endif

#pragma unroll
          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
#ifdef USE_PRIMITIVE_PROJECTION
#ifdef DO_NOT_SAVE_W_PRIM
                  fields_R[f] += w_prim_R[w_idx_internal(b, f, 0)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#else   // DO_NOT_SAVE_W_PRIM
                  fields_R[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_R)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#endif  // DO_NOT_SAVE_W_PRIM
#else   // USE_PRIMITIVE_PROJECTION
                  fields_R[f] += w[w_idx_internal(b, f, cell_R)] *
                                 d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis + ND)];
#endif
#ifdef FINITE_VOLUME
                  fields_Rm[f] += (axis != 0 || x >= 0) ? w[w_idx_internal(0, f, cell_L)] : left_states_to_recv[st_index].fields[f];
                  fields_Rp[f] +=
                      (axis != 0 || xx < Nslabs - 1) ? w[w_idx_internal(0, f, cell_RR)] : right_states_to_recv[st_index].fields[f];
#endif
                }

#ifdef VISCOSITY
              for(decltype(ND) l = 0; l < NB_INCREASED; l++)
                {
                  double wg = 0;

                  for(int i = 0; i < (*d_dggpu->d_bfunc_project_count)[axis][l]; i++)
                    {
                      int index = (*d_dggpu->d_bfunc_project_first)[axis][l] + i;
                      int b     = d_dggpu->d_bfunc_project_list[axis][index];

                      wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_R)] * d_dggpu->d_bfunc_project_value_right[axis][index];
                    }

                  fields_surface_R[f] += wg * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, axis)];

                  for(decltype(ND) d = 0; d < ND; d++)
                    grad_fields_surface_R[f * ND + d] +=
                        wg * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, axis)];
                }

              for(decltype(ND) d = 0; d < ND; d++)
                if(d == axis)
                  grad_fields_surface_R[f * ND + d] *= d_dggpu->fac_cellsize_axis;
                else
                  grad_fields_surface_R[f * ND + d] *= d_dggpu->fac_cellsize;
#endif  // VISCOSITY
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_R, st_R, &d_dggpu->ViscParam);
#endif
#ifdef FINITE_VOLUME
          state st_Rm;
          get_primitive_state_from_conserved_fields(fields_Rm, st_Rm, &d_dggpu->ViscParam);
          state st_Rp;
          get_primitive_state_from_conserved_fields(fields_Rp, st_Rp, &d_dggpu->ViscParam);

          do_slope_limiting(st_Rm, st_R, st_Rp, axis, slope_R);
#endif
        }
      else
        {
#if(ND == 3)
          const int st_count = (DG_NUM_CELLS_1D * y + z) * DG_NUM_OUTER_QPOINTS + qpoint_on_face;
#endif
#if(ND == 2)
          const int st_count = y * DG_NUM_OUTER_QPOINTS + qpoint_on_face;
#endif
#if(ND == 1)
          const int st_count = qpoint_on_face;
#endif
#ifdef FINITE_VOLUME
          slope_R    = right_slopes_to_recv[st_count];
          troubled_R = right_states_to_recv[st_count].troubled;
#endif
          for(decltype(ND) f = 0; f < NF; f++)
            {
              fields_R[f] = right_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
              fields_surface_R[f] = right_states_to_recv[st_count].fields_surface[f];

              for(decltype(ND) d = 0; d < ND; d++)
                grad_fields_surface_R[f * ND + d] = right_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
            }
#ifndef USE_PRIMITIVE_PROJECTION
          get_primitive_state_from_conserved_fields(fields_R, st_R, &d_dggpu->ViscParam);
#endif
        }

#ifdef USE_PRIMITIVE_PROJECTION
      for(int f = 0; f < NF; f++)
        {
          st_L.s[f] = fields_L[f];
          st_R.s[f] = fields_R[f];
        }
#endif
#ifdef FINITE_VOLUME
      if(troubled_L == false && troubled_R == false)
        {
          st_L.rho += 0.5 * slope_L.rho;
          st_R.rho -= 0.5 * slope_R.rho;
          st_L.velx += 0.5 * slope_L.velx;
          st_R.velx -= 0.5 * slope_R.velx;
#if(ND >= 2)
          st_L.vely += 0.5 * slope_L.vely;
          st_R.vely -= 0.5 * slope_R.vely;
#endif
#if(ND >= 3)
          st_L.velz += 0.5 * slope_L.velz;
          st_R.velz -= 0.5 * slope_R.velz;
#endif
#ifndef ISOTHERM_EQS
          st_L.press += 0.5 * slope_L.press;
          st_R.press -= 0.5 * slope_R.press;
#endif
#ifdef ENABLE_DYE
          st_L.dye += 0.5 * slope_L.dye;
          st_R.dye -= 0.5 * slope_R.dye;
#endif
        }
#endif

#if(ND >= 2)
      if(axis == 1)
        {
          double vtmp;
          /* swap x and y velocities */
          vtmp      = st_L.velx;
          st_L.velx = st_L.vely;
          st_L.vely = vtmp;

          vtmp      = st_R.velx;
          st_R.velx = st_R.vely;
          st_R.vely = vtmp;
        }
#endif
#if(ND == 3)
      if(axis == 2)
        {
          double vtmp;
          /* swap x and z velocities */
          vtmp      = st_L.velx;
          st_L.velx = st_L.velz;
          st_L.velz = vtmp;

          vtmp      = st_R.velx;
          st_R.velx = st_R.velz;
          st_R.velz = vtmp;
        }
#endif

      if(st_L.press < 0 || st_R.press < 0)
        {
          printf("axis=%d cell_L=%d  cell_R=%d  st_L.press=%g st_R.press=%g\n", axis, cell_L, cell_R, st_L.press, st_R.press);
          assert(0);
        }

      state sf;  // primitive state on face
      fluxes flx;

      // note: riemann solver assumes that the states are separated in x-direction
      godunov_flux_3d(st_L, st_R, sf, flx, &d_dggpu->ViscParam);

#if(ND >= 2)
      if(axis == 1)
        {
          /* swap computed x and y velocities and momentum fluxes */
          const double vtemp = sf.velx;
          sf.velx            = sf.vely;
          sf.vely            = vtemp;

          const double flxtemp = flx.field_flx[1];
          flx.field_flx[1]     = flx.field_flx[2];
          flx.field_flx[2]     = flxtemp;
        }
#endif
#if(ND == 3)
      else if(axis == 2)
        {
          /* swap x and z velocities and momentum fluxes */
          const double vtemp = sf.velx;
          sf.velx            = sf.velz;
          sf.velz            = vtemp;

          const double flxtemp = flx.field_flx[1];
          flx.field_flx[1]     = flx.field_flx[3];
          flx.field_flx[3]     = flxtemp;
        }
#endif

#ifdef VISCOSITY
      double full_viscous_flux[NF * ND] = {0};
      double fields_surface[NF] = {0}, grad_fields_surface[NF * ND] = {0};

      for(decltype(ND) f = 0; f < NF; f++)
        {
          fields_surface[f] = fields_surface_L[f] + fields_surface_R[f];
          for(decltype(ND) d = 0; d < ND; d++)
            grad_fields_surface[f * ND + d] = grad_fields_surface_L[f * ND + d] + grad_fields_surface_R[f * ND + d];
        }

      // calculate viscous flux
      get_viscous_flux_from_conserved_fields_and_their_gradients(fields_surface, grad_fields_surface, full_viscous_flux,
                                                                 &d_dggpu->ViscParam);

      // correct the surface flux with the viscous flux
      for(decltype(ND) f = 0; f < NF; f++)
        flx.field_flx[f] -= full_viscous_flux[f * ND + axis];
#endif  // VISCOSITY

      // update weight changes
      for(decltype(ND) b = 0; b < NB; b++)
        {
          double fac_L = d_dggpu->faceArea * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, axis)] *
                         d_qpoint_weights_external[qpoint_on_face];

#if(ND == 2)
          fac_L *= 0.5;
#endif
#if(ND == 3)
          fac_L *= 0.25;
#endif

          double fac_R = d_dggpu->faceArea * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, ND + axis)] *
                         d_qpoint_weights_external[qpoint_on_face];
#if(ND == 2)
          fac_R *= 0.5;
#endif
#if(ND == 3)
          fac_R *= 0.25;
#endif

          for(decltype(ND) f = 0; f < NF; f++)
            dw_shared_left[dw_shared_idx(b, f)] += -fac_L * flx.field_flx[f];

          for(decltype(ND) f = 0; f < NF; f++)
            dw_shared_right[dw_shared_idx(b, f)] += fac_R * flx.field_flx[f];
        }  // for b
    }      // for qpoint_on_face

  const auto starting_index_L = w_idx_internal(0, 0, cell_L);
  const auto starting_index_R = w_idx_internal(0, 0, cell_R);
  if(x >= 0 && x < Nslabs)
    {
      for(int i = 0; i < NB * NF; i++)
        {
          dwdt[starting_index_L + i] += dw_shared_left[i];
        }
    }
  if(xx >= 0 && xx < Nslabs)
    {
      for(int i = 0; i < NB * NF; i++)
        {
          dwdt[starting_index_R + i] += dw_shared_right[i];
        }
    }
}
#endif  // USE_SLOW_EXTERNAL_FLUX

__global__ void extract_right_and_left_states(const dg_data_for_gpu *d_dggpu, int numstates, int Nslabs, double *d_w,
                                              fields_face *d_right_states_to_send, fields_face *d_left_states_to_send)
{
  const auto i = threadIdx.x + blockDim.x * blockIdx.x;

  if(i < numstates)
    {
      const int cell_R         = i / DG_NUM_OUTER_QPOINTS;
      const int qpoint_on_face = i - cell_R * DG_NUM_OUTER_QPOINTS;
      const int cell_L         = cell_R + cell_stride_x * (Nslabs - 1);

      fields_face *st_right = &d_right_states_to_send[i];
      fields_face *st_left  = &d_left_states_to_send[i];
      for(decltype(ND) f = 0; f < NF; f++)
        {
          st_right->fields[f] = 0;
          st_left->fields[f]  = 0;
        }

#ifdef DO_NOT_SAVE_W_PRIM
      double w_prim_L[NF * NB] = {0};
      double w_prim_R[NF * NB] = {0};
      project_single_cell_to_primitives(cell_L, d_w, w_prim_L, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal,
                                        &d_dggpu->ViscParam);
      project_single_cell_to_primitives(cell_R, d_w, w_prim_R, d_dggpu->d_bfunc_internal, d_qpoint_weights_internal,
                                        &d_dggpu->ViscParam);
#endif  // DO_NOT_SAVE_W_PRIM

      for(decltype(ND) f = 0; f < NF; f++)
        {
          for(decltype(ND) b = 0; b < NB; b++)
            {
#ifdef USE_PRIMITIVE_PROJECTION
#ifdef DO_NOT_SAVE_W_PRIM
              st_right->fields[f] +=
                  w_prim_R[w_idx_internal(b, f, 0)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0 + ND)];
              st_left->fields[f] +=
                  w_prim_L[w_idx_internal(b, f, 0)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0)];
#else   // DO_NOT_SAVE_W_PRIM
              st_right->fields[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_R)] *
                                     d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0 + ND)];
              st_left->fields[f] += d_dggpu->d_w_prim[w_idx_internal(b, f, cell_L)] *
                                    d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0)];
#endif  // DO_NOT_SAVE_W_PRIM
#else   // USE_PRIMITIVE_PROJECTION
              st_right->fields[f] +=
                  d_w[w_idx_internal(b, f, cell_R)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0 + ND)];
              st_left->fields[f] +=
                  d_w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_external[bfunc_external_side_idx(qpoint_on_face, b, 0)];
#endif  // USE_PRIMITIVE_PROJECTION
            }

#ifdef VISCOSITY
          st_right->fields_surface[f] = 0;
          st_left->fields_surface[f]  = 0;

          for(decltype(ND) d = 0; d < ND; d++)
            {
              st_right->grad_fields_surface[f * ND + d] = 0;
              st_left->grad_fields_surface[f * ND + d]  = 0;
            }

          for(decltype(ND) l = 0; l < NB_INCREASED; l++)
            {
              double wg_R = 0;
              double wg_L = 0;
              for(int i = 0; i < (*d_dggpu->d_bfunc_project_count)[0][l]; i++)
                {
                  const int index = (*d_dggpu->d_bfunc_project_first[0])[l] + i;
                  const int b     = d_dggpu->d_bfunc_project_list[0][index];

                  wg_R += (1.0 / 2) * d_w[w_idx_internal(b, f, cell_R)] * d_dggpu->d_bfunc_project_value_right[0][index];
                  wg_L += (1.0 / 2) * d_w[w_idx_internal(b, f, cell_L)] * d_dggpu->d_bfunc_project_value_left[0][index];
                }
              st_right->fields_surface[f] += wg_R * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, 0)];
              st_left->fields_surface[f] += wg_L * d_dggpu->d_bfunc_mid[bfunc_mid_idx(qpoint_on_face, l, 0)];

              for(decltype(ND) d = 0; d < ND; d++)
                {
                  st_right->grad_fields_surface[f * ND + d] +=
                      wg_R * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, 0)];
                  st_left->grad_fields_surface[f * ND + d] +=
                      wg_L * d_dggpu->d_bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, l, d, 0)];
                }
            }

          for(decltype(ND) d = 0; d < ND; d++)
            if(d == 0)
              {
                st_right->grad_fields_surface[f * ND + d] *= d_dggpu->fac_cellsize_axis;
                st_left->grad_fields_surface[f * ND + d] *= d_dggpu->fac_cellsize_axis;
              }
            else
              {
                st_right->grad_fields_surface[f * ND + d] *= d_dggpu->fac_cellsize;
                st_left->grad_fields_surface[f * ND + d] *= d_dggpu->fac_cellsize;
              }
#endif  // VISCOSITY
        }
    }
}

#ifdef TURBULENCE

__global__ void dg_store_driving_energy_gpu(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, double *w, double *EgyDrive)
{
  // threadIdx.x -  cell
  // blockIdx.x  -  nothing physical
  const uint64_t c = threadIdx.x + blockDim.x * blockIdx.x;

  if(c < Nc)
    {
#ifndef ISOTHERM_EQS
#if(ND == 3)
      const int f = 4;
#endif

#if(ND == 2)
      const int f = 3;
#endif
      const double etot = w[w_idx_internal(0, f, c)];  // mean energy density in cell
#else
      double rho = w[w_idx_internal(0, 0, c)];
      double px  = w[w_idx_internal(0, 1, c)];
      double py  = w[w_idx_internal(0, 2, c)];

#if(ND == 3)
      double pz       = w[w_idx_internal(0, 3, c)];
      const double p2 = px * px + py * py + pz * pz;
#endif

#if(ND == 2)
      const double p2 = px * px + py * py;
#endif

      double etot = 0.5 * p2 / rho + rho * d_dggpu->ViscParam.IsoSoundSpeed * d_dggpu->ViscParam.IsoSoundSpeed;
#endif
      const double egy = etot * d_dggpu->cellVolume;  // total energy in cell

      EgyDrive[c] = egy;
    }
}

void dg_store_driving_energy_cuda(dg_data_t *dg, const dg_data_for_gpu *d_dggpu, int flag)
{
  // events for timing
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  double *array;
  myCudaMalloc(&array, Nc * sizeof(double));
  gpuErrchk(cudaMemset(array, 0, Nc * sizeof(double)));

  dg_store_driving_energy_gpu<<<(Nc + 255) / 256, 256>>>(Nc, d_dggpu, dg->d_w, array);
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  thrust::device_ptr<double> array_ptr(array);
  const auto egy = thrust::reduce(array_ptr, array_ptr + Nc);

  // double egy = sum_array(Nc, array);

  myCudaFree(array, Nc * sizeof(double));

  if(flag == 0)
    dg->TurbInjectedEnergy -= egy;
  else if(flag == 1)
    dg->TurbInjectedEnergy += egy;
  else
    Terminate("Invalid flag in dg_store_driving_energy_cuda");
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[dg_store_driving_energy_cuda_time]: %f ms\n", time);
}

__global__ void dg_reset_temperature_gpu_old(const uint64_t Nc, const dg_data_for_gpu *dggpu, double *w, double *EgyDiss)
{
#ifndef ISOTHERM_EQS
  // threadIdx.x -  qpoint in cell
  // blockIdx.x  -  cell

  if(blockIdx.x < Nc)
    {
      const auto q = threadIdx.x;
      const auto c = blockIdx.x;
#if(ND == 2)
      constexpr double fac = 0.25;
#endif
#if(ND == 3)
      constexpr double fac = 0.125;
#endif
      // storage for new energy weights
      extern __shared__ double w_new[];

      double fields[NF];  // fields evaluated at the Gauss quadrature point

      for(auto f = 0; f < NF; f++)
        {
          fields[f] = 0;

          for(decltype(ND) b = 0; b < NB; b++)
            fields[f] += w[w_idx_internal(b, f, c)] * dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)];
        }

      // calculate primitive variables
      const double irho = 1.0 / fields[0];

#if(ND == 3)
      const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];

      // recomputing the desired value for fields[4]
      const double etot = 0.5 * p2 * irho + fields[0] * dggpu->ViscParam.IsoSoundSpeed * dggpu->ViscParam.IsoSoundSpeed / GAMMA_MINUS1;
#endif

#if(ND == 2)
      const double p2 = fields[1] * fields[1] + fields[2] * fields[2];

      // recomputing the desired value for fields[3]
      const double etot = 0.5 * p2 * irho + fields[0] * dggpu->ViscParam.IsoSoundSpeed * dggpu->ViscParam.IsoSoundSpeed / GAMMA_MINUS1;
#endif

      for(auto b = 0; b < NB; b++)
        {
          w_new[q + b * DG_NUM_INNER_QPOINTS] =
              fac * etot * dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)] * d_qpoint_weights_internal[q];
        }

      __syncthreads();

      // this for loop is only executed if DG_NUM_INNER_QPOINTS < NB
      // otherwize it just limits the number of threads doing work to NB
      // result is now in w_new[b * DG_NUM_INNER_QPOINTS + 0]
      for(auto b = q; b < NB; b += DG_NUM_INNER_QPOINTS)
        for(auto q = 1; q < DG_NUM_INNER_QPOINTS; q++)
          w_new[b * DG_NUM_INNER_QPOINTS] += w_new[q + b * DG_NUM_INNER_QPOINTS];
      __syncthreads();

#if(ND == 3)
      constexpr int f = 4;
#endif

#if(ND == 2)
      constexpr int f = 3;
#endif
      if(threadIdx.x == 0)
        EgyDiss[c] = (w[w_idx_internal(0, f, c)] - w_new[0]) * dggpu->cellVolume;
      __syncthreads();

      // now store the new expansion of the energy density in the corresponding weigths
      for(auto b = q; b < NB; b += DG_NUM_INNER_QPOINTS)
        w[w_idx_internal(b, f, c)] = w_new[b * DG_NUM_INNER_QPOINTS + 0];
      __syncthreads();
    }

#endif
}

void dg_reset_temperature_cuda(dg_data_t *dg, const dg_data_for_gpu *d_dggpu)
{
#if !(defined(ISOTHERM_EQS) || defined(COOLING))
  // events for timing
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  double *array;
  myCudaMalloc(&array, Nc * sizeof(double));
  gpuErrchk(cudaMemset(array, 0, Nc * sizeof(double)));
#define USE_SLOW_DG_RESET_TEMPERATURE false
#if USE_SLOW_DG_RESET_TEMPERATURE
  cudaFuncSetAttribute(dg_reset_temperature_gpu_old, cudaFuncAttributeMaxDynamicSharedMemorySize, (NB * DG_NUM_INNER_QPOINTS * 8));
  dg_reset_temperature_gpu_old<<<Nc, DG_NUM_INNER_QPOINTS, NB * DG_NUM_INNER_QPOINTS * 8>>>(Nc, d_dggpu, dg->d_w, array);
#else
  constexpr uint64_t THREADS_PER_BLOCK = 64;
  dg_reset_temperature_gpu<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(Nc, d_dggpu, dg->d_w, array);
#endif  // USE_SLOW_DG_RESET_TEMPERATURE

  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  thrust::device_ptr<double> array_ptr(array);
  dg->TurbDissipatedEnergy += thrust::reduce(array_ptr, array_ptr + Nc);

  myCudaFree(array, Nc * sizeof(double));
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[dg_reset_temperature_cuda_time]: %f ms\n", time);
#endif  // !(defined(ISOTHERM_EQS) || defined(COOLING))
}

__global__ void dg_reset_temperature_gpu(const uint64_t Nc, const dg_data_for_gpu *dggpu, double *w, double *EgyDiss)
{
#ifndef ISOTHERM_EQS
  const auto c = threadIdx.x + blockDim.x * blockIdx.x;

  if(c < Nc)
    {
      double w_new[NB] = {0};

      // const auto q = threadIdx.x;
      // const auto c = blockIdx.x;
      for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
        {
#if(ND == 2)
          constexpr double fac = 0.25;
#endif
#if(ND == 3)
          constexpr double fac = 0.125;
#endif
          // storage for new energy weights

          double fields[NF] = {0};  // fields evaluated at the Gauss quadrature point

          for(auto f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                fields[f] += w[w_idx_internal(b, f, c)] * dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)];
            }

          // calculate primitive variables
          const double irho = 1.0 / fields[0];

#if(ND == 3)
          const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];

          // recomputing the desired value for fields[4]
          const double etot =
              0.5 * p2 * irho + fields[0] * dggpu->ViscParam.IsoSoundSpeed * dggpu->ViscParam.IsoSoundSpeed / GAMMA_MINUS1;
#endif

#if(ND == 2)
          const double p2 = fields[1] * fields[1] + fields[2] * fields[2];

          // recomputing the desired value for fields[3]
          const double etot =
              0.5 * p2 * irho + fields[0] * dggpu->ViscParam.IsoSoundSpeed * dggpu->ViscParam.IsoSoundSpeed / GAMMA_MINUS1;
#endif

          for(auto b = 0; b < NB; b++)
            {
              w_new[b] += fac * etot * dggpu->d_bfunc_internal[bfunc_internal_idx(q, b)] * d_qpoint_weights_internal[q];
            }
        }
        // __syncthreads();

        // // this for loop is only executed if DG_NUM_INNER_QPOINTS < NB
        // // otherwize it just limits the number of threads doing work to NB
        // // result is now in w_new[b * DG_NUM_INNER_QPOINTS + 0]
        // for(auto b = q; b < NB; b += DG_NUM_INNER_QPOINTS)
        //   for(auto q = 1; q < DG_NUM_INNER_QPOINTS; q++)
        //     w_new[b * DG_NUM_INNER_QPOINTS] += w_new[q + b * DG_NUM_INNER_QPOINTS];
        // __syncthreads();

#if(ND == 3)
      constexpr int f = 4;
#endif

#if(ND == 2)
      constexpr int f = 3;
#endif
      // if(threadIdx.x == 0)
      EgyDiss[c] = (w[w_idx_internal(0, f, c)] - w_new[0]) * dggpu->cellVolume;
      // __syncthreads();

      // now store the new expansion of the energy density in the corresponding weigths
      for(int b = 0; b < NB; b++)
        {
          w[w_idx_internal(b, f, c)] = w_new[b];
        }
    }
#endif
}

__global__ void fill_in_ekin(uint64_t nelem, double *w, double *data)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < nelem)
    {
      double rho = w[w_idx_internal(0, 0, c)];  // mean cell density

#if(ND == 3)
      const double p2 = pow(w[w_idx_internal(0, 1, c)], 2) + pow(w[w_idx_internal(0, 2, c)], 2) + pow(w[w_idx_internal(0, 3, c)], 2);
      data[c]         = 0.5 * p2 / rho;  // kinetic energy density
#endif

#if(ND == 2)
      const double p2 = pow(w[w_idx_internal(0, 1, c)], 2) + pow(w[w_idx_internal(0, 2, c)], 2);
      data[c]         = 0.5 * p2 / rho;  // kinetic energy density
#endif
    }
}

double dg_get_ekin_cuda(dg_data_t *dg)
{
  // events for timing
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  double *array;
  myCudaMalloc(&array, Nc * sizeof(double));

  fill_in_ekin<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(Nc, dg->d_w, array);

  thrust::device_ptr<double> array_ptr(array);
  const double Ekin = thrust::reduce(array_ptr, array_ptr + Nc, 0.0, thrust::plus<double>());

  myCudaFree(array, Nc * sizeof(double));

  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[dg_get_ekin_cuda_time]: %f ms\n", time);
  return Ekin;
}

__global__ void fill_in_mass(const uint64_t nelem, double *w, double *data)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < nelem)
    {
      data[c] = w[w_idx_internal(0, 0, c)];  // mean cell density
    }
}

double dg_get_mass_cuda(dg_data_t *dg)
{
  // events for timing
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  double *array;
  myCudaMalloc(&array, Nc * sizeof(double));

  fill_in_mass<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(Nc, dg->d_w, array);

  // double Mass = sum_array(Nc, array);
  thrust::device_ptr<double> array_ptr(array);
  const double Mass = thrust::reduce(array_ptr, array_ptr + Nc, 0.0, thrust::plus<double>());

  myCudaFree(array, Nc * sizeof(double));
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[dg_get_mass_cuda_time]: %f ms\n", time);
  return Mass;
}

#endif  // TURBULENCE

#ifdef ART_VISCOSITY

__global__ void smoothness_indicator_kernel(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, const double *w)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < Nc)
    {
      double AA = 0;

      for(int b = NB_REDUCED; b < NB; b++)
        {
          AA += pow(w[w_idx_internal(b, 0, c)], 2);
        }

      double BB = AA;

      for(decltype(ND) b = 0; b < NB_REDUCED; b++)
        {
          BB += pow(w[w_idx_internal(b, 0, c)], 2);
        }

      double smth = 0;

      if(BB != 0)
        smth = AA / BB;  // smoothness estimator

      d_dggpu->d_smthness[c] = smth;  // smoothness estimator
    }
}

__global__ void smoothrate_indicator_kernel(const uint64_t Nc, const dg_data_for_gpu *d_dggpu, const double *w, const double *dwdt)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < Nc)
    {
      double A = 0, Adot = 0;

      for(int b = NB_REDUCED; b < NB; b++)
        {
          A += w[w_idx_internal(b, 0, c)] * w[w_idx_internal(b, 0, c)];
          Adot += w[w_idx_internal(b, 0, c)] * dwdt[w_idx_internal(b, 0, c)];
        }

      double B    = A;
      double Bdot = Adot;

      for(decltype(ND) b = 0; b < NB_REDUCED; b++)
        {
          B += w[w_idx_internal(b, 0, c)] * w[w_idx_internal(b, 0, c)];
          Bdot += w[w_idx_internal(b, 0, c)] * dwdt[w_idx_internal(b, 0, c)];
        }

      double smthrate = 0;

      if(A != 0 && B != 0)
        smthrate = 2 * (Adot / A - Bdot / B);

      d_dggpu->d_smthrate[c] = smthrate;
    }
}

#endif  // ART_VISCOSITY

#if defined(SINGLE_SHOCK)
__global__ void single_shock_clear(const uint64_t Nc, double *dwdt, int ThisTask, int NTask)
{
  const auto c = blockIdx.x * blockDim.x + threadIdx.x;

  if(c < Nc)
    {
      if((ThisTask == 0 && c == 0) || (ThisTask == NTask - 1 && c == Nc - 1))
        {
          for(decltype(ND) b = 0; b < NB; b++)
            for(decltype(ND) f = 0; f < NF; f++)
              dwdt[w_idx_internal(b, f, c)] = 0;
        }
    }
}
#endif

#ifdef GRAVITY_WAVES
template <typename T>
__device__ T clamp(T x, T lowerlimit, T upperlimit)
{
  if(x < lowerlimit)
    x = lowerlimit;
  if(x > upperlimit)
    x = upperlimit;
  return x;
}

template <typename T>
__device__ T smootherstep(T edge0, T edge1, T x)
{
  // Scale, and clamp x to 0..1 range
  x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
  // Evaluate polynomial
  return x * x * x * (x * (x * 6 - 15) + 10);
}

__device__ void dg_add_in_source_gravity(int c, int qpoint_in_cell, double *fields, const dg_data_for_gpu *d_dggpu, state &st,
                                         double *dw_shared, const int dw_shared_index)
{
  const auto cell_offset = d_dggpu->FirstSlabThisTask * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D;

  // get coordinates of cell center
  const auto cell_center = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);

  auto xx = d_dggpu->cellSize * (cell_center.x + .5);
  auto yy = d_dggpu->cellSize * (cell_center.y + .5);
  auto zz = d_dggpu->cellSize * (cell_center.z + .5);

  // add qpoint coordinates
  const auto qxyz = dg_geometry_idx_to_cell(qpoint_in_cell, DG_NUM_QPOINTS_1D);

  xx += d_qpoint_location[qxyz.x] / 2 * d_dggpu->cellSize - .5 * d_dggpu->boxSize;
  yy += d_qpoint_location[qxyz.y] / 2 * d_dggpu->cellSize - .5 * d_dggpu->boxSize;
  zz += d_qpoint_location[qxyz.z] / 2 * d_dggpu->cellSize - .5 * d_dggpu->boxSize;

  // smoothly turn off gravity over a few kpc longward 200kpc
  const auto rr                    = sqrt(xx * xx + yy * yy + zz * zz);
  const auto smooth_transition_fac = 1 - smootherstep(200., 202., rr);
  const auto radial_g              = G0 * tanh(rr / RS) * smooth_transition_fac * st.rho;

  // project g onto the x-y plane
  const auto rr2d      = sqrt(xx * xx + yy * yy);
  const auto irr2d     = 1. / rr2d;
  const auto irr       = 1. / rr;
  const auto cos_theta = xx * irr2d;
  const auto sin_theta = yy * irr2d;
  const auto cos_phi   = zz * irr;
  const auto sin_phi   = rr2d * irr;

  // compute force density at the desired point
  const auto x_g = radial_g * cos_theta * sin_phi;
  const auto y_g = radial_g * sin_theta * sin_phi;
  const auto z_g = radial_g * cos_phi;

  auto fx = -x_g;
  auto fy = -y_g;
  auto fz = -z_g;

  double source[NF];  // storage for source function
  source[0] = 0;
  source[1] = fx;
  source[2] = fy;
  source[3] = fz;
#ifndef ISOTHERM_EQS
  source[4] = st.velx * fx + st.vely * fy + st.velz * fz;  // work term
#endif

  const auto fac = 0.125 * d_dggpu->cellVolume;

  // update dw/dt of the weights with the source term
  for(int f = 1; f < NF; f++)
    {
      for(decltype(ND) b = 0; b < NB; b++)
        {
          dw_shared[b + NB * f + NB * NF * dw_shared_index] += fac * source[f] *
                                                               d_dggpu->d_bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] *
                                                               d_qpoint_weights_internal[qpoint_in_cell];
        }
    }
}

#endif  // GRAVITY_WAVES
#ifdef VERY_CONSERVATIVE_TIMESTEP_CRITERION
__global__ void getMaxTistepUsingAllQpointsKernel(uint64_t Nc, double *w, double *qpoint_weights_positivity, double *v_arr,
                                                  double *csnd_arr)
{
  const auto cell = threadIdx.x + blockDim.x * blockIdx.x;
  const auto c    = cell;
  if(cell < Nc)
    {
      double v_max    = 0;
      double csnd_max = 0;

      for(int q = threadIdx.x; q < DG_NUM_POSITIVE_POINTS; q++)
        {
          double fields[NF]      = {0};
          double w_prim[NF * NB] = {0};

          for(decltype(ND) f = 0; f < NF; f++)
            {
              for(decltype(ND) b = 0; b < NB; b++)
                {
                  fields[f] += w[w_idx_internal(b, f, c)] * qpoint_weights_positivity[q * NB + b];
                }
              // calculate primitive variables
              // state st;
              // visc_params *viscParams;  // dummy visc_params
              // get_primitive_state_from_conserved_fields(fields, st, ViscParam);
              const auto v2 = calculate_velocity_squared_from_fields(fields);
              const auto v  = sqrt(v2);

              const auto rho = fields[0];

              v_max = max(v_max, v);

#ifndef ISOTHERM_EQS
              // const auto p2   = calculate_momenta_squared_from_fields(fields);
              const auto ptot = calculate_pressure_from_fields(fields, w);

              double csnd = sqrt(GAMMA * ptot / rho);
#else   // ISOTHERM_EQS
              double csnd = dggpu.ViscParam.IsoSoundSpeed;
#endif  // ISOTHERM_EQS

              csnd_max = max(csnd_max, csnd);
            }
          v_arr[cell]    = v_max;
          csnd_arr[cell] = csnd_max;
        }
    }
}
#endif  // VERY_CONSERVATIVE_TIMESTEP_CRITERION

__global__ void getMaxTistepKernelCellAverages(uint64_t Nc, double *w, double *v_arr, double *csnd_arr)
{
  const auto cell = threadIdx.x + blockDim.x * blockIdx.x;

  if(cell < Nc)
    {
      double fields[NF];

      for(decltype(ND) f = 0; f < NF; f++)
        fields[f] = w[w_idx_internal(0, f, cell)];  // we use the cell average here

      // calculate primitive variables
      const double rho  = fields[0];
      const double irho = 1.0 / rho;
      const double vx   = fields[1] * irho;

#if(ND == 1)
      double v = sqrt(vx * vx);
#endif
#if(ND == 2)
      const double vy = fields[2] * irho;
      double v        = sqrt(vx * vx + vy * vy);
#endif
#if(ND == 3)
      const double vy = fields[2] * irho;
      const double vz = fields[3] * irho;
      double v        = sqrt(vx * vx + vy * vy + vz * vz);
#endif

      const double v_max = v;

#ifndef ISOTHERM_EQS

#if(ND == 1)
      const double p2   = fields[1] * fields[1];
      const double ptot = (fields[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
      const double p2   = fields[1] * fields[1] + fields[2] * fields[2];
      const double ptot = (fields[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
      const double p2   = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];
      const double ptot = (fields[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif

      double csnd = sqrt(GAMMA * ptot / rho);
#else   // ISOTHERM_EQS
      double csnd = dggpu.ViscParam.IsoSoundSpeed;
#endif  // ISOTHERM_EQS

      const double csnd_max = csnd;

      v_arr[cell]    = v_max;
      csnd_arr[cell] = csnd_max;
    }
}

double getMaxTistepGpu(dg_data_t *dg)
{
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));
  double *v_arr, *csnd_arr;
  myCudaMalloc(&v_arr, Nc * sizeof(double));
  myCudaMalloc(&csnd_arr, Nc * sizeof(double));
#ifdef VERY_CONSERVATIVE_TIMESTEP_CRITERION
  getMaxTistepUsingAllQpointsKernel<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(
      Nc, dg->d_w, dg->d_qpoint_weights_positivity, v_arr, csnd_arr);
#else
  getMaxTistepKernelCellAverages<<<(Nc + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(Nc, dg->d_w, v_arr,
                                                                                                          csnd_arr);
#endif  // VERY_CONSERVATIVE_TIMESTEP_CRITERION

  thrust::pair<thrust::device_ptr<double>, thrust::device_ptr<double>> tuple;
  tuple        = thrust::minmax_element(thrust::device_pointer_cast(v_arr), thrust::device_pointer_cast(v_arr) + Nc);
  double v_max = tuple.second[0];

  tuple           = thrust::minmax_element(thrust::device_pointer_cast(csnd_arr), thrust::device_pointer_cast(csnd_arr) + Nc);
  double csnd_max = tuple.second[0];

  myCudaFree(v_arr, Nc * sizeof(double));
  myCudaFree(csnd_arr, Nc * sizeof(double));

  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[getMaxTistepGpu]: %f ms\n", time);

  MPI_Allreduce(MPI_IN_PLACE, &v_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &csnd_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);

  return All.CourantFac / (2.0 * DEGREE_K + 1) * dg->getCellSize() / (csnd_max + v_max);
}

void check_cell_for_trouble_gpu(double *d_wtemp, dg_data_t *dg, dg_data_for_gpu dggpu, char *d_troubled)
{
  float time, time_max;
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  // char trouble_detected = false;
  // cudaMemcpy(&dggpu.trouble_detected, &trouble_detected, sizeof(char), cudaMemcpyHostToDevice);

  check_cell_for_trouble_kernel<<<(Nc + 255) / 256, 256>>>(dg->d_w, d_wtemp, dggpu, d_troubled);

  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[check_cell_for_trouble_kernel_time]: %f ms\n", time);
}

#ifdef USE_PRIMITIVE_PROJECTION
void project_to_primitives_gpu(dg_data_t *dg, double *d_w, dg_data_for_gpu *d_dggpu)
{
  float time, time_max;
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  gpuErrchk(cudaMemset(dg->d_w_prim, 0, dg->N_w * sizeof(double)));
  project_to_primitives_kernel<<<(dg->N_w + 255) / 256, 256>>>(d_w, d_dggpu);
  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[project_to_primitives_kernel_time]: %f ms\n", time);
}
#endif

#endif  // GPU
