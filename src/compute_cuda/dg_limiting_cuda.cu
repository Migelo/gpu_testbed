#include "runrun.h"

#ifdef GPU

#include <assert.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_constantmemory.cuh"
#include "../compute_cuda/dg_cuda_kernels.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../compute_cuda/dg_limiting_cuda.h"
#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../io/dg_io.hpp"
#include "../tests/dg_geometry.hpp"
#include "../turbulence/dg_ab_turb.hpp"
#include "../utils/cuda_utils.h"
#include "../utils/dbl_compare.hpp"
#include "../utils/dg_utils.hpp"

void positivity_limiter_cuda(dg_data_t *dg, double *d_w, dg_data_for_gpu &dggpu)
{
#ifndef ENABLE_POSITIVITY_LIMITING
  return;
#else

  // events for timing
  cudaEvent_t startEvent, stopEvent;

  gpuErrchk(cudaEventCreate(&startEvent));
  gpuErrchk(cudaEventCreate(&stopEvent));
  gpuErrchk(cudaEventRecord(startEvent, 0));

  int Nlimited_rho[Nc], Nlimited_p[Nc];
  int *d_Nlimited_rho, *d_Nlimited_p;
  double LimiterInjectedMass[Nc], LimiterInjectedEnergy[Nc];
  double *d_LimiterInjectedMass, *d_LimiterInjectedEnergy;
  myCudaMalloc(&d_Nlimited_rho, sizeof(int) * Nc);
  myCudaMalloc(&d_Nlimited_p, sizeof(int) * Nc);
  myCudaMalloc(&d_LimiterInjectedMass, sizeof(double) * Nc);
  myCudaMalloc(&d_LimiterInjectedEnergy, sizeof(double) * Nc);
  gpuErrchk(cudaMemset(d_LimiterInjectedMass, 0, sizeof(double) * Nc));
  gpuErrchk(cudaMemset(d_LimiterInjectedEnergy, 0, sizeof(double) * Nc));
  gpuErrchk(cudaMemset(d_Nlimited_rho, 0, sizeof(int) * Nc));
  gpuErrchk(cudaMemset(d_Nlimited_p, 0, sizeof(int) * Nc));

  // now call positivity limiter kernel
  // clang-format off
  positivity_limiter_kernel<<<(Nc + 255) / 256, 256>>>(Nc, Nslabs, d_w, d_Nlimited_rho, d_Nlimited_p, d_LimiterInjectedMass, d_LimiterInjectedEnergy, dggpu);
  // clang-format on
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  dg_data_copy_array_from_gpu(Nc, d_Nlimited_rho, Nlimited_rho);
  dg_data_copy_array_from_gpu(Nc, d_Nlimited_p, Nlimited_p);
  dg_data_copy_array_from_gpu(Nc, d_LimiterInjectedMass, LimiterInjectedMass);
  dg_data_copy_array_from_gpu(Nc, d_LimiterInjectedEnergy, LimiterInjectedEnergy);
  myCudaFree(d_Nlimited_rho, sizeof(int) * Nc);
  myCudaFree(d_Nlimited_p, sizeof(int) * Nc);
  myCudaFree(d_LimiterInjectedMass, sizeof(double) * Nc);
  myCudaFree(d_LimiterInjectedEnergy, sizeof(double) * Nc);

  int NlimitedRho = 0;
  int NlimitedP   = 0;

  double LimiterInjectedMassTotal   = 0;
  double LimiterInjectedEnergyTotal = 0;
  for(uint64_t c = 0; c < Nc; c++)
    {
      NlimitedRho += Nlimited_rho[c];
      NlimitedP += Nlimited_p[c];
      LimiterInjectedMassTotal += LimiterInjectedMass[c];
      LimiterInjectedEnergyTotal += LimiterInjectedEnergy[c];
    }

  MPI_Allreduce(MPI_IN_PLACE, &NlimitedRho, 1, MPI_INT, MPI_SUM, MyComm);
  mpi_printf("[LIMITING] NlimitedRho= %d out of %ld , fraction of limited %1.4f at step %d\n", NlimitedRho, NC, 1. * NlimitedRho / NC,
             All.step);
  MPI_Allreduce(MPI_IN_PLACE, &NlimitedP, 1, MPI_INT, MPI_SUM, MyComm);
  mpi_printf("[LIMITING] NlimitedP= %d out of %ld , fraction of limited %1.4f at step %d\n", NlimitedP, NC, 1. * NlimitedP / NC,
             All.step);
  MPI_Allreduce(MPI_IN_PLACE, &LimiterInjectedMass, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &LimiterInjectedEnergy, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  mpi_printf("[LIMITING] injected mass=%e e=%e\n", LimiterInjectedMassTotal, LimiterInjectedEnergyTotal);

  dg->LimiterInjectedMass += LimiterInjectedMassTotal;
  dg->LimiterInjectedEnergy += LimiterInjectedEnergyTotal;

  gpuErrchk(cudaEventRecord(stopEvent, 0));
  gpuErrchk(cudaEventSynchronize(stopEvent));
  float time, time_max;
  gpuErrchk(cudaEventElapsedTime(&time, startEvent, stopEvent));
  MPI_Reduce(&time, &time_max, 1, MPI_FLOAT, MPI_MAX, 0, MyComm);
  mpi_printf("[positivity_limiter_cuda_time]: %f ms\n", time);

#endif
}

__global__ void fill_mean_cell_values(int numw, double *w, double *leftw_to_send, double *rightw_to_send, int celloffset)
{
  const int i = threadIdx.x + blockDim.x * blockIdx.x;

  if(i < numw)
    {
      int cell = i / NF;
      int f    = i - cell * NF;

      leftw_to_send[i]  = w[w_idx_internal(0, f, cell)];
      rightw_to_send[i] = w[w_idx_internal(0, f, cell + celloffset)];
    }
}

__global__ void positivity_limiter_kernel(int Nc, int Nslabs, double *w, int *Nlimited_rho, int *Nlimited_p,
                                          double *LimiterInjectedMass, double *LimiterInjectedEnergy, const dg_data_for_gpu dggpu)
{
  //  minimod limiter on the gpu
#if(DEGREE_K == 0)
  return;
#endif

  const auto i = blockIdx.x * blockDim.x + threadIdx.x;

  constexpr double rho_floor = 1e-2;

  if(i < Nc)
    {
      const auto c = i;

      const double rhomean = w[w_idx_internal(0, 0, c)];

      bool negative_density = rhomean < 0 ? true : false;

      if(negative_density)
        {
          if(!DENSITY_FLOOR_VAR)
            {
              printf("mean density rho=%g of cell=%u is negative\n", rhomean, c);
              assert(0);
            }

          LimiterInjectedMass[c] += (rho_floor - rhomean) * dggpu.cellVolume;
          w[w_idx_internal(0, 0, c)] = rho_floor;
          zero_out_higher_orders_of_field_in_cell(w, 0, c, &dggpu.ViscParam);
        }
      else
        {
          double rhomin = rhomean;

          for(auto q = 0; q < DG_NUM_POSITIVE_POINTS; q++)
            {
              double rho = 0;  // storage for density evaluated at the Gauss quadrature point

              for(uint b = 0; b < NB; b++)
                rho += w[w_idx_internal(b, 0, c)] * dggpu.d_qpoint_weights_positivity[q * NB + b];

              if(rho < rhomin)
                rhomin = rho;
            }

          double rho_bottom = 1.0e-6 * rhomean;

          if(rhomin < rho_bottom)
            {
              double fac = (rhomean - rho_bottom) / (rhomean - rhomin);

              // reducing the higher order weights
              for(decltype(ND) f = 0; f < NF; f++)
                {
                  for(int b = 1; b < NB; b++)
                    {
                      w[w_idx_internal(b, f, c)] *= fac;
                    }
                }

              Nlimited_rho[c]++;
            }
        }

#ifndef ISOTHERM_EQS
      // calculate the mean fields
      double fm[NF];

      get_average_fields_of_cell(fm, w, c);
      const auto ptot        = calculate_pressure_from_fields(fm, w);
      bool negative_pressure = ptot < 0 ? true : false;

      if(negative_pressure)
        {
          if(!PRESSURE_FLOOR_VAR)
            {
              printf("mean pressure=%g of cell=%u is negative\n", ptot, c);
              assert(0);
            }

          printf("[PRESSURE_FLOOR] detected negative pressure %f\n", ptot);

          constexpr auto f = ND + 1;

          // calculate primitive variables
          double fields[NF];
          get_average_fields_of_cell(fields, w, c);
          auto etot = calculate_energy_from_fields_and_IsoSoundSpeed(fields, dggpu);

          const auto etot_old        = w[w_idx_internal(0, f, c)];
          w[w_idx_internal(0, f, c)] = etot;

          zero_out_higher_orders_in_cell(w, c, &dggpu.ViscParam);

          LimiterInjectedEnergy[c] = (etot - etot_old) * dggpu.cellVolume;
        }
      else
        {
          const double pmean           = ptot;
          const double pressure_bottom = 1.0e-6 * pmean;

          for(int qpoint = 0; qpoint < DG_NUM_POSITIVE_POINTS; qpoint++)
            {
              // calculate the fields at the chosen point
              double fq[NF];

              get_conserved_fields_from_weights(w, dggpu.d_qpoint_weights_positivity, fq, c, qpoint);

              auto ptot = calculate_pressure_from_fields(fq, w);

              if(ptot < pressure_bottom)
                {
                  Nlimited_p[c]++;

                  int iter = 0;
                  do
                    {
                      iter++;
                      double fac = 0.5;

                      if(iter > 10)
                        fac = 0;

                      for(decltype(ND) f = 0; f < NF; f++)
                        for(int b = 1; b < NB; b++)
                          w[w_idx_internal(b, f, c)] *= fac;

                      for(decltype(ND) f = 0; f < NF; f++)
                        {
                          fq[f] = 0;

                          for(decltype(ND) b = 0; b < NB; b++)
                            fq[f] += w[w_idx_internal(b, f, c)] * dggpu.d_qpoint_weights_positivity[qpoint * NB + b];
                        }

                      const double rho  = fq[0];
                      const double irho = 1.0 / rho;

#if(ND == 1)
                      const double p2 = fq[1] * fq[1];
                      ptot            = (fq[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
                      const double p2 = fq[1] * fq[1] + fq[2] * fq[2];
                      ptot            = (fq[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
                      const double p2 = fq[1] * fq[1] + fq[2] * fq[2] + fq[3] * fq[3];
                      ptot            = (fq[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif

                      // printf("ptot=%g  pbottom=%g  pmean=%g\n", ptot, pressure_bottom, pmean);

                      if(iter > 11)
                        {
                          printf("too many iterations");
                          assert(0);
                        }
                    }
                  while(ptot < pressure_bottom);
                }
            }
        }
#endif  // ISOTHERM_EQS
    }
}

#endif  // GPU
