// Copyright 2021 Volker Springel
#ifndef DG_LIMITING_HPP_
#define DG_LIMITING_HPP_

#include "runrun.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <vector>

#ifdef GPU
#include <cuda_runtime.h>
#else
#define __host__
#define __device__
#define __forceinline__ inline
#define __global__
#endif

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/dg_params.h"
#include "../utils/dg_utils.hpp"

void positivity_limiter(dg_data_t *dg, double *w);

#endif  // DG_LIMITING_HPP_
