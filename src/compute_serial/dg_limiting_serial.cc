// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_serial/dg_limiting_serial.h"
#include "../data/allvars.h"
#include "../data/dg_params.h"
#include "../riemann/riemann_hllc.hpp"
#include "../utils/dbl_compare.hpp"
#include "../utils/dg_utils.hpp"

/*!
 * apply the positivity limiter to a weights array
 */
void positivity_limiter(dg_data_t *dg, double *w)
{
#ifndef ENABLE_POSITIVITY_LIMITING
  return;
#else

  mpi_printf("[LIMITING] applying positivity limiting\n");

  int count       = 0;
  int count_reset = 0;

  constexpr double rho_floor = 1e-2;  // FIX-ME
  double LimiterInjectedMass = 0;
  // loop over all cells, enforcing positive density
  for(uint64_t c = 0; c < Nc; c++)
    {
      bool negative_density = false;
      double rhomean        = w[w_idx_internal(0, 0, c)];
      if(rhomean < 0)
        {
          negative_density = true;

          if(!DENSITY_FLOOR_VAR)
            {
              Terminate("mean density rho=%g of cell=%lu is negative\n", rhomean, c);
            }
        }

      if(negative_density)
        {
          count_reset++;
          LimiterInjectedMass += (rho_floor - rhomean) * dg->getCellVolume();
          w[w_idx_internal(0, 0, c)] = rho_floor;
          for(uint b = 1; b < NB; b++)
            {
              w[w_idx_internal(b, 0, c)] = 0;
            }
        }
#if(DEGREE_K > 0)
      else
        {
          double rhomin = rhomean;

          for(decltype(ND) q = 0; q < DG_NUM_POSITIVE_POINTS; q++)
            {
              double rho = 0;  // storage for density evaluated at the Gauss quadrature point

              for(decltype(ND) b = 0; b < NB; b++)
                rho += w[w_idx_internal(b, 0, c)] * dg->qpoint_weights_positivity[q * NB + b];

              if(rho < rhomin)
                rhomin = rho;
            }

          double rho_bottom = 1.0e-6 * rhomean;

          if(rhomin < rho_bottom)
            {
              double fac = (rhomean - rho_bottom) / (rhomean - rhomin);

              // reducing the higher order weights

              for(decltype(ND) f = 0; f < NF; f++)
                for(decltype(ND) b = 1; b < NB; b++)
                  w[w_idx_internal(b, f, c)] *= fac;

              count++;
            }
        }
#endif
    }

  MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPI_INT, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &count_reset, 1, MPI_INT, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &LimiterInjectedMass, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  mpi_printf("[LIMITING] triggered the density minimum limiting %d times, fraction: %1.4f\n", count, ((double)count) / NC);
  mpi_printf("[LIMITING] triggered the density floor %d times, fraction: %1.4f\n", count_reset, ((double)count_reset) / NC);

#ifndef ISOTHERM_EQS
  count       = 0;
  count_reset = 0;

  double LimiterInjectedEnergy = 0;
  // loop over all cells, enforcing positive pressure
  for(uint64_t c = 0; c < Nc; c++)
    {
      bool negative_pressure = false;
      // calculate the mean fields
      double fm[NF];

      for(decltype(ND) f = 0; f < NF; f++)
        fm[f] = w[w_idx_internal(0, f, c)];

      const double rho  = fm[0];
      const double irho = 1.0 / rho;

#if(ND == 1)
      const double p2 = fm[1] * fm[1];
      double ptot     = (fm[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
      const double p2 = fm[1] * fm[1] + fm[2] * fm[2];
      double ptot     = (fm[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
      const double p2 = fm[1] * fm[1] + fm[2] * fm[2] + fm[3] * fm[3];
      double ptot     = (fm[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif

      if(ptot < 0)
        {
          negative_pressure = true;
          if(!PRESSURE_FLOOR_VAR)
            {
              Terminate("mean pressure=%g of cell=%lu is negative, e=%1.2e, Wk=%1.2e\n", ptot, c, fm[4], 0.5 * p2 * irho);
            }
        }

      if(negative_pressure)
        {
#ifdef TURBULENCE
          count_reset++;

#if(DEGREE_K > 0)
#if(ND == 2)
          constexpr double fac = 0.25;
#endif
#if(ND == 3)
          constexpr double fac = 0.125;
#endif

          double w_new[NB];
          for(decltype(ND) b = 0; b < NB; b++)
            w_new[b] = 0;

          for(decltype(ND) q = 0; q < DG_NUM_INNER_QPOINTS; q++)
            {
              double fields[NF];  // fields evaluated at the Gauss quadrature point

              for(decltype(ND) f = 0; f < NF; f++)
                {
                  fields[f] = 0;

                  for(decltype(ND) b = 0; b < NB; b++)
                    fields[f] += dg->w[w_idx_internal(b, f, c)] * dg->bfunc_internal[bfunc_internal_idx(q, b)];
                }

              // calculate primitive variables
              const double irho = 1.0 / fields[0];

#if(ND == 3)
              const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];

// recomputing the desired value for fields[4]
#ifdef TURBULENCE
              const double etot =
                  0.5 * p2 * irho + fields[0] * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed / (GAMMA - 1.0);
#else
              const double etot = 0.5 * p2 * irho + fields[0] * 1 / (GAMMA - 1.0);

#endif  // TURBULENCE

#endif

#if(ND == 2)
              const double p2 = fields[1] * fields[1] + fields[2] * fields[2];

// recomputing the desired value for fields[3]
#ifdef TURBULENCE
              const double etot =
                  0.5 * p2 * irho + fields[0] * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed / (GAMMA - 1.0);
#else
              const double etot = 0.5 * p2 * irho + fields[0] * 1 / (GAMMA - 1.0);
#endif
#endif

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  w_new[b] += fac * etot * dg->bfunc_internal[bfunc_internal_idx(q, b)] * dg->qpoint_weights_internal[q];
                }
            }

#if(ND == 3)
          constexpr auto f = 4;
#endif

#if(ND == 2)
          constexpr auto f = 3;
#endif

          LimiterInjectedEnergy += (w_new[0] - dg->w[w_idx_internal(0, f, c)]) * dg->getCellVolume();

          // now store the new expansion of the energy density in the corresponding weigths
          for(decltype(ND) b = 0; b < NB; b++)
            {
              dg->w[w_idx_internal(b, f, c)] = w_new[b];
            }

#else  // here comes the DEGREE_K==0 case

#ifdef TURBULENCE
          const double etot = 0.5 * p2 * irho + fm[0] * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed / (GAMMA - 1.0);
#else
          const double etot = 0.5 * p2 * irho + fm[0] * 1 / (GAMMA - 1.0);
#endif
          fm[1 + ND] = etot;
#endif  // if(DEGREE_K > 0)

#endif  // TURBULENCE
        }

#if(DEGREE_K > 0)
      {
        const double pmean           = ptot;
        const double pressure_bottom = 1.0e-6 * pmean;

        for(decltype(ND) q = 0; q < DG_NUM_POSITIVE_POINTS; q++)
          {
            // calculate the fields at the chosen point
            double fq[NF];

            for(decltype(ND) f = 0; f < NF; f++)
              {
                fq[f] = 0;

                for(decltype(ND) b = 0; b < NB; b++)
                  fq[f] += w[w_idx_internal(b, f, c)] * dg->qpoint_weights_positivity[q * NB + b];
              }

            const double rho  = fq[0];
            const double irho = 1.0 / rho;

#if(ND == 1)
            const double p2 = fq[1] * fq[1];
            double ptot     = (fq[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
            const double p2 = fq[1] * fq[1] + fq[2] * fq[2];
            double ptot     = (fq[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
            const double p2 = fq[1] * fq[1] + fq[2] * fq[2] + fq[3] * fq[3];
            double ptot     = (fq[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif

            if(ptot < pressure_bottom)
              {
                count++;

                int iter = 0;
                do
                  {
                    iter++;
                    double fac = 0.5;

                    if(iter > 10)
                      fac = 0;

                    for(decltype(ND) f = 0; f < NF; f++)
                      for(decltype(ND) b = 1; b < NB; b++)
                        w[w_idx_internal(b, f, c)] *= fac;

                    for(decltype(ND) f = 0; f < NF; f++)
                      {
                        fq[f] = 0;

                        for(decltype(ND) b = 0; b < NB; b++)
                          fq[f] += w[w_idx_internal(b, f, c)] * dg->qpoint_weights_positivity[q * NB + b];
                      }

                    const double rho  = fq[0];
                    const double irho = 1.0 / rho;

#if(ND == 1)
                    const double p2 = fq[1] * fq[1];
                    ptot            = (fq[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
                    const double p2 = fq[1] * fq[1] + fq[2] * fq[2];
                    ptot            = (fq[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
                    const double p2 = fq[1] * fq[1] + fq[2] * fq[2] + fq[3] * fq[3];
                    ptot            = (fq[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
                    if(iter > 11)
                      Terminate("too many iterations");
                  }
                while(ptot < pressure_bottom);
              }
          }
      }
#endif
    }
  MPI_Allreduce(MPI_IN_PLACE, &LimiterInjectedEnergy, 1, MPI_DOUBLE, MPI_SUM, MyComm);
  dg->LimiterInjectedMass += LimiterInjectedMass;
  dg->LimiterInjectedEnergy += LimiterInjectedEnergy;

  MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPI_INT, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &count_reset, 1, MPI_INT, MPI_SUM, MyComm);

  mpi_printf("[LIMITING] triggered the pressure minimum limiting %d times, fraction: %1.4f\n", count, ((double)count) / NC);
  mpi_printf("[LIMITING] triggered the pressure floor %d times, fraction: %1.4f\n", count_reset, ((double)count_reset) / NC);
  mpi_printf("[LIMITING] injected mass=%e e=%e\n", LimiterInjectedMass, LimiterInjectedEnergy);

#endif

#endif
}

void positivity_density_limiter(dg_data_t *dg, double *w)
{
  return;
  mpi_printf("[LIMITING] applying positive density limiting\n");

  constexpr double rho_floor = 1e-4;  // FIX-ME

  int count_reset = 0;

  double LimiterInjectedMass = 0;

  // loop over all cells, enforcing positive density
  for(uint64_t c = 0; c < Nc; c++)
    {
      double rhomean = w[w_idx_internal(0, 0, c)];

      if(rhomean < rho_floor)
        {
          count_reset++;
          LimiterInjectedMass += (rho_floor - rhomean) * dg->getCellVolume();
          w[w_idx_internal(0, 0, c)] = rho_floor;
          for(uint b = 1; b < NB; b++)
            w[w_idx_internal(b, 0, c)] = 0;
        }
#if(DEGREE_K > 0)
      else
        {
          for(decltype(ND) q = 0; q < DG_NUM_POSITIVE_POINTS; q++)
            {
              double rho = 0;  // storage for density evaluated at the Gauss quadrature point

              for(decltype(ND) b = 0; b < NB; b++)
                rho += w[w_idx_internal(b, 0, c)] * dg->qpoint_weights_positivity[q * NB + b];

              if(rho < rho_floor)
                {
                  count_reset++;
                  for(uint b = 1; b < NB; b++)
                    w[w_idx_internal(b, 0, c)] = 0;

                  break;
                }
            }
        }
#endif
    }

  MPI_Allreduce(MPI_IN_PLACE, &count_reset, 1, MPI_INT, MPI_SUM, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &LimiterInjectedMass, 1, MPI_DOUBLE, MPI_SUM, MyComm);

  mpi_printf("[LIMITING] triggered the density floor %d times, fraction: %1.4f, injected mass=%g\n", count_reset,
             ((double)count_reset) / NC, LimiterInjectedMass);
}
