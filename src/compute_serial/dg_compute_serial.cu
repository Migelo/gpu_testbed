// Copyright 2020 Miha Cernetic
#include "runrun.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_general/compute_general.cuh"
#include "../compute_serial/dg_compute_serial.hpp"
#include "../compute_serial/dg_limiting_serial.h"
#include "../cooling/source_function.hpp"
#include "../data/allvars.h"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../io/dg_io.hpp"
#include "../legendre/dg_legendre.hpp"
#include "../riemann/riemann_hllc.hpp"
#include "../tests/dg_setup_test_problems.hpp"
#include "../turbulence/dg_ab_turb.hpp"
#include "../units/units.h"
#include "../utils/dg_utils.hpp"
#include "../utils/misc.h"

bool dg_advance_timestep_serial(dg_data_t *dg, double dt)
{
  // here we do a timestep with a Runge-Kutta scheme
  mpi_printf("Starting a serial DG timestep\n");

  // create space to store the right hand side of the different stages
  vector<double> dWdt[RK_STAGES];
  double *w_temp = new double[dg->N_w];

  for(int s = 0; s < RK_STAGES; s++)
    dWdt[s].resize(dg->N_w);

  // marker for cells that need to be treated somehow to avoid negative density
  char *troubled = (char *)malloc(Nc * sizeof(char));
  for(uint64_t i = 0; i < Nc; i++)
    troubled[i] = false;

  int n_troubled_cells                = 0;
  int n_troubled_cells_total          = 0;
  int previous_n_troubled_cells_total = 0;

  int timestep_reduction_count = 0;

#ifdef COOLING
  measure_box_temperature(dg);
#endif  // COOLING

  do
    {
      char trouble_detected = false;

      // calculate the r.h.s of the differential equation
      for(dg->current_rk_stage = 0; dg->current_rk_stage < RK_STAGES; dg->current_rk_stage++)
        {
          for(uint64_t i = 0; i < dg->N_w; i++)
            w_temp[i] = dg->w[i];

          // calculate argument w for R(t,w) (current solution)
          for(int m = 0; m < dg->current_rk_stage; m++)
            {
              if(dg->ButcherTab_a[dg->current_rk_stage][m] != 0)
                for(uint64_t i = 0; i < dg->N_w; i++)
                  w_temp[i] += (dg->ButcherTab_a[dg->current_rk_stage][m] * dt) * dWdt[m][i];
            }

          // first, let's check whether the current intermediate state would be valid
          if(dg->current_rk_stage != 0)
            {
              mpi_printf("dg->current_rk_stage=%d|RK_STAGES=%d\n", dg->current_rk_stage, RK_STAGES);
              //         positivity_density_limiter(dg, w_temp);
              for(uint64_t cell = 0; cell < Nc; cell++)  // check all cells
                {
                  if(check_cell_for_trouble(dg->qpoint_weights_positivity.data(), w_temp, cell, &All.ViscParam))
                    {
                      if(!troubled[cell])
                        {
                          trouble_detected = true;
                          n_troubled_cells++;
                          troubled[cell] = true;
                          make_cell_flat(dg, cell);
                        }
                      else
                        make_cell_flat(w_temp, cell);
                    }
                }

              MPI_Allreduce(MPI_IN_PLACE, &trouble_detected, 1, MPI_BYTE, MPI_MAX, MyComm);
              if(trouble_detected)  // we break in this case and will abondon this step
                break;
            }
#ifdef COOLING
          enforce_temperature_floor(dg, w_temp);
#endif  // COOLING

          // now do main computation of right-hand-side
          dg_compute_serial(dg, w_temp, dWdt[dg->current_rk_stage], troubled, dt);

#ifdef ART_VISCOSITY
          if(dg->current_rk_stage == RK_STAGES - 1)
            for(uint64_t c = 0; c < Nc; c++)  // loop over local cell
              {
                dg->smthness[c] = dg_compute_smoothness_indicator(w_temp, c);
                dg->smthrate[c] = dg_compute_smoothrate_indicator(w_temp, dWdt[dg->current_rk_stage], c);
              }
#endif
        }

      if(trouble_detected == false)
        {
          // we are OK so far, determine the final weights
          for(uint64_t i = 0; i < dg->N_w; i++)
            {
              w_temp[i] = dg->w[i];
              for(int s = 0; s < RK_STAGES; s++)
                w_temp[i] += (dg->ButcherTab_b[s] * dt) * dWdt[s][i];
            }

          // check whether the final state would run into problems
          //      positivity_density_limiter(dg, w_temp);

          for(uint64_t cell = 0; cell < Nc; cell++)
            {
              if(check_cell_for_trouble(dg->qpoint_weights_positivity.data(), w_temp, cell, &All.ViscParam))
                {
                  if(!troubled[cell])
                    {
                      trouble_detected = true;
                      n_troubled_cells++;
                      troubled[cell] = true;
                      make_cell_flat(dg, cell);
                    }
                  else
                    make_cell_flat(w_temp, cell);
                }
            }
        }
      MPI_Allreduce(MPI_IN_PLACE, &trouble_detected, 1, MPI_BYTE, MPI_MAX, MyComm);
      MPI_Allreduce(&n_troubled_cells, &n_troubled_cells_total, 1, MPI_INT, MPI_SUM, MyComm);

      if(trouble_detected == false)
        {
#ifdef TURBULENCE
          dg_store_driving_energy(dg, 0);
#endif
          // final state OK, update the final weights
          for(uint64_t i = 0; i < dg->N_w; i++)
            dg->w[i] = w_temp[i];

#ifdef TURBULENCE
          dg_store_driving_energy(dg, 1);
          dg_reset_temperature(dg);
#endif
          break;
        }
      else
        {
          timestep_reduction_count++;

          if(n_troubled_cells_total == previous_n_troubled_cells_total)
            {
              // continue - no new troubled cells found, so let's try to continue the step again with half the step size

              dt /= 2;
              dg->TimeStep /= 2;
            }
        }

      if(timestep_reduction_count > 100)
        {
          Terminate("Iteration count exceeded. Something is probably wrong\n");
        }

      previous_n_troubled_cells_total = n_troubled_cells_total;
      mpi_printf("REPEAT needed (%d iteration, %d troubled cells, frac=%g) timestep %g\n", timestep_reduction_count,
                 n_troubled_cells_total, (double)n_troubled_cells_total / pow(DG_NUM_CELLS_1D, ND), dt);
    }
  while(true);

#ifdef COOLING
  enforce_temperature_floor(dg, dg->w);
#endif  // COOLING

  free(troubled);
  delete[] w_temp;
  mpi_printf("Finished serial DG timestep.\n");

  return timestep_reduction_count;
}

void make_cell_flat(dg_data_t *dg, uint64_t cell) { make_cell_flat(dg->w, cell); }

void make_cell_flat(double *w, uint64_t cell)
{
  for(int f = 0; f < NF; f++)
    for(int b = 1; b < NB; b++)
      w[w_idx_internal(b, f, cell)] = 0;

#ifdef DENSITY_FLOOR
  if(w[w_idx_internal(0, 0, cell)] < All.ViscParam.DensityFloor)
    {
      w[w_idx_internal(0, 0, cell)] = 2.0 * All.ViscParam.DensityFloor;
    }
#endif
}

void dg_compute_serial(dg_data_t *dg, double *w, vector<double> &dwdt, char *troubled, double dt)
{
  // Args, input:
  // dg - struct holding basic dg data
  // w  - current state of weights
  // Output:
  // dwdt - calculated rate of change of the weights for this value of w

  // start the clock
  float t = clock();

#ifdef USE_PRIMITIVE_PROJECTION
  double *w_prim = (double *)malloc(dg->N_w * sizeof(double));
  // vector<double> w_prim(dg->N_w, 0);
  project_to_primitives_cpu(dg, w, w_prim);
#endif

  // clear derivative
  for(uint64_t i = 0; i < dg->N_w; i++)
    dwdt[i] = 0.0;

  const int leftTask  = ThisTask > 0 ? ThisTask - 1 : NTask - 1;
  const int rightTask = ThisTask < NTask - 1 ? ThisTask + 1 : 0;

#if(ND == 1)
  const double fac_area = dg->getFaceArea();
#elif(ND == 2)
  const double fac_area = 0.5 * dg->getFaceArea();
#elif(ND == 3)
  const double fac_area = 0.25 * dg->getFaceArea();
#endif

  const double fac_cellsize = 2.0 / dg->getCellSize();
#ifdef VISCOSITY
  const double fac_cellsize_axis = 2.0 / dg->getCellSize() / (2 * dg->legendre_overlapp_eta);
#endif

  // ----------------------------------------------------------------------------------------------------------------------------
  // compute the contribution from the internal volume integral over the fluxes, get minimum und maximum of primitive vars
  // ----------------------------------------------------------------------------------------------------------------------------
  for(decltype(Nc) c = 0; c < Nc; c++)  // loop over local cells
    {
      for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
        {
          double fields[NF];  // storage for field evaluated at the Gauss quadrature point

          // get conserved fields at quadrature point
          get_conserved_fields_from_weights(w, dg->bfunc_internal.data(), fields, c, q);

          // calculate primitive variables
          state st;
          get_primitive_state_from_conserved_fields(fields, st, &All.ViscParam);

#ifdef RICHTMYER_VISCOSITY
          // evaluate velocity divergence
          double divvel = 0;
          for(int d = 0; d < ND; d++)
            {
              double dgrad[1 + ND] = {0};
              for(int f = 0; f < 1 + ND; f++)
                for(int b = 0; b < NB; b++)
                  dgrad[f] += fac_cellsize * w[w_idx_internal(b, f, c)] * dg->bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];

              divvel += (dgrad[1 + d] - st.s[1 + d] * dgrad[0]) / st.rho;
            }

          if(divvel < 0)
            {
              double csnd = sqrt(GAMMA * st.press / st.rho);

              double press = All.ViscParam.ArtViscRichtmyer * st.rho * pow(All.ViscParam.Resolution * divvel, 2) +
                             All.ViscParam.ArtViscLandshoff * st.rho * fabs(All.ViscParam.Resolution * divvel) * csnd;

              double press_max = 0.5 * st.rho * pow(All.ViscParam.Resolution, 2) * fabs(divvel) / dt;

              if(press > press_max)
                press = press_max;

              st.press += press;
            }
#endif

          // calculate full flux
          double flux[NF * ND];
          get_flux_from_primitive_state_and_conserved_fields(fields, st, flux);

#if defined(VISCOSITY)
          // evaluate gradient of conserved states
          double grad_fields[NF * ND];

          for(int f = 0; f < NF; f++)
            for(int d = 0; d < ND; d++)
              {
                grad_fields[f * ND + d] = 0;

                for(int b = 0; b < NB; b++)
                  grad_fields[f * ND + d] +=
                      fac_cellsize * w[w_idx_internal(b, f, c)] * dg->bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)];
              }

          // calculate viscous flux internally
          double viscous_flux[NF * ND];
          get_viscous_flux_from_conserved_fields_and_their_gradients(fields, grad_fields, viscous_flux, &All.ViscParam, -1, st, st);

          // subtract viscous flux from regular flux
          for(int f = 0; f < NF; f++)
            for(int d = 0; d < ND; d++)
              flux[f + d * NF] -= viscous_flux[f * ND + d];
#endif

          // update rate of weight changes
          for(int f = 0; f < NF; f++)
            for(int b = 0; b < NB; b++)
              {
                double temp = 0;
                for(int d = 0; d < ND; d++)
                  temp +=
                      flux[f + d * NF] * dg->bfunc_internal_diff[bfunc_diff_internal_idx(q, b, d)] * dg->qpoint_weights_internal[q];

                dwdt[w_idx_internal(b, f, c)] += fac_area * temp;
              }

#ifdef TURBULENCE
          dg_add_in_source_function_serial(dg, c, q, fields, st, dwdt);
#endif

#ifdef ART_VISCOSITY
          dg_add_in_source_alpha_serial(dg, c, q, fields, grad_fields, st, dwdt);
#endif

#ifdef GRAVITY_WAVES
          dg_add_in_gravity_source_function_serial(dg, c, q, fields, st, dwdt);
#endif
#ifdef COOLING
          source_function_cooling_serial(dg, c, q, fields, st, dwdt);
#endif
        }  // for every qpoint

    }  // for every cell

  // -----------------------------------------------------------------------------------------------------------------------
  // start, compute the states at the quadpoints on the right/left side of the leftmost/rightmost slab interface
  // -----------------------------------------------------------------------------------------------------------------------

  constexpr int states_on_plane = cell_stride_x * DG_NUM_OUTER_QPOINTS;

  fields_face *right_states_to_recv = (fields_face *)malloc(states_on_plane * sizeof(fields_face));
  fields_face *left_states_to_recv  = (fields_face *)malloc(states_on_plane * sizeof(fields_face));

  fields_face *right_states_to_send = (fields_face *)malloc(states_on_plane * sizeof(fields_face));
  fields_face *left_states_to_send  = (fields_face *)malloc(states_on_plane * sizeof(fields_face));

  for(int phase = 0; phase < 2; phase++)
    {
      int x = (phase == 0) ? 0 : Nslabs - 1;

      int st_count = 0;

      for(int y = 0; y < NUM_CELLS_Y; y++)
        for(int z = 0; z < NUM_CELLS_Z; z++)
          {
            const auto cell = cell_stride_x * x + cell_stride_y * y + z;

            for(int q = 0; q < DG_NUM_OUTER_QPOINTS; q++)
              {
                // evaluate fields at right or left of boundary

                fields_face *face = (phase == 0) ? &right_states_to_send[st_count] : &left_states_to_send[st_count];
                st_count++;

                for(int f = 0; f < NF; f++)
                  {
                    face->fields[f] = 0;

                    for(int b = 0; b < NB; b++)
                      {
#ifdef USE_PRIMITIVE_PROJECTION
                        face->fields[f] +=
                            (phase == 0)
                                ? w_prim[w_idx_internal(b, f, cell)] * dg->bfunc_external[bfunc_external_side_idx(q, b, 0 + ND)]
                                : w_prim[w_idx_internal(b, f, cell)] * dg->bfunc_external[bfunc_external_side_idx(q, b, 0)];
#else
                        face->fields[f] +=
                            (phase == 0) ? w[w_idx_internal(b, f, cell)] * dg->bfunc_external[bfunc_external_side_idx(q, b, 0 + ND)]
                                         : w[w_idx_internal(b, f, cell)] * dg->bfunc_external[bfunc_external_side_idx(q, b, 0)];
#endif
                      }
#ifdef VISCOSITY
                    face->fields_surface[f] = 0;

                    for(int d = 0; d < ND; d++)
                      face->grad_fields_surface[f * ND + d] = 0;

                    for(int l = 0; l < NB_INCREASED; l++)
                      {
                        double wg = 0;

                        for(int i = 0; i < dg->bfunc_project_count[0][l]; i++)
                          {
                            int index = dg->bfunc_project_first[0][l] + i;
                            int b     = dg->bfunc_project_list[0][index];

                            wg += (phase == 0) ? (1.0 / 2) * w[w_idx_internal(b, f, cell)] * dg->bfunc_project_value_right[0][index]
                                               : (1.0 / 2) * w[w_idx_internal(b, f, cell)] * dg->bfunc_project_value_left[0][index];
                          }

                        face->fields_surface[f] += wg * dg->bfunc_mid[bfunc_mid_idx(q, l, 0)];

                        for(int d = 0; d < ND; d++)
                          face->grad_fields_surface[f * ND + d] += wg * dg->bfunc_mid_diff[bfunc_mid_diff_idx(q, l, d, 0)];
                      }

                    for(int d = 0; d < ND; d++)
                      if(d == 0)
                        face->grad_fields_surface[f * ND + d] *= fac_cellsize_axis;
                      else
                        face->grad_fields_surface[f * ND + d] *= fac_cellsize;
#endif
#ifdef FINITE_VOLUME
                    face->troubled = troubled[cell];
#endif
                  }
              }
          }

      if(st_count != states_on_plane)
        Terminate("st_count != states_on_plane");
    }

  // ----------------------------------------------------------------------------------------------------------------------------
  // carry out the MPI transfer of these plane of states
  // ----------------------------------------------------------------------------------------------------------------------------

  myMPI_exchange_of_interface_data(right_states_to_send, left_states_to_recv, leftTask, left_states_to_send, right_states_to_recv,
                                   rightTask, states_on_plane * sizeof(fields_face));

  free(left_states_to_send);
  free(right_states_to_send);

#ifdef FINITE_VOLUME
  // --------------------------------------------------------------------------------------------------------------------------------
  // estimate the x-direction slopes for the leftmost and rightmost slabs, and communicate those
  // --------------------------------------------------------------------------------------------------------------------------------
  slopes *right_slopes_to_recv = (slopes *)malloc(states_on_plane * sizeof(slopes));
  slopes *left_slopes_to_recv  = (slopes *)malloc(states_on_plane * sizeof(slopes));

  slopes *right_slopes_to_send = (slopes *)malloc(states_on_plane * sizeof(slopes));
  slopes *left_slopes_to_send  = (slopes *)malloc(states_on_plane * sizeof(slopes));

  for(int phase = 0; phase < 2; phase++)
    {
      int x = (phase == 0) ? 0 : Nslabs - 1;

      int st_count = 0;
      for(int y = 0; y < NUM_CELLS_Y; y++)
        for(int z = 0; z < NUM_CELLS_Z; z++)
          {
            const auto cell   = cell_stride_x * x + cell_stride_y * y + z;
            const auto cell_L = cell - cell_stride_x;
            const auto cell_R = cell + cell_stride_x;

            // note: DG_NUM_OUTER_QPOINTS==1 for FINITE_VOLUME

            slopes &sl = (phase == 0) ? right_slopes_to_send[st_count] : left_slopes_to_send[st_count];

            double fields_L[NF];
            double fields[NF];
            double fields_R[NF];

            // note: for finite volume, we have NB=1 and the bfunc-values are just 1, thus no need to loop over the basis
            // functions
            for(int f = 0; f < NF; f++)
              {
                fields_L[f] = (x > 0) ? w[w_idx_internal(0, f, cell_L)] : left_states_to_recv[st_count].fields[f];
                fields[f]   = w[w_idx_internal(0, f, cell)];
                fields_R[f] = (x < Nslabs - 1) ? w[w_idx_internal(0, f, cell_R)] : right_states_to_recv[st_count].fields[f];
              }

            state st_L;
            get_primitive_state_from_conserved_fields(fields_L, st_L, &All.ViscParam);
            state st;
            get_primitive_state_from_conserved_fields(fields, st, &All.ViscParam);
            state st_R;
            get_primitive_state_from_conserved_fields(fields_R, st_R, &All.ViscParam);

            do_slope_limiting(st_L, st, st_R, 0, sl);

            st_count++;
          }

      if(st_count != states_on_plane)
        Terminate("st_count != states_on_plane");
    }

  // ----------------------------------------------------------------------------------------------------------------------------
  // carry out the MPI transfer of these planes of slopes
  // ----------------------------------------------------------------------------------------------------------------------------

  myMPI_exchange_of_interface_data(right_slopes_to_send, left_slopes_to_recv, leftTask, left_slopes_to_send, right_slopes_to_recv,
                                   rightTask, states_on_plane * sizeof(slopes));

  free(left_slopes_to_send);
  free(right_slopes_to_send);
#endif

  // ----------------------------------------------------------------------------------------------------------------------------
  // now let's compute the surface fluxes
  // ----------------------------------------------------------------------------------------------------------------------------

  for(int axis = 0; axis < ND; axis++)  // do the three coordinate directions in turn
    for(int x = -1; x < Nslabs; x++)    // loop over all slab interfaces
      {
        if(axis != 0 && x == -1)  // only for the x-direction we consider the leftmost interface on the slab
          continue;

        int st_count = 0;

        for(int y = -1; y < NUM_CELLS_Y; y++)
          {
            if(axis != 1 && y == -1)  // only for the y-direction we consider the leftmost interface
              continue;

            for(int z = -1; z < NUM_CELLS_Z; z++)
              {
                if(axis != 2 && z == -1)  // only for the y-direction we consider the leftmost interface
                  continue;

                const auto cell_L =
                    cell_stride_x * x + cell_stride_y * ((y >= 0) ? y : y + NUM_CELLS_Y) + ((z >= 0) ? z : z + NUM_CELLS_Z);

                int xx = x;
                if(axis == 0)
                  xx++;

                int yy = y;
                if(axis == 1)
                  yy++;

                int zz = z;
                if(axis == 2)
                  zz++;

                const auto cell_R = cell_stride_x * xx + cell_stride_y * ((yy < NUM_CELLS_Y) ? yy : yy - NUM_CELLS_Y) +
                                    ((zz < NUM_CELLS_Z) ? zz : zz - NUM_CELLS_Z);

#ifdef FINITE_VOLUME
                int xm = x;
                if(axis == 0)
                  xm--;

                int xp = xx;
                if(axis == 0)
                  xp++;
                int ym = y;
                int yp = yy;
                if(axis == 1)
                  {
                    ym--;
                    if(ym < 0)
                      ym += NUM_CELLS_Y;
                    yp++;
                    if(yp >= NUM_CELLS_Y)
                      yp -= NUM_CELLS_Y;
                  }
                int zm = z;
                int zp = zz;
                if(axis == 2)
                  {
                    zm--;
                    zp++;
                    if(zm < 0)
                      zm += NUM_CELLS_Z;
                    if(zp >= NUM_CELLS_Z)
                      zp -= NUM_CELLS_Z;
                  }
                const auto cell_LL = cell_stride_x * xm + cell_stride_y * ym + zm;
                const auto cell_RR = cell_stride_x * xp + cell_stride_y * yp + zp;
#endif

                const auto cell_on_boundary = is_cell_on_boundary_in_direction_of_current_axis(x + FirstSlabThisTask, y, z, axis);
                const auto cell_on_opposite_boundary =
                    is_cell_on_boundary_in_direction_opposite_of_current_axis(x + FirstSlabThisTask, y, z, axis);

                for(int q = 0; q < DG_NUM_OUTER_QPOINTS; q++)
                  {
                    double fields_L[NF] = {0};  // evaluated field left of boundary
                    double fields_R[NF] = {0};  // evaluated field right of boundary
                    state st_L{};
                    state st_R{};

#ifdef FINITE_VOLUME
                    slopes slope_L;
                    slopes slope_R;
                    char troubled_L;
                    char troubled_R;
#endif
#ifdef VISCOSITY
                    double fields_surface_L[NF] = {0};
                    double fields_surface_R[NF] = {0};

                    double grad_fields_surface_L[NF * ND] = {0};
                    double grad_fields_surface_R[NF * ND] = {0};
#endif

                    if(x >= 0)
                      {
#ifdef FINITE_VOLUME
                        troubled_L           = troubled[cell_L];
                        double fields_Lm[NF] = {0};
                        double fields_Lp[NF] = {0};
#endif
                        for(int f = 0; f < NF; f++)
                          {
                            for(int b = 0; b < NB; b++)
                              {
#ifdef USE_PRIMITIVE_PROJECTION
                                fields_L[f] +=
                                    w_prim[w_idx_internal(b, f, cell_L)] * dg->bfunc_external[bfunc_external_side_idx(q, b, axis)];
#else
                                fields_L[f] +=
                                    w[w_idx_internal(b, f, cell_L)] * dg->bfunc_external[bfunc_external_side_idx(q, b, axis)];
#endif

#ifdef FINITE_VOLUME
                                // note: for finite volume, NB=1, and the bfunc is just 1
                                fields_Lm[f] +=
                                    (axis != 0 || x > 0) ? w[w_idx_internal(0, f, cell_LL)] : left_states_to_recv[st_count].fields[f];
                                fields_Lp[f] += (axis != 0 || x < Nslabs - 1) ? w[w_idx_internal(0, f, cell_R)]
                                                                              : right_states_to_recv[st_count].fields[f];
#endif
                              }
#ifdef VISCOSITY
                            for(int l = 0; l < NB_INCREASED; l++)
                              {
                                double wg = 0;

                                for(int i = 0; i < dg->bfunc_project_count[axis][l]; i++)
                                  {
                                    int index = dg->bfunc_project_first[axis][l] + i;
                                    int b     = dg->bfunc_project_list[axis][index];

                                    wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_L)] * dg->bfunc_project_value_left[axis][index];
                                  }

                                fields_surface_L[f] += wg * dg->bfunc_mid[bfunc_mid_idx(q, l, axis)];

                                for(int d = 0; d < ND; d++)
                                  grad_fields_surface_L[f * ND + d] += wg * dg->bfunc_mid_diff[bfunc_mid_diff_idx(q, l, d, axis)];
                              }

                            for(int d = 0; d < ND; d++)
                              if(d == axis)
                                grad_fields_surface_L[f * ND + d] *= fac_cellsize_axis;
                              else
                                grad_fields_surface_L[f * ND + d] *= fac_cellsize;
#endif
                          }
#ifndef USE_PRIMITIVE_PROJECTION
                        get_primitive_state_from_conserved_fields(fields_L, st_L, &All.ViscParam);
#endif
#ifdef FINITE_VOLUME
                        state st_Lm;
                        get_primitive_state_from_conserved_fields(fields_Lm, st_Lm, &All.ViscParam);
                        state st_Lp;
                        get_primitive_state_from_conserved_fields(fields_Lp, st_Lp, &All.ViscParam);

                        do_slope_limiting(st_Lm, st_L, st_Lp, axis, slope_L);
#endif
                      }
                    else
                      {
#ifdef FINITE_VOLUME
                        slope_L    = left_slopes_to_recv[st_count];
                        troubled_L = left_states_to_recv[st_count].troubled;
#endif
                        for(int f = 0; f < NF; f++)
                          {
                            fields_L[f] = left_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
                            fields_surface_L[f] = left_states_to_recv[st_count].fields_surface[f];

                            for(int d = 0; d < ND; d++)
                              grad_fields_surface_L[f * ND + d] = left_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
                          }
#ifndef USE_PRIMITIVE_PROJECTION
                        get_primitive_state_from_conserved_fields(fields_L, st_L, &All.ViscParam);
#endif
                      }

                    if(xx < Nslabs)
                      {
#ifdef FINITE_VOLUME
                        troubled_R           = troubled[cell_R];
                        double fields_Rm[NF] = {0};
                        double fields_Rp[NF] = {0};
#endif
                        for(int f = 0; f < NF; f++)
                          {
                            for(int b = 0; b < NB; b++)
                              {
#ifdef USE_PRIMITIVE_PROJECTION
                                fields_R[f] += w_prim[w_idx_internal(b, f, cell_R)] *
                                               dg->bfunc_external[bfunc_external_side_idx(q, b, axis + ND)];
#else
                                fields_R[f] +=
                                    w[w_idx_internal(b, f, cell_R)] * dg->bfunc_external[bfunc_external_side_idx(q, b, axis + ND)];
#endif
#ifdef FINITE_VOLUME
                                fields_Rm[f] +=
                                    (axis != 0 || x >= 0) ? w[w_idx_internal(0, f, cell_L)] : left_states_to_recv[st_count].fields[f];
                                fields_Rp[f] += (axis != 0 || xx < Nslabs - 1) ? w[w_idx_internal(0, f, cell_RR)]
                                                                               : right_states_to_recv[st_count].fields[f];
#endif
                              }
#ifdef VISCOSITY
                            for(int l = 0; l < NB_INCREASED; l++)
                              {
                                double wg = 0;

                                for(int i = 0; i < dg->bfunc_project_count[axis][l]; i++)
                                  {
                                    int index = dg->bfunc_project_first[axis][l] + i;
                                    int b     = dg->bfunc_project_list[axis][index];

                                    wg += (1.0 / 2) * w[w_idx_internal(b, f, cell_R)] * dg->bfunc_project_value_right[axis][index];
                                  }

                                fields_surface_R[f] += wg * dg->bfunc_mid[bfunc_mid_idx(q, l, axis)];

                                for(int d = 0; d < ND; d++)
                                  grad_fields_surface_R[f * ND + d] += wg * dg->bfunc_mid_diff[bfunc_mid_diff_idx(q, l, d, axis)];
                              }

                            for(int d = 0; d < ND; d++)
                              if(d == axis)
                                grad_fields_surface_R[f * ND + d] *= fac_cellsize_axis;
                              else
                                grad_fields_surface_R[f * ND + d] *= fac_cellsize;
#endif
                          }
#ifndef USE_PRIMITIVE_PROJECTION
                        get_primitive_state_from_conserved_fields(fields_R, st_R, &All.ViscParam);
#endif
#ifdef FINITE_VOLUME
                        state st_Rm;
                        get_primitive_state_from_conserved_fields(fields_Rm, st_Rm, &All.ViscParam);
                        state st_Rp;
                        get_primitive_state_from_conserved_fields(fields_Rp, st_Rp, &All.ViscParam);

                        do_slope_limiting(st_Rm, st_R, st_Rp, axis, slope_R);
#endif
                      }
                    else
                      {
#ifdef FINITE_VOLUME
                        slope_R    = right_slopes_to_recv[st_count];
                        troubled_R = right_states_to_recv[st_count].troubled;
#endif
                        for(int f = 0; f < NF; f++)
                          {
                            fields_R[f] = right_states_to_recv[st_count].fields[f];
#ifdef VISCOSITY
                            fields_surface_R[f] = right_states_to_recv[st_count].fields_surface[f];

                            for(int d = 0; d < ND; d++)
                              grad_fields_surface_R[f * ND + d] = right_states_to_recv[st_count].grad_fields_surface[f * ND + d];
#endif
                          }
#ifndef USE_PRIMITIVE_PROJECTION
                        get_primitive_state_from_conserved_fields(fields_R, st_R, &All.ViscParam);
#endif
                      }

#ifdef USE_PRIMITIVE_PROJECTION
                    for(int f = 0; f < NF; f++)
                      {
                        st_L.s[f] = fields_L[f];
                        st_R.s[f] = fields_R[f];
                      }
#endif
#ifdef FINITE_VOLUME
                    if(troubled_L == false && troubled_R == false)
                      {
                        st_L.rho += 0.5 * slope_L.rho;
                        st_R.rho -= 0.5 * slope_R.rho;
                        st_L.velx += 0.5 * slope_L.velx;
                        st_R.velx -= 0.5 * slope_R.velx;
#if(ND >= 2)
                        st_L.vely += 0.5 * slope_L.vely;
                        st_R.vely -= 0.5 * slope_R.vely;
#endif
#if(ND >= 3)
                        st_L.velz += 0.5 * slope_L.velz;
                        st_R.velz -= 0.5 * slope_R.velz;
#endif
#ifndef ISOTHERM_EQS
                        st_L.press += 0.5 * slope_L.press;
                        st_R.press -= 0.5 * slope_R.press;
#endif
#ifdef ENABLE_DYE
                        st_L.dye += 0.5 * slope_L.dye;
                        st_R.dye -= 0.5 * slope_R.dye;
#endif
                      }
#endif

                    // deal with reflective boundaries
                    if(cell_on_boundary)
                      {
                        st_R = st_L;
                        reflect_state_if_needed(st_R, axis);
                      }
                    else if(cell_on_opposite_boundary)
                      {
                        st_L = st_R;
                        reflect_state_if_needed(st_L, axis);
                      }

#if(ND >= 2)
                    if(axis == 1)
                      {
                        double vtmp;
                        /* swap x and y velocities */
                        vtmp      = st_L.velx;
                        st_L.velx = st_L.vely;
                        st_L.vely = vtmp;

                        vtmp      = st_R.velx;
                        st_R.velx = st_R.vely;
                        st_R.vely = vtmp;
                      }
#endif
#if(ND == 3)
                    if(axis == 2)
                      {
                        double vtmp;
                        /* swap x and z velocities */
                        vtmp      = st_L.velx;
                        st_L.velx = st_L.velz;
                        st_L.velz = vtmp;

                        vtmp      = st_R.velx;
                        st_R.velx = st_R.velz;
                        st_R.velz = vtmp;
                      }
#endif

                    if(st_L.rho < 0 || st_R.rho < 0 || st_L.press < 0 || st_R.press < 0)
                      {
                        Terminate(
                            "axis=%d  x=%d y=%d z=%d cell_L=%d  cell_R=%d  st_L.rho=%g  st_R.rho=%g  st_L.press=%g st_R.press=%g \n",
                            axis, x, y, z, cell_L, cell_R, st_L.rho, st_R.rho, st_L.press, st_R.press);
                      }

                    // note: riemann solver assumes that the states are separated in x-direction
                    state sf;  // primitive state on face
                    fluxes flx;
                    godunov_flux_3d(st_L, st_R, sf, flx, &All.ViscParam);

#if(ND >= 2)
                    if(axis == 1)
                      {
                        /* swap computed x and y velocities and momentum fluxes */
                        const double vtemp = sf.velx;
                        sf.velx            = sf.vely;
                        sf.vely            = vtemp;

                        const double flxtemp = flx.field_flx[1];
                        flx.field_flx[1]     = flx.field_flx[2];
                        flx.field_flx[2]     = flxtemp;
                      }
#endif
#if(ND == 3)
                    else if(axis == 2)
                      {
                        /* swap x and z velocities and momentum fluxes */
                        const double vtemp = sf.velx;
                        sf.velx            = sf.velz;
                        sf.velz            = vtemp;

                        const double flxtemp = flx.field_flx[1];
                        flx.field_flx[1]     = flx.field_flx[3];
                        flx.field_flx[3]     = flxtemp;
                      }
#endif

#ifdef VISCOSITY
                    double full_viscous_flux[NF * ND];
                    double fields_surface[NF], grad_fields_surface[NF * ND];

                    for(int f = 0; f < NF; f++)
                      {
                        fields_surface[f] = fields_surface_L[f] + fields_surface_R[f];
                        for(int d = 0; d < ND; d++)
                          grad_fields_surface[f * ND + d] = grad_fields_surface_L[f * ND + d] + grad_fields_surface_R[f * ND + d];
                      }
                    // calculate viscous flux
                    get_viscous_flux_from_conserved_fields_and_their_gradients(fields_surface, grad_fields_surface, full_viscous_flux,
                                                                               &All.ViscParam, axis, st_L, st_R);

                    // correct the surface flux with the viscous flux
                    for(int f = 0; f < NF; f++)
                      flx.field_flx[f] -= full_viscous_flux[f * ND + axis];
#endif

                    // update weight changes
                    for(int b = 0; b < NB; b++)
                      {
                        double fac_L =
                            fac_area * dg->bfunc_external[bfunc_external_side_idx(q, b, axis)] * dg->qpoint_weights_external[q];

                        double fac_R =
                            fac_area * dg->bfunc_external[bfunc_external_side_idx(q, b, ND + axis)] * dg->qpoint_weights_external[q];

                        if(x >= 0 && y >= 0 && z >= 0)
                          for(int f = 0; f < NF; f++)
                            dwdt[w_idx_internal(b, f, cell_L)] -= fac_L * flx.field_flx[f];

                        if(xx < Nslabs && yy < NUM_CELLS_Y && zz < NUM_CELLS_Z)
                          for(int f = 0; f < NF; f++)
                            dwdt[w_idx_internal(b, f, cell_R)] += fac_R * flx.field_flx[f];
                      }

                    st_count++;

                  }  // for qpoint
              }      // z - axis
          }
      }

#ifdef FINITE_VOLUME
  free(left_slopes_to_recv);
  free(right_slopes_to_recv);
#endif
  free(left_states_to_recv);
  free(right_states_to_recv);

  // ----------------------------------------------------------------------------------------------------------------------------
  // normalize result
  // ----------------------------------------------------------------------------------------------------------------------------

  const double fac_vol = 1.0 / dg->getCellVolume();

  for(uint64_t i = 0; i < dg->N_w; i++)
    dwdt[i] *= fac_vol;

  const double time_taken = static_cast<double>(clock() - t) / CLOCKS_PER_SEC;  // in seconds
  mpi_printf("Time taken %f seconds - serial computation of weight changes \n", time_taken);

#if defined(SINGLE_SHOCK) || defined(SHU_OSHER_SHOCKTUBE)
  for(uint c = 0; c < Nc; c++)
    {
      const auto cell_offset = getCellOffset();
      const xyz cell_idx     = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);

      if((ThisTask == 0 && cell_idx.x == 0) || (ThisTask == NTask - 1 && cell_idx.x == DG_NUM_CELLS_1D - 1))
        {
          for(int b = 0; b < NB; b++)
            for(int f = 0; f < NF; f++)
              dwdt[w_idx_internal(b, f, c)] = 0;
        }
    }
#endif

#ifdef USE_PRIMITIVE_PROJECTION
  free(w_prim);
#endif
}

#ifdef ART_VISCOSITY

double dg_compute_smoothness_indicator(double *w, int c)
{
  double AA = 0;

  for(int b = NB_REDUCED; b < NB; b++)
    {
      AA += pow(w[w_idx_internal(b, 0, c)], 2);
    }

  double BB = AA;

  for(int b = 0; b < NB_REDUCED; b++)
    {
      BB += pow(w[w_idx_internal(b, 0, c)], 2);
    }

  double smth = 0;

  if(BB != 0)
    smth = AA / BB;  // smoothness estimator

  return smth;
}

double dg_compute_smoothrate_indicator(double *w, vector<double> &dwdt, int c)
{
  double A = 0, Adot = 0;

  for(int b = NB_REDUCED; b < NB; b++)
    {
      A += w[w_idx_internal(b, 0, c)] * w[w_idx_internal(b, 0, c)];
      Adot += w[w_idx_internal(b, 0, c)] * dwdt[w_idx_internal(b, 0, c)];
    }

  double B    = A;
  double Bdot = Adot;

  for(int b = 0; b < NB_REDUCED; b++)
    {
      B += w[w_idx_internal(b, 0, c)] * w[w_idx_internal(b, 0, c)];
      Bdot += w[w_idx_internal(b, 0, c)] * dwdt[w_idx_internal(b, 0, c)];
    }

  double smthrate = 0;

  if(A != 0 && B != 0)
    smthrate = 2 * (Adot / A - Bdot / B);

  return smthrate;
}

void dg_add_in_source_alpha_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, double *grad_fields, state &st,
                                   vector<double> &dwdt)
{
  double source = 0;  // storage for source function

  // get matrix of velocity derivatives
  double vel[ND], dvel[ND][ND], u, du[ND], dye, ddye[ND];
  get_primitive_variable_gradients(fields, grad_fields, vel, dvel, u, du, dye, ddye);

  // get velocity divergence
  double divvel = 0;
  for(int i = 0; i < ND; i++)
    divvel += dvel[i][i];

  if(divvel < 0)
    source += -All.ViscParam.ArtViscShockGrowth * divvel;

#ifndef ISOTHERM_EQS
  double cs = 0;
  if(st.press > 0 && st.rho > 0)
    cs = sqrt(GAMMA * st.press / st.rho);
  else
    Terminate("cannot define sound speed\n");
#else
  double cs = vp->IsoSoundSpeed;
#endif

  double res = All.ViscParam.Resolution;

  double tau = All.ViscParam.ArtViscDecay * res / cs;

  source += -fields[ALPHA_FIELD] / tau;

#define S0 0.01
  const double sonset = (S0 / (DG_ORDER * DG_ORDER)) * All.ViscParam.ArtViscWiggleOnset;

  if(dg->smthness[c] > sonset && dg->smthrate[c] > 0)
    source += All.ViscParam.ArtViscWiggleGrowth * dg->smthrate[c];

#if(ND == 1)
  const double fac = 0.5 * dg->getCellVolume();
#endif
#if(ND == 2)
  const double fac = 0.25 * dg->getCellVolume();
#endif
#if(ND == 3)
  const double fac = 0.125 * dg->getCellVolume();
#endif

  // update dw/dt of the weights with the source term
  int f = ALPHA_FIELD;

  for(int b = 0; b < NB; b++)
    {
      dwdt[w_idx_internal(b, f, c)] +=
          fac * source * dg->bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] * dg->qpoint_weights_internal[qpoint_in_cell];
    }
}
#endif

#ifdef TURBULENCE

void dg_add_in_source_function_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, state &st, vector<double> &dwdt)
{
#ifdef DECAYING_TURBULENCE
  if(dg->Time > All.StopDrivingAfterTime)
    return;
#endif  // DECAYING_TURBULENCE
        // compute force density at the desired point
  double fx = 0;
  double fy = 0;
#ifdef THREEDIMS
  double fz = 0;
#endif  // THREEDIMS

  const auto cell_offset = getCellOffset();

  // get coordinates of cell center
  const xyz cell_center = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);

  double xx = dg->getCellSize() * (cell_center.x + .5);
  double yy = dg->getCellSize() * (cell_center.y + .5);
#ifdef THREEDIMS
  double zz = dg->getCellSize() * (cell_center.z + .5);
#endif  // THREEDIMS

  // add qpoint coordinates
  const xyz qxyz = dg_geometry_idx_to_cell(qpoint_in_cell, DG_NUM_QPOINTS_1D);

  xx += dg->qpoint_location[qxyz.x] / 2 * dg->getCellSize();
  yy += dg->qpoint_location[qxyz.y] / 2 * dg->getCellSize();
#ifdef THREEDIMS
  zz += dg->qpoint_location[qxyz.z] / 2 * dg->getCellSize();
#endif  // THREEDIMS

  for(int m = 0; m < dg->StNModes; m++)  // calc force
    {
      const double kxx = dg->StMode[3 * m + 0] * xx;
      const double kyy = dg->StMode[3 * m + 1] * yy;
      double kzz       = 0;
#ifdef THREEDIMS
      kzz = dg->StMode[3 * m + 2] * zz;
#endif  // THREEDIMS

      const double kdotx = kxx + kyy + kzz;
      const double ampl  = dg->StAmpl[m];

      const double realt = cos(kdotx);
      const double imagt = sin(kdotx);

      fx += ampl * (dg->StAka[3 * m + 0] * realt - dg->StAkb[3 * m + 0] * imagt);
      fy += ampl * (dg->StAka[3 * m + 1] * realt - dg->StAkb[3 * m + 1] * imagt);
#ifdef THREEDIMS
      fz += ampl * (dg->StAka[3 * m + 2] * realt - dg->StAkb[3 * m + 2] * imagt);
#endif  // THREEDIMS
    }

  fx *= 2. * All.StAmplFac * dg->StSolWeightNorm * st.rho;
  fy *= 2. * All.StAmplFac * dg->StSolWeightNorm * st.rho;
#ifdef THREEDIMS
  fz *= 2. * All.StAmplFac * dg->StSolWeightNorm * st.rho;
#endif  // THREEDIMS

  vector<double> source(NF, 0);  // storage for source function

#if(ND == 3)
  source[1] = fx;
  source[2] = fy;
  source[3] = fz;
#ifndef ISOTHERM_EQS
  source[4] = st.velx * fx + st.vely * fy + st.velz * fz;  // work term
#endif
#elif(ND == 2)
  source[1] = fx;
  source[2] = fy;
#ifndef ISOTHERM_EQS
  source[3] = st.velx * fx + st.vely * fy;  // work term
#endif
#endif

#if(ND == 2)
  const double fac = 0.25 * dg->getCellVolume();
#endif
#if(ND == 3)
  const double fac = 0.125 * dg->getCellVolume();
#endif

  // update dw/dt of the weights with the source term
  for(int f = 1; f < NF; f++)
    {
      for(int b = 0; b < NB; b++)
        {
          dwdt[w_idx_internal(b, f, c)] += fac * source[f] * dg->bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] *
                                           dg->qpoint_weights_internal[qpoint_in_cell];
        }
    }
}

double dg_reset_temperature_cell(double *w, dg_data_t *dg, int c)
{
#if(ND == 2)
  const double fac = 0.25;
#endif
#if(ND == 3)
  const double fac = 0.125;
#endif

  double w_new[NB];
  for(int b = 0; b < NB; b++)
    w_new[b] = 0;

  for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
    {
      double fields[NF];  // fields evaluated at the Gauss quadrature point

      for(int f = 0; f < NF; f++)
        {
          fields[f] = 0;

          for(int b = 0; b < NB; b++)
            fields[f] += w[w_idx_internal(b, f, c)] * dg->bfunc_internal[bfunc_internal_idx(q, b)];
        }

      // calculate primitive variables
      const double irho = 1.0 / fields[0];

#if(ND == 3)
      const double p2 = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];

      // recomputing the desired value for fields[4]
      const double etot = 0.5 * p2 * irho + fields[0] * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed / (GAMMA - 1.0);
#endif

#if(ND == 2)
      const double p2 = fields[1] * fields[1] + fields[2] * fields[2];

      // recomputing the desired value for fields[3]
      const double etot = 0.5 * p2 * irho + fields[0] * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed / (GAMMA - 1.0);
#endif

      for(int b = 0; b < NB; b++)
        {
          w_new[b] += fac * etot * dg->bfunc_internal[bfunc_internal_idx(q, b)] * dg->qpoint_weights_internal[q];
        }
    }

#if(ND == 3)
  int f = 4;
#endif

#if(ND == 2)
  int f = 3;
#endif

  double dissipated_energy = (w[w_idx_internal(0, f, c)] - w_new[0]) * dg->getCellVolume();

  // now store the new expansion of the energy density in the corresponding weigths
  for(int b = 0; b < NB; b++)
    {
      w[w_idx_internal(b, f, c)] = w_new[b];
    }

  return dissipated_energy;
}

void dg_reset_temperature(dg_data_t *dg) { dg->TurbDissipatedEnergy += dg_reset_temperature(dg->w, dg); }

double dg_reset_temperature(double *w, dg_data_t *dg)
{
  double dissipated_energy = 0;

#if !(defined(ISOTHERM_EQS) || defined(COOLING))
  for(decltype(Nc) c = 0; c < Nc; c++)
    {
      dissipated_energy += dg_reset_temperature_cell(w, dg, c);
    }
#endif

  return dissipated_energy;
}

void dg_store_driving_energy(dg_data_t *dg, int flag)
{
  for(decltype(Nc) c = 0; c < Nc; c++)
    {
#ifndef ISOTHERM_EQS
#if(ND == 3)
      int f = 4;
#endif

#if(ND == 2)
      int f = 3;
#endif
      double etot = dg->w[w_idx_internal(0, f, c)];  // mean energy density in cell

#else
      double rho = dg->w[w_idx_internal(0, 0, c)];
      double px  = dg->w[w_idx_internal(0, 1, c)];
      double py  = dg->w[w_idx_internal(0, 2, c)];

#if(ND == 3)
      double pz       = dg->w[w_idx_internal(0, 3, c)];
      const double p2 = px * px + py * py + pz * pz;
#endif

#if(ND == 2)
      const double p2 = px * px + py * py;
#endif

      double etot = 0.5 * p2 / rho + rho * All.ViscParam.IsoSoundSpeed * All.ViscParam.IsoSoundSpeed;
#endif

      double egy = etot * dg->getCellVolume();  // total energy in cell

      if(flag == 0)
        dg->TurbInjectedEnergy -= egy;
      else if(flag == 1)
        dg->TurbInjectedEnergy += egy;
      else
        Terminate("Invalid flag in dg_store_driving_energy");
    }
}

#endif  // TURBULENCE

#ifdef GRAVITY_WAVES
template <typename T>
T clamp(T x, T lowerlimit, T upperlimit)
{
  if(x < lowerlimit)
    x = lowerlimit;
  if(x > upperlimit)
    x = upperlimit;
  return x;
}

template <typename T>
T smootherstep(T edge0, T edge1, T x)
{
  // Scale, and clamp x to 0..1 range
  x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
  // Evaluate polynomial
  return x * x * x * (x * (x * 6 - 15) + 10);
}

void dg_add_in_gravity_source_function_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, state &st,
                                              vector<double> &dwdt)
{
  // only 3D
  static_assert(ND == 3);

  // cell id offset
  const auto cell_offset = getCellOffset();

  // get coordinates of cell center
  const auto cell_center = dg_geometry_idx_to_cell(c + cell_offset, DG_NUM_CELLS_1D);

  auto xx = dg->getCellSize() * (cell_center.x + .5);
  auto yy = dg->getCellSize() * (cell_center.y + .5);
  auto zz = dg->getCellSize() * (cell_center.z + .5);

  // add qpoint coordinates
  const auto qxyz = dg_geometry_idx_to_cell(qpoint_in_cell, DG_NUM_QPOINTS_1D);

  xx += dg->qpoint_location[qxyz.x] / 2 * dg->getCellSize() - .5 * dg->getBoxSize();
  yy += dg->qpoint_location[qxyz.y] / 2 * dg->getCellSize() - .5 * dg->getBoxSize();
  zz += dg->qpoint_location[qxyz.z] / 2 * dg->getCellSize() - .5 * dg->getBoxSize();

  // smoothly turn off gravity over a few kpc longward 200kpc
  const auto rr                    = sqrt(xx * xx + yy * yy + zz * zz);
  const auto smooth_transition_fac = 1 - smootherstep(200., 202., rr);
  const auto radial_g              = G0 * tanh(rr / RS) * smooth_transition_fac * st.rho;

  // project g onto the x-y plane
  const auto rr2d      = sqrt(xx * xx + yy * yy);
  const auto irr2d     = 1. / rr2d;
  const auto irr       = 1. / rr;
  const auto cos_theta = xx * irr2d;
  const auto sin_theta = yy * irr2d;
  const auto cos_phi   = zz * irr;
  const auto sin_phi   = rr2d * irr;

  // compute force density at the desired point
  const auto x_g = radial_g * cos_theta * sin_phi;
  const auto y_g = radial_g * sin_theta * sin_phi;
  const auto z_g = radial_g * cos_phi;

  auto fx = -x_g;
  auto fy = -y_g;
  auto fz = -z_g;

  vector<double> source(NF, 0);  // storage for source function
  source[1] = fx;
  source[2] = fy;
  source[3] = fz;
#ifndef ISOTHERM_EQS
  source[4] = st.velx * fx + st.vely * fy + st.velz * fz;  // work term
#endif

  const auto fac = 0.125 * dg->getCellVolume();

  // update dw/dt of the weights with the source term
  for(int f = 1; f < NF; f++)
    {
      for(int b = 0; b < NB; b++)
        {
          const auto temp = fac * source[f] * dg->bfunc_internal[bfunc_internal_idx(qpoint_in_cell, b)] *
                            dg->qpoint_weights_internal[qpoint_in_cell];
          dwdt[w_idx_internal(b, f, c)] += temp;
        }
    }
}

#endif  // GRAVITY_WAVES

double getMaxTistepCpu(dg_data_t *dg)
{
#ifdef VERY_CONSERVATIVE_TIMESTEP_CRITERION
  return getMaxTistepUsingAllQpointsCpu(dg);
#endif  // VERY_CONSERVATIVE_TIMESTEP_CRITERION
  return getMaxTistepFromCellAverages(dg);
}

double getMaxTistepFromCellAverages(const dg_data_t *dg)
{
  double csnd_max = 0;
  double v_max    = 0;

  for(uint64_t cell = 0; cell < Nc; cell++)
    {
      double fields[NF];

      for(decltype(ND) f = 0; f < NF; f++)
        fields[f] = dg->w[w_idx_internal(0, f, cell)];  // we use the cell average here

      // calculate primitive variables
      const double rho  = fields[0];
      const double irho = 1.0 / rho;
      const double vx   = fields[1] * irho;

#if(ND == 1)
      double v = sqrt(vx * vx);
#endif
#if(ND == 2)
      const double vy = fields[2] * irho;
      double v        = sqrt(vx * vx + vy * vy);
#endif
#if(ND == 3)
      const double vy = fields[2] * irho;
      const double vz = fields[3] * irho;
      double v        = sqrt(vx * vx + vy * vy + vz * vz);
#endif

      if(v > v_max)
        v_max = v;

#ifndef ISOTHERM_EQS

#if(ND == 1)
      const double p2   = fields[1] * fields[1];
      const double ptot = (fields[2] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 2)
      const double p2   = fields[1] * fields[1] + fields[2] * fields[2];
      const double ptot = (fields[3] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif
#if(ND == 3)
      const double p2   = fields[1] * fields[1] + fields[2] * fields[2] + fields[3] * fields[3];
      const double ptot = (fields[4] - 0.5 * p2 * irho) * (GAMMA - 1.0);
#endif

      double csnd = sqrt(GAMMA * ptot / rho);
#else   // ISOTHERM_EQS
      double csnd = All.ViscParam.IsoSoundSpeed;
#endif  // ISOTHERM_EQS

      if(csnd > csnd_max)
        {
          csnd_max = csnd;
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &csnd_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);

  return All.CourantFac / (2.0 * DEGREE_K + 1) * dg->getCellSize() / (csnd_max + v_max);
}

#ifdef VERY_CONSERVATIVE_TIMESTEP_CRITERION
double getMaxTistepUsingAllQpointsCpu(dg_data_t *dg)
{
  double csnd_max = 0;
  double v_max    = 0;
  double rho_min  = 1.0e30;
  double rho_max  = -1.0e30;

  for(uint64_t cell = 0; cell < Nc; cell++)
    {
      for(int q = 0; q < DG_NUM_INNER_QPOINTS; q++)
        {
          double fields[NF];

          // get conserved fields at quadrature point
          get_conserved_fields_from_weights(dg->w, dg->bfunc_internal.data(), fields, cell, q);
          // calculate primitive variables
          state st;
          get_primitive_state_from_conserved_fields(fields, st, &All.ViscParam);

          if(st.rho > rho_max)
            rho_max = st.rho;

          if(st.rho < rho_min)
            rho_min = st.rho;

          double v2 = 0;
          for(int d = 0; d < ND; d++)
            v2 += st.s[1 + d] * st.s[1 + d];

          double v = sqrt(v2);

          if(v > v_max)
            v_max = v;

#ifndef ISOTHERM_EQS
          double csnd = sqrt(st.press / st.rho);
#else   // ISOTHERM_EQS
          double csnd = All.ViscParam.IsoSoundSpeed;
#endif  // ISOTHERM_EQS

          if(csnd > csnd_max)
            csnd_max = csnd;
        }
    }

  double v_max_face  = 0;
  double v_max_face2 = 0;

  for(uint64_t cell = 0; cell < Nc; cell++)
    {
      // go over points on interfaces
      for(decltype(ND) q = DG_NUM_INNER_QPOINTS; q < DG_NUM_POSITIVE_POINTS; q++)
        {
          double fields[NF] = {0};

          for(int f = 0; f < NF; f++)
            for(decltype(ND) b = 0; b < NB; b++)
              fields[f] += dg->w[w_idx_internal(b, f, cell)] * dg->qpoint_weights_positivity[q * NB + b];

          state st;
          get_primitive_state_from_conserved_fields(fields, st, &All.ViscParam);

          double v2 = 0;
          for(int d = 0; d < ND; d++)
            v2 += st.s[1 + d] * st.s[1 + d];

          double v = sqrt(v2);

          if(v > v_max_face)
            v_max_face = v;

          ///  now try to determine the velocities differently, by fitting a polynomial to their run in the cell and use this to
          ///  extrapolate to the faces
          vector<double> w_prim(NF * NB, 0);

          project_single_cell_to_primitives(cell, dg->w, w_prim.data(), dg->bfunc_internal.data(), dg->qpoint_weights_internal.data(),
                                            &All.ViscParam);

          for(int f = 0; f < NF; f++)
            {
              st.s[f] = 0;
              for(decltype(ND) b = 0; b < NB; b++)
                st.s[f] += w_prim[f * NB + b] * dg->qpoint_weights_positivity[q * NB + b];
            }

          v2 = 0;
          for(int d = 0; d < ND; d++)
            v2 += st.s[1 + d] * st.s[1 + d];

          v = sqrt(v2);

          if(v > v_max_face2)
            v_max_face2 = v;
        }
    }

  MPI_Allreduce(MPI_IN_PLACE, &csnd_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &rho_max, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &rho_min, 1, MPI_DOUBLE, MPI_MIN, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max_face, 1, MPI_DOUBLE, MPI_MAX, MyComm);
  MPI_Allreduce(MPI_IN_PLACE, &v_max_face2, 1, MPI_DOUBLE, MPI_MAX, MyComm);

  mpi_printf("\n[TIMESTEP] csnd_max=%g  v_max=%g  v_max_face=%g  v_max_face2=%g rho_min=%g  rho_max=%g\n", csnd_max, v_max, v_max_face,
             v_max_face2, rho_min, rho_max);

  return All.CourantFac / (2.0 * DEGREE_K + 1) * dg->getCellSize() / (csnd_max + v_max);
}
#endif  // VERY_CONSERVATIVE_TIMESTEP_CRITERION

void project_to_primitives_cpu(const dg_data_t *dg, double *w, double *w_prim)
{
  // cpu version of the wrapper function for project_cell_to_primitives
  memset(w_prim, 0, dg->N_w * sizeof(double));

  for(uint64_t c = 0; c < Nc; c++)
    {
      project_cell_to_primitives(c, w, w_prim, dg->bfunc_internal.data(), dg->qpoint_weights_internal.data(), &All.ViscParam);
    }
}
