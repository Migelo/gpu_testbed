// Copyright 2020 Miha Cernetic

#ifndef DG_COMPUTE_SERIAL_HPP_
#define DG_COMPUTE_SERIAL_HPP_

#include <vector>

#include "../data/dg_data.hpp"

using ::std::vector;

// compute results serially:
void dg_compute_serial(dg_data_t *dg, double *w, vector<double> &dwdt, char *troubled, double dt);
bool dg_advance_timestep_serial(dg_data_t *dg, double dt);
void dg_add_in_source_function_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, state &st, vector<double> &dwdt);
void dg_add_in_source_alpha_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, double *grad_fields, state &st,
                                   vector<double> &dwdt);
void dg_store_driving_energy(dg_data_t *dg, int flag);
void dg_reset_temperature(dg_data_t *dg);
double dg_reset_temperature(double *w, dg_data_t *dg);
double dg_reset_temperature_cell(double *w, dg_data_t *dg, int c);
void positivity_density_limiter(dg_data_t *dg, double *w);
void project_to_primitives_cpu(const dg_data_t *dg, double *w, double *w_prim);
void make_cell_flat(dg_data_t *dg, uint64_t cell);
void make_cell_flat(double *w, uint64_t cell);
double dg_compute_smoothness_indicator(double *w, int c);
double dg_compute_smoothrate_indicator(double *w, vector<double> &dwdt, int c);
void output_turbulence_log(dg_data_t *dg);
double getMaxTistepFromCellAverages(const dg_data_t *dg);
void dg_add_in_gravity_source_function_serial(dg_data_t *dg, int c, int qpoint_in_cell, double *fields, state &st,
                                              vector<double> &dwdt);
#endif  // DG_COMPUTE_SERIAL_HPP_
