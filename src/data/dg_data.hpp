// Copyright 2020 Miha Cernetic
#ifndef SRC_DATA_DG_DATA_HPP_
#define SRC_DATA_DG_DATA_HPP_

#include "runrun.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>

#include <string>
#include <vector>

#include "../data/dg_params.h"
class dg_data_t;
#include "../cooling/cooling_function_table.cuh"
#include "../cooling/cooling_function_table.hpp"
#include "../legendre/dg_legendre.hpp"
#include "../tests/dg_geometry.hpp"

using ::std::string;
using ::std::vector;

struct coordinates
{
  vector<double> x;
  vector<double> y;
  vector<double> z;
};

class dg_data_t
{
 public:
  uint64_t N_w;
  double scale_factor;

  // timestepping
  double Time;
  integertime Ti_Current;
  double TimeStep;

  // table with mapping of basis function to legrendre functions in different dimensions
  int bfunc_to_legendre_orders[NB_INCREASED][ND];

  double *w;                               // weights array conserved variables
  vector<double> qpoint_weights;           // quadrature weights  in 1D
  vector<double> qpoint_location;          // quadrature location in 1D
  vector<double> qpoint_weights_internal;  // quadrature weights at 3D inner qpoint_locations
  vector<double> qpoint_weights_external;  // quadrature weights at surface qpoint_locations
  vector<double> bfunc_internal;           // basis func values at 3D inner qpoint_locations
  vector<double> bfunc_external;           // basis func values at surface qpoint_locations
  vector<double> bfunc_internal_diff;      // basis func derivatives at internal qpoint_locations
  vector<double> bfunc_external_diff;      // derivative of basis func values at surface qpoint_locations
  vector<double> bfunc_mid;
  vector<double> bfunc_mid_diff;
  vector<double> qpoint_weights_positivity;  // quadrature weights at points for positivity check

#ifdef GPU
  fields_face *right_states_to_send;
  fields_face *right_states_to_recv;
  fields_face *left_states_to_send;
  fields_face *left_states_to_recv;
#ifdef FINITE_VOLUME
  slopes *right_slopes_to_send;
  slopes *right_slopes_to_recv;
  slopes *left_slopes_to_send;
  slopes *left_slopes_to_recv;
#endif
#endif  // GPU

#ifdef ART_VISCOSITY
  double *smthness;
  double *smthrate;
#endif

#ifdef VISCOSITY

#if(DEGREE_K < 3)
  // Results A[m][l] of the integrals  int_{-1}^{0} P_m(2 eta x + 1) P_l(x) dx
  // where P_l/m(x) are normalized Legendre polynomials, and eta = 3/4 was chosen

  // Mathematica:  For[m = 0, m <= 10, m++,  Print["{"];  For[l = 0, l <= 10, l++,   Print[  N[ Integrate[ Sqrt[2*m+1] Sqrt[2*l+1]
  // LegendreP[m,2 eta x + 1] LegendreP[l,x] ,  {x,-1,0} ], 17]]; Print[","] ] ; Print["},"]]

  const double legendre_overlapp_eta = (3.0 / 4);

  const double legendre_overlapp_left[11][11] = {
      {1.0000000000000000, -0.86602540378443865, 0, 0.33071891388307382, 0, -0.20728904939721249, 0, 0.15128841196122722, 0,
       -0.11918864298744029, 0},
      {0.43301270189221932, 0, -0.72618437741389067, 0.57282196186948000, 0.16237976320958225, -0.35903516540862679,
       -0.073183570293731229, 0.26203921611325661, 0.041844307198493262, -0.20644078533943456, -0.027129285904470900},
      {-0.27950849718747371, 0.60515364784490889, -0.56250000000000000, -0.092438746610931501, 0.62889411867181585,
       -0.33314954141250048, -0.28343874896362089, 0.29262186495060134, 0.16206230491335153, -0.24509761169807697,
       -0.10507127250251541},
      {0.041339864235384228, -0.14320549046737000, 0.41597435974919175, -0.67187500000000000, 0.44181979901566894, 0.22280183014862810,
       -0.54497503205948555, 0.13008824214597146, 0.34755540306639666, -0.18864299166087484, -0.23437235353883062},
      {0.11718750000000000, -0.18267723361078003, 0.15722352966795396, 0.050382959536874528, -0.43066406250000000, 0.62429631673926106,
       -0.26936003180956561, -0.33117978930887397, 0.43938857558156954, 0.041104119065981083, -0.35661523505057959},
      {-0.12307787307959492, 0.22439697838039174, -0.30961180207357382, 0.36419529928141132, -0.28785647289339469,
       -0.030517578125000000, 0.44996751392346264, -0.53547936103688251, 0.092019413758075078, 0.39486282055196715,
       -0.30999589728585795},
      {0.024647323172117114, -0.060223979720882991, 0.12400445267158414, -0.22765516830690048, 0.36046710139221280,
       -0.44358111368529508, 0.31832885742187500, 0.076601322871931982, -0.46751354504735473, 0.41961625600460878,
       0.068686008498058659},
      {0.068316173526241667, -0.11054779429778013, 0.11877191664450889, -0.086933969511009776, -0.0031025943859236051,
       0.16562227674823050, -0.36643984703478449, 0.46543121337890625, -0.28112274967446971, -0.15580654481757933,
       0.46609024851963398},
      {-0.079396961967918207, 0.14133350634491083, -0.18991676357033383, 0.22991422437043122, -0.24885474093827295,
       0.21927642048303208, -0.10258757920798941, -0.11927890662016412, 0.37118124961853027, -0.45374053992361357,
       0.20176262579413572},
      {0.017525785394637565, -0.038246841926725600, 0.069770277359138568, -0.11803384949303339, 0.18328496295895752,
       -0.25657811070084178, 0.31218574887118016, -0.30084831040202469, 0.16285162339035833, 0.10911792516708374,
       -0.38299711804244768},
      {0.048265424704639718, -0.079503038463911233, 0.090823326510265834, -0.082992834806525122, 0.050489899638962574,
       0.013436942261924557, -0.11138135901446634, 0.23171034732191634, -0.33386041916651951, 0.34169045197871875,
       -0.17926921695470810}};

  // Results A[m][l] of the integrals  int_{0}^{1} P_m(2 eta x - 1) P_l(x) dx
  // where P_l/m(x) are normalized Legendre polynomials, and eta = 3/4 was chosen

  // Mathematica: For[m = 0, m <= 10, m++,  Print["{"];  For[l = 0, l <= 10, l++,   Print[  N[ Integrate[ Sqrt[2*m+1] Sqrt[2*l+1]
  // LegendreP[m,2 eta x - 1] LegendreP[l,x] ,  {x,0,1} ], 17]]; Print[","] ] ; Print["},"]]

  const double legendre_overlapp_right[11][11] = {
      {1.0000000000000000, 0.86602540378443865, 0, -0.33071891388307382, 0, 0.20728904939721249, 0, -0.15128841196122722, 0,
       0.11918864298744029, 0},
      {-0.43301270189221932, 0, 0.72618437741389067, 0.57282196186948000, -0.16237976320958225, -0.35903516540862679,
       0.073183570293731229, 0.26203921611325661, -0.041844307198493262, -0.20644078533943456, 0.027129285904470900},
      {-0.27950849718747371, -0.60515364784490889, -0.56250000000000000, 0.092438746610931501, 0.62889411867181585,
       0.33314954141250048, -0.28343874896362089, -0.29262186495060134, 0.16206230491335153, 0.24509761169807697,
       -0.10507127250251541},
      {-0.041339864235384228, -0.14320549046737000, -0.41597435974919175, -0.67187500000000000, -0.44181979901566894,
       0.22280183014862810, 0.54497503205948555, 0.13008824214597146, -0.34755540306639666, -0.18864299166087484, 0.23437235353883062},
      {0.11718750000000000, 0.18267723361078003, 0.15722352966795396, -0.050382959536874528, -0.43066406250000000,
       -0.62429631673926106, -0.26936003180956561, 0.33117978930887397, 0.43938857558156954, -0.041104119065981083,
       -0.35661523505057959},
      {0.12307787307959492, 0.22439697838039174, 0.30961180207357382, 0.36419529928141132, 0.28785647289339469, -0.030517578125000000,
       -0.44996751392346264, -0.53547936103688251, -0.092019413758075078, 0.39486282055196715, 0.30999589728585795},
      {0.024647323172117114, 0.060223979720882991, 0.12400445267158414, 0.22765516830690048, 0.36046710139221280, 0.44358111368529508,
       0.31832885742187500, -0.076601322871931982, -0.46751354504735473, -0.41961625600460878, 0.068686008498058659},
      {-0.068316173526241667, -0.11054779429778013, -0.11877191664450889, -0.086933969511009776, 0.0031025943859236051,
       0.16562227674823050, 0.36643984703478449, 0.46543121337890625, 0.28112274967446971, -0.15580654481757933, -0.46609024851963398},
      {-0.079396961967918207, -0.14133350634491083, -0.18991676357033383, -0.22991422437043122, -0.24885474093827295,
       -0.21927642048303208, -0.10258757920798941, 0.11927890662016412, 0.37118124961853027, 0.45374053992361357, 0.20176262579413572},
      {-0.017525785394637565, -0.038246841926725600, -0.069770277359138568, -0.11803384949303339, -0.18328496295895752,
       -0.25657811070084178, -0.31218574887118016, -0.30084831040202469, -0.16285162339035833, 0.10911792516708374,
       0.38299711804244768},
      {0.048265424704639718, 0.079503038463911233, 0.090823326510265834, 0.082992834806525122, 0.050489899638962574,
       -0.013436942261924557, -0.11138135901446634, -0.23171034732191634, -0.33386041916651951, -0.34169045197871875,
       -0.17926921695470810}};

#else

  // for DEGREE_K >=3, eta = 1.0 is better

  const double legendre_overlapp_eta = 1.0;

  const double legendre_overlapp_left[11][11] = {
      {1.0000000000000000, -0.86602540378443865, 0, 0.33071891388307382, 0, -0.20728904939721249, 0, 0.15128841196122722, 0,
       -0.11918864298744029, 0},
      {0, 0.50000000000000000, -0.96824583655185422, 0.57282196186948000, 0.21650635094610966, -0.35903516540862679,
       -0.097578093724974972, 0.26203921611325661, 0.055792409597991016, -0.20644078533943456, -0.036172381205961199},
      {0, 0, 0.25000000000000000, -0.73950997288745201, 0.83852549156242114, -0.23175620272173947, -0.37791833195149451,
       0.25710129174850522, 0.21608307321780205, -0.22844049245646009, -0.14009503000335388},
      {0, 0, 0, 0.12500000000000000, -0.49607837082461074, 0.82265291131801144, -0.59621200088559103, -0.080054302859059362,
       0.42612156697795759, -0.090098145270865593, -0.29769623255090078},
      {0, 0, 0, 0, 0.062500000000000000, -0.31093357409581874, 0.67604086414949799, -0.75644205980613611, 0.28990586430124176,
       0.30648508196770361, -0.35801372616842500},
      {0, 0, 0, 0, 0, 0.031250000000000000, -0.18684782411095934, 0.50176689760410660, -0.74784031498626095, 0.56472001151566251,
       0.014842464993721351},
      {0, 0, 0, 0, 0, 0, 0.015625000000000000, -0.10909562534194485, 0.34842348626527747, -0.64461114561628111, 0.69382480527334684},
      {0, 0, 0, 0, 0, 0, 0, 0.0078125000000000000, -0.062377810244809812, 0.23080781467370884, -0.50841310636012325},
      {0, 0, 0, 0, 0, 0, 0, 0, 0.0039062500000000000, -0.035101954600803571, 0.14761284084133738},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0019531250000000000, -0.019506820659607597},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00097656250000000000}};

  const double legendre_overlapp_right[11][11] = {
      {1.0000000000000000, 0.86602540378443865, 0, -0.33071891388307382, 0, 0.20728904939721249, 0, -0.15128841196122722, 0,
       0.11918864298744029, 0},
      {0, 0.50000000000000000, 0.96824583655185422, 0.57282196186948000, -0.21650635094610966, -0.35903516540862679,
       0.097578093724974972, 0.26203921611325661, -0.055792409597991016, -0.20644078533943456, 0.036172381205961199},
      {0, 0, 0.25000000000000000, 0.73950997288745201, 0.83852549156242114, 0.23175620272173947, -0.37791833195149451,
       -0.25710129174850522, 0.21608307321780205, 0.22844049245646009, -0.14009503000335388},
      {0, 0, 0, 0.12500000000000000, 0.49607837082461074, 0.82265291131801144, 0.59621200088559103, -0.080054302859059362,
       -0.42612156697795759, -0.090098145270865593, 0.29769623255090078},
      {0, 0, 0, 0, 0.062500000000000000, 0.31093357409581874, 0.67604086414949799, 0.75644205980613611, 0.28990586430124176,
       -0.30648508196770361, -0.35801372616842500},
      {0, 0, 0, 0, 0, 0.031250000000000000, 0.18684782411095934, 0.50176689760410660, 0.74784031498626095, 0.56472001151566251,
       -0.014842464993721351},
      {0, 0, 0, 0, 0, 0, 0.015625000000000000, 0.10909562534194485, 0.34842348626527747, 0.64461114561628111, 0.69382480527334684},
      {0, 0, 0, 0, 0, 0, 0, 0.0078125000000000000, 0.062377810244809812, 0.23080781467370884, 0.50841310636012325},
      {0, 0, 0, 0, 0, 0, 0, 0, 0.0039062500000000000, 0.035101954600803571, 0.14761284084133738},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0019531250000000000, 0.019506820659607597},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00097656250000000000}};
#endif

  int bfunc_project_count[ND][NB_INCREASED];
  int bfunc_project_first[ND][NB_INCREASED];
  vector<int> bfunc_project_list[ND];
  vector<double> bfunc_project_value_left[ND];
  vector<double> bfunc_project_value_right[ND];
#endif  // VISCOSITY

  // output folder
  string output_dir;

  int dump;
  int step;
  double next_dump;
  int RotationID;
  int num_reduced_steps;

#ifdef DATACUBE_OUTPUT
  int dump_cube_count;
  double next_cube_dump;
#endif  // DATACUBE_OUTPUT

#if defined(SOUNDWAVE_TEST) || defined(VORTEX_SHEET_TEST) || defined(YEE_VORTEX) || defined(GAUSSIAN_DIFFUSION_TEST) || \
    defined(SQUARE_ADVECTION)
  FILE *FdL1;
#endif

  double LimiterInjectedEnergy;
  double LimiterInjectedMass;

#ifdef TURBULENCE
  double next_powerspectra;
  int powerspectra_output_count;
  double StOUVar;
  int StNModes;

  FILE *FdTurb;

  double RefDensity;
  double TurbInjectedEnergy;
  double TurbDissipatedEnergy;
  double TimeBetTurbSpectrum;
  double TimeNextTurbSpectrum;
  double StSolWeightNorm;
  double StTimeSinceLastUpdate;

  double *StAmpl;
  double *StAka;  // phases (real part)
  double *StAkb;  // phases (imag part)
  double *StMode;
  double *StOUPhases;
  gsl_rng *StRng;

#ifdef AB_TURB_DECAYING
  double TimeTurbDecay;
#endif  // AB_TURB_DECAYING

#endif  // TURBULENCE

#ifdef COOLING
  CoolingFunctionTable cooling_function_table;
#ifdef GPU
  CoolingFunctionTableGpu cooling_function_table_gpu;
  CoolingFunctionTableGpuData cooling_function_table_gpu_data;
#endif  // GPU
#endif  // COOLING

// GPU device pointers
#ifdef GPU
  double *d_w;
  double *d_dwdt;
  double *d_bfunc_internal;
  double *d_bfunc_external;
  double *d_bfunc_internal_diff;
  double *d_qpoint_weights_positivity;
  double *d_bfunc_mid;
  double *d_bfunc_mid_diff;
#ifdef VISCOSITY
  int (*d_bfunc_project_count)[ND][NB_INCREASED];
  int (*d_bfunc_project_first)[ND][NB_INCREASED];
  int *d_bfunc_project_list[ND];
  double *d_bfunc_project_value_left[ND];
  double *d_bfunc_project_value_right[ND];
#endif
#ifdef ART_VISCOSITY
  double *d_smthness;
  double *d_smthrate;
#endif
#ifdef TURBULENCE
  double *d_StAmpl;
  double *d_StMode;
  double *d_StAka;  // phases (real part)
  double *d_StAkb;  // phases (imag part)
#endif              // TURBULENCE
#endif              // GPU

  // Runge-Kutta tables for time integration
  double ButcherTab_a[RK_STAGES][RK_STAGES];
  double ButcherTab_b[RK_STAGES];
  double ButcherTab_c[RK_STAGES];
  int current_rk_stage;

  dg_data_t();

  void dg_populate_basis_func_number_to_legendre_order_lookup(void);

  void dg_legendre_fill_bfunc_internal(vector<double> &coords);
  void dg_legendre_fill_bfunc_external(vector<double> &coords);
  void dg_legendre_fill_bfunc_internal_diff(vector<double> &coords);
  void dg_legendre_fill_bfunc_external_diff(vector<double> &coords);
  void dg_legendre_fill_bfunc_mid(vector<double> &coords);
  void dg_legendre_fill_bfunc_mid_diff(vector<double> &coords);

  // initialize random data
  void init_random(const int seed);

  void dg_output_highres_field_maps(const int pix, const int snapnum);

  void dg_output_data_cube(int64_t pix, int snapnum);

  // initialize field to value
  void set_field_in_global_cell_to_value(const string field, const double value, const int cell);
  void set_field_in_local_cell_to_value(const string field, const double value, const int cell);

  void set_field_at_coords(const string field, const double value, const long x_start, const long x_end, const long y_start,
                           const long y_end, const long z_start, const long z_end);

  void set_field_all_cells_to_value(const string field, const double value);
  void init_constant_density_and_pressure_no_velocity(const double density, const double pressure);

 private:
  double BoxSize;
  double CellSize;
  double FaceArea;
  double CellVolume;

 public:
#if(ND == 1)
  double dg_legendre_bfunc_eval(int bfunc, double x) const { return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x); }

  double dg_legendre_bfunc_eval_diff_x(int bfunc, double x) const
  {
    return legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][0], x);
  }

#endif

#if(ND == 2)
  double dg_legendre_bfunc_eval(int bfunc, double x, double y) const
  {
    return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x) * legendre_normalized(bfunc_to_legendre_orders[bfunc][1], y);
  }

  double dg_legendre_bfunc_eval_diff_x(int bfunc, double x, double y) const
  {
    return legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][0], x) *
           legendre_normalized(bfunc_to_legendre_orders[bfunc][1], y);
  }

  double dg_legendre_bfunc_eval_diff_y(int bfunc, double x, double y) const
  {
    return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x) *
           legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][1], y);
  }

#endif
  double dg_legendre_bfunc_eval_coords_wrapper(int bfunc, const coords coordinates) const
  {
#if(ND == 3)
    return dg_legendre_bfunc_eval(bfunc, coordinates.x, coordinates.y, coordinates.z);
#endif
#if(ND == 2)
    return dg_legendre_bfunc_eval(bfunc, coordinates.x, coordinates.y);
#endif
#if(ND == 1)
    return dg_legendre_bfunc_eval(bfunc, coordinates.x);
#endif
  }

#if(ND == 3)
  double dg_legendre_bfunc_eval(int bfunc, double x, double y, double z) const
  {
    return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x) * legendre_normalized(bfunc_to_legendre_orders[bfunc][1], y) *
           legendre_normalized(bfunc_to_legendre_orders[bfunc][2], z);
  }

  double dg_legendre_bfunc_eval_diff_x(int bfunc, double x, double y, double z) const
  {
    return legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][0], x) *
           legendre_normalized(bfunc_to_legendre_orders[bfunc][1], y) * legendre_normalized(bfunc_to_legendre_orders[bfunc][2], z);
  }

  double dg_legendre_bfunc_eval_diff_y(int bfunc, double x, double y, double z) const
  {
    return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x) *
           legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][1], y) *
           legendre_normalized(bfunc_to_legendre_orders[bfunc][2], z);
  }

  double dg_legendre_bfunc_eval_diff_z(int bfunc, double x, double y, double z) const
  {
    return legendre_normalized(bfunc_to_legendre_orders[bfunc][0], x) * legendre_normalized(bfunc_to_legendre_orders[bfunc][1], y) *
           legendre_diff_normalized(bfunc_to_legendre_orders[bfunc][2], z);
  }

#endif

  inline double getBoxSize(void) const { return BoxSize; }
  inline double getCellSize(void) const { return CellSize; }
  inline double getFaceArea(void) const { return FaceArea; }
  inline double getCellVolume(void) const { return CellVolume; }

  void setBoxSize(double boxsize)
  {
    BoxSize  = boxsize;
    CellSize = BoxSize / DG_NUM_CELLS_1D;
#if(ND == 1)
    FaceArea = 1.0;
#endif
#if(ND == 2)
    FaceArea = CellSize;
#endif
#if(ND == 3)
    FaceArea = CellSize * CellSize;
#endif
    CellVolume = FaceArea * CellSize;
#if defined(ART_VISCOSITY) || defined(RICHTMYER_VISCOSITY)
    All.ViscParam.Resolution = CellSize / DG_ORDER;
#endif
    mpi_printf("[setBoxSize] BoxSize = %f CellSize = %f FaceArea = %f CellVolume = %f\n", BoxSize, CellSize, FaceArea, CellVolume);
  }
#if defined(USE_PRIMITIVE_PROJECTION) && defined(GPU)
  double *d_w_prim;
#endif  // RICHTMYER_VISCOSITY GPU
};
// tests
void set_field_in_cell_to_value_test();
void set_field_at_coords_test();
void set_field_all_cells_to_value_test();
// void init_constant_density_and_pressure_no_velocity_test();

// constructor for dg_data_t, sets sizes and allocates arrays:
void dg_data_create(dg_data_t *dg);

// create a dg_data_t dst from src
void dg_data_create_like(dg_data_t *src, dg_data_t *dst);

// destructor, deallocates struct memory:
void dg_data_destroy(dg_data_t *dg);

#endif  // SRC_DATA_DG_DATA_HPP_
