// Copyright 2021 Volker Springel

#include "allvars.h"

#include <stdio.h>
#include <cstdarg>

#include "../turbulence/powerspectra.h"
#include "runrun.h"

// create instances of global objects

global_data_all_processes All;

int World_NTask, World_ThisTask;
int Island_NTask, Island_ThisTask;
int NtotGPUs;
int LocalDeviceRank  = -1;
int GlobalDeviceRank = -1;
int FirstSlabThisTask;
int *NslabsPerTask;
int *FirstSlabOfTask;
int *DeviceIDList;
MPI_Comm SharedMemComm;
MPI_Comm MyComm;
int NTask, ThisTask;
int Restart_bool;
int step;

/* description of data local to this slab */
int Nslabs = 0;
uint64_t Nc;

void global_data_all_processes::register_parameters(void)
{
  add_param("Tend", &All.Tend, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("CourantFac", &All.CourantFac, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("OutputDir", OutputDir, PARAM_STRING, PARAM_CHANGEABLE);

  add_param("DesiredDumps", &All.DesiredDumps, PARAM_INT, PARAM_CHANGEABLE);

#ifdef DATACUBE_OUTPUT
  add_param("DesiredCubeDumps", &All.DesiredCubeDumps, PARAM_INT, PARAM_CHANGEABLE);
  add_param("PixelsFieldCubes", &All.PixelsFieldCubes, PARAM_INT, PARAM_CHANGEABLE);
#endif  // DATACUBE_OUTPUT

  add_param("PixelsFieldMaps", &All.PixelsFieldMaps, PARAM_INT, PARAM_CHANGEABLE);

  add_param("MinimumSlabsPerCPURank", &All.MinimumSlabsPerCPURank, PARAM_INT, PARAM_CHANGEABLE);

  add_param("WriteRestartAfter", &All.WriteRestartAfter, PARAM_INT, PARAM_CHANGEABLE);

  add_param("WriteCheckpointAfter", &All.WriteCheckpointAfter, PARAM_INT, PARAM_CHANGEABLE);

#if defined(TURBULENCE) || defined(ISOTHERM_EQS)
  add_param("IsoSoundSpeed", &All.ViscParam.IsoSoundSpeed, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif

#if defined(TURBULENCE)
  add_param("DesiredPowerspectra", &All.DesiredPowerspectra, PARAM_INT, PARAM_CHANGEABLE);

  add_param("PowerspectraDelay", &All.PowerspectraDelay, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StDecay", &All.StDecay, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StEnergy", &All.StEnergy, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StDtFreq", &All.StDtFreq, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StKmin", &All.StKmin, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StKmax", &All.StKmax, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StSolWeight", &All.StSolWeight, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StAmplFac", &All.StAmplFac, PARAM_DOUBLE, PARAM_CHANGEABLE);

  add_param("StSeed", &All.StSeed, PARAM_INT, PARAM_CHANGEABLE);

  add_param("StSpectForm", &All.StSpectForm, PARAM_INT, PARAM_CHANGEABLE);

#ifdef DECAYING_TURBULENCE
  add_param("StopDrivingAfterTime", &All.StopDrivingAfterTime, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif  // DECAYING_TURBULENCE
#endif  // defined(TURBULENCE)

#ifdef ART_VISCOSITY
  add_param("ArtViscMax", &All.ViscParam.ArtViscMax, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ArtViscDecay", &All.ViscParam.ArtViscDecay, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ArtViscWiggleOnset", &All.ViscParam.ArtViscWiggleOnset, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ArtViscWiggleGrowth", &All.ViscParam.ArtViscWiggleGrowth, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ArtViscShockGrowth", &All.ViscParam.ArtViscShockGrowth, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif

#ifdef RICHTMYER_VISCOSITY
  add_param("ArtViscRichtmyer", &All.ViscParam.ArtViscRichtmyer, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ArtViscLandshoff", &All.ViscParam.ArtViscLandshoff, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif

#ifdef DENSITY_FLOOR
  add_param("DensityFloor", &All.ViscParam.DensityFloor, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif

#ifdef NAVIER_STOKES
  add_param("ShearViscosity", &All.ViscParam.ShearViscosity, PARAM_DOUBLE, PARAM_CHANGEABLE);
  add_param("ThermalDiffusivity", &All.ViscParam.ThermalDiffusivity, PARAM_DOUBLE, PARAM_CHANGEABLE);
#ifdef ENABLE_DYE
  add_param("DyeDiffusivity", &All.ViscParam.DyeDiffusivity, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif
#endif

#ifdef COOLING
  add_param("CoolingDataPath", &All.CoolingDataPath, PARAM_STRING, PARAM_CHANGEABLE);
  add_param("CoolingTimeDelay", &All.CoolingTimeDelay, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif  // COOLING

#ifdef GRAVITY_WAVES
  add_param("GravityWavesAmplitude", &All.WaveAmplitude, PARAM_DOUBLE, PARAM_CHANGEABLE);
#endif  // GRAVITY_WAVES

  /*

  add_param("SnapshotFileBase", SnapshotFileBase, PARAM_STRING, PARAM_CHANGEABLE);
  add_param("OutputListFilename", OutputListFilename, PARAM_STRING, PARAM_CHANGEABLE);
  add_param("OutputListOn", &OutputListOn, PARAM_INT, PARAM_CHANGEABLE);

  */
}

void global_data_all_processes::some_parameter_checks(void) {}

void mpi_printf(const char *fmt, ...)
{
  if(ThisTask == 0)
    {
      va_list l;
      va_start(l, fmt);
      vprintf(fmt, l);
      //        myflush(stdout);
      va_end(l);
    }
}

void gpu_printf(const char *fmt, ...)
{
  if(GlobalDeviceRank == 0)
    {
      va_list l;
      va_start(l, fmt);
      vprintf(fmt, l);
      //        myflush(stdout);
      va_end(l);
    }
}
