#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define MAXLEN_PATH_EXTRA 2048 /**< maximum length of various filenames, plus extra space */
#define MAXLEN_PATH 512

#define TAG_LEFT 10
#define TAG_RIGHT 11

#endif
