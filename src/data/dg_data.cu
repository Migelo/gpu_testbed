#include "runrun.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>
#include <vector>

#include "../compute_cuda/dg_compute_cuda.cuh"
#include "../compute_cuda/dg_cuda_memory.cuh"
#include "../data/allvars.h"
#include "../data/dg_data.hpp"
#include "../data/dg_params.h"
#include "../data/macros.h"
#include "../legendre/dg_gauss_autogen.hpp"
#include "../legendre/dg_legendre.hpp"
#include "../utils/cuda_utils.h"
#include "../utils/dg_utils.hpp"

using namespace std;

// main constructor
dg_data_t::dg_data_t()
{
  mpi_printf("Creating dg data:\n NB %lu\n NF %lu\n DG_NUM_INNER_QPOINTS %lu\n DG_NUM_OUTER_QPOINTS %lu\n", NB, NF,
             DG_NUM_INNER_QPOINTS, DG_NUM_OUTER_QPOINTS);
  // #ifdef GPU
  //   cudaDeviceProp prop;
  //   gpuErrchk(cudaGetDeviceProperties(&prop, 0));
  //   if((INTERNAL_KERNEL_BLOCK_SIZE > 16) || (EXTERNAL_KERNEL_BLOCK_SIZE > 16))
  //     if(prop.sharedMemPerBlockOptin != MAX_SHARED_MEMORY)
  //       Terminate("error, GPU reports %lu bytes of max shared memory, code compiled with %d, fix in src/data/dg_param.h:153\n",
  //                 prop.sharedMemPerBlockOptin, MAX_SHARED_MEMORY);

  //   mpi_printf("GPU computing information:\nMAX_SHARED_MEMORY=%d\nINTERNAL_KERNEL_BLOCK_SIZE=%d\nEXTERNAL_KERNEL_BLOCK_SIZE=%d\n",
  //              MAX_SHARED_MEMORY, INTERNAL_KERNEL_BLOCK_SIZE, EXTERNAL_KERNEL_BLOCK_SIZE);
  // #endif  // GPU
  N_w        = NB * NF * Nc;
  dump       = 0;
  step       = 0;
  Time       = 0;
  RotationID = 0;

#ifdef DATACUBE_OUTPUT
  dump_cube_count = 0;
#endif  // DATACUBE_OUTPUT

  mpi_printf("allocating %s for weights\n", sizeof_fmt(1. * N_w * sizeof(double)).c_str());
  mpi_printf("allocating %s for temporary weights storage\n", sizeof_fmt(1. * N_w * sizeof(double)).c_str());
  mpi_printf("allocating %s for weight updates in timestepping\n", sizeof_fmt(1. * N_w * RK_STAGES * sizeof(double)).c_str());
  mpi_printf("allocating a total of %s\n", sizeof_fmt((2. + RK_STAGES) * N_w * sizeof(double)).c_str());

#ifdef GPU
  gpuErrchk(cudaMallocHost((void **)&w, N_w * sizeof(double)));
#else
  w = (double *)malloc(N_w * sizeof(double));
  memset(w, 0, N_w * sizeof(double));
#endif

#ifdef GPU
#if(ND == 1)
  int states_on_plane = DG_NUM_OUTER_QPOINTS;
#endif
#if(ND == 2)
  int states_on_plane = DG_NUM_CELLS_1D * DG_NUM_OUTER_QPOINTS;
#endif
#if(ND == 3)
  int states_on_plane = DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * DG_NUM_OUTER_QPOINTS;
#endif

  gpuErrchk(cudaMallocHost((void **)&right_states_to_send, states_on_plane * sizeof(fields_face)));
  gpuErrchk(cudaMallocHost((void **)&right_states_to_recv, states_on_plane * sizeof(fields_face)));
  gpuErrchk(cudaMallocHost((void **)&left_states_to_send, states_on_plane * sizeof(fields_face)));
  gpuErrchk(cudaMallocHost((void **)&left_states_to_recv, states_on_plane * sizeof(fields_face)));
#ifdef FINITE_VOLUME
  gpuErrchk(cudaMallocHost((void **)&right_slopes_to_send, cell_stride_x * sizeof(slopes)));
  gpuErrchk(cudaMallocHost((void **)&right_slopes_to_recv, cell_stride_x * sizeof(slopes)));
  gpuErrchk(cudaMallocHost((void **)&left_slopes_to_send, cell_stride_x * sizeof(slopes)));
  gpuErrchk(cudaMallocHost((void **)&left_slopes_to_recv, cell_stride_x * sizeof(slopes)));
#endif
#endif  // GPU

#ifdef ART_VISCOSITY
  smthness = (double *)malloc(Nc * sizeof(double));
  smthrate = (double *)malloc(Nc * sizeof(double));

  memset(smthness, 0, Nc * sizeof(double));
  memset(smthrate, 0, Nc * sizeof(double));
#endif

  // timestepping
  Ti_Current = 0;

  dg_populate_basis_func_number_to_legendre_order_lookup();

  // this is put into constant cache on the gpu
  qpoint_location.resize(DG_NUM_QPOINTS_1D);
  qpoint_weights.resize(DG_NUM_QPOINTS_1D);
  qpoint_weights_internal.resize(DG_NUM_INNER_QPOINTS);
  qpoint_weights_external.resize(DG_NUM_OUTER_QPOINTS);
  bfunc_internal.resize(NB * DG_NUM_INNER_QPOINTS);
  bfunc_external.resize(2 * ND * NB * DG_NUM_OUTER_QPOINTS);
  bfunc_internal_diff.resize(NB * ND * DG_NUM_INNER_QPOINTS);
  bfunc_external_diff.resize(2 * ND * ND * NB * DG_NUM_OUTER_QPOINTS);
  bfunc_mid.resize(ND * NB_INCREASED * DG_NUM_OUTER_QPOINTS);
  bfunc_mid_diff.resize(ND * ND * NB_INCREASED * DG_NUM_OUTER_QPOINTS);
  qpoint_weights_positivity.resize(NB * DG_NUM_POSITIVE_POINTS);

  LimiterInjectedEnergy = 0;
  LimiterInjectedMass   = 0;
#ifdef TURBULENCE
  powerspectra_output_count = 0;
  RefDensity                = 1;
  TurbInjectedEnergy        = 0;
  TurbDissipatedEnergy      = 0;
  StNModes                  = 0;
  StTimeSinceLastUpdate     = 0;
#endif  // TURBULENCE

#ifdef COOLING
  cooling_function_table.init(string(All.CoolingDataPath) + string("/cooling_function_table.dat"), 176,
                              string(All.CoolingDataPath) + string("/temperature_bins.dat"), 41,
                              string(All.CoolingDataPath) + string("/hydrogen_density_bins.dat"));
#ifdef GPU
  cooling_function_table_gpu_data.init(string(All.CoolingDataPath) + string("/cooling_function_table.dat"),
                                       string(All.CoolingDataPath) + string("/temperature_bins.dat"));
  cooling_function_table_gpu.init();

#endif  // GPU
#endif  // COOLING

  // fill internal and external qpoint arrays
  dg_gauss_get_points_weights((size_t)DG_NUM_QPOINTS_1D, qpoint_location, qpoint_weights);
  dg_gauss_get_points_weights_internal(DG_NUM_QPOINTS_1D, qpoint_weights, qpoint_weights_internal, ND);
  dg_gauss_get_points_weights_external(DG_NUM_QPOINTS_1D, qpoint_weights, qpoint_weights_external, ND);

  dg_legendre_fill_bfunc_internal(qpoint_location);
  dg_legendre_fill_bfunc_external(qpoint_location);
  dg_legendre_fill_bfunc_mid(qpoint_location);
  dg_legendre_fill_bfunc_mid_diff(qpoint_location);
  dg_legendre_fill_bfunc_internal_diff(qpoint_location);
  dg_legendre_fill_bfunc_external_diff(qpoint_location);

  // fill the positivity qpoint array
  dg_get_point_weights_positivity(qpoint_weights_positivity, bfunc_internal, bfunc_external);

  // now initialize time integration tables
  if(RK_STAGES == 1)  // explicit Euler
    {
      ButcherTab_c[0] = 0;

      ButcherTab_b[0] = 1;
    }

  if(RK_STAGES == 2)  // Heun's method
    {
      ButcherTab_c[0] = 0;

      ButcherTab_c[1]    = 1;
      ButcherTab_a[1][0] = 1;

      ButcherTab_b[0] = 0.5;
      ButcherTab_b[1] = 0.5;
    }

  if(RK_STAGES == 3)  // SSP RK3
    {
      ButcherTab_c[0] = 0;

      ButcherTab_c[1]    = 1;
      ButcherTab_a[1][0] = 1.;

      ButcherTab_c[2]    = 0.5;
      ButcherTab_a[2][0] = 1. / 4.;
      ButcherTab_a[2][1] = 1. / 4.;

      ButcherTab_b[0] = 1. / 6.;
      ButcherTab_b[1] = 1. / 6.;
      ButcherTab_b[2] = 2. / 3.;
    }

  if(RK_STAGES >= 4)  // SSP RK4
    {
      ButcherTab_c[0] = 0;

      ButcherTab_c[1]    = 0.39175222700392;
      ButcherTab_a[1][0] = 0.39175222700392;

      ButcherTab_c[2]    = 0.58607968896779;
      ButcherTab_a[2][0] = 0.21766909633821;
      ButcherTab_a[2][1] = 0.36841059262959;

      ButcherTab_c[3]    = 0.47454236302687;
      ButcherTab_a[3][0] = 0.08269208670950;
      ButcherTab_a[3][1] = 0.13995850206999;
      ButcherTab_a[3][2] = 0.25189177424738;

      ButcherTab_c[4]    = 0.93501063100924;
      ButcherTab_a[4][0] = 0.06796628370320;
      ButcherTab_a[4][1] = 0.11503469844438;
      ButcherTab_a[4][2] = 0.20703489864929;
      ButcherTab_a[4][3] = 0.54497475021237;

      ButcherTab_b[0] = 0.14681187618661;
      ButcherTab_b[1] = 0.24848290924556;
      ButcherTab_b[2] = 0.10425883036650;
      ButcherTab_b[3] = 0.27443890091960;
      ButcherTab_b[4] = 0.22600748319395;
    }
}

void dg_data_t::dg_populate_basis_func_number_to_legendre_order_lookup(void)
{
  // DEGREE_K gives the maximum degree we are going to use for the basis functions

  // To keep the sequence of overlapping basis functions the same when the order is
  // changed, we fill the mapping from inside out

#if(ND == 1)
  int count = 0;
  for(decltype(ND) degree = 0; degree <= DEGREE_K + 1; degree++)
    {
      for(decltype(ND) u = 0; u <= DEGREE_K + 1; u++)
        {
          if(u == degree)
            {
              bfunc_to_legendre_orders[count][0] = u;

              mpi_printf("Basis function %4d maps to legendre functions: %2d    (degree = %d)\n", count, u, degree);
              count++;
            }
        }

      if(degree == DEGREE_K)
        mpi_printf("\n");
    }

  mpi_printf("Defined  %d  basis functions (expected %lu)\n", count, (DEGREE_K + 1));

  mpi_printf("\n");

#ifdef VISCOSITY
  for(int rep = 0; rep < 2; rep++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              bfunc_project_count[d][q] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  int lx = bfunc_to_legendre_orders[q][d];
                  int mx = bfunc_to_legendre_orders[b][d];

                  if(mx <= lx)
                    {
                      if(legendre_overlapp_left[mx][lx] != 0 || legendre_overlapp_right[mx][lx] != 0)
                        {
                          if(rep == 0)
                            bfunc_project_count[d][q]++;
                          else
                            {
                              int index = bfunc_project_first[d][q] + bfunc_project_count[d][q]++;

                              bfunc_project_list[d][index]        = b;
                              bfunc_project_value_left[d][index]  = legendre_overlapp_left[mx][lx];
                              bfunc_project_value_right[d][index] = legendre_overlapp_right[mx][lx];
                            }
                        }
                    }
                }

              if(rep == 0)
                {
                  mpi_printf("Basis function %4d based on   %d  others (for axis=%d-extension)\n", q, bfunc_project_count[d][q], d);
                }
            }
        }

      if(rep == 0)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              int count = 0;

              for(decltype(ND) q = 0; q < NB_INCREASED; q++)
                {
                  count += bfunc_project_count[d][q];

                  if(q == 0)
                    {
                      bfunc_project_first[d][0] = 0;
                    }
                  else
                    {
                      bfunc_project_first[d][q] = bfunc_project_first[d][q - 1] + bfunc_project_count[d][q - 1];
                    }
                }

              bfunc_project_list[d].resize(count);
              bfunc_project_value_left[d].resize(count);
              bfunc_project_value_right[d].resize(count);
            }
        }
    }

  mpi_printf("\n");

  for(decltype(ND) d = 0; d < ND; d++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          mpi_printf("For surface axis=%d-derivative, basis function %4d maps to: ", d, q);
          for(int i = 0; i < bfunc_project_count[d][q]; i++)
            mpi_printf(" %4d ", bfunc_project_list[d][bfunc_project_first[d][q] + i]);

          mpi_printf("\n");
        }
      mpi_printf("\n");
    }
#endif
#endif

#if(ND == 2)
  int count = 0;
  for(decltype(ND) degree = 0; degree <= DEGREE_K + 1; degree++)
    {
      for(decltype(ND) u = 0; u <= DEGREE_K + 1; u++)
        for(decltype(ND) v = 0; v <= DEGREE_K + 1 - u; v++)
          {
            if(u + v == degree)
              {
                bfunc_to_legendre_orders[count][0] = v;
                bfunc_to_legendre_orders[count][1] = u;

                mpi_printf("Basis function %4d maps to legendre functions: %2d %2d   (degree = %d)\n", count, v, u, degree);
                count++;
              }
          }

      if(degree == DEGREE_K)
        mpi_printf("\n");
    }

  mpi_printf("Defined  %d  basis functions (expected %lu)\n", count, (DEGREE_K + 1) * (DEGREE_K + 2) / 2);

  mpi_printf("\n");
#ifdef VISCOSITY
  for(int rep = 0; rep < 2; rep++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              bfunc_project_count[d][q] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  int lx = bfunc_to_legendre_orders[q][d];
                  int mx = bfunc_to_legendre_orders[b][d];

                  // the other dimension
                  int ly = bfunc_to_legendre_orders[q][1 - d];
                  int my = bfunc_to_legendre_orders[b][1 - d];

                  if(mx <= lx && my == ly)
                    {
                      if(legendre_overlapp_left[mx][lx] != 0 || legendre_overlapp_right[mx][lx] != 0)
                        {
                          if(rep == 0)
                            bfunc_project_count[d][q]++;
                          else
                            {
                              int index = bfunc_project_first[d][q] + bfunc_project_count[d][q]++;

                              bfunc_project_list[d][index]        = b;
                              bfunc_project_value_left[d][index]  = legendre_overlapp_left[mx][lx];
                              bfunc_project_value_right[d][index] = legendre_overlapp_right[mx][lx];
                            }
                        }
                    }
                }

              if(rep == 0)
                {
                  mpi_printf("Basis function %4d based on   %d  others (for axis=%d-extension)\n", q, bfunc_project_count[d][q], d);
                }
            }
        }

      if(rep == 0)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              int count = 0;

              for(decltype(ND) q = 0; q < NB_INCREASED; q++)
                {
                  count += bfunc_project_count[d][q];

                  if(q == 0)
                    {
                      bfunc_project_first[d][0] = 0;
                    }
                  else
                    {
                      bfunc_project_first[d][q] = bfunc_project_first[d][q - 1] + bfunc_project_count[d][q - 1];
                    }
                }

              bfunc_project_list[d].resize(count);
              bfunc_project_value_left[d].resize(count);
              bfunc_project_value_right[d].resize(count);
            }
        }
    }

  mpi_printf("\n");

  for(decltype(ND) d = 0; d < ND; d++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          mpi_printf("For surface axis=%d-derivative, basis function %4d maps to: ", d, q);
          for(int i = 0; i < bfunc_project_count[d][q]; i++)
            mpi_printf(" %4d ", bfunc_project_list[d][bfunc_project_first[d][q] + i]);

          mpi_printf("\n");
        }
      mpi_printf("\n");
    }
#endif
#endif

#if(ND == 3)
  int count = 0;
  for(decltype(ND) degree = 0; degree <= DEGREE_K + 1; degree++)
    {
      for(decltype(ND) u = 0; u <= DEGREE_K + 1; u++)
        for(decltype(ND) v = 0; v <= DEGREE_K + 1 - u; v++)
          for(decltype(ND) w = 0; w <= DEGREE_K + 1 - u - v; w++)
            {
              if(u + v + w == degree)
                {
                  bfunc_to_legendre_orders[count][0] = w;
                  bfunc_to_legendre_orders[count][1] = v;
                  bfunc_to_legendre_orders[count][2] = u;

                  mpi_printf("Basis function %4d maps to legendre functions: %2d %2d %2d  (degree = %d)\n", count, w, v, u, degree);
                  count++;
                }
            }

      if(degree == DEGREE_K)
        mpi_printf("\n");
    }

  mpi_printf("Defined  %d  basis functions (expected %lu)\n", count, (DEGREE_K + 1) * (DEGREE_K + 2) * (DEGREE_K + 3) / 6);

  mpi_printf("\n");

#ifdef VISCOSITY
  for(int rep = 0; rep < 2; rep++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              bfunc_project_count[d][q] = 0;

              for(decltype(ND) b = 0; b < NB; b++)
                {
                  auto lx = bfunc_to_legendre_orders[q][d];
                  auto mx = bfunc_to_legendre_orders[b][d];

                  // the other dimensions
                  auto ly = bfunc_to_legendre_orders[q][d + 1 >= static_cast<int>(ND) ? d + 1 - ND : d + 1];
                  auto my = bfunc_to_legendre_orders[b][d + 1 >= static_cast<int>(ND) ? d + 1 - ND : d + 1];

                  auto lz = bfunc_to_legendre_orders[q][d + 2 >= static_cast<int>(ND) ? d + 2 - ND : d + 2];
                  auto mz = bfunc_to_legendre_orders[b][d + 2 >= static_cast<int>(ND) ? d + 2 - ND : d + 2];

                  if(mx <= lx && my == ly && mz == lz)
                    {
                      if(legendre_overlapp_left[mx][lx] != 0 || legendre_overlapp_right[mx][lx] != 0)
                        {
                          if(rep == 0)
                            bfunc_project_count[d][q]++;
                          else
                            {
                              int index = bfunc_project_first[d][q] + bfunc_project_count[d][q]++;

                              bfunc_project_list[d][index]        = b;
                              bfunc_project_value_left[d][index]  = legendre_overlapp_left[mx][lx];
                              bfunc_project_value_right[d][index] = legendre_overlapp_right[mx][lx];
                            }
                        }
                    }
                }

              if(rep == 0)
                {
                  mpi_printf("Basis function %4d based on   %d  others (for axis=%d-extension)\n", q, bfunc_project_count[d][q], d);
                }
            }
        }

      if(rep == 0)
        {
          for(decltype(ND) d = 0; d < ND; d++)
            {
              int count = 0;

              for(decltype(ND) q = 0; q < NB_INCREASED; q++)
                {
                  count += bfunc_project_count[d][q];

                  if(q == 0)
                    {
                      bfunc_project_first[d][0] = 0;
                    }
                  else
                    {
                      bfunc_project_first[d][q] = bfunc_project_first[d][q - 1] + bfunc_project_count[d][q - 1];
                    }
                }

              bfunc_project_list[d].resize(count);
              bfunc_project_value_left[d].resize(count);
              bfunc_project_value_right[d].resize(count);
            }
        }
    }

  mpi_printf("\n");

  for(decltype(ND) d = 0; d < ND; d++)
    {
      for(decltype(ND) q = 0; q < NB_INCREASED; q++)
        {
          mpi_printf("For surface axis=%d-derivative, basis function %4d maps to: ", d, q);
          for(int i = 0; i < bfunc_project_count[d][q]; i++)
            mpi_printf(" %4d ", bfunc_project_list[d][bfunc_project_first[d][q] + i]);

          mpi_printf("\n");
        }
      mpi_printf("\n");
    }
#endif
#endif
}

// evaluate the basis function for later look-up at the internal Gauss points
void dg_data_t::dg_legendre_fill_bfunc_internal(vector<double> &coords)
{
  for(uint order = 0; order < NB; order++)
    {
#if(ND == 3)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(decltype(ND) y = 0; y < DG_NUM_QPOINTS_1D; y++)
          for(decltype(ND) z = 0; z < DG_NUM_QPOINTS_1D; z++)
            {
              double xx = coords[x];
              double yy = coords[y];
              double zz = coords[z];

              auto qpoint_in_cell = (x * DG_NUM_QPOINTS_1D + y) * DG_NUM_QPOINTS_1D + z;

              bfunc_internal[bfunc_internal_idx(qpoint_in_cell, order)] = dg_legendre_bfunc_eval(order, xx, yy, zz);
            }
#elif(ND == 2)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(decltype(ND) y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_in_cell = x * DG_NUM_QPOINTS_1D + y;

            bfunc_internal[bfunc_internal_idx(qpoint_in_cell, order)] = dg_legendre_bfunc_eval(order, xx, yy);
          }
#elif(ND == 1)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_in_cell = x;

          bfunc_internal[bfunc_internal_idx(qpoint_in_cell, order)] = dg_legendre_bfunc_eval(order, xx);
        }
#endif
    }
}

void dg_data_t::dg_legendre_fill_bfunc_internal_diff(vector<double> &coords)
{
  for(decltype(ND) order = 0; order < NB; order++)
    {
#if(ND == 3)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(decltype(ND) y = 0; y < DG_NUM_QPOINTS_1D; y++)
          for(decltype(ND) z = 0; z < DG_NUM_QPOINTS_1D; z++)
            {
              double xx = coords[x];
              double yy = coords[y];
              double zz = coords[z];

              auto qpoint_in_cell = (x * DG_NUM_QPOINTS_1D + y) * DG_NUM_QPOINTS_1D + z;

              bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 0)] =
                  dg_legendre_bfunc_eval_diff_x(order, xx, yy, zz);
              bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 1)] =
                  dg_legendre_bfunc_eval_diff_y(order, xx, yy, zz);
              bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 2)] =
                  dg_legendre_bfunc_eval_diff_z(order, xx, yy, zz);
            }
#elif(ND == 2)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(decltype(ND) y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_in_cell = x * DG_NUM_QPOINTS_1D + y;

            bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 0)] = dg_legendre_bfunc_eval_diff_x(order, xx, yy);
            bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 1)] = dg_legendre_bfunc_eval_diff_y(order, xx, yy);
          }
#elif(ND == 1)
      for(decltype(ND) x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_in_cell = x;

          bfunc_internal_diff[bfunc_diff_internal_idx(qpoint_in_cell, order, 0)] = dg_legendre_bfunc_eval_diff_x(order, xx);
        }
#endif
    }
}

void dg_data_t::dg_legendre_fill_bfunc_mid(vector<double> &coords)
{
  for(uint order = 0; order < NB_INCREASED; order++)
    {
#if(ND == 3)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(uint y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_on_face = x * DG_NUM_QPOINTS_1D + y;

            // axis = 0
            bfunc_mid[bfunc_mid_idx(qpoint_on_face, order, 0)] = dg_legendre_bfunc_eval(order, 0.0, xx, yy);

            // axis = 1
            bfunc_mid[bfunc_mid_idx(qpoint_on_face, order, 1)] = dg_legendre_bfunc_eval(order, xx, 0.0, yy);

            // axis = 2
            bfunc_mid[bfunc_mid_idx(qpoint_on_face, order, 2)] = dg_legendre_bfunc_eval(order, xx, yy, 0.0);
          }
#elif(ND == 2)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_on_face = x;

          // axis = 0
          bfunc_mid[bfunc_mid_idx(qpoint_on_face, order, 0)] = dg_legendre_bfunc_eval(order, 0.0, xx);

          // axis = 1
          bfunc_mid[bfunc_mid_idx(qpoint_on_face, order, 1)] = dg_legendre_bfunc_eval(order, xx, 0.0);
        }
#elif(ND == 1)
      // axis = 0
      bfunc_mid[bfunc_mid_idx(0, order, 0)] = dg_legendre_bfunc_eval(order, 0.0);
#endif
    }
}

void dg_data_t::dg_legendre_fill_bfunc_mid_diff(vector<double> &coords)
{
  for(uint order = 0; order < NB_INCREASED; order++)
    {
#if(ND == 3)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(uint y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_on_face = x * DG_NUM_QPOINTS_1D + y;

            // axis = 0
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 0.0, xx, yy);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 1, 0)] = dg_legendre_bfunc_eval_diff_y(order, 0.0, xx, yy);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 2, 0)] = dg_legendre_bfunc_eval_diff_z(order, 0.0, xx, yy);

            // axis = 1
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 0, 1)] = dg_legendre_bfunc_eval_diff_x(order, xx, 0.0, yy);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 1, 1)] = dg_legendre_bfunc_eval_diff_y(order, xx, 0.0, yy);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 2, 1)] = dg_legendre_bfunc_eval_diff_z(order, xx, 0.0, yy);

            dg_legendre_bfunc_eval_diff_z(order, xx, -0.0, yy);

            // axis = 2
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 0, 2)] = dg_legendre_bfunc_eval_diff_x(order, xx, yy, 0.0);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 1, 2)] = dg_legendre_bfunc_eval_diff_y(order, xx, yy, 0.0);
            bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 2, 2)] = dg_legendre_bfunc_eval_diff_z(order, xx, yy, 0.0);
          }
#elif(ND == 2)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_on_face = x;

          // axis = 0
          bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 0.0, xx);
          bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 1, 0)] = dg_legendre_bfunc_eval_diff_y(order, 0.0, xx);

          // axis = 1
          bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 0, 1)] = dg_legendre_bfunc_eval_diff_x(order, xx, 0.0);
          bfunc_mid_diff[bfunc_mid_diff_idx(qpoint_on_face, order, 1, 1)] = dg_legendre_bfunc_eval_diff_y(order, xx, 0.0);
        }
#elif(ND == 1)
      // axis = 0
      bfunc_mid_diff[bfunc_mid_diff_idx(0, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 0.0);
      bfunc_mid_diff[bfunc_mid_diff_idx(0, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 0.0);
#endif
    }
}

// evaluate the basis function for later look-up at the external Gauss points, separately on each face of cell
void dg_data_t::dg_legendre_fill_bfunc_external(vector<double> &coords)
{
  for(uint order = 0; order < NB; order++)
    {
#if(ND == 3)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(uint y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_on_face = x * DG_NUM_QPOINTS_1D + y;

            // axis = 0
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 0)]      = dg_legendre_bfunc_eval(order, 1.0, xx, yy);
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 0 + ND)] = dg_legendre_bfunc_eval(order, -1.0, xx, yy);

            // axis = 1
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 1)]      = dg_legendre_bfunc_eval(order, xx, 1.0, yy);
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 1 + ND)] = dg_legendre_bfunc_eval(order, xx, -1.0, yy);

            // axis = 2
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 2)]      = dg_legendre_bfunc_eval(order, xx, yy, 1.0);
            bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 2 + ND)] = dg_legendre_bfunc_eval(order, xx, yy, -1.0);
          }
#elif(ND == 2)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_on_face = x;

          // axis = 0
          bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 0)]      = dg_legendre_bfunc_eval(order, 1.0, xx);
          bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 0 + ND)] = dg_legendre_bfunc_eval(order, -1.0, xx);

          // axis = 1
          bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 1)]      = dg_legendre_bfunc_eval(order, xx, 1.0);
          bfunc_external[bfunc_external_side_idx(qpoint_on_face, order, 1 + ND)] = dg_legendre_bfunc_eval(order, xx, -1.0);
        }
#elif(ND == 1)
      // axis = 0
      bfunc_external[bfunc_external_side_idx(0, order, 0)]      = dg_legendre_bfunc_eval(order, 1.0);
      bfunc_external[bfunc_external_side_idx(0, order, 0 + ND)] = dg_legendre_bfunc_eval(order, -1.0);
#endif
    }
}

// evaluate the basis function for later look-up at the external Gauss points, separately on each face of cell
void dg_data_t::dg_legendre_fill_bfunc_external_diff(vector<double> &coords)
{
  for(uint order = 0; order < NB; order++)
    {
#if(ND == 3)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        for(uint y = 0; y < DG_NUM_QPOINTS_1D; y++)
          {
            double xx = coords[x];
            double yy = coords[y];

            auto qpoint_on_face = x * DG_NUM_QPOINTS_1D + y;

            // axis = 0
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 0)] =
                dg_legendre_bfunc_eval_diff_x(order, 1.0, xx, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 0)] =
                dg_legendre_bfunc_eval_diff_y(order, 1.0, xx, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 0)] =
                dg_legendre_bfunc_eval_diff_z(order, 1.0, xx, yy);

            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 0 + ND)] =
                dg_legendre_bfunc_eval_diff_x(order, -1.0, xx, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 0 + ND)] =
                dg_legendre_bfunc_eval_diff_y(order, -1.0, xx, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 0 + ND)] =
                dg_legendre_bfunc_eval_diff_z(order, -1.0, xx, yy);

            // axis = 1
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 1)] =
                dg_legendre_bfunc_eval_diff_x(order, xx, 1.0, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 1)] =
                dg_legendre_bfunc_eval_diff_y(order, xx, 1.0, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 1)] =
                dg_legendre_bfunc_eval_diff_z(order, xx, 1.0, yy);

            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 1 + ND)] =
                dg_legendre_bfunc_eval_diff_x(order, xx, -1.0, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 1 + ND)] =
                dg_legendre_bfunc_eval_diff_y(order, xx, -1.0, yy);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 1 + ND)] =
                dg_legendre_bfunc_eval_diff_z(order, xx, -1.0, yy);

            // axis = 2
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 2)] =
                dg_legendre_bfunc_eval_diff_x(order, xx, yy, 1.0);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 2)] =
                dg_legendre_bfunc_eval_diff_y(order, xx, yy, 1.0);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 2)] =
                dg_legendre_bfunc_eval_diff_z(order, xx, yy, 1.0);

            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 2 + ND)] =
                dg_legendre_bfunc_eval_diff_x(order, xx, yy, -1.0);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 2 + ND)] =
                dg_legendre_bfunc_eval_diff_y(order, xx, yy, -1.0);
            bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 2, 2 + ND)] =
                dg_legendre_bfunc_eval_diff_z(order, xx, yy, -1.0);
          }
#elif(ND == 2)
      for(uint x = 0; x < DG_NUM_QPOINTS_1D; x++)
        {
          double xx = coords[x];

          auto qpoint_on_face = x;

          // axis = 0
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 0)] =
              dg_legendre_bfunc_eval_diff_x(order, 1.0, xx);
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 0)] =
              dg_legendre_bfunc_eval_diff_y(order, 1.0, xx);

          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 0 + ND)] =
              dg_legendre_bfunc_eval_diff_x(order, -1.0, xx);
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 0 + ND)] =
              dg_legendre_bfunc_eval_diff_y(order, -1.0, xx);

          // axis = 1
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 1)] =
              dg_legendre_bfunc_eval_diff_x(order, xx, 1.0);
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 1)] =
              dg_legendre_bfunc_eval_diff_y(order, xx, 1.0);

          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 0, 1 + ND)] =
              dg_legendre_bfunc_eval_diff_x(order, xx, -1.0);
          bfunc_external_diff[bfunc_external_diff_side_idx(qpoint_on_face, order, 1, 1 + ND)] =
              dg_legendre_bfunc_eval_diff_y(order, xx, -1.0);
        }
#elif(ND == 1)
      // axis = 0
      bfunc_external_diff[bfunc_external_diff_side_idx(0, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 1.0);
      bfunc_external_diff[bfunc_external_diff_side_idx(0, order, 0, 0)] = dg_legendre_bfunc_eval_diff_x(order, 1.0);

      bfunc_external_diff[bfunc_external_diff_side_idx(0, order, 0, 0 + ND)] = dg_legendre_bfunc_eval_diff_x(order, -1.0);
      bfunc_external_diff[bfunc_external_diff_side_idx(0, order, 0, 0 + ND)] = dg_legendre_bfunc_eval_diff_x(order, -1.0);
#endif
    }
}

void dg_data_t::init_random(const int seed)
{
  cout << "Filling dg->w and dg->w_external with random data, using seed " << seed << endl;
  mpi_printf("Total number of weights: %lld\n", (long long)this->N_w);
  mpi_printf("Total size of weights: %g MB\n", this->N_w * sizeof(double) / 1024.0 / 1024.0);

  // initialize the random seed
  srand(seed);

  // dimensionallity scale factor
  this->scale_factor = (double)rand() / RAND_MAX;

  // set random data
  for(uint64_t i = 0; i < this->N_w; i++)
    this->w[i] = (double)rand() / RAND_MAX;
  // for (uint64_t i = 0; i < NB; i++)
  //     this->bfunc.at(i) = (double)rand() / RAND_MAX;
  mpi_printf("DONE!\n");
}

void dg_data_t::set_field_in_global_cell_to_value(const string field, const double value, const int cell)
{
  /*
  field(string) : field you want to set
  value(double) : value to set the field to
  cell(int) : cell ID

  */

  // general variables
  int f;

  // check for 3D
  if((field == "vz") && (ND != 3))
    {
      Terminate("dg_data_t::set_field_in_global_cell_to_value: vz can only be set when in 3D.");
    }

  // verify we're setting an exisitng field
  if(field == "rho")
    f = 0;
  else if(field == "vx")
    f = 1;
  else if(field == "vy")
    f = 2;
  else if((field == "vz") and (ND == 3))
    f = 3;
  else if((field == "e") && (ND == 3))
    f = 4;
  else if((field == "e") && (ND == 2))
    f = 3;
  else
    {
      printf("unknown field %s, exiting\n", field.c_str());
      assert(0);
    }

  if(dg_geometry_is_cell_in_this_rank(cell))
    {
      auto cell_local_idx = dg_geometry_global_cell_idx_to_local(cell);

      // set internal qpoints of the cell
      this->w[w_idx_internal(0, f, cell_local_idx)] = value;
      for(decltype(ND) b = 1; b < NB; b++)
        this->w[w_idx_internal(b, f, cell_local_idx)] = 0.;
    }
}

void dg_data_t::set_field_in_local_cell_to_value(const string field, const double value, const int cell)
{
  /*
  field(string) : field you want to set
  value(double) : value to set the field to
  cell(int) : cell ID

  */

  // general variables
  int f;

  // check for 3D
  if((field == "vz") && (ND != 3))
    {
      Terminate("dg_data_t::set_field_in_local_cell_to_value: vz can only be set when in 3D.");
    }

  // verify we're setting an exisitng field
  if(field == "rho")
    f = 0;
  else if(field == "vx")
    f = 1;
  else if(field == "vy")
    f = 2;
  else if((field == "vz") and (ND == 3))
    f = 3;
  else if((field == "e") && (ND == 3))
    f = 4;
  else if((field == "e") && (ND == 2))
    f = 3;
  else
    {
      printf("unknown field %s, exiting\n", field.c_str());
      assert(0);
    }

  this->w[w_idx_internal(0, f, cell)] = value;
  for(decltype(ND) b = 1; b < NB; b++)
    {
      this->w[w_idx_internal(b, f, cell)] = 0.;
    }
}

#if(ND >= 2)
void dg_data_t::set_field_at_coords(const string field, const double value, const long x_start, const long x_end, const long y_start,
                                    const long y_end, const long z_start, const long z_end)
{
  if(ND == 2)
    {
      if(!((z_start == 0) && (z_end == 1)))
        {
          cout << "in 2D z_start must = 0 and z_end = 1" << endl;
          assert(0);
        }
    }

  assert((x_end <= DG_NUM_CELLS_1D) && (y_end <= DG_NUM_CELLS_1D) && (z_end <= DG_NUM_CELLS_1D));
  assert((x_start < x_end) && (y_start < y_end) && (z_start < z_end));

  for(decltype(ND) x = x_start; x < x_end; x++)
    for(decltype(ND) y = y_start; y < y_end; y++)
#if(ND == 3)
      for(decltype(ND) z = z_start; z < z_end; z++)
#endif
        {
#if(ND == 2)
          const auto idx = dg_geometry_cell_to_idx(x, y, DG_NUM_CELLS_1D);
#endif
#if(ND == 3)
          const auto idx = dg_geometry_cell_to_idx(x, y, z, DG_NUM_CELLS_1D);
#endif
          this->set_field_in_global_cell_to_value(field, value, idx);
        }
}
#endif

void dg_data_t::set_field_all_cells_to_value(const string field, const double value)
{
  mpi_printf("set_field_all_cells_to_value: field %s to %g...", field.c_str(), value);
  for(uint64_t cell = 0; cell < Nc; cell++)
    this->set_field_in_local_cell_to_value(field, value, cell);

  mpi_printf("DONE\n");
}

void dg_data_t::init_constant_density_and_pressure_no_velocity(const double density, const double pressure)
{
  mpi_printf("Filling our domain with density = %g , pressure = %g and zero velocity.\n", density, pressure);

  this->set_field_all_cells_to_value("rho", density);
  this->set_field_all_cells_to_value("vx", 0);
#if(ND >= 2)
  this->set_field_all_cells_to_value("vy", 0);
#endif
#if(ND == 3)
  this->set_field_all_cells_to_value("vz", 0);
#endif
#ifndef ISOTHERM_EQS
  this->set_field_all_cells_to_value("e", pressure / GAMMA_MINUS1);
#endif
  mpi_printf("[init_constant_density_and_pressure_no_velocity] DONE!\n");
}
