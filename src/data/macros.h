
#ifndef MACROS_H_
#define MACROS_H_

#include <mpi.h>

#define Terminate(...)                                                                                                      \
  {                                                                                                                         \
    {                                                                                                                       \
      char termbuf1__[8000], termbuf2__[8000];                                                                              \
      int thistask;                                                                                                         \
      MPI_Comm_rank(MPI_COMM_WORLD, &thistask);                                                                             \
      sprintf(termbuf1__, "Code termination on task=%d, function %s(), file %s, line %d", thistask, __FUNCTION__, __FILE__, \
              __LINE__);                                                                                                    \
      sprintf(termbuf2__, __VA_ARGS__);                                                                                     \
      printf("%s: %s\n", termbuf1__, termbuf2__);                                                                           \
      fflush(stdout);                                                                                                       \
      MPI_Abort(MPI_COMM_WORLD, 1);                                                                                         \
    }                                                                                                                       \
    exit(0);                                                                                                                \
  }

#endif
