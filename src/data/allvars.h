// Copyright 2021 Volker Springel

#ifndef SRC_DATA_ALLVARS_H_
#define SRC_DATA_ALLVARS_H_

#include "runrun.h"

#include <mpi.h>

#include <string>

#include "../data/constants.h"
#include "../io/parameters.h"

using ::std::string;

void mpi_printf(const char *fmt, ...);
void gpu_printf(const char *fmt, ...);

/** Data which is the SAME for all tasks (mostly code parameters read
 * from the parameter file).  Holding this data in a structure is
 * convenient for writing/reading the restart file, and it allows the
 * introduction of new global variables in a simple way. The only
 * thing to do is to introduce them into this structure.
 */
#define GET_VARIABLE_NAME(Variable) (#Variable)

struct memory_allocation
{
  string name;
  size_t size;
  string file;
  int line;
};

struct visc_params
{
#if defined(ART_VISCOSITY) || defined(RICHTMYER_VISCOSITY)
  double Resolution;
#endif
#ifdef DENSITY_FLOOR
  double DensityFloor;
#endif
#ifdef RICHTMYER_VISCOSITY
  double ArtViscRichtmyer;
  double ArtViscLandshoff;
#endif
#ifdef VISCOSITY
#ifdef ART_VISCOSITY
  double ArtViscMax;
  double ArtViscShockGrowth;
  double ArtViscWiggleGrowth;
  double ArtViscDecay;
  double ArtViscWiggleOnset;
#endif
#ifdef NAVIER_STOKES
  double ShearViscosity;
  double ThermalDiffusivity;
  double DyeDiffusivity;
#endif
#endif

#if defined(ISOTHERM_EQS) || defined(TURBULENCE)
  double IsoSoundSpeed;
#endif
};

struct global_data_all_processes : public parameters
{
  char OutputDir[MAXLEN_PATH];
  double CourantFac;
  double Tend;

  int MinimumSlabsPerCPURank;

  int DesiredDumps;
  int DesiredRestarts;
  int PixelsFieldMaps;
#ifdef DATACUBE_OUTPUT
  int DesiredCubeDumps;
  int PixelsFieldCubes;
#endif  // DATACUBE_OUTPUT

  int WriteRestartAfter;
  int WriteCheckpointAfter;

  int Restart_bool;

#ifdef GRAVITY_WAVES
  double WaveAmplitude;
#endif  // GRAVITY_WAVES

  size_t GpuMemoryAllocation;
  memory_allocation memory_allocations[100];
  int step;

  visc_params ViscParam;

#if defined(TURBULENCE)
  int DesiredPowerspectra;
  double PowerspectraDelay;
  double StDecay;
  double StEnergy;
  double StDtFreq;
  double StKmin;
  double StKmax;
  double StSolWeight;
  double StAmplFac;
  int StSeed;
  int StSpectForm;
#ifdef DECAYING_TURBULENCE
  double StopDrivingAfterTime;
#endif  // DECAYING_TURBULENCE

#endif

#ifdef COOLING
  char CoolingDataPath[1000];
  double CoolingTimeDelay;
#endif

  void register_parameters(void);
  void some_parameter_checks(void);
};

extern global_data_all_processes All;

extern int World_NTask, World_ThisTask;
extern int Island_NTask, Island_ThisTask;
extern int NtotGPUs;
extern int LocalDeviceRank;
extern int32_t Nslabs;
extern int GlobalDeviceRank;
extern int FirstSlabThisTask;
extern int *NslabsPerTask;
extern int *FirstSlabOfTask;
extern int *DeviceIDList;
extern MPI_Comm SharedMemComm;
extern MPI_Comm MyComm;
extern int NTask, ThisTask;
extern int Restart_bool;
extern size_t GpuMemoryAllocation;
extern memory_allocation memory_allocations[100];

extern uint64_t Nc;

#endif  // SRC_DATA_ALLVARS_H_
