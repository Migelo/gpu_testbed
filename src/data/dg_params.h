// Copyright 2020 Miha Cernetic

#ifndef SRC_DATA_DG_PARAMS_H_
#define SRC_DATA_DG_PARAMS_H_

#include "runrun.h"

#ifndef ISOTHERM_EQS

#ifndef GAMMA
#define GAMMA (5.0 / 3)  // ideal gas
#endif

constexpr double GAMMA_MINUS1 = (GAMMA - 1.);

#else

#undef GAMMA

#endif  // ISOTHERM_EQS

#ifdef DEBUG
constexpr bool DEBUG_VAR = true;
#else
constexpr bool DEBUG_VAR = false;
#endif  // DEBUG

constexpr int DG_ORDER = DEGREE_K + 1;

// NB - dimensions of polynomial spaces / number of basis functions
// DG_NUM_QPOINTS_1D - internal quadpoints per dimension
// DG_NUM_INNER_QPOINTS - total internal quadpoints per cell (from Gauss-Legendre quadrature)
// DG_NUM_OUTER_QPOINTS - total quadpoints per single cell interface

#ifdef ISOTHERM_EQS
#define NF_EGY_PRESENT 0
#else
#define NF_EGY_PRESENT 1
#endif  // ISOTHERM_EQS

#ifdef ENABLE_DYE
#define NF_DYE_PRESENT 1
#else
#define NF_DYE_PRESENT 0
#endif  // ENABLE_DYE

#ifdef ART_VISCOSITY
#define ALPHA_FIELD_PRESENT 1
#else
#define ALPHA_FIELD_PRESENT 0
#endif  // ART_VISCOSITY

constexpr int NF = (1 + ND + NF_EGY_PRESENT + NF_DYE_PRESENT + ALPHA_FIELD_PRESENT);

constexpr int NF_VISC = (1 + ND + NF_EGY_PRESENT + NF_DYE_PRESENT);

#ifdef ENABLE_DYE
constexpr int DYE_FIELD = (1 + ND + NF_EGY_PRESENT);
#endif  // ENABLE_DYE

#ifdef ART_VISCOSITY
constexpr int ALPHA_FIELD = (1 + ND + NF_EGY_PRESENT + NF_DYE_PRESENT);
#endif  // ART_VISCOSITY

constexpr int iDG_NUM_CELLS_1D = (DG_NUM_CELLS_1D);

constexpr int DG_NUM_QPOINTS_1D = DEGREE_K + 1;

#if(ND == 1)
#define ONEDIMS
constexpr int NB                     = (DEGREE_K + 1);
constexpr int NB_REDUCED             = (DEGREE_K);
constexpr int NB_INCREASED           = (DEGREE_K + 2);
constexpr int DG_NUM_INNER_QPOINTS   = (DG_NUM_QPOINTS_1D);
constexpr int DG_NUM_OUTER_QPOINTS   = 1;
constexpr int DG_NUM_SURFACE_CELLS   = 1;
constexpr int64_t NC                 = (DG_NUM_CELLS_1D);
constexpr int DG_NUM_POSITIVE_POINTS = (DG_NUM_INNER_QPOINTS + 2 * ND * DG_NUM_OUTER_QPOINTS);
#endif  // (ND == 1)

#if(ND == 2)
#define TWODIMS
constexpr int NB                     = ((DEGREE_K + 1) * (DEGREE_K + 2) / 2);
constexpr int NB_REDUCED             = ((DEGREE_K) * (DEGREE_K + 1) / 2);
constexpr int NB_INCREASED           = ((DEGREE_K + 2) * (DEGREE_K + 3) / 2);
constexpr int DG_NUM_INNER_QPOINTS   = ((DG_NUM_QPOINTS_1D) * (DG_NUM_QPOINTS_1D));
constexpr int DG_NUM_OUTER_QPOINTS   = (DG_NUM_QPOINTS_1D);
constexpr int DG_NUM_SURFACE_CELLS   = (DG_NUM_CELLS_1D);
constexpr int64_t NC                 = (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D);
constexpr int DG_NUM_POSITIVE_POINTS = (DG_NUM_INNER_QPOINTS + 2 * ND * DG_NUM_OUTER_QPOINTS);
#endif  // (ND == 2)

#if(ND == 3)
#define THREEDIMS
constexpr int NB                     = ((DEGREE_K + 1) * (DEGREE_K + 2) * (DEGREE_K + 3) / 6);
constexpr int NB_REDUCED             = ((DEGREE_K) * (DEGREE_K + 1) * (DEGREE_K + 2) / 6);
constexpr int NB_INCREASED           = ((DEGREE_K + 2) * (DEGREE_K + 3) * (DEGREE_K + 4) / 6);
constexpr int DG_NUM_INNER_QPOINTS   = ((DG_NUM_QPOINTS_1D) * (DG_NUM_QPOINTS_1D) * (DG_NUM_QPOINTS_1D));
constexpr int DG_NUM_OUTER_QPOINTS   = ((DG_NUM_QPOINTS_1D) * (DG_NUM_QPOINTS_1D));
constexpr int DG_NUM_SURFACE_CELLS   = (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D);
constexpr int64_t NC                 = (DG_NUM_CELLS_1D * DG_NUM_CELLS_1D * DG_NUM_CELLS_1D);
constexpr int DG_NUM_POSITIVE_POINTS = (DG_NUM_INNER_QPOINTS + 2 * ND * DG_NUM_OUTER_QPOINTS);
#endif  // (ND == 3)

#if(ND == 1)
constexpr int NUM_CELLS_X = DG_NUM_CELLS_1D;
constexpr int NUM_CELLS_Y = 1;
constexpr int NUM_CELLS_Z = 1;
#elif(ND == 2)
constexpr int NUM_CELLS_X = DG_NUM_CELLS_1D;
constexpr int NUM_CELLS_Y = DG_NUM_CELLS_1D;
constexpr int NUM_CELLS_Z = 1;
#elif(ND == 3)
constexpr int NUM_CELLS_X = DG_NUM_CELLS_1D;
constexpr int NUM_CELLS_Y = DG_NUM_CELLS_1D;
constexpr int NUM_CELLS_Z = DG_NUM_CELLS_1D;
#endif

constexpr int cell_stride_x = NUM_CELLS_Y * NUM_CELLS_Z;
constexpr int cell_stride_y = NUM_CELLS_Z;
constexpr int cell_stride_z = 1;

// time integration definitions
constexpr int get_rk_stages()
{
#ifdef FINITE_VOLUME
  return 2;
#else
  if(DG_ORDER < 4)
    return DG_ORDER;
  return 5;
#endif
}
// select it automatically consistent with the spatial order
constexpr int RK_STAGES = get_rk_stages();

// do some basic consistency checks on options
#if defined(ART_VISCOSITY) && !defined(VISCOSITY)
#error "the option ART_VISCOSITY requires VISCOSITY as master switch"
#endif

#if defined(USE_PRIMITIVE_PROJECTION) && defined(FINITE_VOLUME)
#error "the option FINITE_VOLUME and USE_PRIMITIVE_PROJECTION cannot be used together"
#endif

#if defined(FINITE_VOLUME) && (DEGREE_K != 0)
#error "the option FINITE_VOLUME requires DEGREE_K=0"
#endif

#if defined(ADVECT_ALPHA) && !defined(ART_VISCOSITY)
#error "the option ADVECT_ALPHA requires ART_VISCOSITY as master switch"
#endif

#if defined(NAVIER_STOKES) && !defined(VISCOSITY)
#error "the option NAVIER_STOKES requires VISCOSITY as master switch"
#endif

#if defined(MAPS_AT_CORNERS) && defined(MAPS_AT_FAR_CORNERS)
#error "the options MAPS_AT_CORNERS and MAPS_AT_FAR_CORNERS are mutually exclusive"
#endif

#if defined(GRAVITY_WAVES) && !defined(THREEDIMS)
#error "the option GRAVITY_WAVES is only suitable for 3D"
#endif

#if defined(KELVIN_HELMHOLTZ_TEST)
static_assert(ND == 2);
#endif

#if defined(DECAYING_TURBULENCE) && !defined(TURBULENCE)
#error "the option DECAYING_TURBULENCE requires TURBULENCE as master switch"
#endif

#ifdef ENABLE_POSITIVITY_LIMITING
constexpr bool ENABLE_POSITIVITY_LIMITING_VAR = true;
#else
constexpr bool ENABLE_POSITIVITY_LIMITING_VAR = false;
#endif

#ifdef DENSITY_FLOOR
constexpr bool DENSITY_FLOOR_VAR = true;
#else
constexpr bool DENSITY_FLOOR_VAR = false;
#endif

#ifdef PRESSURE_FLOOR
constexpr bool PRESSURE_FLOOR_VAR = true;
#else
constexpr bool PRESSURE_FLOOR_VAR = false;
#endif

constexpr int MAX_SHARED_MEMORY = 98304;  // 49152;  // 166912;

#if defined(DO_NOT_SAVE_W_PRIM) && !defined(USE_PRIMITIVE_PROJECTION)
#error "the option DO_NOT_SAVE_W_PRIM requires USE_PRIMITIVE_PROJECTION as master switch"
#endif

#ifdef GPU
constexpr int determine_internal_kernel_block_size()
{
  constexpr auto memory_usage_per_qpoint = (NB * NF * 8);

  for(unsigned long counter = 1; counter <= DG_NUM_INNER_QPOINTS; counter++)
    {
      auto current_num_of_qpoints = (DG_NUM_INNER_QPOINTS + counter - 1) / counter;
      int memory_usage            = memory_usage_per_qpoint * current_num_of_qpoints;
      if(memory_usage <= MAX_SHARED_MEMORY)
        return current_num_of_qpoints;
    }
  return 1;
}
constexpr int determine_external_kernel_block_size()
{
  constexpr auto memory_usage_per_qpoint = (NB * NF * 8) * 2;

  for(unsigned long counter = 1; counter <= DG_NUM_OUTER_QPOINTS; counter++)
    {
      auto current_num_of_qpoints = (DG_NUM_OUTER_QPOINTS + counter - 1) / counter;
      int memory_usage            = memory_usage_per_qpoint * current_num_of_qpoints;
      if(memory_usage <= MAX_SHARED_MEMORY)
        return current_num_of_qpoints;
    }
  return 1;
}
constexpr auto INTERNAL_KERNEL_BLOCK_SIZE = determine_internal_kernel_block_size();
constexpr auto EXTERNAL_KERNEL_BLOCK_SIZE = determine_external_kernel_block_size();
// constexpr auto EXTERNAL_KERNEL_BLOCK_SIZE = 1;
#endif

#endif  // SRC_DATA_DG_PARAMS_H_
