// Copyright Miha Cernetic 2020
#ifndef SRC_DATA_DATA_TYPES_H_
#define SRC_DATA_DATA_TYPES_H_

#include "../data/dg_params.h"

typedef int integertime;

/*!< left or right state of a face */
struct state
{
  union
  {
    double s[NF];
    struct
    {
      double rho;
      double velx;
#if(ND >= 2)
      double vely;
#endif
#if(ND == 3)
      double velz;
#endif
#if(NF_EGY_PRESENT == 1)
      double press;
#endif
#if(NF_DYE_PRESENT == 1)
      double dye;
#endif
#if(ALPHA_FIELD_PRESENT == 1)
      double alpha;
#endif
    };
  };
};

/*!< state on a face determined by riemann solver */
struct fields_face
{
  double fields[NF];
#ifdef VISCOSITY
  double fields_surface[NF];
  double grad_fields_surface[NF * ND];
#ifdef ART_VISCOSITY
  double smth;
#endif
#endif
#ifdef FINITE_VOLUME
  char troubled;
#endif
};

struct slopes
{
  double rho;
  double velx;
#if(ND >= 2)
  double vely;
#endif
#if(ND >= 2)
  double velz;
#endif
#ifndef ISOTHERM_EQS
  double press;
#endif
#ifdef ENABLE_DYE
  double dye;
#endif
};

/*!< flux through a face */
struct fluxes
{
  double field_flx[NF];
};

struct xyz
{
  int x, y, z;
};

struct coords
{
  double x, y, z;
};

struct xyz_t
{
  int x;
  int y;
  int z;
};

#endif /* SRC_DATA_DATA_TYPES_H_ */
