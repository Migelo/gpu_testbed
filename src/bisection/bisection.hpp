#pragma once

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>

double brents_fun(std::function<double(double)> f, double lower, double upper, double tol, unsigned int max_iter);
