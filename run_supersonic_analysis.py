import argparse
import subprocess
from functools import partial
from itertools import product

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Do not submit the slurm job",
)
args = vars(parser.parse_args())

assert args["cluster"] in ("raven", "freya", "rusty")


def format_job_script(
    template: str,
    cluster: str,
    nnodes: int,
    job_name: str,
):
    if cluster == "rusty":
        return template.format(job_name)
    else:
        return template.format(job_name, nnodes)


def calculate_eddy_turnover_time(box_size: float, mach_number: float) -> float:
    """
    Calculates the eddy turnover time for a turbulent box.

    Parameters:
        box_size (float): The size of the simulation box.
        mach_number (float): The Mach number of the flow.

    Returns:
        float: The eddy turnover time.
    """
    assert box_size > 0, "box_size must be positive"
    assert mach_number > 0, "mach_number must be positive"

    return box_size / (2 * mach_number)


def calculate_simulation_runtime(box_size: float, mach_number: float) -> float:
    """
    Calculates the total simulation runtime.

    Parameters:
        box_size (float): The size of the simulation box.
        mach_number (float): The Mach number of the flow.

    Returns:
        float: The total simulation runtime.
    """

    return 8 * calculate_eddy_turnover_time(box_size, mach_number)


def calculate_powerspectra_delay_time(box_size: float, mach_number: float) -> float:
    """
    Calculates the time at which we start calculating powerspectra.

    Parameters:
        box_size (float): The size of the simulation box.
        mach_number (float): The Mach number of the flow.

    Returns:
        float: The total simulation runtime.
    """

    return 3 * calculate_eddy_turnover_time(box_size, mach_number)


modules = {
    "raven": """
module load anaconda/3/2021.11
module load gcc/11
module load openmpi/4
module load fftw-serial
module load hdf5-serial
module load gsl
module load git
module load hwloc
module load cuda/11.6
""",
    "freya": "",
    "rusty": """
module purge
module load git ffmpeg cmake
module load gcc/11
module load openmpi/4
module load gsl/2.7.1
module load fftw/3.3.10
module load hdf5/1.8.22
module load hwloc/2.9.0
module load cuda
""",
}

gpu_config = {
    "freya": """
#SBATCH --ntasks-per-node=4
#SBATCH --nodes={}
#SBATCH --partition=p.gpu.ampere
#SBATCH --gres=gpu:a100:4
##SBATCH --exclusive
""",
    "raven": """
#SBATCH --ntasks-per-node=4
#SBATCH --nodes={}
#SBATCH --partition=gpu
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive
""",
    "rusty": """
#SBATCH -p gpupreempt
#SBATCH -q gpupreempt
#SBATCH --ntasks-per-node=4
#SBATCH --gpus-per-node=4
#SBATCH -C a100
#SBATCH --mem-per-gpu=80G
#SBATCH -n 16""",
}

run_command = {
    "freya": "mpiexec -np $SLURM_NPROCS ./run.run param.txt | tee log.txt",
    "raven": "mpiexec -np $SLURM_NPROCS ./run.run param.txt | tee log.txt",
    "rusty": "mpirun --map-by socket:pe=$OMP_NUM_THREADS -np $SLURM_NPROCS"
    " ./run.run param.txt | tee log.txt",
}

JOB_HEADER_TEMPLATE = (
    """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --job-name {}
"""
    + gpu_config[args["cluster"]]
    + """
echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

"""
    + modules[args["cluster"]]
)

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
"""
    + run_command[args["cluster"]]
    + """
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    mamba activate ~/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plot_vel_power_spec.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)

JOB_RESTART_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
touch "$FILE"
(
    #mamba activate ~/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plots.py failed in some way"
) &

mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1 | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    #mamba activate ~/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plots.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)

CONFIG_OPTIONS = {
    "dg": """
RICHTMYER_VISCOSITY
USE_PRIMITIVE_PROJECTION

""",
    "fv": """
FINITE_VOLUME
""",
}

CONFIG_TEMPLATE = """
GPU

GAMMA=1.0001
TURBULENCE


ND=3                                          # sets number of dimensions
DEGREE_K={}                                  # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension

DENSITY_FLOOR
{}
"""

PARAM_OPTIONS = {"fv": "", "dg": "ArtViscRichtmyer 2.0\nArtViscLandshoff 0.2"}

PARAM_TEMPLATE = r"""
Tend       {:1.5f}
CourantFac 0.3

DesiredDumps     128
PixelsFieldMaps  1024


OutputDir  ./data

DesiredPowerspectra 128
PowerspectraDelay {:1.5f}

IsoSoundSpeed 1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2

StDtFreq    {:1.9f}   \% update frequency damping = exp(-All.StDtFreq / All.StDecay)
StDecay     {:1.9f}   \% coherence timescale ts
StEnergy    {:1.9e}   \% sqrt(All.StEnergy / All.StDecay)

StSolWeight  1.0      \% set to 1 for purely solenoidal driving
StAmplFac    1.0
StSeed       42

MinimumSlabsPerCPURank  0

WriteRestartAfter       84600
WriteCheckpointAfter    1800

DensityFloor             0.001

"""

run_shell = partial(subprocess.call, shell=True)

degree_ks = [
    0,
    1,
    2,
]
ncs = [
    # 32,
    64,
    128,
    # 256,
]

mach_numbers = [0.1, 0.2, 0.4, 0.8, 1.6, 3.2, 6.4, 12.8]


mach_number_base = 0.1
StDtFreqBase = 0.1
StDecayBase = 0.5
StEnergyBase = 7.81250e-06

# stdecays = (2.0, 1.0, 0.5, 0.25, 0.125, 0.0625)
# stenergies = (0.25, 0.5, 1.0, 2.0, 4.0)
# stamplfactors = [0.125, 0.25, 1.0, 1.25, 1.5, 1.75, 2.00]
# stamplfactors = [
#     0.04,
# ]
# visc_decays = [0.5, 0.75, 1.0]
# visc_maxs = [0.5, 1.0]


# create a product of lists: degree_ks, ncs, stamplfactors, visc_decays, visc_maxs
products = list(product(degree_ks, ncs, mach_numbers))

# print how many jobs will be submitted
print(f"Submitting {len(list(products))} jobs.")
# print all jobs

OUTPUT_FOLDER_TEMPLATE = (
    "/freya/ptmp/mpa/mihac/gpu_testbed2/output/turbulence_supersonic_ns/"
    "order_analysis/n{:d}_k{:d}_mach{:1.1f}/"
)


def are_we_running_fv(degree_k: int) -> str:
    if degree_k == 0:
        return "fv"
    else:
        return "dg"


if __name__ == "__main__":
    for degree, nc, mach_number in products:
        simulation_runtime = calculate_simulation_runtime(1.0, mach_number)
        powerspectra_delay_time = calculate_powerspectra_delay_time(1.0, mach_number)
        assert simulation_runtime > powerspectra_delay_time
        StDtFreq = StDtFreqBase * (mach_number_base / mach_number)
        StDecay = StDecayBase * (mach_number_base / mach_number)
        StEnergy = StEnergyBase * (mach_number / mach_number_base) ** 3

        nnodes = 1
        problem = dg_python.Problem(nc, degree, 20.48, 4 * nnodes)
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, degree, mach_number)

        simulation_type_switch = are_we_running_fv(degree)
        config = CONFIG_TEMPLATE.format(
            degree, nc, CONFIG_OPTIONS[simulation_type_switch]
        )
        param = (
            PARAM_TEMPLATE.format(
                simulation_runtime,
                powerspectra_delay_time,
                StDtFreq,
                StDecay,
                StEnergy,
            )
            + PARAM_OPTIONS[simulation_type_switch]
        )
        job_name = "k{:d}m{:1.1f}{}".format(degree, mach_number, simulation_type_switch)

        job_script = format_job_script(JOB_TEMPLATE, args["cluster"], nnodes, job_name)
        job_restart_script = format_job_script(
            JOB_RESTART_TEMPLATE, args["cluster"], nnodes, job_name
        )

        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            output_folder,
            args["dry_run"],
            "24:00:00",
            config_template=config,
            param_template=param,
            job_template=job_script,
            job_restart_template=job_restart_script,
        )
