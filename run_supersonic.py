import argparse
import subprocess
from functools import partial
from itertools import product

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Do not submit the slurm job",
)
args = vars(parser.parse_args())

assert args["cluster"] in ("raven", "freya")


modules = {
    "raven": """
module load anaconda/3/2021.11
module load gcc/11
module load openmpi/4
module load fftw-serial
module load hdf5-serial
module load gsl
module load git
module load hwloc
module load cuda/11.4
""",
    "freya": "",
}

partition = {"raven": "gpu", "freya": "p.gpu.ampere"}


JOB_HEADER_TEMPLATE = (
    """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --nodes={}
#SBATCH --job-name d{}e{}a{}c{}k{}
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive
#SBATCH --partition="""
    + partition[args["cluster"]]
    + """
echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

"""
    + modules[args["cluster"]]
)

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    #mamba activate /freya/u/mihac/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plot_vel_power_spec.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)

JOB_RESTART_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
touch "$FILE"
(
    #mamba activate /freya/u/mihac/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plots.py failed in some way"
) &

mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1 | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    #mamba activate /freya/u/mihac/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plots.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)


CONFIG_TEMPLATE = """
GPU

VISCOSITY
ART_VISCOSITY
NAVIER_STOKES
TURBULENCE
GAMMA=1.0001
DATACUBE_OUTPUT

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K={}                                   # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension
"""


PARAM_TEMPLATE = r"""
Tend       1.28
CourantFac 0.2

DesiredDumps     128
PixelsFieldMaps  1024

DesiredCubeDumps 10
PixelsFieldCubes {:d}

ShearViscosity     2e-4
ThermalDiffusivity 0

OutputDir  ./data

DesiredPowerspectra 4
PowerspectraDelay 0.5

IsoSoundSpeed 1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2

StDtFreq    0.005         \% update frequency damping = exp(-All.StDtFreq / All.StDecay)
StDecay     {:1.4f}       \% coherence timescale ts
StEnergy    {:1.2f}       \% sqrt(All.StEnergy / All.StDecay)

StSolWeight  1.0          \% set to 1 for purely solenoidal driving
StAmplFac    {:1.1f}
StSeed       42

MinimumSlabsPerCPURank  0

WriteRestartAfter       84600
WriteCheckpointAfter    180

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01

"""

run_shell = partial(subprocess.call, shell=True)

degree_ks = [
    1,
    2,
    3,
]
ncs = [
    32,
    64,
    128,
    256,
]


OUTPUT_FOLDER_TEMPLATE = (
    "/freya/ptmp/mpa/mihac/gpu_testbed2/output/"
    "turbulence_supersonic_ns/new_limiter_with_art_viscosity/"
    "NC{:03d}_k{:d}_dec{:1.5f}_ene{:1.5f}_amp{:2.2f}/"
)

stdecays = (2.0, 1.0, 0.5, 0.25, 0.125, 0.0625)
stenergies = (0.25, 0.5, 1.0, 2.0, 4.0)
stamplfactors = (2.00,)


products = list(product(ncs, degree_ks, stdecays, stenergies, stamplfactors))

if __name__ == "__main__":
    for nc, degree, decay, energy, amplfac in products:
        nnodes = 4
        problem = dg_python.Problem(nc, degree, 20.48, 4 * nnodes)
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(
            nc, degree, decay, energy, amplfac
        )

        config = CONFIG_TEMPLATE.format(degree, nc)
        param = PARAM_TEMPLATE.format((degree + 1) * nc, decay, energy, amplfac)

        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            output_folder,
            args["dry_run"],
            "24:00:00",
            config_template=config,
            param_template=param,
            job_template=JOB_TEMPLATE.format(
                nnodes, decay, energy, amplfac, nc, degree
            ),
            job_restart_template=JOB_RESTART_TEMPLATE.format(
                nnodes, decay, energy, amplfac, nc, degree
            ),
        )
