
#GPU

SOUNDWAVE_TEST

ND=2                                          # sets number of dimensions
DEGREE_K=1                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D=32                            # number of cells per dimension
