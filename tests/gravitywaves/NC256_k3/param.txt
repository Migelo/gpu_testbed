Tend       4 # 1 Gyr
CourantFac 0.5

DesiredDumps     64
PixelsFieldMaps  1024


OutputDir  ./data
ShearViscosity     1e-5
ThermalDiffusivity 1e-5
#DyeDiffusivity     1e-5

MinimumSlabsPerCPURank  0

WriteRestartAfter       85100
WriteCheckpointAfter    7200

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01
