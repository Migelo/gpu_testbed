GPU

VISCOSITY
ART_VISCOSITY
NAVIER_STOKES

GRAVITY_WAVES

DATACUBE_OUTPUT

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K=3                                   # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D=256                            # number of cells per dimension
