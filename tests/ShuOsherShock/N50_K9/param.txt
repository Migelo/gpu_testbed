
Tend               1.8

CourantFac         0.3

DesiredDumps       50
PixelsFieldMaps    4000

OutputDir          ./output


MinimumSlabsPerCPURank  0

WriteRestartAfter    360000
WriteCheckpointAfter 720000

ArtViscRichtmyer       1.5
ArtViscLandshoff       0.15
