
#GPU

GAMMA=1.4
SHU_OSHER_SHOCKTUBE

OUTFLOW

ND=1                                          # sets number of dimensions
DEGREE_K=5                                    # order of the DG scheme
DG_NUM_CELLS_1D=200                           # number of cells per dimension


RICHTMYER_VISCOSITY
USE_PRIMITIVE_PROJECTION


#OUTPUT_EVERY_STEP

