#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=vspringel@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --nodes=1
#SBATCH --partition=p.gpu
#SBATCH --ntasks-per-node=40
#SBATCH --job-name YV16

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

cd Serial_K0/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K1/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K2/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K3/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K4/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K5/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K6/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

cd Serial_K7/Res16
mpiexec -np 16 ./run.run  param.txt
cd ../../

