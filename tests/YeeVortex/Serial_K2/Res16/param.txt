Tend       10.0
CourantFac 0.5

DesiredDumps     2
PixelsFieldMaps  512

OutputDir  ./output

MinimumSlabsPerCPURank  0

WriteRestartAfter       864000
WriteCheckpointAfter    86400

