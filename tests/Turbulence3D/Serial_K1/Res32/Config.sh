
TURBULENCE

ND=3                                          # sets number of dimensions
DEGREE_K=1                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D=32                            # number of cells per dimension

VISCOSITY
ART_VISCOSITY
VISC_SIMPLE
ENABLE_POSITIVITY_LIMITING
