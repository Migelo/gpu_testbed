
Tend       0.08
CourantFac 0.5

DesiredDumps     32
PixelsFieldMaps  960


OutputDir  ./output_1.0


MinimumSlabsPerCPURank  0

WriteRestartAfter       20000
WriteCheckpointAfter    7200

ArtViscParameter    0.5
ArtViscOnset        0.1

