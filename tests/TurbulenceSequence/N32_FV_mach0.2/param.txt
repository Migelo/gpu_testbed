
Tend               50.0


CourantFac         0.3

DesiredDumps       32
PixelsFieldMaps    320


OutputDir  ./output

DesiredPowerspectra 16
PowerspectraDelay   0

IsoSoundSpeed       1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2
StSeed       42
StSolWeight  1.0              % set to 1 for purely solenoidal driving
StAmplFac    1.0


StDtFreq     0.05         % update frequency
StDecay      2.5          % coherence timescale ts
StEnergy     6.25000e-05     


MinimumSlabsPerCPURank  0

WriteRestartAfter    360000
WriteCheckpointAfter 720000




DensityFloor             0.001
