
#GPU

GAMMA=1.0001
TURBULENCE


ND=3                                          # sets number of dimensions
DEGREE_K=0                                    # order of the DG scheme
DG_NUM_CELLS_1D=32                            # number of cells per dimension

FINITE_VOLUME
DENSITY_FLOOR
