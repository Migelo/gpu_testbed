#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=vspringel@mpa-garching.mpg.de
#SBATCH --time=24:00:00
##SBATCH --partition=p.test
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --job-name K1

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


mpiexec -np $SLURM_NTASKS ./run.run  param.txt 

