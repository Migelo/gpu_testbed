
Tend               12.5


CourantFac         0.3

DesiredDumps       32
PixelsFieldMaps    320


OutputDir  ./output

DesiredPowerspectra 16
PowerspectraDelay   0

IsoSoundSpeed       1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2
StSeed       42
StSolWeight  1.0              % set to 1 for purely solenoidal driving
StAmplFac    1.0


StDtFreq     0.0125         % update frequency
StDecay      0.625          % coherence timescale ts
StEnergy     0.004     


MinimumSlabsPerCPURank  0

WriteRestartAfter    360000
WriteCheckpointAfter 720000



ArtViscRichtmyer         2.0
ArtViscLandshoff         0.2

DensityFloor             0.001
