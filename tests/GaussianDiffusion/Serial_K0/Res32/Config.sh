
GAUSSIAN_DIFFUSION_TEST
ISOTHERM_EQS



DEGREE_K=0                                    # order of the DG scheme
ND=2                                          # sets number of dimensions
DG_NUM_CELLS_1D=32                            # number of cells per dimension

VISCOSITY
NAVIER_STOKES
ENABLE_DYE
