
Tend       3.0
CourantFac 0.4

DesiredDumps     16
PixelsFieldMaps  512

OutputDir  ./output

MinimumSlabsPerCPURank  0

WriteRestartAfter       864000
WriteCheckpointAfter    86400


ShearViscosity          0
DyeDiffusivity          0.00781250
ThermalDiffusivity      0.0
IsoSoundSpeed    1.0
