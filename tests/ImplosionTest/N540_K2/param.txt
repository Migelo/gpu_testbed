
Tend               2.5

CourantFac         0.3

DesiredDumps       1800
PixelsFieldMaps    1080


OutputDir          ./output


MinimumSlabsPerCPURank  0

WriteRestartAfter    360000
WriteCheckpointAfter 720000

ArtViscRichtmyer         2.0
ArtViscLandshoff         0.0
