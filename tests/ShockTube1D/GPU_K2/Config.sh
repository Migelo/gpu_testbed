SHOCK_TUBE

GPU

VISCOSITY
ART_VISCOSITY


DEGREE_K=2                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
ND=1                                          # sets number of dimensions
DG_NUM_CELLS_1D=160                           # number of cells per dimension
