
Tend       0.10
CourantFac 0.5

DesiredDumps     100
PixelsFieldMaps  1920


OutputDir  ./output_1.0


MinimumSlabsPerCPURank  0

WriteRestartAfter       20000
WriteCheckpointAfter    7200


ArtViscParameter    1.0
ArtViscOnset        0.1
