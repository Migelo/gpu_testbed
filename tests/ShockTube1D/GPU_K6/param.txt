
Tend       0.1
CourantFac 0.3

DesiredDumps     32
PixelsFieldMaps  1920

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01

OutputDir  ./data


MinimumSlabsPerCPURank  0

WriteRestartAfter       86500
WriteCheckpointAfter    86400
