
Tend       5.12
CourantFac 0.2

DesiredDumps     100
PixelsFieldMaps  768

ShearViscosity     2e-4
ThermalDiffusivity 0

OutputDir  ./data

DesiredPowerspectra 64
PowerspectraDelay 0.5

IsoSoundSpeed 1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2

StDtFreq    0.005         \% update frequency damping = exp(-All.StDtFreq / All.StDecay)
StDecay     0.2       \% coherence timescale ts
StEnergy    4.0       \% sqrt(All.StEnergy / All.StDecay)

StSolWeight  1.0          \% set to 1 for purely solenoidal driving
StAmplFac    2.0
StSeed       42

MinimumSlabsPerCPURank  0

WriteRestartAfter       86000
WriteCheckpointAfter    600

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01
