GPU

VISCOSITY
ART_VISCOSITY
NAVIER_STOKES
TURBULENCE
GAMMA=1.0001
DATACUBE_OUTPUT

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K=3                                   # order of the DG scheme
DG_NUM_CELLS_1D=128                            # number of cells per dimension
