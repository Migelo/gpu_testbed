
ArtViscParameter  1.0
ArtViscOnset      0.1

Tend              0.1

CourantFac        0.3


DesiredDumps     10
PixelsFieldMaps  160

OutputDir        ./output

MinimumSlabsPerCPURank  0

WriteRestartAfter       86500
WriteCheckpointAfter    86400
