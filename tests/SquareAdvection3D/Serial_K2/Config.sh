VISCOSITY
ART_VISCOSITY 


SQUARE_ADVECTION

GAMMA=7./5

DEGREE_K=2                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
ND=3                                          # sets number of dimensions
DG_NUM_CELLS_1D=8                            # number of cells per dimension
