
Tend       0.16
CourantFac 0.5

DesiredDumps     32
PixelsFieldMaps  960


OutputDir  ./output


MinimumSlabsPerCPURank  0

WriteRestartAfter       864000
WriteCheckpointAfter    86400

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01
