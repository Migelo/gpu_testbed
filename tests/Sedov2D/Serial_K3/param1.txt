
Tend       0.16
CourantFac 0.5

DesiredDumps     64
PixelsFieldMaps  960


OutputDir  ./output_1.0


MinimumSlabsPerCPURank  0

WriteRestartAfter       20000
WriteCheckpointAfter    7200

ArtViscParameter    0.1
ArtViscOnset        0.1

