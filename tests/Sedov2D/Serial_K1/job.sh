#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=vspringel@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --nodes=1
##SBATCH --partition=p.test
#SBATCH --ntasks-per-node=40
#SBATCH --job-name S3D

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


mpiexec -np 16 ./run.run  param0.txt 
mpiexec -np 16 ./run.run  param1.txt 
