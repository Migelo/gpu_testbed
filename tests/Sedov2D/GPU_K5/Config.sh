GPU

POINT_EXPLOSION

VISCOSITY
ART_VISCOSITY


DEGREE_K=5                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
ND=2                                          # sets number of dimensions
DG_NUM_CELLS_1D=16                           # number of cells per dimension
ENABLE_POSITIVITY_LIMITING
