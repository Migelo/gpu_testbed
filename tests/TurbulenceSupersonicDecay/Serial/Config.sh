TURBULENCE
DECAYING_TURBULENCE
COOLING

GAMMA=1.0001

#VISCOSITY
#ART_VISCOSITY

ENABLE_POSITIVITY_LIMITING

DEBUG

DEGREE_K=2                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
ND=3                                          # sets number of dimensions
DG_NUM_CELLS_1D=32                           # number of cells per dimension
