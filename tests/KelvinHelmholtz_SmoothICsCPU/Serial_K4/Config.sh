KELVIN_HELMHOLTZ_TEST
KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS

DEGREE_K=4                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
ND=2                                          # sets number of dimensions
DG_NUM_CELLS_1D=60                           # number of cells per dimension
KELVIN_HELMHOLTZ_TEST_SMOOTH_ICS_NO_DENSITY_JUMP
