
Tend       4.0
CourantFac 0.5

DesiredDumps     32
PixelsFieldMaps  960


OutputDir  ./output


MinimumSlabsPerCPURank  0

WriteRestartAfter       864000
WriteCheckpointAfter    86400
