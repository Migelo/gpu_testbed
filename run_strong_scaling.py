import argparse
import subprocess
from functools import partial

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())


JOB_HEADER_TEMPLATE = """#!/bin/bash -l
##SBATCH --mail-type=BEGIN,END,FAIL
##SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=01:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks={0}
#SBATCH --job-name c{1}k{2}n{0}
#SBATCH --partition=p.gpu.ampere
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


"""

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
"""
)


CONFIG_TEMPLATE = """
GPU

BENCHMARK

VISCOSITY
ART_VISCOSITY
NAVIER_STOKES

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K={}                                   # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension
RK1
"""


PARAM_TEMPLATE = """Tend       20.48
CourantFac 0.5

DesiredDumps     1
PixelsFieldMaps  2

ShearViscosity     0
ThermalDiffusivity 0

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01

OutputDir  ./data

MinimumSlabsPerCPURank  0

WriteRestartAfter       82800
WriteCheckpointAfter    720000

"""

run_shell = partial(subprocess.call, shell=True)


degree_ks = [0, 1, 2, 3, 4, 5, 6, 7]

OUTPUT_FOLDER_TEMPLATE = "output/strongscaling_gpu/NC{:03d}_k{:d}_{:03d}"

if __name__ == "__main__":
    for degree in degree_ks:
        for ngpu in (1, 2, 4, 8, 16, 32):
            nc = 256

            problem = dg_python.Problem(nc, degree, 20.48, ngpu)
            output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, degree, ngpu)

            config = CONFIG_TEMPLATE.format(degree, nc)
            param = PARAM_TEMPLATE

            dg_python.prepare_code_run(
                problem,
                args["cluster"],
                output_folder,
                "02:00:00",
                args["dry_run"],
                config_template=config,
                param_template=param,
                job_template=JOB_TEMPLATE.format(ngpu, nc, degree),
            )

# CPU test
CONFIG_TEMPLATE = CONFIG_TEMPLATE.replace("GPU", "")
OUTPUT_FOLDER_TEMPLATE = OUTPUT_FOLDER_TEMPLATE.replace("_gpu", "_cpu")


JOB_HEADER_TEMPLATE = """#!/bin/bash -l
##SBATCH --mail-type=BEGIN,END,FAIL
##SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=1-
#SBATCH --ntasks-per-node=40
#SBATCH --ntasks={0}
#SBATCH --job-name c{1}k{2}n{0}
#SBATCH --partition=p.24h
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


"""

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
"""
)


degree_ks = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
]


if __name__ == "__main__":
    for degree in degree_ks:
        for ngpu in (1, 2, 4, 8, 16, 32, 64, 128, 256):
            nc = 256

            problem = dg_python.Problem(nc, degree, 20.48, ngpu)
            output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, degree, ngpu)

            config = CONFIG_TEMPLATE.format(degree, nc)
            param = PARAM_TEMPLATE

            dg_python.prepare_code_run(
                problem,
                args["cluster"],
                output_folder,
                "24:00:00",
                args["dry_run"],
                config_template=config,
                param_template=param,
                job_template=JOB_TEMPLATE.format(ngpu, nc, degree),
            )
