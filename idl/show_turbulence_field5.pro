



frun = '../order_analysis/n64_k1_mach1.5/data/'
frun = '../order_analysis/n64_k1_mach3.0/data/'

frun = '../order_analysis/n128_k1_mach3.0/data/'
frun = '../order_analysis/n128_k3_mach3.0/data/'

frun = '../order_analysis/n256_k0_mach3.0/data/'
frun = '../order_analysis/n256_k1_mach3.0/data/'

frun = '../order_analysis/n256_k3_mach3.0/data/'


frun = '../order_analysis/n64_k3_mach9.0/data/'

frun = '../order_analysis/n256_k2_mach3.0/data/'

mi = 0.6
ma = 2.1

BoxSize = 1.0

loadct, 3
tvlct, r, g, b, /get

;openr,1,"rainbow.clt"
;readf,1,r
;readf,1,g
;readf,1,b
;close,1



for num = 0, 100 do begin

   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= frun + "/field_5_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N

  ti = 0.0D
  readu, 1, ti

  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  ind = where(dens lt 0)
  dens(ind) = 0
  
  
  
  mi = min(dens)
  ma = max(dens)

  print, exts, ": time=", ti, "  mi= ", mi, " ma=", ma

  mi = 0
  ma = 1.0
  
   dens= transpose(dens)

   colindex= (dens - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   Pic=bytarr(N, N, 3)

   pic(*,*,0) = r(colindex)
   pic(*,*,1) = g(colindex)
   pic(*,*,2) = b(colindex)



   if num eq 0 then begin
      window, xsize= N, ysize= N
   endif
   
   tv, Pic, true=3,0

   

   wait, 0.1

;   fname ="../tests/KelvinHelmholtzGPU/pics/pic_"+exts+".jpg"
;   write_jpeg, fname, pic, true=3, quality=98
      
endfor

end





 









