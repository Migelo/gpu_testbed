

BaseList = ["../tests/SquareAdvection1D/Serial_K1/" , $
            "../tests/SquareAdvection1D/Serial_K2/", $
            "../tests/SquareAdvection1D/Serial_K3/" , $
       ;     "../tests/SquareAdvection1D/Serial_K6/", $
            "../tests/SquareAdvection1D/Serial_K9/" ]


TagList = [ "!20p!7 = 2!3", "!20p!7 = 3!3", "!20p!7 = 4!3", "!20p!7 = 10!3"]

set_plot,'PS'
!p.font=0

device,/times,/italic,font_index=20
device,xsize=18.0,ysize=18.0

!x.margin=[6,3]
!p.thick=2.5
!p.ticklen=0.03

device,filename= "plots/square_advection_shape.eps", /encapsulated, /color

v1=[235,  0,  0,215,0 ,  255, 100,120, 0, 180]
v2=[  0,155,  0,000,255, 255, 200,123, 12, 180]
v3=[  0,  0,205,215,255, 0,   50,0, 100, 180]

tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

usersym, xc, yc, thick=1.5      ;,/fill


plot, [1], [1], /nodata, charsize = 1.2, xrange=[0.0, 1.0], yrange=[0, 5], xstyle=1, ystyle=1, xthick=2.5, ythick=2.5,$
      xtitle="!20x!3", ytitle = "!9r!3"
      


for rep = 0, n_elements(baselist)-1 do begin

   if rep eq 0 then begin
      num = 0
      
      exts='0000'
      exts=exts+strcompress(string(num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      
      f= BaseList[rep] + "/output/field_0_"  + exts + ".dat"
      
      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0D
      readu, 1, Ti
      dens = dblarr(N)
      readu, 1, dens

      print, num, ti
      close, 1
      
      x = (indgen(N)+0.5)/N 
      
      oplot, x, dens(*), color=10, thick=6.0   ;; analytic 
   endif 
   
   num = 40
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
  
   f= BaseList[rep] + "/output/field_0_"  + exts + ".dat"

   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, Ti
   dens = dblarr(N)
   readu, 1, dens

   print, num, ti
   close, 1

   x = (indgen(N)+0.5)/N 


   oplot, x,  dens(*), color = 4-rep, thick=2.5

   xyouts,0.48, 0.6-0.07*rep, /normal, TagList[rep], charsize=1.5, color = 4-rep

      
endfor

device,/close

set_plot,"X"

ende:
end





 









