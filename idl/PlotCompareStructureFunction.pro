compile_opt idl2
Box = 1.0

Base = "/Users/springel/data/"


frunlist = ['TurbulenceSequence/N1024_FV_mach3.2_GPU/data/'] ;, $
         ;   'TurbulenceSequence/N512_FV_mach3.2_GPU/data/']

;frunlist = ['/ptmp/vrs/TurbulenceSequence/N512_FV_mach3.2_GPU/data/']
;frunlist = ['/ptmp/vrs/TurbulenceSequence/N512_K1_mach3.2_GPU/data/']
;frunlist = ['/ptmp/mihac/turublence_runs/kfinal0/data/']

TagList = ['N1024_FV_mach3.2_GPU', $
           'N512_FV_mach3.2_GPU']

set_plot, 'PS'
!p.font = 0

device, /times, /italic, font_index = 20
device, xsize = 18.0, ysize = 12.0

!x.margin = [10, 2]
!p.thick = 2.5
!p.ticklen = 0.03

device, filename = 'plots/structure_comparison_func.eps', /encapsulated, /color

v1 = [255, 0, 30, 255, 0, 155, 100, 120, 0, 255]
v2 = [100, 255, 100, 000, 255, 155, 200, 123, 12, 99]
v3 = [0, 0, 255, 255, 255, 0, 50, 0, 100, 56]

tvlct, v1, v2, v3, 1

xc = cos(indgen(32) / 31.0 * 2 * !pi) * 0.4
yc = sin(indgen(32) / 31.0 * 2 * !pi) * 0.4

usersym, xc, yc, thick = 1.5 ; ,/fill

plot, [1], [1], /nodata, /xlog, /ylog, xtitle = '!20l!7  [ !20L!7!Dbox!N / 2 ]!3', ytitle = '!7turbulent Mach number!3', charsize = 1.2, $
  xrange = [0.002, Box], xstyle = 1, yrange = [2.5, 5.0], xthick = 2.5, ythick = 2.5, ystyle = 1

for rep = 0, n_elements(frunlist) - 1 do begin
  Nspec = 0
  SfSum = 0.0

;  for Num = 120, 125 do begin
;    for Num = 14, 28 do begin
;      for Num = 40, 128 do begin

  for Num = 32, 128 do begin
;   for Num = 120, 128 do begin
    
    exts = '0000'
    exts = exts + strcompress(string(Num), /remove_all)
    exts = strmid(exts, strlen(exts) - 4, 4)

    fname = Base+ frunlist[rep] + '/velocity_structure_function_' + exts + '.dat'
    print, fname
    openr, 1, fname
    Bins = 0l
    readf, 1, Bins
    Ti = 0.0
    Mach = 0.0
    if rep eq 0 then begin
    readf, 1, Ti
    readf, 1, Mach
    endif
    da = dblarr(2, Bins)
    readf, 1, da
    close, 1

    print,  "ti=", Ti, "   Mach=", Mach
    
    R = da[0, *]
    SF = da[1, *]

    SfSum += SF
    Nspec += 1
  endfor


    
  SfSum /= Nspec

  Mach = sqrt(SfSum / 2)


  RR = R / (0.5)
  
  oplot, RR , Mach / sqrt(RR), color = 1 + rep, thick = 4.0

  xyouts, 0.6, 0.85 - 0.065 * rep, /normal, '!7' + TagList[rep] + '!3', charsize = 1.1, color = 1 + rep

  ind = where((RR ge 0.1) and (RR lt 0.4))

  fit = linfit(alog(RR[ind]), alog(Mach[ind]))

  mfit = fit[1] * alog(RR) + fit[0]

  print, "slope=", fit[1]
  
  oplot, RR, exp(mfit) / sqrt(RR), linestyle=2


  
  ind = where((RR ge 0.04) and (RR lt 0.07))

  fit = linfit(alog(RR[ind]), alog(Mach[ind]))

  mfit = fit[1] * alog(RR) + fit[0]

  print, "slope=", fit[1]
  
  oplot, RR, exp(mfit) / sqrt(RR), linestyle=1
 
  
  ; oplot, knyquist*[1,1],[1.0e-20,1.0e30], linestyle=1
endfor

device, /close
set_plot, 'X'

end
