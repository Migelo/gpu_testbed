
field = 0  & mi = 0.9  & ma = 2.1


for num = 0, 1000 do begin

  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= "output/field_" + strcompress(string(field),/remove_all) + "_" + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  da = dblarr(N, N)
  readu, 1, da
  close, 1

   da = transpose(da)

  if num eq 0 then begin
   window, xsize = N, ysize = N 
  endif
   
  tv,  (da - mi)/(ma - mi) * 255.0
  
  wait, 0.1

endfor

end


