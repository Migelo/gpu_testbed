
fout = "plots/yee_vortex_vs_res.eps"

NList = [16, 32, 64, 128]
KList = [0, 1, 2, 3, 4, 5, 6, 7]







  set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=16.0

    !x.margin=[8,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color

;    v1=[255,  0,  0,255,0 ,  255, 100,120, 0, 255]
;    v2=[  0,255,  0,000,255, 255, 200,123, 12, 99]
;    v3=[  0,  0,255,255,255, 0,   50,0, 100, 56]


     v1=[115, 135, 155, 175, 195, 215, 235, 255, 200]
     v2=[  0, 0,  0, 0, 0, 0, 0, 0, 200 ]
     v3=[  10, 20,  30, 40, 50, 60, 70, 80 , 200]


    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill


    plot, [1],[1], /nodata, xrange=[10.0, 200], xstyle=1, yrange=[2.0e-11, 5.0e-2], ystyle =1, charsize=1.2, /xlog,/ylog, $
           xthick = 2.5, ythick = 2.5, xtitle = "!20N!D!7cells!N!3", ytitle = "!7L1!3" 





for K=0, n_elements(KList)-1 do begin
   
   L1list = dblarr(n_elements(Nlist))

   for rep=0, n_elements(Nlist)-1 do begin


      f = "../tests/YeeVortex/Serial_K" + string(Klist[K]) +"/Res" + string(NList[rep]) + "/output/L1.txt"
      
      f = strcompress(f, /remove_all)
      
      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)

      print, f
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)
      L1 = da(1,*)

   ;   print, L1
      
      L1max = max(L1)
    
      L1list[rep] = L1max

   endfor

   ind = where(L1List ge 5.74e-11)
   Nlist = Nlist(ind)
   L1list = L1list(ind)
   
   print, L1list


   xx =[1,32,1000]
   yy = exp(  -(klist[k]+1)  * (alog(xx) - alog(32)) + alog(L1List[1]))
   oplot, xx, yy, color=9, linestyle=2, thick=5


   
   oplot, Nlist, L1list, psym =4, thick=3, color = K+1
   oplot, Nlist, L1list, thick=3 , color = K+1

   fit = linfit(alog(Nlist), alog(L1list)) 

   xx =[1,1000]
   yy = exp(  fit[1]*alog(xx) + fit[0])
   oplot, xx, yy, color=K+1, linestyle=1


   xyouts, Max(Nlist)/1.15, min(L1list)/1.9, /data, "!20n!7 = "+strcompress(string(Klist[k]),/remove_all) + "!3", color=K
   
   print, -(klist[k]+1), " slope= ", fit[1]


   
endfor


device,/close
set_plot,"X"

ende:

end





 









