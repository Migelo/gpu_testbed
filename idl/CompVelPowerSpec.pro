
Box = 1.0

 


frun1  = "../tests/Turbulence3D/Serial_K5/Res32/output/"

frun1  = "../tests/Turbulence3D/GPU_K1/res32/output/"
frun2  = "../tests/Turbulence3D/Serial_K1/Res32/output/"


frun1  = "../tests/Turbulence3D/Serial_K1/Res32/output/"
frun2  = "../tests/Turbulence3D/Serial_K2/Res32/output/"


frun1  = "../tests/Turbulence3D/GPU_K3/Res32/output/"
frun2  = "../tests/Turbulence3D/GPU_K4/Res32/output/"


frun1  = "../tests/Turbulence3D/GPU_N32_K1/output/"
frun2  = "../tests/Turbulence3D/GPU_N32_K1_rerun/output/"



 
window, xsize=1200, ysize=1200


for Num = 1,63 do begin
    exts='000'
    exts=exts+strcompress(string(Num),/remove_all)
    exts=strmid(exts,strlen(exts)-3,3)
    
    fname = frun1 +"/powerspec_vel_" +exts+".txt"
    print, fname
    openr, 1, fname
    Time = 0.0D 
    Grid = 0L
    Bins = 0L
    readf, 1, Time
    readf, 1, Grid
    readf, 1, Bins
    print, "Time = ", Time, "  Grid=", Grid
    vel_disp = dblarr(3)
    readf, 1, vel_disp
    da= fltarr(4, bins)
    readf, 1, da
    close,1

    print, "vel_disp=",vel_disp

    knyquist = 2*!PI/box * Grid/2

    K = da(0,*)
    ModePow = da(1,*)
    ModeCount = da(2,*)
    SumPower =  da(3,*)


    print, num, total(ModeCount * ModePow, /double), total(vel_disp)


; we will do a band averaging of the finely binned points, 
; subject to two conditions:
; We want enough modes per bin in order to reduce the variance in a bin,
; and simultaneously, for large k, we don't want the bins become too narrow.
;
; The first condition is set by "MinModeCount",
; the second by "TargetBinNummer", which is used to compute a minimum 
; logarithmic bin-size.


    MinModeCount = 1
    TargetBinNummer = 200

    MinDlogK = (alog10(max(K)) - alog10(min(K)))/TargetbinNummer


    istart=0
    ind=[istart]

    k_list     = [0]
    power_list = [0]
    count_list = [0]

    repeat begin
        count = total(ModeCount(ind))
        deltak =  (alog10(max(K(ind))) - alog10(min(K(ind))))

        if (deltak ge mindlogk) and (count ge MinModeCount) then begin
            d2 = total(SumPower(ind))/total(ModeCount(ind))
            kk = total(K(ind)*ModeCount(ind))/total(ModeCount(ind))

            
            k_list = [k_list, kk]
            power_list = [power_list, d2]
            count_list = [count_list, total(ModeCount(ind))]
            istart = istart + 1
            ind = [istart]
        endif else begin
            istart = istart + 1
            ind = [ind, istart]
        endelse
    endrep until istart ge Bins

    k_list     = k_list(1:*)
    power_list = power_list(1:*)
    count_list = count_list(1:*)



    E_list = k_list^2 * power_list ; convert to energy-spectrum, E(k) = k^2 * P(k)

    plot, K_list,  E_list, /xlog, /ylog , xtitle = "k [ h/kpc ]", ytitle = "E(k)", charsize=2.0, xrange=[2 * !PI/Box / 1.5, 1.5 * knyquist], xstyle=1

    oplot, K_list,  E_list, psym=4, color=255

    oplot, K_list, E_list(4) * (k_list/K_list(4))^(-5.0/3), linestyle=2


    oplot, knyquist*[1,1],[1.0e-20,1.0e30], linestyle=1

    oplot, 2.0*!PI/Box *[1,1],[1.0e-20,1.0e30], linestyle=1











    fname = frun2 +"/powerspec_vel_" +exts+".txt"
    print, fname
    openr, 1, fname
    Time = 0.0D 
    Grid = 0L
    Bins = 0L
    readf, 1, Time
    readf, 1, Grid
    readf, 1, Bins
    print, "Time = ", Time, "  Grid=", Grid
    vel_disp = dblarr(3)
    readf, 1, vel_disp
    da= fltarr(4, bins)
    readf, 1, da
    close,1

    print, "vel_disp=",vel_disp

    knyquist = 2*!PI/box * Grid/2

    K = da(0,*)
    ModePow = da(1,*)
    ModeCount = da(2,*)
    SumPower =  da(3,*)


    print, num, total(ModeCount * ModePow, /double), total(vel_disp)


; we will do a band averaging of the finely binned points, 
; subject to two conditions:
; We want enough modes per bin in order to reduce the variance in a bin,
; and simultaneously, for large k, we don't want the bins become too narrow.
;
; The first condition is set by "MinModeCount",
; the second by "TargetBinNummer", which is used to compute a minimum 
; logarithmic bin-size.


    MinModeCount = 1
    TargetBinNummer = 200

    MinDlogK = (alog10(max(K)) - alog10(min(K)))/TargetbinNummer


    istart=0
    ind=[istart]

    k_list     = [0]
    power_list = [0]
    count_list = [0]

    repeat begin
        count = total(ModeCount(ind))
        deltak =  (alog10(max(K(ind))) - alog10(min(K(ind))))

        if (deltak ge mindlogk) and (count ge MinModeCount) then begin
            d2 = total(SumPower(ind))/total(ModeCount(ind))
            kk = total(K(ind)*ModeCount(ind))/total(ModeCount(ind))

            
            k_list = [k_list, kk]
            power_list = [power_list, d2]
            count_list = [count_list, total(ModeCount(ind))]
            istart = istart + 1
            ind = [istart]
        endif else begin
            istart = istart + 1
            ind = [ind, istart]
        endelse
    endrep until istart ge Bins

    k_list     = k_list(1:*)
    power_list = power_list(1:*)
    count_list = count_list(1:*)



    E_list = k_list^2 * power_list ; convert to energy-spectrum, E(k) = k^2 * P(k)

    oplot, K_list,  E_list, color=256*255L
    oplot, K_list,  E_list, psym=4, color=255*256L

    oplot, K_list, E_list(4) * (k_list/K_list(4))^(-5.0/3), linestyle=2, color=255*256L


    oplot, knyquist*[1,1],[1.0e-20,1.0e30], linestyle=1, color=255*256L

    oplot, 2.0*!PI/Box *[1,1],[1.0e-20,1.0e30], linestyle=1, color=255*256L











    wait,0.1

endfor

end
