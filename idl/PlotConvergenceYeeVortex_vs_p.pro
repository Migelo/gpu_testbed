
fout = "plots/yee_vortex_vs_p.eps"

NList = [16, 32, 64, 128]
KList = [0, 1, 2, 3, 4, 5, 6, 7]







  set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=16.0

    !x.margin=[8,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color

;    v1=[255,  0,  0,255,0 ,  255, 100,120, 0, 255]
;    v2=[  0,255,  0,000,255, 255, 200,123, 12, 99]
;    v3=[  0,  0,255,255,255, 0,   50,0, 100, 56]


     v1=[115, 135, 155, 175, 195, 215, 235, 255, 200]
     v2=[  0, 0,  0, 0, 0, 0, 0, 0, 200 ]
     v3=[  10, 20,  30, 40, 50, 60, 70, 80 , 200]


    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill


    plot, [1],[1], /nodata, xrange=[0.5, 8.5], xstyle=1, yrange=[2.0e-11, 5.0e-2], ystyle =1, charsize=1.2, /ylog, $
           xthick = 2.5, ythick = 2.5, xtitle = "!20p!3", ytitle = "!7L1!3" 





    for rep=0, n_elements(Nlist)-1 do begin

L1list = dblarr(n_elements(Klist))

for K=0, n_elements(KList)-1 do begin
   

   f = "../tests/YeeVortex/Serial_K" + string(Klist[K]) +"/Res" + string(NList[rep]) + "/output/L1.txt"
  
   
      f = strcompress(f, /remove_all)
      
      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)
      L1 = da(1,*)

      L1max = max(L1)
      print, L1Max

      L1list[K] = L1max

endfor


   ind = where(L1List ge 5.74e-11)
   Klist = Klist(ind)
   L1list = L1list(ind)


oplot, Klist+1, L1list, psym =4, thick=3, color = rep+1
oplot, Klist+1, L1list, thick=3, color = rep+1


xyouts, max(KList)+1 - 1.7, min(L1list)/1.1, /data, "!20N!7!Dcell!N = "+strcompress(string(Nlist[rep]),/remove_all) + "!3", color=rep+1

endfor


device,/close
set_plot,"X"

ende:

end





 









