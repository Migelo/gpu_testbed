
Base = "../tests/GaussianDiffusion/Serial_K4/Res128/"

outdir = "output"

window, xsize=1000, ysize=800

ShearViscosity = 1.0/128D

mi = -1.0
ma =  1.0

BoxSize = 2.0

loadct, 15
tvlct, r, g, b, /get

openr,1,"rainbow.clt"
readf,1,r
readf,1,g
readf,1,b
close,1


for num = 0, 16,1 do begin

   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/" + outdir + "/field_3_"  + exts + ".dat"
  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, ti
  rho = dblarr(N, N)
  readu, 1, rho
  close, 1

  print,"total=", total(rho)
  
  x = (indgen(N) + 0.5)/N *  Boxsize - Boxsize/2
  
   d = rho

   d= transpose(d)

   plot, x, d(*,N/2), charsize=2

   Diff = 1.0/128

   ti += 1.0
   
   sigma2 = 2*Diff * ti
   
   rhoa  =  1.0/(2*!PI*sigma2) * exp( -x^2/ (2*sigma2))  + 0.1
   
   oplot, x, rhoa, color=255*256L + 255, thick=2, linestyle=2                           

   

   

skip:
   wait, 0.2



endfor


ende:

end





 









