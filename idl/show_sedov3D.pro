
Base = "../tests/Sedov3D/Serial_K1/output_0.0/"
Base = "../tests/Sedov3D/Serial_K1/output_1.0/"

Base = "../tests/Sedov3D/Serial_K2/output_1.0/"

Base = "../tests/Sedov3D/Serial_K5/output_1.0/"
Base = "../tests/Sedov3D/Serial_K5/output_0.0/"

Base = "../tests/Sedov3D/GPU_K1/output_0.0/"


mi = 0.1
ma = 3.5

BoxSize = 2.0

loadct, 15
tvlct, r, g, b, /get

openr,1,"rainbow.clt"
readf,1,r
readf,1,g
readf,1,b
close,1



;for num = 0, 31 do begin
for num = 31, 31 do begin



   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  dens = dblarr(N, N)

  readu, 1, dens
  readu, 1, dens
  readu, 1, dens
  close, 1

  
   dens= transpose(dens)

   colindex= (dens - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   Pic=bytarr(N, N, 3)

   pic(*,*,0) = r(colindex)
   pic(*,*,1) = g(colindex)
   pic(*,*,2) = b(colindex)



   if (num eq 0) or (num eq 31) then begin
      window, xsize= N, ysize= N
   endif
   
   tv, Pic, true=3,0

   

   wait, 0.05

   fname = Base + "/output/pic_"+exts+".jpg"
;   write_jpeg, fname, pic, true=3, quality=98
      
endfor

end





 









