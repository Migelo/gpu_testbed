compile_opt idl2

Base = '/ptmp/mpa/vrs/Simulations/gpu_testbed/'

; frun = 'N1024_FV_mach3.2_GPU'
frun = 'N1024_K1_mach3.2_GPU'

mi = 0.6
ma = 2.1

BoxSize = 1.0

loadct, 15
tvlct, r, g, b, /get

openr, 1, 'rainbow.clt'
readf, 1, r
readf, 1, g
readf, 1, b
close, 1

for num = 26, 26 do begin
  print, num

  exts = '0000'
  exts = exts + strcompress(string(num), /remove_all)
  exts = strmid(exts, strlen(exts) - 4, 4)

  f = Base + frun + '/field_0_' + exts + '.dat'

  openr, 1, f
  N = 0l
  readu, 1, N

  print, N
  ti = 0.0d
  readu, 1, ti

  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  mi = min(dens)
  ma = max(dens)

  print, exts, ': time=', ti, '  mi_dens= ', mi, ' ma_dens=', ma

  f = Base + frun + '/field_1_' + exts + '.dat'
  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti

  momx = dblarr(N, N)
  readu, 1, momx
  close, 1

  vx = momx / dens

  f = Base + frun + '/field_2_' + exts + '.dat'
  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti

  momy = dblarr(N, N)
  readu, 1, momy
  close, 1

  vy = momy / dens

  f = Base + frun + '/field_3_' + exts + '.dat'
  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti

  momz = dblarr(N, N)
  readu, 1, momz
  close, 1

  vz = momz / dens

  v = sqrt(vx ^ 2 + vy ^ 2 + vz ^ 2)

  ma = max(v)
  mi = min(v)

  print, exts, ': time=', ti, '  mi_v= ', mi, ' ma_v=', ma

  dens = transpose(dens)

  mi = 0.05
  ma = 50.0

  colindex = (alog(dens) - alog(mi)) / (alog(ma) - alog(mi)) * 255.0
  ind = where(colindex ge 256.0)
  if ind[0] ne -1 then colindex[ind] = 255.9
  ind = where(colindex lt 0)
  if ind[0] ne -1 then colindex[ind] = 0
  colindex = byte(colindex)

  mi = 0.1
  ma = 10.0

  v = transpose(v)

  imageTemp = (alog(v) - alog(mi)) / (alog(ma) - alog(mi)) * 255.0
  ind = where(imageTemp ge 256.0)
  if ind[0] ne -1 then imageTemp[ind] = 255.9
  ind = where(imageTemp lt 0)
  if ind[0] ne -1 then imageTemp[ind] = 0
  imageTemp = byte(imageTemp)

  cols = 256
  ColPlane = fltarr(cols, cols, 3)

  openr, 1, 'colplane2.dat'
  readu, 1, ColPlane
  close, 1

  Pic = bytarr(N, N, 3)

  ; Pic[*, *, 0] = r[colindex]
  ; Pic[*, *, 1] = g[colindex]
  ; Pic[*, *, 2] = b[colindex]

  for i = 0, N - 1 do begin
    for j = 0, N - 1 do begin
      Pic[i, j, 0] = ColPlane[colindex[i, j], imageTemp[i, j], 0]
      Pic[i, j, 1] = ColPlane[colindex[i, j], imageTemp[i, j], 1]
      Pic[i, j, 2] = ColPlane[colindex[i, j], imageTemp[i, j], 2]
    endfor
  endfor

  if num eq 0 then begin
    window, xsize = N, ysize = N
  endif

  window, xsize = N, ysize = N
  tv, Pic, true = 3, 0

  wait, 0.1

  fname = 'plots/image_' + frun + '_' + exts + '.jpg'
  write_jpeg, fname, Pic, true = 3, quality = 98

  Pic = Pic[*, 0 : 1024 + 255, *]
  fname = 'plots/image_' + frun + '_small_' + exts + '.jpg'
  write_jpeg, fname, Pic, true = 3, quality = 98
endfor

end