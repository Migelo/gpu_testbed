
Base = "../tests/Sedov3D/Serial_K1/output_0.0/"

Base = "../tests/Sedov3D/Serial_K2/output_0.0/"

Base = "../tests/Sedov3D/Serial_K5/output_0.0/"


mi = 0.1
ma = 3.5

BoxSize = 2.0

loadct, 15
tvlct, r, g, b, /get

openr,1,"rainbow.clt"
readf,1,r
readf,1,g
readf,1,b
close,1

timax = 0.06

plot, [0],[0], xrange=[0,0.1], yrange=[0.001,  0.5] ,/ylog

for num = 0, 31 do begin


   print, num

   ti = 0.08/32.0 * num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  dens = dblarr(N, N)

  readu, 1, dens
  readu, 1, dens
  readu, 1, dens

  NN = 0L
  readu, 1, NN
  smth = dblarr(NN)
  readu,1, smth

  print, ti, "max=", max(smth)
  
  close, 1

  x = dblarr(NN)
  x(*) = ti
  
  oplot, x, smth, psym=4
  
endfor




end





 









