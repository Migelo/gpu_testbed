

fout = "plots/dyeentropy.eps"


BaseList = [ "../tests/KelvinHelmholtz_Lecoanet/N64_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N128_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N256_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N512_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K5_Re5/"]


BaseList = [ "../tests/KelvinHelmholtz_Lecoanet/N512_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K2_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K3_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K4_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K6_Re5/"]


BaseList = [ "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K1_Re5/", $,
             "../tests/KelvinHelmholtz_Lecoanet/N128_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N128_K3_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N256_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet/N64_K6_Re5/"]


BaseList = [ "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K1_Re5/", $,
             "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K2_Re5/"]

BaseList = [ "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K1_Re5/", $,
             "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K6_Re5/", $,
             "../tests/KelvinHelmholtz_Lecoanet_nojump/N128_K1_Re5/", $,
             "../tests/KelvinHelmholtz_Lecoanet_nojump/N256_K1_Re5/", $
             "../tests/KelvinHelmholtz_Lecoanet_nojump/N512_K1_Re5/"]

BaseList = ["../tests/KelvinHelmholtz_Lecoanet_nojump/N512_K1_Re5/"]

BaseList = ["../tests/KelvinHelmholtz_Lecoanet_nojump/N256_K3_Re5/"]
TagList = ["N256_K3_Re5"]




outdir = "output"




openr, 1, "lecoanet_D2048.txt"
da= dblarr(2,21)
readf,1,da
close,1


openr, 1, "lecoanet_nojump.txt"
da= dblarr(2,21)
readf,1,da
close,1



 set_plot,'PS'
 !p.font=0
 
 device,/times,/italic,font_index=20
 device,xsize=15.0,ysize=12.0
 
 !x.margin=[8,3]
 !p.thick=2.5
 !p.ticklen=0.03
 
 device,filename=fout, /encapsulated, /color
 
 v1=[240, 255,  0,  0,255,0 ,  255, 100,120, 0, 255]
 v2=[240, 100,255,  0,000,255, 255, 200,123, 12, 99]
 v3=[240,  0,  0,255,255,255, 0,   50,0, 100, 56]
 
 tvlct,v1,v2,v3,1
 
 xc= cos(indgen(32)/31.0 * 2*!PI )
 yc= sin(indgen(32)/31.0 * 2*!PI )
 
 usersym, xc, yc, thick=1.5     ;,/fill
 
 plot, [0],[0], psym=3, xrange=[0,10], yrange=[0.05, 0.35], ystyle =1, $
       xthick=2.5, ythick=2.5, /nodata, charsize=1.2, xtitle="!20t!3", ytitle = "!20S!3"
   


for rep = 0, n_elements(BaseList)-1 do begin

   Base = BaseList[rep]
   
   tilist =[0]
   Slist = [0]


   for num = 0, 100,1 do begin

      print, num
      
      exts='0000'
      exts=exts+strcompress(string(num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      
      f= Base + "/" + outdir + "/field_0_"  + exts + ".dat"

      Result = FILE_INFO(f)
      if Result.Size gt 0 then begin
         
         openr, 1, f
         N = 0L
         readu, 1, N
         ti = 0.0D
         readu, 1, Ti
         dens = dblarr(N, N)
         readu, 1, dens
         close, 1

         f= Base + "/" + outdir + "/field_4_"  + exts + ".dat"
         

         openr, 1, f
         N = 0L
         readu, 1, N
         ti = 0.0D
         readu, 1, Ti
         dyedens = dblarr(N, N)
         readu, 1, dyedens
         close, 1



         d = dyedens / dens

         ind = where(d gt 0)

         S  = total(-dens[ind] * d[ind] * alog(d[ind]), /double) / (N*N)

         print, "S=", 2*s    
         
         tilist = [tilist, ti]
         slist  = [slist, S]
         
         print, num, "time=", ti, "  minimum dye=", min(d), "  maximum dye=",  max(d), "  S=", S

      endif
      
   endfor


   tilist = tilist[1:*]
   slist  = slist[1:*]

   oplot, tilist,  2 * slist, linestyle=rep, thick=4 

  xyouts, 0.6, 0.5-rep*0.05, /normal,"!7" + TagList[rep] +"!3"
   
endfor

oplot, da[0,*], da[1,*], psym=8, color=2
;oplot, da[0,*], da[1,*], color=2


xyouts, 0.6,0.4, /normal, "!7Lecoanet et al. (2016)!3", color =2

device,/close

set_plot, "X"


end















