

fout = "plots/gaussian_diffusion_vs_order.eps"

  set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=8.0

    !x.margin=[8,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color


     v1=[115, 135, 155, 175, 195, 215, 235, 255, 200]
     v2=[  0, 0,  0, 0, 0, 0, 0, 0, 200 ]
     v3=[  10, 20,  30, 40, 50, 60, 70, 80 , 200]

    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill

    plot, [1],[1], /nodata, xrange=[0, 11], xstyle=1, yrange=[1.0e-7,0.4], ystyle =1, charsize=1.2, /ylog, $
           xthick = 2.5, ythick = 2.5, xtitle = "!20p!3", ytitle = "!7L1!3" 


NList = ["Res8"]
XList = [8]

BaseList = ["../tests/GaussianDiffusion/Serial_K0/", $
            "../tests/GaussianDiffusion/Serial_K1/", $
            "../tests/GaussianDiffusion/Serial_K2/", $
            "../tests/GaussianDiffusion/Serial_K3/", $
            "../tests/GaussianDiffusion/Serial_K4/", $
            "../tests/GaussianDiffusion/Serial_K5/", $
            "../tests/GaussianDiffusion/Serial_K6/", $
            "../tests/GaussianDiffusion/Serial_K7/", $
            "../tests/GaussianDiffusion/Serial_K8/", $
            "../tests/GaussianDiffusion/Serial_K9/"]

OrderList = lindgen(n_elements(BaseList))



L1list = dblarr(n_elements(baseList), n_elements(Nlist))

for run = 0, n_elements(BaseList)-1 do begin
   for rep=0, n_elements(Nlist)-1 do begin

      f = BaseList[run] + Nlist[rep] + "/output/L1.txt"

      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)+1.0
      L1   = da(1,*)
      
      oplot, time, L1  , color=255*256L^(rep mod 3), linestyle=run, thick=3
      
      
      L1max = L1[n_elements(L1)-1]
      print, L1Max

      L1list[run,rep] = L1max
      
   endfor
endfor




oplot, 1 + Orderlist, L1list[*,0], psym =4, color=2, thick=3
oplot, 1 + Orderlist, L1list[*,0], color=2, linestyle=run, thick=3
   
   fit = linfit(Orderlist, alog(L1list[*,0])) 

   xx =[-1,20]

   yy = exp(  fit[1]*xx + fit[0])

   oplot, 1 + xx, yy, color=3

   print, "slope= ", fit[1]

   device, /close
   set_plot, "X"
   
end





 









