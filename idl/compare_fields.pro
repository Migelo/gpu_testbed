compile_opt idl2

Base0 = '/u/vrs/Coding/gpu_testbed/tests/SupersonicTurbulence/N32_K1/output/'
Base1 = '/u/vrs/Coding/gpu_testbed/tests/SupersonicTurbulence/GPU_N32_K1/output/'

mi = 0.95
ma = 1.05

loadct, 15
tvlct, r, g, b, /get

openr, 1, 'rainbow.clt'
readf, 1, r
readf, 1, g
readf, 1, b
close, 1

for num = 0, 1200 do begin
  print
  print, num

  exts = '0000'
  exts = exts + strcompress(string(num), /remove_all)
  exts = strmid(exts, strlen(exts) - 4, 4)

  f = Base0 + '/field_4_' + exts + '.dat'

  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti
  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  print, 'Time=', ti, '  ma=', max(dens), '  mi=', min(dens)

  dens = transpose(dens)

  colindex = (dens - mi) / (ma - mi) * 255.0
  ind = where(colindex ge 256.0)
  if ind[0] ne -1 then colindex[ind] = 255.9
  ind = where(colindex lt 0)
  if ind[0] ne -1 then colindex[ind] = 0
  colindex = byte(colindex)

  Pic = bytarr(N, N, 3)

  Pic[*, *, 0] = r[colindex]
  Pic[*, *, 1] = g[colindex]
  Pic[*, *, 2] = b[colindex]

  if num eq 0 then begin
    window, xsize = 2 * N, ysize = N
  endif

  tv, Pic, true = 3, 0

  ; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  f = Base1 + '/field_4_' + exts + '.dat'

  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti
  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  print, 'Time=', ti, '  ma=', max(dens), '  mi=', min(dens)

  dens = transpose(dens)

  colindex = (dens - mi) / (ma - mi) * 255.0
  ind = where(colindex ge 256.0)
  if ind[0] ne -1 then colindex[ind] = 255.9
  ind = where(colindex lt 0)
  if ind[0] ne -1 then colindex[ind] = 0
  colindex = byte(colindex)

  Pic = bytarr(N, N, 3)

  Pic[*, *, 0] = r[colindex]
  Pic[*, *, 1] = g[colindex]
  Pic[*, *, 2] = b[colindex]

  tv, Pic, true = 3, 1
endfor

end