
BoxSize = 10.0


num = 50
   
exts='0000'
exts=exts+strcompress(string(num),/remove_all)
exts=strmid(exts,strlen(exts)-4,4)

BaseHighRes = "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N8000_FV/"

f= BaseHighRes + "/output/field_0_"  + exts + ".dat"

openr, 1, f
N = 0L
readu, 1, N
ti = 0.0D
readu,1,ti
dens_ref = dblarr(N)
readu, 1, dens_ref
close, 1



Base = "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N200_K5/"



L1min = 1.0e30

openw,2, base + "/optimization.txt"

for smth = 0.0, 0.5, 0.1 do begin

    for visc = 0.0, 1.5, 0.1 do begin


   
      f= Base + "/output/field_0_"  + exts + ".dat"

      cmd = "rm -f " + f
      spawn, cmd
      
      
      cmd = "sed -i -e " + "'" + "s/ArtViscRichtmyer.*/ArtViscRichtmyer " + string(visc) + "/g" + "'" + " " +base + "/param.txt"
      print, cmd
      spawn, cmd
      
      cmd = "sed -i -e " + "'" + "s/ArtViscLandshoff.*/ArtViscLandshoff  " + string(smth) + "/g" + "'" + " " + base + "/param.txt"
      print, cmd
      spawn, cmd
      
      cmd = "cd " + base + "; ./run.run param.txt"
      print, cmd
      spawn, cmd
      
      openr, 1, f, err = ernr
      if ernr eq 0 then begin
         N = 0L
         readu, 1, N
         ti = 0.0D
         readu,1,ti
         print, num, ti
         dens = dblarr(N)
         readu, 1, dens
         close, 1

         L1 = total(abs(dens - dens_ref), /double) / n_elements(dens)
         print, "L1 = ", L1

         printf, 2, visc, smth, L1
         flush, 2

         if L1 lt L1min then begin
            L1min = L1
            visc_min = visc
            smth_min = smth
         endif
      endif

   endfor

endfor
   
close,2

print

print, "L1min = ", L1min
print, "visc_min = ", visc_min
print, "smth_min = ", smth_min

ende:
end





 









