
cells = 21


BaseList = ["../tests/SingleShock/K0/", $
            "../tests/SingleShock/K1/", $
            "../tests/SingleShock/K2/", $
            "../tests/SingleShock/K3/", $
            "../tests/SingleShock/K4/", $
            "../tests/SingleShock/K5/", $
            "../tests/SingleShock/K6/", $
            "../tests/SingleShock/K7/", $
            "../tests/SingleShock/K8/", $
            "../tests/SingleShock/K9/"]
            
Num =200

pathoutput = "output"

widthlist = dblarr(n_elements(baselist))

window, xsize = 1600, ysize=600

for rep = 0, n_elements(baselist)-1 do begin

   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= BaseList[rep] + "/" + pathoutput+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   rho = dblarr(N)
   readu, 1, rho
   smth = dblarr(N)
   readu, 1, smth
   rate = dblarr(N)
   readu, 1, rate
   close, 1


   x = 1.0*(indgen(N)+0.5)/N

   plot, x, rho


   rhomax = rho[0]
   rhomin = rho[n_elements(rho)-1]

   
   if rep eq 0 then begin
      xx = congrid(x, 21)
      yy = congrid(rho, 21)
      rr = interpol(yy, xx + 0.5 /21.0, x) 

      oplot, x, rr

      rho = rr
     
   endif

   


   imax = -1
   imin = -1
   
   for i = n_elements(rho)-1, 0, -1 do begin
      if (imax eq -1) and ( rho[i] ge (rhomin + 0.2 * (rhomax - rhomin))) then begin
         imax = i
      endif
      if (imin eq -1) and ( rho[i] ge (rhomin + 0.8 * (rhomax - rhomin))) then begin
         imin = i+1
         goto, skip
      endif
   endfor
   skip:

   ind = indgen(imax-imin+1) + imin
   
   oplot, x[ind], rho[ind], color=255, thick=2

   dx = max(x[ind]) - min(x[ind])

   print, dx

   widthlist[rep] = dx
   
    for i=0,cells do begin
       oplot, i * 1.0/cells   *[1,1], [1.0e-6,1.0e6], linestyle=1
    endfor


    
 ;  wait, 1.0
endfor

order = lindgen(n_elements(baselist))+1.0

plot, order, widthlist / (1.0/cells) , xrange=[0.9,11],/xlog,/ylog, xstyle=1

oplot, order, widthlist / (1.0/cells) , psym= 4

oplot, [0.1,100], [1,1], linestyle=2

slope=-1

x = [0.1,100]
oplot, x , 2.5 * 1.0/x, linestyle=1


set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=12.0

    !x.margin=[8,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename= "plots/shockwidth.eps", /encapsulated, /color

     v1=[255, 135, 155, 175, 195, 215, 235, 255, 200]
     v2=[  0, 0,  0, 0, 0, 0, 0, 0, 200 ]
     v3=[  10, 20,  30, 40, 50, 60, 70, 80 , 200]

    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )
    yc= sin(indgen(32)/31.0 * 2*!PI )

    usersym, xc, yc, thick=1.5  ,/fill



    plot, order, widthlist / (1.0/cells) , /nodata, xrange=[0.9,12],/xlog,/ylog, xstyle=1, yrange=[0.1,7], ystyle =1, $
          xthick = 2.5, ythick = 2.5, xtitle = "!7order !20p!7!3", ytitle = "!7shock width [ !20h!7 ]!3"
    
    oplot, [0.1,100], [1,1], linestyle=1


   slope=-1

   x = [0.1,100]
   oplot, x , 2.5 * 1.0/x, linestyle=2, thick=3


    oplot, order, widthlist / (1.0/cells) , color=1, thick=4
    oplot, order, widthlist / (1.0/cells) , psym= 8, color=2

    device,/close
    set_plot, "X"
    
    


    ende:
end





 









