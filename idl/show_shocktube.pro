







order = 3
Base = "../tests/ShockTube1D/Serial_K3/"


order = 2
Base = "../tests/ShockTube1D/Serial_K2/"


order = 0
Base = "../tests/ShockTube1D/Serial_K0/"


order = 4
Base = "../tests/ShockTube1D/Serial_K4/"



order = 5
Base = "../tests/ShockTube1D/Serial_K5/"



order = 1
Base = "../tests/ShockTube1D/Serial_K1/"
Base = "../tests/ShockTube1D/Serial_K1_nolimiting/"
Base = "../tests/ShockTube1D/Serial_K0/"
Base = "../tests/ShockTube1D/Serial_K1_minmod/"


BoxSize = 1.0

;pathoutput = "output_1.0"
pathoutput = "data"




window, xsize=1800, ysize=1400

for num = 0, 99 do begin

   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1


   x = (indgen(N)+0.5)/N
  
   x-=0.5

   plot, x, dens, xrange=[-0.25,0.25], charsize=2, yrange=[0,1.8], ystyle=1

   NN = 64
   
   for k=0,NN do begin
      oplot, (1.0/NN)*k * [1,1] - 0.5, [-10,10],linestyle=1
   endfor


   S0 = 0.022 * order^(-2.0)
   
 ;;  fac = 0.8*order^4
   
   oplot, x, alog10(smth/S0) + 1, color=255,thick=3

   oplot, [-10,10], 1.0 * [1,1], color=255, linestyle=2

   
   wait, 0.1
      
endfor




end





 









