compile_opt idl2
window, xsize = 1400, ysize = 700

frunlist = ['../tests/Turbulence3D/N32_K1/output/', $
  '../tests/Turbulence3D/GPU_N32_K1/output/']

frunlist = ['../tests/Turbulence3D/N32_K1/output/', $
  '../tests/IsoThermalTurbulence/N32_K1/output/']

frunlist = ['../tests/Turbulence3D/GPU_N32_K1/output/', $
  '../tests/IsoThermalTurbulence/GPU_N32_K1/output/']

frunlist = ['../tests/Turbulence3D/GPU_N32_K1/output/', $
  '../tests/Turbulence3D/GPU_N32_K2/output/', $
  '../tests/Turbulence3D/GPU_N32_K3/output/', $
  '../tests/Turbulence3D/GPU_N32_K1_rerun/output/']

Machmax = 0.6
frunlist = ['../tests/Turbulence3D/N32_K1/output/', $
  '../tests/Turbulence3D/GPU_N32_K1/output/', $
  '../tests/Turbulence3D/N32_K1/output_old/', $
  '../tests/Turbulence3D/GPU_N32_K1/output_old/']

Machmax = 1.8
frunlist = ['../tests/SupersonicTurbulence/N32_K1/output/', $
  '../tests/SupersonicTurbulence/GPU_N32_K1/output3/']



Machmax = 2.0
frunlist = ['../order_analysis/n64_k1_mach1.5/data/', $
            '../order_analysis/n64_k2_mach1.5/data/', $
            '../order_analysis/n64_k3_mach1.5/data/']

Machmax = 4.0
frunlist = ['../order_analysis/n64_k1_mach3.0/data/', $
            '../order_analysis/n64_k2_mach3.0/data/', $
            '../order_analysis/n64_k3_mach3.0/data/']

Machmax = 4.0
frunlist = ['../order_analysis/n128_k1_mach3.0/data/', $
            '../order_analysis/n128_k2_mach3.0/data/', $
            '../order_analysis/n128_k3_mach3.0/data/']

Machmax = 4.0
frunlist = ['../order_analysis/n256_k0_mach3.0/data/', $
            '../order_analysis/n256_k1_mach3.0/data/', $
            '../order_analysis/n256_k2_mach3.0/data/', $
            '../order_analysis/n256_k3_mach3.0/data/']   ; highest res not complete



Machmax = 20.0
frunlist = ['../order_analysis/n64_k1_mach9.0/data/', $
            '../order_analysis/n64_k2_mach9.0/data/', $
            '../order_analysis/n64_k3_mach9.0/data/']



Mach =  12;  0.8
TiMax = 1.0 ; 100.0

Teddy = 1.0/( Mach)

frunlist = ['../tests/TurbTest/N32_mach0.1/output/', $
            '../tests/TurbTest/N32_mach0.2/output/', $
            '../tests/TurbTest/N32_mach0.4/output/', $
            '../tests/TurbTest/N32_mach0.8/output/', $
            '../tests/TurbTest/N32_mach1.6/output/', $
            '../tests/TurbTest/N32_mach3.2/output/', $
            '../tests/TurbTest/N32_mach6.4/output/', $
            '../tests/TurbTest/N32_mach12.8/output/', $
            '../tests/TurbTest/minmod_N32_mach3.2/output/']


MachMax = 1.2 * Mach
!p.position = [0.1, 0.1, 0.45, 0.95]

plot, [0], [0], /nodata, xrange = [0, timax], yrange = [0, 20.0], xstyle = 1, ystyle = 1, charsize = 1.5

colorlist =[255, 255*256L, 255*256L^2, 255+255*256L, 255*256L+255*256L^2, 255+ 255*256L^2,  $
            255+255*256L+100*256L^2, 100+ 255*256L+255*256L^2, 255+ 255*256L^2 + 100 * 256L]


for rep = 0, n_elements(frunlist) - 1 do begin
  f = frunlist[rep] + '/turbulence.txt'

  spawn, 'wc ' + f, result
  lines = long(result)
  lines = lines[0]

;  if rep ge 2 then begin
;    en = dblarr(4, lines)
;  endif else begin
    en = dblarr(6, lines)
;  endelse

  openr, 1, f
  readf, 1, en
  close, 1

  ti = en[0, *]
  inj = en[1, *]
  diss = en[2, *]
  mach = en[3, *]

  oplot, ti, inj, color = colorlist[rep], linestyle = 0

  print, 'energy inhection rate=', max(inj) / max(ti)

  oplot, ti, diss, color = colorlist[rep], linestyle = 2
endfor

!p.position = [0.55, 0.1, 0.95, 0.95]

plot, [0], [0], /nodata, xrange = [0, timax], yrange = [0, Machmax], xstyle = 1, ystyle = 1, charsize = 1.5, /noerase

for rep = 0, n_elements(frunlist) - 1 do begin
  f = frunlist[rep] + '/turbulence.txt'

  spawn, 'wc ' + f, result
  lines = long(result)
  lines = lines[0]

;  if rep ge 2 then begin
;    en = dblarr(4, lines)
;  endif else begin
    en = dblarr(6, lines)
;  endelse

  openr, 1, f
  readf, 1, en
  close, 1

  ti = en[0, *]
  inj = en[1, *]
  diss = en[2, *]
  mach = en[3, *]

  oplot, ti, mach, linestyle = rep, color = colorlist[rep]
;  oplot, ti / 2, 2 * mach, linestyle = rep, color = 255 * (256l ^ rep)

  ind = where((ti gt 4 * Teddy) and (ti lt 10 * Teddy))
  mean_mach = total(mach[ind])/n_elements(ind)

  print, "Mean Mach=", mean_mach

  oplot, [0, 1000], Mean_mach* [1, 1], linestyle=2
  
  oplot, 2 * Teddy * [1, 1], [0, 1000], linestyle=2

  oplot, 3 * Teddy * [1, 1], [0, 1000], linestyle=1
  
  oplot, 8 * Teddy * [1, 1], [0, 1000], linestyle=2
  
  print, max(ti), max(mach)
endfor

end
