
fout = "plots/mach_number_comparison.eps"

Box = 1.0

Base = "/Users/springel/data/"

frunlist = [$; "TurbulenceSequence/N512_FV_mach3.2_GPU/data/", $
            "TurbulenceSequence/N1024_FV_mach3.2_GPU/data/" ] ;, $
        ;    "TurbulenceSequence/N512_K1_mach3.2_GPU/data/" ] ;, $
;            "TurbulenceSequence/N1024_K1_mach3.2_GPU/data/"]

TagList = ["N512-FV, mach 3.2", "N1024-FV, mach 3.2", "N512-K1, mach 3.2", "N1024-K1, mach 3.2"]



set_plot,'PS'
!p.font=0

device,/times,/italic,font_index=20
device,xsize=26.0,ysize=14.0

!x.margin=[10,2]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout, /encapsulated, /color

v1=[255,  0,    30,255,0 ,  155, 100,120, 0,  255, 200]
v2=[100, 255,  100,000,255, 155, 200,123, 12,  99, 200]
v3=[0,      0, 255,255,255, 0,   50, 0,   100, 56, 200]


tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

usersym, xc, yc, thick=1.5      ;,/fill



plot, [1], [1], /nodata, xtitle = "!20t!3", ytitle = "!20Mach!3", charsize=1.2, $
      xrange=[0,3.5], xstyle=1, yrange =[0,4.0],    xthick=2.5, ythick=2.5, ystyle=1



for rep=0, n_elements(frunlist)-1 do begin


   f = Base + frunlist[rep] + '/turbulence.txt'

   spawn, 'wc ' + f, result
   lines = long(result)
   lines = lines[0]
   en = dblarr(6, lines)
   openr, 1, f
   readf, 1, en
   close, 1

   ti = en[0, *]
   inj = en[1, *]
   diss = en[2, *]
   mach = en[3, *]





   oplot, ti, mach, linestyle = rep, color = rep+1

   for Num = 18, 128 do begin
    
    exts = '0000'
    exts = exts + strcompress(string(Num), /remove_all)
    exts = strmid(exts, strlen(exts) - 4, 4)

    fname = Base + frunlist[rep] + '/velocity_structure_function_' + exts + '.dat'
    print, fname
    openr, 1, fname
    Bins = 0l
    readf, 1, Bins
    Ti = 0.0
    readf, 1, Ti
    Mach = 0.0
    readf, 1, Mach
    da = dblarr(2, Bins)
    readf, 1, da
    close, 1

    oplot, [ti], [Mach], psym=4, color = rep+1
 endfor


endfor




device,/close
set_plot, "X"
  

end
