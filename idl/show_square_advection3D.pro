
S0 = 0.022 / 5.0^2
print, S0

Base = "../tests/SquareAdvection3D/Serial_K1/output/"


BoxSize = 1.0

loadct, 15
tvlct, r, g, b, /get

;openr,1,"rainbow.clt"
;readf,1,r
;readf,1,g
;readf,1,b
;close,1


window, xsize=1000, ysize=1000

for num = 0, 10,5 do begin


   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  dens = dblarr(N, N)
  readu, 1, dens

   print, num, ti

   close, 1

  
   dens = transpose(dens)


x = (indgen(N)+0.5)/N 

plot, x, dens(*, N/2), charsize=1.5, yrange=[0,5]
oplot, x,  dens(*,  N/2), color = 255
print, total(dens)



for i=0, 8 do begin

   oplot, (i + 0.0)/8 * [1,1], [0, 1000], linestyle=1
   
endfor


   wait, 1.0



;   fname = Base + "/output/pic_"+exts+".jpg"
;   write_jpeg, fname, pic, true=3, quality=98
      
endfor


ende:
end





 









