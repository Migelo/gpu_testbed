

BaseList = ["../tests/SquareAdvection1D/Serial_K0/" , $
            "../tests/SquareAdvection1D/Serial_K1/" , $
            "../tests/SquareAdvection1D/Serial_K2/", $
            "../tests/SquareAdvection1D/Serial_K3/" , $
            "../tests/SquareAdvection1D/Serial_K4/", $
            "../tests/SquareAdvection1D/Serial_K5/" , $
            "../tests/SquareAdvection1D/Serial_K6/", $
            "../tests/SquareAdvection1D/Serial_K7/" , $
            "../tests/SquareAdvection1D/Serial_K8/", $
            "../tests/SquareAdvection1D/Serial_K9/" ]




set_plot,'PS'
!p.font=0

device,/times,/italic,font_index=20
device,xsize=16.0,ysize=22.0

!x.margin=[8,3]
!p.thick=2.5
!p.ticklen=0.03

device,filename= "plots/square_advection_L1.eps", /encapsulated, /color

v1=[205,  0,  0,215,0 ,  255, 100,120, 0, 120]
v2=[  0,155,  0,000,255, 255, 200,123, 12, 120]
v3=[  0,  0,205,215,255, 0,   50,0, 100, 220]

tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

usersym, xc, yc, thick=4.5   ,/fill



plot, [1], [1], /nodata, charsize = 1.2, xrange=[0.01, 1.1], yrange=[1.0e-2, 4], xstyle=1, ystyle=1, xthick=3.0, ythick=3.0,$
      xtitle="!20t!3", ytitle = "!7L1!3", /xlog, /ylog











for rep = 0, n_elements(baselist)-1 do begin


   f = BaseList[rep] + "/output/L1.txt"  
   f = strcompress(f, /remove_all)
      
   spawn,"wc "+f,result
   lines=long(result)
   lines=lines(0)
   
   da = dblarr(2,LINES)
   
   print, f
   openr, 1,f
   readf, 1,da
   close,1

   time = da(0,1:*)
   L1 = da(1,1:*)

   
   oplot, Time, L1, psym=8, color=10

   if rep eq 0 then begin
      time = time[0:2]
      L1 = L1[0:2]
   endif


   fit = linfit(alog(time), alog(L1)) 
   xx =[0.001,1.0]
   yy = exp(  fit[1]*alog(xx) + fit[0])
   oplot, xx, yy, color=1, thick=2.5

   print, "slope=", fit[1]



   xx = 0.02
   yy = exp(  fit[1]*alog(xx) + fit[0])

   if rep eq 6 then begin
      xyouts,  xx*1.6, yy*1.1, /data, "!20p!7 = " + strcompress(string(rep+1),/remove_all) + "!3", charsize=1.0, color=10
   endif else begin
      xyouts,  xx, yy/1.1, /data, "!20p!7 = " + strcompress(string(rep+1),/remove_all) + "!3", charsize=1.0, color=10
   endelse
   
endfor

device,/close
set_plot,"X"


ende:
end





 









