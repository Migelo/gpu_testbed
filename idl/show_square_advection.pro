
S0 = 0.022 / 5.0^2
print, S0

Base = "../tests/SquareAdvection/Serial_K9/output/"


BoxSize = 1.0

loadct, 15
tvlct, r, g, b, /get

;openr,1,"rainbow.clt"
;readf,1,r
;readf,1,g
;readf,1,b
;close,1


window, xsize=1000, ysize=1000

for num = 0, 10,2 do begin


   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  dens = dblarr(N, N)
  readu, 1, dens

   print, num, ti
  
  ;Nc = 0L
;  readu, 1, Nc
;  Nc = sqrt(Nc)
;  smth = dblarr(Nc, Nc)
;  readu, 1, smth
  close, 1

  
   dens= transpose(dens)

;print, "min(dens)=", min(dens)
   
;   smth= transpose(smth)


  f= Base + "/field_1_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  rhovx = dblarr(N, N)
  readu, 1, rhovx
  close, 1

  
   rhovx= transpose(rhovx)

   vx = rhovx/dens


  f= Base + "/field_2_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  rhovy = dblarr(N, N)
  readu, 1, rhovy
  close, 1

  
   rhovy= transpose(rhovy)

   vy = rhovy/dens



  f= Base + "/field_3_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  e = dblarr(N, N)
  readu, 1, e
  close, 1

  
  e= transpose(e)

u = e/dens - 0.5 * (vx^2 + vy^2)


x = (indgen(N)+0.5)/N 
;xx = (indgen(Nc)+0.5)/Nc 


;plot, x, dens(*,N/2), charsize=1.5
;oplot, x,  dens(*,N/2), color = 255

plot, x, dens(*,N/2), charsize=1.5, yrange=[0,5]
oplot, x,  dens(*,N/2), color = 255
print, total(dens)


;plot, x, dens(N/4, *), charsize=1.5
;oplot, x,  dens(N/4, *), color = 255


;plot, x, u(*,N/2), charsize=1.5
;oplot, x,  u(*,N/2), color = 255

;plot, xx, smth(Nc/4, *), color = 255*256L 
;oplot, [0,1], S0*[1,1], color=255*256L^2

;plot, x, vx(*,N/2), charsize=1.5
;oplot, x, vx(*,N/2), psym=4, color=255


for i=0, 8 do begin

   oplot, (i + 0.0)/8 * [1,1], [0, 1000], linestyle=1
   
endfor


   wait, 1.0



;   fname = Base + "/output/pic_"+exts+".jpg"
;   write_jpeg, fname, pic, true=3, quality=98
      
endfor


ende:
end





 









