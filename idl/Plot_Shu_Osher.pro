
fout = "plots/shu_osher.eps"

BoxSize = 10.0

Num = 50
   
exts='0000'
exts=exts+strcompress(string(num),/remove_all)
exts=strmid(exts,strlen(exts)-4,4)

BaseHighRes = "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N8000_FV/"

f= BaseHighRes + "/output/field_0_"  + exts + ".dat"

openr, 1, f
N = 0L
readu, 1, N
ti = 0.0D
readu,1,ti
dens_ref = dblarr(N)
readu, 1, dens_ref
close, 1




BaseList = ["/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N100_FV/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N200_FV/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N400_FV/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N100_K1/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N200_K1/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N400_K1/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N100_K3/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N200_K4/", $
            "/u/vrs/Coding/gpu_testbed/tests/ShuOsherShock/N400_K9/"]

TagList = ["!20N!7!Dcell!N = 100!3", $
           "!20N!7!Dcell!N = 200!3", $
           "!20N!7!Dcell!N = 400!3", $
           "!20N!7!Dcell!N = 100!3", $
           "!20N!7!Dcell!N = 200!3", $
           "!20N!7!Dcell!N = 400!3", $
           "!20N!7!Dcell!N = 100!3", $
           "!20N!7!Dcell!N = 200!3", $
           "!20N!7!Dcell!N = 400!3"]

TagList2 = ["!7finite volume!3", $
           "!7finite volume!3", $
           "!7finite volume!3", $
           "!7order K = 1!3", $
           "!7order K = 1!3", $
           "!7order K = 1!3", $
           "!7order K = 3!3", $
           "!7order K = 4!3", $
           "!7order K = 9!3"]

ColorList = [2, 2, 2, 3, 3, 3, 4, 5, 6]


 set_plot,'PS'
 !p.font=0
 
 device,/times,/italic,font_index=20
 device,xsize=28.0,ysize=22.0
 
 !x.margin=[6,3]
 !p.thick=2.5
 !p.ticklen=0.03
 
 device,filename=fout, /encapsulated, /color

 v1=[140, 255, 220,   0 , 255,   0,  255, 100,120, 0, 255]
 v2=[140, 100,   0,   0,    0, 200,  255, 200,123, 12, 99]
 v3=[140,  0,    0, 255,  255, 200,   0,   50,0, 100, 56]
 
 tvlct,v1,v2,v3,1
 
 xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
 yc= sin(indgen(32)/31.0 * 2*!PI )*0.4
 
 usersym, xc, yc, thick=1.5     ;,/fill


 for rep =0, n_elements(BaseList) -1 do begin

    
   
   f= BaseList[rep] + "/output/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu,1,ti
   print, num, ti
   dens = dblarr(N)
   readu, 1, dens
   close, 1


   row = rep / 3
   col = rep mod 3

   xl = (col+0.12) * 1.0/3
   xr = (col+0.96) * 1.0/3 

   bo = ((2-row)+0.12) * 1.0/3
   to = ((2-row)+0.96) * 1.0/3 

   
   !p.position = [xl, bo, xr, to]
 
   x = (indgen(N)+0.5)/N
   x -= 0.5
   x *= BoxSize
   
   
   plot, x, dens_ref, xrange=[-5,5], xstyle=1, charsize=1.1, yrange=[0,5], ystyle=1, $
         xthick=2.5, ythick=2.5, xtitle = "!20x!3", ytitle="!9r!3", /noerase, /nodata

   oplot, x, dens_ref, color=1, thick=3.0
   
   
   oplot, x, dens, color=colorlist[rep], thick=3.0


   xyouts,  -4, 4.3, taglist[rep], /data, charsize=1.15

   xyouts,  -4, 2.8, taglist2[rep], /data, charsize=1.1, color = colorlist[rep]


   
   xl = (col+0.22) * 1.0/3
   xr = (col+0.6) * 1.0/3 

   bo = ((2-row)+0.22) * 1.0/3
   to = ((2-row)+0.5) * 1.0/3 

   !p.position = [xl, bo, xr, to]

   plot, x, dens_ref, xrange=[0.5,2.5], xstyle=1, charsize=0.0001, yrange=[2.8,5], ystyle=1, $
         xthick=2.5, ythick=2.5, xtitle = "!20x!3", ytitle="!9r!3", /noerase, /nodata

   oplot, x, dens_ref, color=1, thick=3.0
   
   oplot, x, dens, color=colorlist[rep], thick=3.0


   
  
endfor

device,/close
 


ende:
end





 









