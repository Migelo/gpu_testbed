
Box = 1.0

 
epsilon = 0.040
nu = 0.0002

epsilon = 0.035

eta =  (nu^3 / epsilon)^(1.0/4)






TagList = ["FV", $
           "K1"]


frunlist = ['/ptmp/vrs/TurbulenceSequence/N512_FV_mach3.2_GPU/data/', $
            '/ptmp/vrs/TurbulenceSequence/N512_K1_mach3.2_GPU/data/']


frunlist = ['/ptmp/vrs/TurbulenceSequence/N512_FV_mach3.2_GPU/data/']



    set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=12.0

    !x.margin=[8,2]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename="powerspec_512.eps", /encapsulated, /color

    v1=[255,  0,  0,255,0 ,  155, 100,120, 0, 255]
    v2=[100,255,  0,000,255, 155, 200,123, 12, 99]
    v3=[0,  0,255,255,255, 0,   50,0, 100, 56]


    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill


     
    plot, [1], [1], /nodata, /xlog, /ylog , xtitle = "k", ytitle = "E(k)", charsize=1.2, $
           xrange=[2 * !PI/Box / 1.5, 2000], xstyle=1, yrange =[1.0e-8, 2.0e-2]*100,    xthick=2.5, ythick=2.5, ystyle=1



for rep=0, n_elements(frunlist)-1 do begin


   Nspec = 0
   ElistSum =0.0
   
   for Num = 28,66 do begin
   ;for Num = 1,1 do begin

                                ;  for Num = 8,37 do begin

;     for Num = 0,9 do begin

     exts='000'
     exts=exts+strcompress(string(Num),/remove_all)
     exts=strmid(exts,strlen(exts)-3,3)
     
     fname = frunList[rep]+"/powerspec_vel_" +exts+".txt"
     print, fname
     openr, 1, fname
     Time = 0.0D 
     Grid = 0L
     Bins = 0L
     readf, 1, Time
     readf, 1, Grid
     readf, 1, Bins
     print, "Time = ", Time, "  Grid=", Grid
     vel_disp = dblarr(3)
     readf, 1, vel_disp
     da= fltarr(4, bins)
     readf, 1, da
     close,1
     
     print, "vel_disp=",vel_disp
     
    knyquist = 2*!PI/box * Grid/2
    
    K = da(0,*)
    ModePow = da(1,*)
    ModeCount = da(2,*)
    SumPower =  da(3,*)


    print, num, total(ModeCount * ModePow, /double), total(vel_disp)


; we will do a band averaging of the finely binned points, 
; subject to two conditions:
; We want enough modes per bin in order to reduce the variance in a bin,
; and simultaneously, for large k, we don't want the bins become too narrow.
;
; The first condition is set by "MinModeCount",
; the second by "TargetBinNummer", which is used to compute a minimum 
; logarithmic bin-size.


    MinModeCount = 1
    TargetBinNummer = 100

    MinDlogK = (alog10(max(K)) - alog10(min(K)))/TargetbinNummer


    istart=0
    ind=[istart]

    k_list     = [0]
    power_list = [0]
    count_list = [0]

    repeat begin
        count = total(ModeCount(ind))
        deltak =  (alog10(max(K(ind))) - alog10(min(K(ind))))

        if (deltak ge mindlogk) and (count ge MinModeCount) then begin
            d2 = total(SumPower(ind))/total(ModeCount(ind))
            kk = total(K(ind)*ModeCount(ind))/total(ModeCount(ind))

            
            k_list = [k_list, kk]
            power_list = [power_list, d2]
            count_list = [count_list, total(ModeCount(ind))]
            istart = istart + 1
            ind = [istart]
        endif else begin
            istart = istart + 1
            ind = [ind, istart]
        endelse
    endrep until istart ge Bins

    k_list     = k_list(1:*)
    power_list = power_list(1:*)
    count_list = count_list(1:*)

    print, total(power_list * count_list)

    E_list =1.0/2 *  1.0/(2*!PI^2) * k_list^2 * power_list ; convert to energy-spectrum, E(k) = k^2 * P(k)

    

    ElistSum += E_list
    Nspec += 1

 endfor

  E_List = ElistSum / Nspec


  oplot, K_list,  E_list, color=1+rep, thick=4.0
  
;  oplot, K_list,  E_list, psym=4, color=1+rep

  if rep eq n_elements(Frunlist)-1 then begin
     oplot, K_list, E_list(4) * (k_list/K_list(4))^(-5.0/3), linestyle=2

          oplot, K_list, E_list(4) * (k_list/K_list(4))^(-2.0), linestyle=1
     
  endif

  xyouts, 0.8, 0.85-0.065 * rep, /normal, "!7" + taglist[rep] +"!3", charsize =1.1, color = 1+ rep
  
 ; oplot, knyquist*[1,1],[1.0e-20,1.0e30], linestyle=1
  


endfor


;; try to fit univeral form

C_Kol = 1.58
x = k_list * eta
c = 0.4
beta = 5.3
Spectrum_Kol =  C_Kol *  epsilon^(2.0/3) * k_list^(-5.0/3) *  exp(- beta * ((x^4+c^4)^(1.0/4) - c))

oplot, K_list, Spectrum_Kol, linestyle = 3



oplot, 2.0*!PI/Box *[1,1],[1.0e-20,1.0e30], linestyle=1

xyouts, 0.26, 0.2, /normal, "!732!U3!N cells!3", charsize =1.1



device,/close
set_plot, "X"
  

end
