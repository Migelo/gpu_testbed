
Base = "../tests/VortexSheet/Serial_K3/"

outdir = "output"


ShearViscosity = 0.005

mi = -1.0
ma =  1.0

BoxSize = 2.0

loadct, 15
tvlct, r, g, b, /get

openr,1,"rainbow.clt"
readf,1,r
readf,1,g
readf,1,b
close,1


for num = 0, 64,1 do begin

   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/" + outdir + "/field_1_"  + exts + ".dat"
  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, ti
  rhovx = dblarr(N, N)
  readu, 1, rhovx
  close, 1

  f= Base + "/" + outdir + "/field_2_"  + exts + ".dat"
  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, ti
  rhovy = dblarr(N, N)
  readu, 1, rhovy
  close, 1

  
  f= Base + "/" + outdir + "/field_0_"  + exts + ".dat"
  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, ti
  rho = dblarr(N, N)
  readu, 1, rho
  close, 1


  x = (indgen(N) + 0.5)/N *  Boxsize
  
   d = rho
   d = rhovx/rho
;   d = rhovy/rho
;   d = rhovy

   d = rhovx
   
   d= transpose(d)

   plot, x, d(0,*), charsize=2

   oplot, x, d(N/2,*), color=255
   
   vx  =    erf((x - 0.25 * BoxSize) / (2 * sqrt(ShearViscosity * ti)))   -  $
            erf((x - 0.75 * BoxSize) / (2 * sqrt(ShearViscosity * ti)))   - 1

   
   oplot, x, vx, color=255*256L + 255, thick=2, linestyle=2                           

   L1 = total(abs(vx - d(0,*)), /double) /n_elements(vx)

   print, L1

   goto,skip

   

  
  f= Base + "/" + outdir + "/gradfield_axis1_1_"  + exts + ".dat"
  openr, 1, f
  N = 0L
  readu, 1, N
  g = dblarr(N, N)
  readu, 1, g
  close, 1





  d = d(0,*)
  
  dd = dblarr(N)

  for i = 1, N-2 do begin
     dd[i] = 0.5 * (d[i+1] - d[i-1])/ (Boxsize / N)
  endfor
  
  


   g= transpose(g)

   plot, x, g(0,*), color=255*256L, charsize=2.0;, yrange=[0,3]

   oplot, x, g(100,*), color=255*256L^2, thick=3

   oplot, x, dd, color=255















   
print, "min=", min(d), "   max=", max(d)
 
   

skip:
   wait, 0.1


;   goto,ende
endfor


ende:

end





 









