

Base = "../tests/ShockTube1D/Serial_K1-2d/"


BoxSize = 1.0


window, xsize=1500, ysize=1000

for num = 0, 99 do begin

   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/output/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   dens = dblarr(N,N)
   readu, 1, dens
   close, 1

   dens = transpose(dens)
   dens = dens[*,0]

   x = (indgen(N)+0.5)/N
  
   x-=0.5

   plot, x, dens, xrange=[-0.25,0.25], charsize=2, yrange=[0,1.2], ystyle=1

   wait, 0.1
      
endfor




end





 









