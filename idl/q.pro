



NList = [16, 32, 64, 128, 256, 512]

BaseList = ["../tests/VortexSheet/K1_64/output_K1", $
            "../tests/VortexSheet/K1_64/output"]

window, xsize=1200, ysize=1000

L1list = dblarr(n_elements(Nlist))


for rep=0, n_elements(Nlist)-1 do begin

   f = BaseList[rep] + "/L1.txt"

   spawn,"wc "+f,result
   lines=long(result)
   lines=lines(0)
    
   da = dblarr(2,LINES)
  
   openr, 1,f
   readf, 1,da
   close,1

   time = da(0,*)
   L1 = da(1,*)

   res = sqrt(time)
    
   
   if rep eq 0 then begin
      plot, res, L1 ,/ylog, /xlog, xrange=[1.0e-1, 10.0], yrange=[0.0000001,0.1]
   endif else begin
      oplot, res, L1  ;, color=255*256L^(rep-1)
   endelse
   
   L1max = max(L1)
   print, L1Max

   L1list[rep] = L1max
   
endfor

goto,ende


plot, Nlist, L1list, psym =4, charsize=2, /xlog,/ylog

fit = linfit(alog(Nlist), alog(L1list)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]







NList = [4, 8];, 16, 32]

BaseList = ["../tests/SoundWaveSerial_K3_4/output", $
            "../tests/SoundWaveSerial_K3_8/output", $
            "../tests/SoundWaveSerial_K3_16/output", $
            "../tests/SoundWaveSerial_K3_32/output"]



L1list = dblarr(n_elements(Nlist))

for rep=0, n_elements(Nlist)-1 do begin

   f = BaseList[rep] + "/L1.txt"

   spawn,"wc "+f,result
   lines=long(result)
   lines=lines(0)
    
   da = dblarr(2,LINES)
  
   openr, 1,f
   readf, 1,da
   close,1

   time = da(0,*)
   L1 = da(1,*)

   L1max = max(L1)
   print, L1Max

   L1list[rep] = L1max
   
endfor


oplot, Nlist, L1list, psym =4

fit = linfit(alog(Nlist), alog(L1list)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]



;;;;;;;;;;;;;;;;;;;;;;;;;;;;



BaseList = ["../tests/SoundWaveSerial_K1_16/output", $
            "../tests/SoundWaveSerial_K1_32/output", $
            "../tests/SoundWaveSerial_K1_64/output", $
            "../tests/SoundWaveSerial_K1_128/output"]

L1list = dblarr(n_elements(Nlist))

for rep=0, n_elements(Nlist)-1 do begin

   f = BaseList[rep] + "/L1.txt"

   spawn,"wc "+f,result
   lines=long(result)
   lines=lines(0)
    
   da = dblarr(2,LINES)
  
   openr, 1,f
   readf, 1,da
   close,1

   time = da(0,*)
   L1 = da(1,*)

   L1max = max(L1)
   print, L1Max

   L1list[rep] = L1max
   
endfor

oplot, Nlist, L1list, psym =4

fit = linfit(alog(Nlist), alog(L1list)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]






ende:

end





 









