
fout = "plots/turb_sequence_sonic_point.eps"

Box = 1.0

Base = "/Users/springel/data/"

frunlist = ["TurbulenceSequence/N512_FV_mach3.2_GPU/data/", $
            "TurbulenceSequence/N1024_FV_mach3.2_GPU/data/" ] ;, $
        ;    "TurbulenceSequence/N512_K1_mach3.2_GPU/data/" ] ;, $
;            "TurbulenceSequence/N1024_K1_mach3.2_GPU/data/"]

TagList = ["N512-FV, mach 3.2", "N1024-FV, mach 3.2", "N512-K1, mach 3.2", "N1024-K1, mach 3.2"]



set_plot,'PS'
!p.font=0

device,/times,/italic,font_index=20
device,xsize=26.0,ysize=14.0

!x.margin=[10,2]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout, /encapsulated, /color

v1=[255,  0,    30,255,0 ,  155, 100,120, 0,  255, 200]
v2=[100, 255,  100,000,255, 155, 200,123, 12,  99, 200]
v3=[0,      0, 255,255,255, 0,   50, 0,   100, 56, 200]


tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

usersym, xc, yc, thick=1.5      ;,/fill



plot, [1], [1], /nodata, /xlog, /ylog , xtitle = "!20k!3", ytitle = "!20k!7!U2!N !20E!7(!20k!7)!3", charsize=1.2, $
      xrange=[2 * !PI/Box / 1.3, 1200], xstyle=1, yrange =[5.0, 100.0],    xthick=2.5, ythick=2.5, ystyle=1



for rep=0, n_elements(frunlist)-1 do begin


   Nspec = 0
   ElistSum =0.0
   
 ;  for Num = 20,32 do begin
  ; for Num = 5,5 do begin

     for Num = 32,127 do begin
 
   
      exts='000'
      exts=exts+strcompress(string(Num),/remove_all)
      exts=strmid(exts,strlen(exts)-3,3)
      
      fname = Base + frunList[rep]+"/powerspec_vel_" +exts+".txt"
      print, fname
      openr, 1, fname
      Time = 0.0D 
      Grid = 0L
      Bins = 0L
      readf, 1, Time
      readf, 1, Grid
      readf, 1, Bins
      print, "Time = ", Time, "  Grid=", Grid
      vel_disp = dblarr(3)
      readf, 1, vel_disp
      da= fltarr(4, bins)
      readf, 1, da
      close,1
      
      print, "vel_disp=",vel_disp
      
      knyquist = 2*!PI/box * Grid/2
      
      K = da(0,*)
      ModePow = da(1,*)
      ModeCount = da(2,*)
      SumPower =  da(3,*)


      print, num, total(ModeCount * ModePow, /double), total(vel_disp)


; we will do a band averaging of the finely binned points, 
; subject to two conditions:
; We want enough modes per bin in order to reduce the variance in a bin,
; and simultaneously, for large k, we don't want the bins become too narrow.
;
; The first condition is set by "MinModeCount",
; the second by "TargetBinNummer", which is used to compute a minimum 
; logarithmic bin-size.


      MinModeCount = 1
      TargetBinNummer = 100

      MinDlogK = (alog10(max(K)) - alog10(min(K)))/TargetbinNummer


      istart=0
      ind=[istart]

      k_list     = [0]
      power_list = [0]
      count_list = [0]

      repeat begin
         count = total(ModeCount(ind))
         deltak =  (alog10(max(K(ind))) - alog10(min(K(ind))))

         if (deltak ge mindlogk) and (count ge MinModeCount) then begin
            d2 = total(SumPower(ind))/total(ModeCount(ind))
            kk = total(K(ind)*ModeCount(ind))/total(ModeCount(ind))

            
            k_list = [k_list, kk]
            power_list = [power_list, d2]
            count_list = [count_list, total(ModeCount(ind))]
            istart = istart + 1
            ind = [istart]
         endif else begin
            istart = istart + 1
            ind = [ind, istart]
         endelse
      endrep until istart ge Bins

      k_list     = k_list(1:*)
      power_list = power_list(1:*)
      count_list = count_list(1:*)

      print, total(power_list * count_list)

      E_list =1.0/2 *  1.0/(2*!PI^2) * k_list^2 * power_list ; convert to energy-spectrum, E(k) = k^2 * P(k)

      

      ElistSum += E_list
      Nspec += 1

   endfor

   E_List = ElistSum / Nspec



   dk = abs(k_list -  3.0 * 2.0*!PI/Box)
   ind = where(dk eq min(dk))
   pivot = ind[0]

   kk = [0.1, 1000]
   
   oplot, Kk, E_list(pivot) * (kk/K_list(pivot))^(-5.0/3)  * kk^2.0, linestyle=2, color =11
   oplot, Kk, E_list(pivot) * (kk/K_list(pivot))^(-2.0)    * kk^2.0, linestyle=1, color =11
   

   oplot, K_list,  E_list * K_List^2, color=1+rep, thick=2.5
   
;  oplot, K_list,  E_list, psym=4, color=1+rep

;  if rep eq n_elements(Frunlist)-1 then begin

                                ; endif

   xyouts, 0.75, 0.93-0.03 * (7-rep), /normal, "!7" + taglist[rep] +"!3", charsize =1.1, color = 1+ rep
   


endfor



oplot, 2.0*!PI/Box *[1,1],[1.0e-20,1.0e30], linestyle=1
oplot, 2.0 * 2.0*!PI/Box *[1,1],[1.0e-20,1.0e30], linestyle=1


;xyouts, 0.26, 0.2, /normal, "!732!U3!N cells!3", charsize =1.1



device,/close
set_plot, "X"
  

end
