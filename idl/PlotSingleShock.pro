
cells = 21


Base = "../tests/SingleShock/K9/" & Order = 10

BaseComp = "../tests/SingleShock/K9_noviscosity/" & Order = 10

pathoutput = "output"





  Num = 200


   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   rho = dblarr(N)
   readu, 1, rho
   smth = dblarr(N)
   readu, 1, smth
   rate = dblarr(N)
   readu, 1, rate
   close, 1


   f= Base + "/" + pathoutput+"/field_1_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   px = dblarr(N)
   readu, 1, px
   close, 1

   
   f= Base + "/" + pathoutput+"/field_2_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   e = dblarr(N)
   readu, 1, e
   close, 1


   f= Base + "/" + pathoutput+"/field_3_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   alpha = dblarr(N)
   readu, 1, alpha
   close, 1


   
   vx = px/rho

   u = e/rho - 0.5 * (px/rho)^2

   x = 1.0*(indgen(N)+0.5)/N


   res = 1.0 / cells / ORDER





   f= BaseComp + "/" + pathoutput+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   rhocomp = dblarr(N)
   readu, 1, rhocomp
   close, 1



   

    
   rhomax = rho[0]
   rhomin = rho[n_elements(rho)-1]


   imax = -1
   imin = -1
   
   for i = n_elements(rho)-1, 0, -1 do begin
      if (imax eq -1) and ( rho[i] ge (rhomin + 0.2 * (rhomax - rhomin))) then begin
         imax = i
      endif
      if (imin eq -1) and ( rho[i] ge (rhomin + 0.8 * (rhomax - rhomin))) then begin
         imin = i+1
         goto, skip
      endif
   endfor
   skip:

   ind = indgen(imax-imin+1) + imin
   










      


    print, "maxium rate=", max(rate),  "minimum rate=", min(rate)
    

set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=12.0

    !x.margin=[6,6]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename= "plots/shockzoom.eps", /encapsulated, /color

     v1=[225, 155, 200, 25, 195, 215, 235, 255, 200]
     v2=[  0, 0,   200, 20, 0, 0, 0, 0, 200 ]
     v3=[  10, 20, 200, 230, 50, 60, 70, 80 , 200]

    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )
    yc= sin(indgen(32)/31.0 * 2*!PI )

    usersym, xc, yc, thick=1.5  ,/fill




    plot, x, rho , /nodata, xrange=[0.4,0.7], xstyle=1, yrange=[0,4], ystyle =1+4, $
          xthick = 2.5, ythick = 2.5, xtitle = "!20x!3", ytitle = "!7density !9r!3"

    axis, yaxis=0, xrange=[0.4,0.7], xstyle=1, yrange=[0,4], ystyle =1, $
          xthick = 2.5, ythick = 2.5, xtitle = "!20x!3", ytitle = "!7density !9r!3"

    for i=0,cells do begin
       oplot, i * 1.0/cells   *[1,1], [1.0e-6,1.0e6], linestyle=1
    endfor

    oplot, x, rhocomp, color =3, thick=2.5
    
    oplot, x, rho, color =1, thick=4.0

    
    

    oplot, [min(x[ind]), max(x[ind])], [max(rho[ind]), min(rho[ind])] , psym= 8, color=2
    
    
    axis, yaxis=1, xrange=[0.4,0.7], xstyle=1, yrange=[0,1], ystyle =1, $
          xthick = 2.5, ythick = 2.5, xtitle = "!20x!3", ytitle = "!7viscosity  !9a!3"

    oplot, x, 4* alpha, color=4, thick= 2.5
    
    device,/close
    set_plot, "X"

    
    ende:
end





 









