
fout = "plots/cost_scaling.eps"

tag ="smooth problem in 3D"


set_plot,'PS'
!p.font=0

device,/times,/italic,font_index=20
device,xsize=20.0,ysize=16.0

!x.margin=[8,3]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout, /encapsulated, /color

v1=[255,  0,    30,255,0 ,  155, 100,120, 0,  255, 200]
v2=[100, 255,  100,000,255, 155, 200,123, 12,  99, 200]
v3=[0,      0, 255,255,255, 0,   50, 0,   100, 56, 200]


tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )
yc= sin(indgen(32)/31.0 * 2*!PI )

usersym, xc, yc, thick=1.5, /fill

symlist = [4, 5, 8, 6, 2] 

plot, [1],[1], /nodata, xrange=[10,1.0e20], yrange=[1.0e-15,1.0e1], xstyle=1, ystyle=1, /xlog,/ylog, $
      xtitle = "!7computational cost!3", ytitle = "!20L!7!D1!N error!3", charsize=1.2, xthick=3.5, ythick=3.5

prefac = 1

for k=0, 9 do begin

   N = 10.0^((indgen(300))/299.0 * 5)
   dof = (1.0+k)*(2.0+k)*(3.0+k) / 6 * N^3
   cost = 5.0/6 * N^4 * (1.0+k)^4 * (10.0*k^4.0 + 100.0 * k^3.0 + 315.0*k^2+ 523.0 * k + 372.0)

   if k gt 0 then begin
      prefac = interpol(L1_old, dof_old, dof[0])
   endif
   
   L1 = prefac * N^(-(1.0+k))
  
   L1_old = L1
   dof_old = dof

   
   oplot, cost, L1, color = 1+k, thick=4


   N = [1.0, 8.0, 64.0, 512.0, 4096.0]

   cost = 5.0/6 * N^4 * (1.0+k)^4 * (10.0*k^4.0 + 100.0 * k^3.0 + 315.0*k^2+ 523.0 * k + 372.0)

   L1 = prefac * N^(-(1.0+k))

   for i=0,n_elements(cost)-1 do begin
      oplot, [cost[i]], [L1[i]], psym= symlist[i], color = 1+k, symsize=1.2

      if k eq 0 then begin
         xyouts, 0.34, 0.55-0.04*i , /normal, "!20N!7 = " + strcompress(string(long(N[i])), /remove_all) + "!3", charsize=1.1
         plots, [0.32], [0.56-0.04*i], /normal, psym= symlist[i], symsize=1.2
      endif
   endfor

   xyouts, 0.2, 0.55-0.04*k , /normal, "!20k!7 = " + string(k, format='(I1)') + "!3", color = 1+k, charsize=1.1
endfor


xyouts, 0.6, 0.85, /normal, "!7"+ tag  +"!3", charsize=1.3



device,/close
set_plot, "X"
  

end
