
cells = 21





Base = "../tests/SingleShock/K9_noviscosity/" & Order = 10


Base = "../tests/SingleShock/K0/" & Order = 1

;Base = "../tests/SingleShock/K1/" & Order = 2

;Base = "../tests/SingleShock/K2/" & Order = 3

;Base = "../tests/SingleShock/K3/" & Order = 4

;Base = "../tests/SingleShock/K4/" & Order = 5

;Base = "../tests/SingleShock/K5/" & Order = 6

;Base = "../tests/SingleShock/K6/" & Order = 7

;Base = "../tests/SingleShock/K7/" & Order = 8

;Base = "../tests/SingleShock/K8/" & Order = 9

Base = "../tests/SingleShock/K9/" & Order = 10

Base = "../tests/SingleShock/K9_GPU/" & Order = 10 & cells = 42




pathoutput = "output"



ArtViscParameter = 1.0
ArtViscOnset = 0.00001

window, xsize = 1600, ysize=600

for   Num = 0, 10000 do begin


   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   rho = dblarr(N)
   readu, 1, rho
   smth = dblarr(N)
   readu, 1, smth
   rate = dblarr(N)
   readu, 1, rate
   close, 1


   f= Base + "/" + pathoutput+"/field_1_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   px = dblarr(N)
   readu, 1, px
   close, 1

   
   f= Base + "/" + pathoutput+"/field_2_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   e = dblarr(N)
   readu, 1, e
   close, 1


   f= Base + "/" + pathoutput+"/field_3_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   alpha = dblarr(N)
   readu, 1, alpha
   close, 1


   
   vx = px/rho

   u = e/rho - 0.5 * (px/rho)^2

   x = 1.0*(indgen(N)+0.5)/N

GAMMA = 5.0/3
S0 =0.022
sfull  = S0 / (ORDER * ORDER);
sonset = ArtViscOnset * sfull
res = 1.0 / cells / ORDER

cs = sqrt(GAMMA * (GAMMA-1) * u)

visc = dblarr(N)

ind = where((smth gt sonset) and (smth lt sfull))

if ind(0) ne -1 then begin
visc[ind] = ArtViscParameter * (cs[ind] + abs(vx[ind])) * res * alog(smth[ind] / sonset) / alog(sfull / sonset)
endif

ind = where((smth ge sfull))

if ind(0) ne -1 then begin
visc[ind] = ArtViscParameter * (cs[ind] + abs(vx[ind])) * res 
endif



    plot, x, rho, xrange=[0.0,1], charsize=2, xstyle=1

;plot, x, vx, xrange=[0.0,1], charsize=2, xstyle=1

;    plot, x, u, xrange=[0.0,1], charsize=2, xstyle=1

oplot, x, alpha*4, color=255+100*256L


;oplot, x, alog10(smth) + 6, color =255 + 255*256L^2

ind = where(rate gt 1.0e-7)

if ind(0) ne -1 then begin
   oplot, x[ind], rate[ind] * 1.0e4, color =255 + 255*256L^2
endif


;oplot, x, visc * 40, color=255
    
  ;  plot, x, u, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog

 ;   plot, x, e, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog
     
 ;     plot, x, vx, xrange=[0.12,0.18], charsize=2, xstyle=1


;plot, x, u*rho, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog
    
                                ; plot, x, smth, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog

 ;  plot, x, smth, xrange=[0.,1], charsize=2, xstyle=1,/ylog
   
;     oplot, [0,2], S * onset *[1,1], linestyle=1, color=255
      
    for i=0,cells do begin
       oplot, i * 1.0/cells   *[1,1], [1.0e-6,1.0e6], linestyle=1
    endfor


    print, "maxium rate=", max(rate),  "minimum rate=", min(rate)
    
   wait, 0.1
endfor

    ende:
end





 









