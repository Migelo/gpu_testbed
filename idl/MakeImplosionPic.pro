


BaseList  =[ "/u/vrs/Coding/gpu_testbed/tests/ImplosionTest/N400_K2/output/", $
             "/u/vrs/Coding/gpu_testbed/tests/ImplosionTest/N400_FV/output/"]

OutList = ["implosion_N400_K2.jpg", "implosion_N400_FV.jpg"]

mi = 0.2
ma = 1.5

BoxSize = 0.3

loadct, 15
tvlct, r, g, b, /get

openr,1,"rainbow.clt"
readf,1,r
readf,1,g
readf,1,b
close,1

for rep = 0, n_elements(BaseList)-1 do begin

for num = 50, 50 do begin

   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= BaseList[rep] + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0d
  readu, 1, ti
  print, "time=", ti
  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  print, "min(dens)=", min(dens), "   max(dens)=", max(dens)
   dens= transpose(dens)

   colindex= (dens - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   Pic=bytarr(N, N, 3)

   pic(*,*,0) = r(colindex)
   pic(*,*,1) = g(colindex)
   pic(*,*,2) = b(colindex)



   if (num eq 0) or (num eq 63) then begin
      window, xsize= N, ysize= N
   endif
   
   tv, Pic, true=3,0

                                ; plot, dens(*, N-100), yrange=[0,1.2]

;    plot, dens(N-100, *), yrange=[0,1.2]
   

   wait, 0.05

   fname = "plots/" + Outlist[rep]
   write_jpeg, fname, pic, true=3, quality=98
      
endfor
endfor


end





 









