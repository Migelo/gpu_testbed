
window, xsize = 800, ysize = 800

for num = 0, 1000 do begin

  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)

  
  f= "output/density_" + exts + ".txt"
  openr, 1, f
  N = 0L
  readf, 1, N
  da = dblarr(N, N)
  readf, 1, da
  close, 1

  plot, da(*, N/2 )
  
  wait, 0.05

endfor

end


