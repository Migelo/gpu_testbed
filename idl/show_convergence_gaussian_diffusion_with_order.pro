



NList = ["Res8"]
XList = [8]

BaseList = ["../tests/GaussianDiffusion/Serial_K0/", $
            "../tests/GaussianDiffusion/Serial_K1/", $
            "../tests/GaussianDiffusion/Serial_K2/", $
            "../tests/GaussianDiffusion/Serial_K3/", $
            "../tests/GaussianDiffusion/Serial_K4/", $
            "../tests/GaussianDiffusion/Serial_K5/", $
            "../tests/GaussianDiffusion/Serial_K6/", $
            "../tests/GaussianDiffusion/Serial_K7/", $
            "../tests/GaussianDiffusion/Serial_K8/", $
            "../tests/GaussianDiffusion/Serial_K9/"]

OrderList = lindgen(n_elements(BaseList))

window, xsize=1000, ysize=800

plot, [0],[0], /nodata, /ylog, /xlog, xrange=[7.0e-1, 10.0], yrange=[0.000000001,0.1], charsize=2, xstyle=1


L1list = dblarr(n_elements(baseList), n_elements(Nlist))

for run = 0, n_elements(BaseList)-1 do begin
   for rep=0, n_elements(Nlist)-1 do begin

      f = BaseList[run] + Nlist[rep] + "/output/L1.txt"

      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)+1.0
      L1   = da(1,*)
      
      oplot, time, L1  , color=255*256L^(rep mod 3), linestyle=run, thick=3
      
      
      L1max = L1[n_elements(L1)-1]
      print, L1Max

      L1list[run,rep] = L1max
      
   endfor
endfor


plot, [0],[0], /nodata, /ylog, xrange=[0, 11], yrange=[1.0e-7,0.4], charsize=2, xstyle=1



oplot, 1 + Orderlist, L1list[*,0], psym =4, color=255*256L^(rep mod 3), thick=3
oplot, 1 + Orderlist, L1list[*,0], color=255*256L^(rep mod 3), linestyle=run, thick=3
   

   fit = linfit(Orderlist, alog(L1list[*,0])) 

   xx =[-1,20]

   yy = exp(  fit[1]*xx + fit[0])

   oplot, 1 + xx, yy

   print, "slope= ", fit[1]

end





 









