


BaseList = ["../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K1_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K2_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K4_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K5_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K6_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K8_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K9_Re5/"]





BaseList = ["../tests/KelvinHelmholtz_Lecoanet_nojump/N128_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N256_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N512_K1_Re5/"]

BaseList = ["../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N128_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet_nojump/N256_K1_Re5/"]


L2List = dblarr(n_elements(BaseList))

for rep = 0,n_elements(BaseList)-1 do begin

   Base = BaseList[rep]
   
outdir = "output"

mi = 0.0
ma = 1.0

BoxSize = 2.0

loadct, 70
tvlct, r, g, b, /get



fname = "/ptmp/mpa/vrs/Simulations/Lecoanet/5_2048_1_4.h5"

file_id = h5f_open(fname)
dataset_id= h5d_open(file_id, 'tasks/c')
dataspace_id = H5D_GET_SPACE(dataset_id)
datatype_id = H5D_GET_TYPE(dataset_id)
idltype = H5T_IDLTYPE(Datatype_id)
dimensions = H5S_GET_SIMPLE_EXTENT_DIMS(dataspace_id)

dims = [6144, 3072, 2]
start = [0, 0, 0]
count = [6144,  3072, 1]
Data  = read_dataslab(file_id, 'tasks/c', dims, start, count)
h5d_close, dataset_id
h5f_close, file_id

dedalus = dblarr(6144, 3072)
dedalus(*,*) = data[*,*,0]

dedalus = transpose(dedalus)

;dedalus  = rebin(dedalus, 1024, 2048)

dedalus  = congrid(dedalus, 1024, 2048)


for num = 40, 40 do begin

   
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
  
   f= Base + "/" + outdir + "/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, Ti
   dens = dblarr(N, N)
   readu, 1, dens
   close, 1
   
   f= Base + "/" + outdir + "/field_4_"  + exts + ".dat"
  
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, Ti
   dyedens = dblarr(N, N)
   readu, 1, dyedens
   close, 1
   
   
   
   d = dyedens / dens
   
   ;print, num, "time=", ti, "  minimum dye=", min(d), "  maximum dye=",  max(d)
  
  
   d= transpose(d)


   d= d[0:1023, *]



   
   L2 = sqrt(total( (dedalus - d)^2, /double) / n_elements(dedalus))

   print, "num=", num, "  L2 =", L2

   L2List[rep] = L2

   
   dedalus  = rebin(dedalus, 512, 1024)
   d        = rebin(d, 512, 1024)
   
   
   colindex= (d - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   N = 1024
   
   Pic = bytarr(N, N, 3)

   pic(0:511,*,0) = r(colindex)
   pic(0:511,*,1) = g(colindex)
   pic(0:511,*,2) = b(colindex)



   d = dedalus
   colindex= (d - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   pic(512:*,*,0) = r(colindex)
   pic(512:*,*,1) = g(colindex)
   pic(512:*,*,2) = b(colindex)



   window, xsize= N, ysize= N
   
   
   tv, Pic, true=3,0

   

   wait, 0.05

   fname = Base + "/output/comp_pic_"+exts+".jpg"
   write_jpeg, fname, pic, true=3, quality=98
      
endfor

endfor

p = lindgen(n_elements(baseList))+1

Plot, P, L2List,/ylog

ende:
end





 









