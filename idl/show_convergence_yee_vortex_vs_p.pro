


NList = [16, 32, 64, 128]
KList = [0, 1, 2, 3, 4, 5, 6, 7] 


window, xsize=1200, ysize=1000

plot, [1],[1], /nodata, xrange=[-0.1, 7.1], xstyle=1, yrange=[1.0e-11, 1.0e-1], ystyle =1, charsize=2, /ylog

for rep=0, n_elements(Nlist)-1 do begin

L1list = dblarr(n_elements(Klist))

for K=0, n_elements(KList)-1 do begin
   

   f = "../tests/YeeVortex/Serial_K" + string(Klist[K]) +"/Res" + string(NList[rep]) + "/output/L1.txt"
  
   
      f = strcompress(f, /remove_all)
      
      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)
      L1 = da(1,*)

      L1max = max(L1)
      print, L1Max

      L1list[K] = L1max

endfor


   ind = where(L1List ge 5.74e-11)
   Klist = Klist(ind)
   L1list = L1list(ind)

oplot, Klist, L1list, psym =4
oplot, Klist, L1list

endfor

ende:

end





 









