
fout = "plots/density_PDF_1024.eps"


frunlist =["/u/vrs/Coding/gpu_testbed/tests/TurbulenceSequence/N1024_K1_mach3.2_GPU/",  $
           "/u/vrs/Coding/gpu_testbed/tests/TurbulenceSequence/N1024_FV_mach3.2_GPU/"]


TagList = ["N1024 - K1 - Mach 3.2", $
           "N1024 - FV - Mach 3.2"]


Nbins = 60

mi = 0.001
ma = 1000.0


set_plot,'PS'
!p.font=0

device, /times, /italic, font_index=20
device, xsize=20.0, ysize=16.0

!x.margin=[10,2]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout, /encapsulated, /color

v1=[255,  30,  200,170, 140 , 110, 100,120, 0,  255, 200]
v2=[0,    0,  0, 0, 0  , 0,   50, 0,   100, 56, 200]
v3=[100, 230,  160,190, 220, 255, 200,123, 12,  99, 200]


tvlct,v1,v2,v3,1

xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

usersym, xc, yc, thick=1.5      ;,/fill



plot, [1], [1], /nodata, /xlog, xtitle = "!9r!3", ytitle = "!7d!D !N!20p!7 / dlog !9r!3", charsize=1.2, $
      xrange=[0.01,100], xstyle=1, yrange =[0.01,3.0],    xthick=2.5, ythick=2.5, ystyle=1, /ylog 


oplot, [1,1], [1.0e-6, 10], linestyle=2


for rep = 0, n_elements(frunlist)-1 do begin


   count = lon64arr(Nbins)
   counttot = 0.0D


   for Num = 20, 56 do begin

      exts='0000'
      exts=exts+strcompress(string(Num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      
      f= frunlist[rep] + "data/field_0_"  + exts + ".dat"
      print, f
      
      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0d
      readu, 1, ti
      print, "time=", ti
      dens = dblarr(N, N)
      readu, 1, dens
      close, 1

      print,n_elements(dens)
      
      bin = (alog(dens) - alog(mi)) / alog(ma/mi) * Nbins
      
      for i = 0L, n_elements(dens)-1 do begin

         if (bin[i] ge 0) and (bin[i] lt Nbins) then begin
            count[long(bin[i])] += 1
         endif
         
      endfor

      counttot += n_elements(dens)
      
   endfor

   count /= counttot
   count /= (alog10(ma) - alog10(mi))/Nbins


   x = exp((indgen(Nbins)+0.5)/Nbins * (alog(ma) - alog(mi)) + alog(mi))


   oplot, x, count , color=1+rep, thick=4.0


   xyouts, 0.65, 0.88-0.04 * rep, /normal, "!7" + taglist[rep] +"!3", charsize =1.18, color = 1+ rep
   
endfor

device,/close
set_plot, "X"


end
