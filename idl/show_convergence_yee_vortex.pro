

NList = [16, 32, 64, 128]
KList = [0, 1, 2, 3, 4, 5, 6, 7]

window, xsize=1200, ysize=1000

plot, [1],[1], /nodata, xrange=[10.0, 300], xstyle=1, yrange=[1.0e-11, 1.0e-1], ystyle =1, charsize=2, /xlog,/ylog

for K=0, n_elements(KList)-1 do begin
   
   L1list = dblarr(n_elements(Nlist))

   for rep=0, n_elements(Nlist)-1 do begin


      f = "../tests/YeeVortex/Serial_K" + string(Klist[K]) +"/Res" + string(NList[rep]) + "/output/L1.txt"
      
      f = strcompress(f, /remove_all)
      
      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)

      print, f
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)
      L1 = da(1,*)

   ;   print, L1
      
      L1max = max(L1)
    
      L1list[rep] = L1max

   endfor

   ind = where(L1List ge 5.74e-11)
   Nlist = Nlist(ind)
   L1list = L1list(ind)
   
   print, L1list
   
   oplot, Nlist, L1list, psym =4, thick=3
   oplot, Nlist, L1list, thick=3

   fit = linfit(alog(Nlist), alog(L1list)) 
   xx =[1,1000]
   yy = exp(  fit[1]*alog(xx) + fit[0])
   oplot, xx, yy, color=255

   
   print, -(klist[k]+1), " slope= ", fit[1]

   
endfor




ende:

end





 









