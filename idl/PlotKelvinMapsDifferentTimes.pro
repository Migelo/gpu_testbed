


BaseList = ["../tests/KelvinHelmholtz_Lecoanet/N512_K1_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N256_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N512_K1_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N256_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N512_K1_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N256_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K7_Re5/"]

            

NumList = [30, 30, 14, 60, 32, 14, 90, 32, 14]

LabelList = ["N = 512, p = 2", "N = 256, p = 4", "N = 128, p = 8", $
             "N = 512, p = 2", "N = 256, p = 4", "N = 128, p = 8", $
             "N = 512, p = 2", "N = 256, p = 4", "N = 128, p = 8"]

Nrow = 3
Ncol = 3

Npix = 512L


gap = 8L

Pic=bytarr(Npix*Ncol + (Ncol-1)*gap, Npix *Nrow+ (nrow-1)*gap, 3)
Pic(*,*, *)= 255


outdir = "output"

mi = 0.0
ma = 1.0

BoxSize = 2.0

loadct, 70
tvlct, r, g, b, /get


cmd = 'mogrify -font '
cmd = cmd + """
cmd += "/System/Library/Fonts/Supplemental/Times New Roman Italic.ttf"
cmd = cmd + """
cmd += ' -fill white '



for row = 0,Nrow-1 do begin
   for col = 0,Ncol-1 do begin


      offx = (Npix + Gap)*col 
      offy = (Npix + Gap)*row

      offx += 40 
      offy += 50


      
      rep = row * 3 + col

      Num = NumList[rep]
      
   
      print, num
   
      exts='0000'
      exts=exts+strcompress(string(num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      
      f= BaseList[rep] + "/" + outdir + "/field_0_"  + exts + ".dat"

      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0D
      readu, 1, Ti
      dens = dblarr(N, N)
      readu, 1, dens
      close, 1

      f= BaseList[rep] + "/" + outdir + "/field_4_"  + exts + ".dat"
      
      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0D
      readu, 1, Ti
      dyedens = dblarr(N, N)
      readu, 1, dyedens
      close, 1
      
      d = dyedens / dens

      print, f
      print, num, "time=", ti, "  minimum dye=", min(d), "  maximum dye=",  max(d)
      print
  
      d= transpose(d)

      colindex= (d - mi)/(ma-mi)*255.0
      ind = where(colindex ge 256.0)
      if ind(0) ne -1 then colindex(ind) = 255.9
      ind = where(colindex lt 0)
      if ind(0) ne -1 then colindex(ind) = 0
      colindex = byte(colindex)
      

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 0] = r[ colindex[0:Npix-1, Npix:2*Npix-1  ]]

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 1] = g[ colindex[0:Npix-1, Npix:2*Npix-1  ]]

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 2] = b[ colindex[0:Npix-1, Npix:2*Npix-1  ]]


     cmd = cmd + ' -pointsize 36  -draw '
     cmd = cmd + "'"
     cmd = cmd + 'text '+ string(offx)+","+string(offy)+" "
     cmd = cmd + """
     cmd = cmd + LabelList[rep] + ', t = ' + string(ti, format = "(F4.1)")
     cmd = cmd + """
     cmd = cmd + "'"
   

      
endfor
endfor

print

   fname = "KH_maps_different_times.jpg"
   write_jpeg, "plots/" + fname, pic, true=3, quality=98


 

   cmd = cmd + ' -quality 95 '
   cmd = cmd + fname 

   print, cmd
;   spawn, cmd


   
end





 









