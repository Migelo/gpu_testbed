compile_opt idl2

Base = '/u/vrs/Coding/gpu_testbed/tests/SupersonicTurbulence/N32_K1/output/'

mi = 0.2
ma = 3.0

loadct, 15
tvlct, r, g, b, /get

openr, 1, 'rainbow.clt'
readf, 1, r
readf, 1, g
readf, 1, b
close, 1

count = 0l

for num = 0, 9000, 10 do begin
  print
  print, num

  exts = '0000'
  exts = exts + strcompress(string(num), /remove_all)
  exts = strmid(exts, strlen(exts) - 4, 4)

  f = Base + '/field_0_' + exts + '.dat'

  openr, 1, f
  N = 0l
  readu, 1, N
  ti = 0.0d
  readu, 1, ti
  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  print, 'Time=', ti, '  ma=', max(dens), '  mi=', min(dens)

  dens = transpose(dens)

  colindex = (dens - mi) / (ma - mi) * 255.0
  ind = where(colindex ge 256.0)
  if ind[0] ne -1 then colindex[ind] = 255.9
  ind = where(colindex lt 0)
  if ind[0] ne -1 then colindex[ind] = 0
  colindex = byte(colindex)

  Pic = bytarr(N, N, 3)

  Pic[*, *, 0] = r[colindex]
  Pic[*, *, 1] = g[colindex]
  Pic[*, *, 2] = b[colindex]

  if num eq 0 then begin
    window, xsize = N, ysize = N
  endif

  tv, Pic, true = 3, 0

  exts = '0000'
  exts = exts + strcompress(string(count), /remove_all)
  exts = strmid(exts, strlen(exts) - 4, 4)
  count++

  fname = Base + '/pic_' + exts + '.jpg'
  write_jpeg, fname, Pic, true = 3, quality = 98
endfor

end