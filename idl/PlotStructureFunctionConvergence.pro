compile_opt idl2
Box = 1.0

Base = '/ptmp/mpa/vrs/Simulations/gpu_testbed/'

frunlist = ['N1024_K1_mach3.2_GPU/', 'N1024_FV_mach3.2_GPU/', 'N512_K1_mach3.2_GPU/', 'N512_FV_mach3.2_GPU/']
TagList = ['N1024 - K1 - Mach 3.2', 'N1024 - FV - Mach 3.2', 'N512 - K1 - Mach 3.2', 'N512 - FV - Mach 3.2']

ThickList = [6.0, 3.0, 4, 3]

Nspec = 0
SfSum = 0.0

NumBeg = 16
NumEnd = 32


for Num = NumBeg, NumEnd do begin
  exts = '0000'
  exts = exts + strcompress(string(Num), /remove_all)
  exts = strmid(exts, strlen(exts) - 4, 4)

  fname = Base + frunlist[0] + '/velocity_structure_function_' + exts + '.dat'
  print, fname
  openr, 1, fname
  Bins = 0l
  readf, 1, Bins
  Ti = 0.0
  Mach = 0.0
  readf, 1, Ti
  readf, 1, Mach
  da = dblarr(2, Bins)
  readf, 1, da
  close, 1

  print, 'ti=', Ti, '   Mach=', Mach

  R = da[0, *]
  SF = da[1, *]

  SfSum += SF
  Nspec += 1
endfor

SfSum /= Nspec
Mach = sqrt(SfSum / 2)

ls = interpol(R, Mach, 1.0)
print, 'ls = ', ls

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

set_plot, 'PS'
!p.font = 0

device, /times, /italic, font_index = 20
device, xsize = 16.0, ysize = 14.0

!x.margin = [10, 3]
!p.thick = 2.5
!p.ticklen = 0.03

device, filename = 'plots/structure_func_convergence.eps', /encapsulated, /color

v1 = [255, 0, 30, 255, 0, 155, 100, 120, 0, 255]
v2 = [100, 155, 100, 000, 255, 155, 200, 123, 12, 99]
v3 = [0, 0, 255, 255, 255, 0, 50, 0, 100, 56]

tvlct, v1, v2, v3, 1

xc = cos(indgen(32) / 31.0 * 2 * !pi) * 0.4
yc = sin(indgen(32) / 31.0 * 2 * !pi) * 0.4

usersym, xc, yc, thick = 1.5 ; ,/fill

plot, [1], [1], /nodata, /xlog, xtitle = '!20l!7!3', ytitle = '!7 log [ (!20l!7 / !20l!7!Ds!N)!U-1/2!N !20M!7(!20l!7) ]!3', charsize = 1.2, $
  xrange = [0.001, 1.0], xstyle = 1, yrange = alog10([0.75, 1.14]), xthick = 2.5, ythick = 2.5, ystyle = 1, /noerase ; , $
; yticks = 3, ytickv = [0.8, 0.9, 1.0, 1.1]

oplot, 1.0 / 6 * [1, 1], alog10([0.001, 1000]), linestyle = 2
oplot, 1.0 / 2 * [1, 1], alog10([0.001, 1000]), linestyle = 2
oplot, ls * [1, 1], alog10([0.001, 1000]), linestyle = 1
oplot, [0.001, 1000], alog10([1, 1]), linestyle = 1

for rep = n_elements(frunlist) - 1, 0, -1 do begin
  Nspec = 0
  SfSum = 0.0
  for Num = NumBeg, NumEnd do begin
     exts = '0000'
    exts = exts + strcompress(string(Num), /remove_all)
    exts = strmid(exts, strlen(exts) - 4, 4)

    fname = Base + frunlist[rep] + '/velocity_structure_function_' + exts + '.dat'
    print, fname
    openr, 1, fname
    Bins = 0l
    readf, 1, Bins
    Ti = 0.0
    Mach = 0.0
    readf, 1, Ti
    readf, 1, Mach
    da = dblarr(2, Bins)
    readf, 1, da
    close, 1

    print, 'ti=', Ti, '   Mach=', Mach

    R = da[0, *]
    SF = da[1, *]

    SfSum += SF
    Nspec += 1
  endfor

  SfSum /= Nspec
  Mach = sqrt(SfSum / 2)

  oplot, R, alog10(Mach * (R / ls) ^ (-0.5)), color = 1 + rep, thick = ThickList[rep]

  xyouts, 0.5, 0.4 - rep * 0.05, /normal, '!7' + TagList[rep] + '!3', charsize = 1.1, color = 1 + rep
endfor

device, /close
set_plot, 'X'
end
