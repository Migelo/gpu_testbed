

Base  = "../tests/DoubleBlast/Serial_K1/Res20000/"
BaseTune = "../tests/DoubleBlast/Tune_K2/"

Num = 38


BoxSize=1.0
pathoutput1 = "output_1.0"

print, num

exts='0000'
exts=exts+strcompress(string(num),/remove_all)
exts=strmid(exts,strlen(exts)-4,4)

f= Base + "/" + pathoutput1+"/field_0_"  + exts + ".dat"

openr, 1, f
N = 0L
readu, 1, N
ti =0.0D
readu, 1, ti
print, "Time=", ti
dens = dblarr(N)
readu, 1, dens
smth = dblarr(N)
readu, 1, smth
close, 1

x = 2.0*(indgen(N)+0.5)/N

L1_best = 1.0e5


for rep=0, 159 do begin

;for rep=153, 153 do begin
   
   plot, [-100],[0], psym=3 , xrange=[0.4,1.0], yrange=[0,7], ystyle=1, xstyle = 1
   
   oplot, x, dens


   f= BaseTune + "/output" + strcompress(string(rep),/remove_all) + "/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   readu, 1, ti
  ; print, "Time=", ti
   densT = dblarr(N)
   readu, 1, densT
   smth = dblarr(N)
   readu, 1, smth
   close, 1

   openr,1, BaseTune + "/output" + strcompress(string(rep),/remove_all) + "/visc.txt"
   visc_onset = 0.0D
   visc_param = 0.0D
   readf,1,visc_onset
   readf,1,visc_param
   close,1
   
    oplot, x, densT, color=255, thick=3.0

    ind = where((x ge 0.4) and (x lt 1.0))

    L1 = total((dens[ind] - densT[ind])^2, /double) / n_elements(ind)

    print, rep, visc_onset, visc_param, L1

    if L1 lt L1_best then begin
       L1_best = L1
       rep_best = rep
       visc_onset_best  = visc_onset
       visc_param_best = visc_param
    endif
  ;  wait, 1.0
    
endfor

print, "rep_best = ", rep_best

end





 









