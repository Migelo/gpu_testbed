



NList = ["Res8", "Res16", "Res32", "Res64", "Res128"]
XList = [8, 16, 32, 64, 128]

BaseList = ["../tests/GaussianDiffusion/Serial_K0/", $
            "../tests/GaussianDiffusion/Serial_K1/", $
            "../tests/GaussianDiffusion/Serial_K2/", $
            "../tests/GaussianDiffusion/Serial_K3/", $
            "../tests/GaussianDiffusion/Serial_K4/"]

window, xsize=1000, ysize=800

plot, [0],[0], /nodata, /ylog, /xlog, xrange=[7.0e-1, 10.0], yrange=[0.000000001,0.1], charsize=2, xstyle=1


L1list = dblarr(n_elements(baseList), n_elements(Nlist))



for run = 0, n_elements(BaseList)-1 do begin
   for rep=0, n_elements(Nlist)-1 do begin

      f = BaseList[run] + Nlist[rep] + "/output/L1.txt"

      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)+1.0
      L1   = da(1,*)
      
      oplot, time, L1  , color=255*256L^(rep mod 3), linestyle=run, thick=3
      
      
      L1max = L1[n_elements(L1)-1]
      print, L1Max

      L1list[run,rep] = L1max
      
   endfor
endfor

;goto,ende


plot, [0],[0], /nodata, /ylog, /xlog, xrange=[1, 300], yrange=[0.0000000001,0.1], charsize=2, xstyle=1

for run = 0, n_elements(BaseList)-1 do begin
   
   oplot, Xlist, L1list[run,*], psym =4, color=255*256L^(rep mod 3), thick=3
   oplot, Xlist, L1list[run,*], color=255*256L^(rep mod 3), linestyle=run, thick=3
   

   fit = linfit(alog(xlist), alog(L1list[run,*])) 

   xx =[1,1000]

   yy = exp(  fit[1]*alog(xx) + fit[0])

   oplot, xx, yy

   print, "slope= ", fit[1]
   
endfor

goto,ende


oplot, Nlist, L1list, psym =4

fit = linfit(alog(Nlist), alog(L1list)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]



;;;;;;;;;;;;;;;;;;;;;;;;;;;;



BaseList = ["../tests/SoundWaveSerial_K1_16/output", $
            "../tests/SoundWaveSerial_K1_32/output", $
            "../tests/SoundWaveSerial_K1_64/output", $
            "../tests/SoundWaveSerial_K1_128/output"]

L1list = dblarr(n_elements(Nlist))

for rep=0, n_elements(Nlist)-1 do begin

   f = BaseList[rep] + "/L1.txt"

   spawn,"wc "+f,result
   lines=long(result)
   lines=lines(0)
    
   da = dblarr(2,LINES)
  
   openr, 1,f
   readf, 1,da
   close,1

   time = da(0,*)
   L1 = da(1,*)

   L1max = max(L1)
   print, L1Max

   L1list[rep] = L1max
   
endfor

oplot, Nlist, L1list, psym =4

fit = linfit(alog(Nlist), alog(L1list)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]






ende:

end





 









