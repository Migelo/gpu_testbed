
cells =  200

Base = "../tests/DoubleBlast/Serial_K7/Res200/"
pathoutput1 = "output_1.0"


onset = 0.001
S = 0.022 / (4.0)^2


window, xsize = 1600, ysize=600

for   Num = 500, 10000 do begin


   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput1+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   rho = dblarr(N)
   readu, 1, rho
   smth = dblarr(N)
   readu, 1, smth
   close, 1


   f= Base + "/" + pathoutput1+"/field_1_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   px = dblarr(N)
   readu, 1, px
   close, 1

   
   f= Base + "/" + pathoutput1+"/field_2_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti = 0.0D
   readu, 1, ti
   print, ti
   e = dblarr(N)
   readu, 1, e
   close, 1

   vx = px/rho

   u = e/rho - 0.5 * (px/rho)^2

   x = 2.0*(indgen(N)+0.5)/N

   print, "min(u)=", min(u)

 ;   plot, x, rho, xrange=[0.0,1], charsize=2, xstyle=1

    plot, x, u, xrange=[0.1,0.3], charsize=2, xstyle=1,/ylog

 ;   plot, x, e, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog
     
 ;     plot, x, vx, xrange=[0.12,0.18], charsize=2, xstyle=1


;plot, x, u*rho, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog
    
                                ; plot, x, smth, xrange=[0.12,0.18], charsize=2, xstyle=1,/ylog

 ;  plot, x, smth, xrange=[0.,1], charsize=2, xstyle=1,/ylog
   
;     oplot, [0,2], S * onset *[1,1], linestyle=1, color=255
      
    for i=0,cells do begin
       oplot, i * 2.0/cells   *[1,1], [1.0e-6,1.0e6], linestyle=1
    endfor
       
    
   wait, 0.1
endfor

    ende:
end





 









