

orderList = [1, 2, 3, 4, 5]

BaseList = ["../tests/ShockTube1D/Serial_K1/",  $
            "../tests/ShockTube1D/Serial_K2/",  $
            "../tests/ShockTube1D/Serial_K3/",  $
            "../tests/ShockTube1D/Serial_K4/",  $
            "../tests/ShockTube1D/Serial_K5/"]


colorlist = [255, 255*256L, 255*256L^2, 255 + 255*256L, 255 + 255*256L^2]

window, xsize=1500, ysize=1000

smmed = dblarr(n_elements(BaseList))


for rep=0,n_elements(baselist)-1 do begin

   Base = BaseList[rep]

   sm = dblarr(100)

   
for num = 0, 99 do begin

   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/output/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1

   sm[num] = max(smth)
endfor


smmed[rep] = median(sm)

if rep eq 0 then begin

   plot, sm, psym=4, /ylog, yrange=[0.0001, 1.0], charsize=2.0,xtitle ="time", ytitle = "max(smoothness indicator)" 
   
endif


   oplot, sm, psym=4, color = colorlist[rep]

 ;  S0 = 0.1 * double(orderList[rep])^4

S0 = smmed[rep]
   
   oplot, [0,1000], [1.0, 1.0] * S0, color = colorlist[rep]
   
endfor


goto,ende
wait, 2.0

plot, orderList, smmed, psym=6 , /xlog, /ylog, xrange=[0.6, 10], xstyle=1, charsize=2.0,xtitle ="order", ytitle = "< max(smoothness indicator) >" 

fit = linfit(alog(orderlist), alog(smmed))

y = exp(fit[1] * alog(orderlist) + fit[0])

oplot, orderlist, y

S0 = 0.022 * orderlist^(-2.0)

oplot, orderlist, S0, thick=3.0



ende:

end





 









