compile_opt idl2
Box = 1.0

Base = '/ptmp/mpa/vrs/Simulations/gpu_testbed/'


frunlist = ['N1024_K1_mach3.2_GPU/'] ; , 'N512_FV_mach3.2_GPU/']
TagList = ['N1024 - K1 - Mach 3.2'] ; , 'N512 - FV - Mach 3.2']

set_plot, 'PS'
!p.font = 0

device, /times, /italic, font_index = 20
device, xsize = 18.0, ysize = 20.0

!x.margin = [10, 2]
!p.thick = 2.5
!p.ticklen = 0.03

device, filename = 'plots//powerspec_example.eps', /encapsulated, /color

v1 = [255, 0, 30, 255, 0, 155, 100, 120, 0, 255]
v2 = [100, 255, 100, 000, 255, 155, 200, 123, 12, 99]
v3 = [0, 0, 255, 255, 255, 0, 50, 0, 100, 56]

tvlct, v1, v2, v3, 1

xc = cos(indgen(32) / 31.0 * 2 * !pi) * 0.4
yc = sin(indgen(32) / 31.0 * 2 * !pi) * 0.4

usersym, xc, yc, thick = 1.5 ; ,/fill

for rep = 0, n_elements(frunlist) - 1 do begin
  Nspec = 0
  ElistSum = 0.0

  for Num = 16, 32 do begin
    exts = '000'
    exts = exts + strcompress(string(Num), /remove_all)
    exts = strmid(exts, strlen(exts) - 3, 3)

    fname = Base + frunlist[rep] + '/powerspec_vel_' + exts + '.txt'
    print, fname
    openr, 1, fname
    Time = 0.0d
    Grid = 0l
    Bins = 0l
    readf, 1, Time
    readf, 1, Grid
    readf, 1, Bins
    print, 'Time = ', Time, '  Grid=', Grid
    vel_disp = dblarr(3)
    readf, 1, vel_disp
    da = fltarr(4, Bins)
    readf, 1, da
    close, 1

    print, 'vel_disp=', vel_disp

    knyquist = 2 * !pi / Box * Grid / 2

    K = da[0, *]
    ModePow = da[1, *]
    ModeCount = da[2, *]
    SumPower = da[3, *]

    ; we will do a band averaging of the finely binned points,
    ; subject to two conditions:
    ; We want enough modes per bin in order to reduce the variance in a bin,
    ; and simultaneously, for large k, we don't want the bins become too narrow.
    ;
    ; The first condition is set by "MinModeCount",
    ; the second by "TargetBinNummer", which is used to compute a minimum
    ; logarithmic bin-size.

    MinModeCount = 1
    TargetBinNummer = 100

    MinDlogK = (alog10(max(K)) - alog10(min(K))) / TargetBinNummer

    istart = 0
    ind = [istart]

    k_list = [0]
    power_list = [0]
    count_list = [0]

    repeat begin
      count = total(ModeCount[ind])
      deltak = (alog10(max(K[ind])) - alog10(min(K[ind])))

      if (deltak ge MinDlogK) and (count ge MinModeCount) then begin
        d2 = total(SumPower[ind]) / total(ModeCount[ind])
        kk = total(K[ind] * ModeCount[ind]) / total(ModeCount[ind])

        k_list = [k_list, kk]
        power_list = [power_list, d2]
        count_list = [count_list, total(ModeCount[ind])]
        istart = istart + 1
        ind = [istart]
      endif else begin
        istart = istart + 1
        ind = [ind, istart]
      endelse
    endrep until istart ge Bins

    k_list = k_list[1 : *]
    power_list = power_list[1 : *]
    count_list = count_list[1 : *]

    print, total(power_list * count_list)

    E_list = 1.0 / 2 * 1.0 / (2 * !pi ^ 2) * k_list ^ 2 * power_list ; convert to energy-spectrum, E(k) = k^2 * P(k)

    ElistSum += E_list
    Nspec += 1
  endfor

  E_list = ElistSum / Nspec

  ; ind = where((R ge 1.0 / 12) and (R lt 1.0 / 6.0))
  ; fitA = linfit(alog(R[ind]), alog(Mach[ind]))
  ; mfitA = fitA[1] * alog(R) + fitA[0]

  ; ind = where((R ge ls / 1.1) and (R lt 1.1 * ls))
  ; fitB = linfit(alog(R[ind]), alog(Mach[ind]))
  ; mfitB = fitB[1] * alog(R) + fitB[0]

  ; print, 'slope=', fitA[1]
  ; print, 'slope=', fitB[1]

  !p.position = [0.12, 0.58, 0.95, 0.98]

  plot, [1], [1], /nodata, /xlog, /ylog, xtitle = '!20k!3', ytitle = '!20E!7(!20k!7)!3', charsize = 1.2, $
    xrange = [2 * !pi / Box, 500 * 2 * !pi / Box], xstyle = 1, yrange = [0.000001, 1.0], xthick = 2.5, ythick = 2.5, ystyle = 1, /noerase

  oplot, k_list, E_list, color = 1 + rep, thick = 4.0

  oplot, 2.0 * 2 * !pi / Box * [1, 1], [1.0e-10, 1000], linestyle = 2

  xyouts, 0.65, 0.93 - rep * 0.035, /normal, '!7' + TagList[rep] + '!3', charsize = 1.1, color = 1 + rep

  !p.position = [0.12, 0.08, 0.95, 0.48]

  plot, [1], [1], /nodata, /xlog, /ylog, xtitle = '!20k!3', ytitle = '!20k!7!U2!N !20E!7(!20k!7)!3', charsize = 1.2, $
    xrange = [2 * !pi / Box, 500 * 2 * !pi / Box], xstyle = 1, yrange = [5.0, 100.0], xthick = 2.5, ythick = 2.5, ystyle = 1, /noerase

  oplot, k_list, E_list * k_list ^ 2, color = 1 + rep, thick = 4.0

  oplot, 2.0 * 2 * !pi / Box * [1, 1], [1.0e-10, 1000], linestyle = 2

  dk = abs(k_list - 80.0)
  ind = where(dk eq min(dk))
  pivot = ind[0]

  kk = [35, 800]

  oplot, kk, E_list[pivot] * k_list[pivot] ^ 2 * (kk / k_list[pivot]) ^ (-5.0 / 3 + 2.0), linestyle = 2, thick = 3, color = 3

  y0 = E_list[pivot] * k_list[pivot] ^ 2 * (1.0 / k_list[pivot]) ^ (-5.0 / 3 + 2.0)

  ; ;;;;;;;;;;;;;;;;

  dk = abs(k_list - 40.0)
  ind = where(dk eq min(dk))
  pivot = ind[0]

  kk = [10, 100]

  oplot, kk, E_list[pivot] * k_list[pivot] ^ 2 * (kk / k_list[pivot]) ^ (0.0), linestyle = 2, thick = 3, color = 3

  y1 = E_list[pivot] * k_list[pivot] ^ 2 * (1.0 / k_list[pivot]) ^ (0.0)

  ; oplot, kk, E_list[pivot] * (kk / k_list[pivot]) ^ (-2.0) * kk ^ 2.0, linestyle = 1, color = 11

  xyouts, 0.65, 0.43 - rep * 0.035, /normal, '!7' + TagList[rep] + '!3', charsize = 1.1, color = 1 + rep

  ; ;;

  ks = (y1 / y0) ^ (1.0 / (-5.0 / 3 + 2.0))

  oplot, ks * [1, 1], [1.0e-10, 1000], linestyle = 1

  print, 'ks = ', ks
  print, 'ls = ', 2 * !pi / ks
endfor

device, /close
set_plot, 'X'

end
