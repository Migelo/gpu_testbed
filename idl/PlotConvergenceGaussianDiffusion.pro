

fout = "plots/gaussian_diffusion_vs_res.eps"




NList = ["Res8", "Res16", "Res32", "Res64", "Res128"]
XList = [8, 16, 32, 64, 128]

BaseList = ["../tests/GaussianDiffusion/Serial_K0/", $
            "../tests/GaussianDiffusion/Serial_K1/", $
            "../tests/GaussianDiffusion/Serial_K2/", $
            "../tests/GaussianDiffusion/Serial_K3/", $
            "../tests/GaussianDiffusion/Serial_K4/"]

KList = [0, 1, 2, 3, 4]



  set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=16.0

    !x.margin=[8,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color


     v1=[115, 135, 155, 175, 195, 215, 235, 255, 200]
     v2=[  0, 0,  0, 0, 0, 0, 0, 0, 200 ]
     v3=[  10, 20,  30, 40, 50, 60, 70, 80 , 200]

    
    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill

    plot, [1],[1], /nodata, xrange=[3.0, 300], xstyle=1, yrange=[0.0000000001,0.1], ystyle =1, charsize=1.2, /xlog,/ylog, $
           xthick = 2.5, ythick = 2.5, xtitle = "!20N!D!7cells!N!3", ytitle = "!7L1!3" 


L1list = dblarr(n_elements(baseList), n_elements(Nlist))



for run = 0, n_elements(BaseList)-1 do begin
   for rep=0, n_elements(Nlist)-1 do begin

      f = BaseList[run] + Nlist[rep] + "/output/L1.txt"

      spawn,"wc "+f,result
      lines=long(result)
      lines=lines(0)
      
      da = dblarr(2,LINES)
      
      openr, 1,f
      readf, 1,da
      close,1

      time = da(0,*)+1.0
      L1   = da(1,*)
      

      
      
      L1max = L1[n_elements(L1)-1]
      print, L1Max

      L1list[run,rep] = L1max
      
   endfor
endfor

;goto,ende




for run = 0, n_elements(BaseList)-1 do begin
   
   oplot, Xlist, L1list[run,*], psym =4, thick=3, color = run+1
   oplot, Xlist, L1list[run,*],  linestyle=run, thick=3, color = run+1
   

   fit = linfit(alog(xlist), alog(L1list[run,*])) 

   xx =[1,1000]

   yy = exp(  fit[1]*alog(xx) + fit[0])

   oplot, xx, yy, color = run+1

   print, "slope= ", fit[1]

   xyouts, Max(xlist)/1.8, min(L1list[run,*])/1.9, /data, "!20p!7 = "+strcompress(string(Klist[run]+1),/remove_all) + "!3", color=K

   
endfor


device,/close
set_plot,"X"






ende:

end





 









