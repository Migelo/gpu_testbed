
Base = "/gpfs/mpa/vrs/ArepoTests/PaperTests/doubleblast/fixed_400/"

;fout = "../plots/double_blast_fixedmesh.eps"
tag = "!7static mesh!3"

SnapBase = "snapshot"

num = 38

BoxSize=1.0


Base = "../tests/DoubleBlast/Serial_K0/Res200/"

pathoutput1 = "output_1.0"
pathoutput0 = "output_1.0"


window, xsize=1200, ysize=800

for num = 0, 38 do begin

   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput1+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1


   x = 2.0*(indgen(N)+0.5)/N
  

   plot, x, dens, xrange=[0,1.0], charsize=2, yrange=[0,7], ystyle=1


   print, num
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput0+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1


   x = 2.0* (indgen(N)+0.5)/N
  

   oplot, x, dens, color=255*256L + 255

   

;   S0 = 0.022 * order^(-2.0)
   
 ;;  fac = 0.8*order^4
   
;   oplot, x, alog10(smth/S0) + 1, color=255,thick=3
;   oplot, [-10,10], 1.0 * [1,1], color=255, linestyle=2

   
   wait, 0.1


endfor













BaseHighRes = "/u/vrs/ptmp/Simulations/Arepo-Tests/PaperTests/doubleblast/fixed_20000/"
Num_Highres = 38



BoxSize = 1.0






    exts='000'
    exts=exts+strcompress(string(Num_Highres),/remove_all)
    exts=strmid(exts,strlen(exts)-3,3)
    
    f= BaseHighRes + "/" + Snapbase + "_"+exts 
    f= Strcompress(f, /remove_all)

    npart=lonarr(6)	
    massarr=dblarr(6)
    time=0.0D
    redshift=0.0D
    flag_sfr=0L
    flag_feedback=0L
    npartall=lonarr(6)	
    bytesleft= 136
    la=intarr(bytesleft/2)


    openr,1,f,/f77_unformatted
    readu,1,npart,massarr,time,redshift,flag_sfr,flag_feedback,npartall,la
    print,npart,massarr
    print,time,redshift
    
    NGas=  npart(0)
    
    N=  NGas
    
    pos2=dblarr(3,N)
    vel=dblarr(3,N)
    id2=lonarr(N)
    readu,1, pos2
    readu,1, vel
    readu,1, id2
    ind=where((npart gt 0) and (massarr eq 0))
    if ind(0) ne -1 then begin
        Nm= total(npart(ind))
        mass=dblarr(Nm) ; masses for variable mass particles (usually gas+stars)
        readu,1,mass
    endif

    u=dblarr(Ngas)
    readu,1,u                   ; internal energy per unit mass
    rho2=dblarr(Ngas)
    readu,1,rho2                 ; comoving gas density
    close,1



    
    IDlim2 = N ;/8
    ind = where(id2 lt IDlim2)
    x = pos2(0,ind)
    d = rho2(ind)

    ind = sort(x)
   

    oplot, x(ind), d(ind), color=255

    goto,ende
    



    set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=12.0,ysize=12.0

    !x.margin=[6,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color

    v1=[255,  0,  0,255,0 ,  255, 100,120, 0, 255]
    v2=[  0,255,  0,000,255, 255, 200,123, 12, 99]
    v3=[  0,  0,255,255,255, 0,   50,0, 100, 56]


    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill






    plot, [-100],[0], psym=3 , xrange=[0.4,1.0], yrange=[0,7], ystyle=1, $
      xtitle="!20x!3", ytitle = "!9r!3", $
      xthick=2.5, ythick=2.5



    ind = where(id2 lt IDlim2)
    x = pos2(0,ind)
    d = rho2(ind)

    ind = sort(x)
   

    oplot, x(ind), d(ind)

    

;    ind = where(id lt IDlim)
    ind = where(pos(1,*) lt 1.0/400)
    x = pos(0,ind)
    d = rho(ind)

    oplot, x, d, psym=8, color=1

    
    xyouts, 0.68,0.86,/normal, tag


    device,/close

    set_plot,"X"



    ende:
end





 









