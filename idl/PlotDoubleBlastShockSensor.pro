

BaseList = ["../tests/DoubleBlast/Serial_K0/Res200/", $
            "../tests/DoubleBlast/Serial_K1/Res200/", $
            "../tests/DoubleBlast/Serial_K2/Res200/", $
            "../tests/DoubleBlast/Serial_K3/Res200/", $
            "../tests/DoubleBlast/Serial_K4/Res200/", $
            "../tests/DoubleBlast/Serial_K5/Res200/", $
            "../tests/DoubleBlast/Serial_K6/Res200/", $
            "../tests/DoubleBlast/Serial_K7/Res200/", $
            "../tests/DoubleBlast/Serial_K8/Res200/", $
            "../tests/DoubleBlast/Serial_K9/Res200/"]

plot, [1], [1], /nodata, xrange = [0.9,15], xstyle =1 , /xlog, yrange=[1.0e-8,1.0], ystyle=1,/ylog, charsize=2

orderlist = [0]
sensorlist = [0]

for rep=1, n_elements(BaseList)-1 do begin
   
   SnapBase = "snapshot"

   Num = 38

   BoxSize=1.0
   pathoutput0 = "output_0.0"

   print, num


   Base = BaseList[rep]

       exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)

   f= Base + "/" + pathoutput0+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti=0.0D
   readu, 1, ti
   print, "Time=", ti
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1

   order =smth
   order[*] = rep

   ind = sort(smth)
   smth = smth[ind]
   smth = smth[n_elements(smth)-100:*]
   order = order[ind[n_elements(ind)-100:*]]

  
   oplot, order+1, smth, psym = 4

   oplot, [median(order)]+1, [median(smth)], color=255, psym=5

   orderlist = [orderlist, median(order)]
   sensorlist = [sensorlist, median(smth)]
   
endfor

orderlist = orderlist[1:*]
sensorlist = sensorlist[1:*]

oplot, orderlist+1, sensorlist, color=255, thick=3


fit = linfit(alog(orderlist+1) , alog(sensorlist)) 

xx =[1,1000]

yy = exp(  fit[1]*alog(xx) + fit[0])

oplot, xx, yy, color=255

print, "slope= ", fit[1]



goto,ende



for rep=0, n_elements(BaseList)-1 do begin
   

   fout = FoutList[rep]
   Base = BaseList[rep]
   tag = TagList[rep]



    set_plot,'PS'
    !p.font=0

    device,/times,/italic,font_index=20
    device,xsize=15.0,ysize=12.0

    !x.margin=[6,3]
    !p.thick=2.5
    !p.ticklen=0.03

    device,filename=fout, /encapsulated, /color

    v1=[240, 255,  0,  0,255,0 ,  255, 100,120, 0, 255]
    v2=[240, 100,255,  0,000,255, 255, 200,123, 12, 99]
    v3=[240,  0,  0,255,255,255, 0,   50,0, 100, 56]


    tvlct,v1,v2,v3,1

    xc= cos(indgen(32)/31.0 * 2*!PI )*0.4
    yc= sin(indgen(32)/31.0 * 2*!PI )*0.4

    usersym, xc, yc, thick=1.5  ;,/fill



   plot, [-100],[0], psym=3 , xrange=[0.4,1.0], yrange=[0,7], ystyle=1, $
      xtitle="!20x!3", ytitle = "!9r!3", $
      xthick=2.5, ythick=2.5, color=0


   
   
   exts='0000'
   exts=exts+strcompress(string(num),/remove_all)
   exts=strmid(exts,strlen(exts)-4,4)
   
   f= Base + "/" + pathoutput1+"/field_0_"  + exts + ".dat"
   
   openr, 1, f
   N = 0L
   readu, 1, N
   ti =0.0D
   readu, 1, ti
   print, "Time=", ti
   dens = dblarr(N)
   readu, 1, dens
   smth = dblarr(N)
   readu, 1, smth
   close, 1


   x = 2.0*(indgen(N)+0.5)/N
  

;   plot, x, dens, xrange=[0,1.0], charsize=2, yrange=[0,7], ystyle=1



   BaseHighRes = "/u/vrs/ptmp/Simulations/Arepo-Tests/PaperTests/doubleblast/fixed_20000/"
   Num_Highres = 38
   
   
   
   exts='000'
   exts=exts+strcompress(string(Num_Highres),/remove_all)
   exts=strmid(exts,strlen(exts)-3,3)
   
   f= BaseHighRes + "/" + Snapbase + "_"+exts 
   f= Strcompress(f, /remove_all)
   
   npart=lonarr(6)	
   massarr=dblarr(6)
    time=0.0D
    redshift=0.0D
    flag_sfr=0L
    flag_feedback=0L
    npartall=lonarr(6)	
    bytesleft= 136
    la=intarr(bytesleft/2)


    openr,1,f,/f77_unformatted
    readu,1,npart,massarr,time,redshift,flag_sfr,flag_feedback,npartall,la
    print,npart,massarr
    print,time,redshift
    
    NGas=  npart(0)
    
    N=  NGas
    
    pos2=dblarr(3,N)
    vel=dblarr(3,N)
    id2=lonarr(N)
    readu,1, pos2
    readu,1, vel
    readu,1, id2
    ind=where((npart gt 0) and (massarr eq 0))
    if ind(0) ne -1 then begin
        Nm= total(npart(ind))
        mass=dblarr(Nm) ; masses for variable mass particles (usually gas+stars)
        readu,1,mass
    endif

    u=dblarr(Ngas)
    readu,1,u                   ; internal energy per unit mass
    rho2=dblarr(Ngas)
    readu,1,rho2                 ; comoving gas density
    close,1
 
    IDlim2 = N ;/8
    ind = where(id2 lt IDlim2)
    xx = pos2(0,ind)
    d = rho2(ind)

    ind = sort(xx)
   

    oplot, xx(ind), d(ind), thick =2.5

    
    oplot, x, dens, color=2, thick=5.0





    
    
    xyouts, 0.74,0.86,/normal, "!7" + tag + "!3", color=2


    device,/close

    set_plot,"X"

endfor

    ende:
end





 









