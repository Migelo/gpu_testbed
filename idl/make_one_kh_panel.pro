
BaseList = ["../tests/KelvinHelmholtz_SmoothICs/Serial_K0/Res60/", $
"../tests/KelvinHelmholtz_SmoothICs/Serial_K1/Res60/", $
 "../tests/KelvinHelmholtz_SmoothICs/Serial_K2/Res60/",$
"../tests/KelvinHelmholtz_SmoothICs/Serial_K3/Res60/", $
 "../tests/KelvinHelmholtz_SmoothICs/Serial_K4/Res60/", $
 "../tests/KelvinHelmholtz_SmoothICs/Serial_K5/Res60/"]


size = 720

gap = 8

BigPic = bytarr(3, size*3L + 2*gap, size*2L + 1 * gap)

BigPic(*) =255

num = 47

for row = 0L,1 do begin
  for col = 0L,2 do begin

     nr = row * 3+ col
     
      exts='0000'
      exts=exts+strcompress(string(num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      

      fpic=BaseList[nr] +"/output/pic_"+exts+".jpg"
      fpic=strcompress(fpic,/remove_all)

      read_jpeg, fpic, pic, /true

      offx = col*gap 
      offy = (1-row)*gap

      BigPic(*, col*size+offx:(col+1)*size-1+offx, (1-row)*size+offy:((1-row)+1)*size-1+offy) = pic(*,*,*)

    ;  tv, pic,/true
    ;  wait, 1.0
  endfor
endfor
 

;window, xsize = size*4+ 3*gap, ysize=size*4 + 3 * gap
;tv, BigPic, /true

write_jpeg, "fig_KH.jpg", BigPic, /true, quality=95


end
