


BaseList = ["../tests/KelvinHelmholtz_Lecoanet/N64_K1_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K2_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K3_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K4_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K5_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K6_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K7_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K8_Re5/", $
            "../tests/KelvinHelmholtz_Lecoanet/N64_K9_Re5/"]

NumList = [30, 30, 30, 30, 30, 26, 14, 9, 5]

LabelList = ["N = 64, p = 2", "N = 64, p = 3", "N = 64, p = 4", $
             "N = 64, p = 5", "N = 64, p = 6", "N = 64, p = 7", $
             "N = 64, p = 8", "N = 64, p = 9", "N = 64, p = 10"]

Nrow = 3
Ncol = 3

Npix = 512L


gap = 8L

Pic=bytarr(Npix*Ncol + (Ncol-1)*gap, Npix *Nrow+ (nrow-1)*gap, 3)
Pic(*,*, *)= 255


outdir = "output"

mi = 0.0
ma = 1.0

BoxSize = 2.0

loadct, 70
tvlct, r, g, b, /get


cmd = 'mogrify -font '
cmd = cmd + """
cmd += "/System/Library/Fonts/Supplemental/Times New Roman Italic.ttf"
cmd = cmd + """
cmd += ' -fill white '



for row = 0,Nrow-1 do begin
   for col = 0,Ncol-1 do begin


      offx = (Npix + Gap)*col 
      offy = (Npix + Gap)*row

      offx += 40 
      offy += 50


      
      rep = row * 3 + col

      Num = NumList[rep]
      
   
      print, num
   
      exts='0000'
      exts=exts+strcompress(string(num),/remove_all)
      exts=strmid(exts,strlen(exts)-4,4)
      
      f= BaseList[rep] + "/" + outdir + "/field_0_"  + exts + ".dat"

      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0D
      readu, 1, Ti
      dens = dblarr(N, N)
      readu, 1, dens
      close, 1

      f= BaseList[rep] + "/" + outdir + "/field_4_"  + exts + ".dat"
      
      openr, 1, f
      N = 0L
      readu, 1, N
      ti = 0.0D
      readu, 1, Ti
      dyedens = dblarr(N, N)
      readu, 1, dyedens
      close, 1
      
      d = dyedens / dens

      print, f
      print, num, "time=", ti, "  minimum dye=", min(d), "  maximum dye=",  max(d)
      print
  
      d= transpose(d)

      colindex= (d - mi)/(ma-mi)*255.0
      ind = where(colindex ge 256.0)
      if ind(0) ne -1 then colindex(ind) = 255.9
      ind = where(colindex lt 0)
      if ind(0) ne -1 then colindex(ind) = 0
      colindex = byte(colindex)
      

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 0] = r[ colindex[0:Npix-1, Npix:2*Npix-1  ]]

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 1] = g[ colindex[0:Npix-1, Npix:2*Npix-1  ]]

      Pic[(Npix + Gap)*col : (Npix + Gap)*col + Npix-1,  (Npix + Gap)*(Nrow-1-row) : (Npix + Gap)*(Nrow-row-1) + Npix-1 , 2] = b[ colindex[0:Npix-1, Npix:2*Npix-1  ]]


     cmd = cmd + ' -pointsize 36  -draw '
     cmd = cmd + "'"
     cmd = cmd + 'text '+ string(offx)+","+string(offy)+" "
     cmd = cmd + """
     cmd = cmd + LabelList[rep] + ', t = ' + string(ti, format = "(F4.1)")
     cmd = cmd + """
     cmd = cmd + "'"
   

      
endfor
endfor

print

   fname = "KH_maps_differentK.jpg"
   write_jpeg, "plots/" + fname, pic, true=3, quality=98


 

   cmd = cmd + ' -quality 95 '
   cmd = cmd + fname 

   print, cmd
;   spawn, cmd


   
end





 









