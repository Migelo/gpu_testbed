

Base = "../tests/KelvinHelmholtz_Lecoanet/N256_K3_Re5/"

Base = "../tests/KelvinHelmholtz_Lecoanet/N512_K3_Re5/"

Base = "../tests/KelvinHelmholtz_Lecoanet/N256_K2_Re5/"

Base = "../tests/KelvinHelmholtz_Lecoanet/N256_K1_Re5/"

Base = "../tests/KelvinHelmholtz_Lecoanet/N64_K1_Re5/"
Base = "../tests/KelvinHelmholtz_Lecoanet/GPU_N64_K1_Re5/"

;Base = "../tests/KelvinHelmholtz_Lecoanet_nojump/N64_K9_Re5/"


outdir = "output"

mi = 0.0
ma = 1.0

BoxSize = 2.0

loadct, 70
tvlct, r, g, b, /get



fname = "/ptmp/mpa/vrs/Simulations/Lecoanet/5_2048_1_2.h5"

file_id = h5f_open(fname)
dataset_id= h5d_open(file_id, 'tasks/c')
dataspace_id = H5D_GET_SPACE(dataset_id)
datatype_id = H5D_GET_TYPE(dataset_id)
idltype = H5T_IDLTYPE(Datatype_id)
dimensions = H5S_GET_SIMPLE_EXTENT_DIMS(dataspace_id)

dims = [6144, 3072, 2]
start = [0, 0, 0]
count = [6144,  3072, 1]
Data  = read_dataslab(file_id, 'tasks/c', dims, start, count)
h5d_close, dataset_id
h5f_close, file_id

dedalus = dblarr(6144, 3072)
dedalus(*,*) = data[*,*,0]

dedalus = transpose(dedalus)

dedalus  = rebin(dedalus, 512, 1024)




;for num = 0, 100,1 do begin

for num = 0, 20,20 do begin

   print, num
   
  exts='0000'
  exts=exts+strcompress(string(num),/remove_all)
  exts=strmid(exts,strlen(exts)-4,4)
  
  f= Base + "/" + outdir + "/field_0_"  + exts + ".dat"

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  dens = dblarr(N, N)
  readu, 1, dens
  close, 1

  f= Base + "/" + outdir + "/field_4_"  + exts + ".dat"
  

  openr, 1, f
  N = 0L
  readu, 1, N
  ti = 0.0D
  readu, 1, Ti
  dyedens = dblarr(N, N)
  readu, 1, dyedens
  close, 1

print, N

  d = dyedens / dens
  
   print, num, "time=", ti, "  minimum dye=", min(d), "  maximum dye=",  max(d)
  
  
   d= transpose(d)

   d= rebin(d, 1024, 1024)
;N/=2
   
   colindex= (d - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   Pic=bytarr(N, N, 3)

   pic(*,*,0) = r(colindex)
   pic(*,*,1) = g(colindex)
   pic(*,*,2) = b(colindex)



   d = dedalus
   colindex= (d - mi)/(ma-mi)*255.0
   ind = where(colindex ge 256.0)
   if ind(0) ne -1 then colindex(ind) = 255.9
   ind = where(colindex lt 0)
   if ind(0) ne -1 then colindex(ind) = 0
   colindex = byte(colindex)


   pic(N/2:*,*,0) = r(colindex)
   pic(N/2:*,*,1) = g(colindex)
   pic(N/2:*,*,2) = b(colindex)







   if num eq 0 then begin
      window, xsize= N, ysize= N
   endif
   
   tv, Pic, true=3,0

   

   wait, 0.05

   fname = Base + "/output/pic_"+exts+".jpg"
   write_jpeg, fname, pic, true=3, quality=98
      
endfor


ende:
end





 









