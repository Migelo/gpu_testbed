import argparse
import subprocess
from functools import partial

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())


JOB_HEADER_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time={:02d}:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --nodes={}
#SBATCH --job-name c{}k{}
#SBATCH --partition=p.gpu.ampere
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


"""

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j 10
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py
    python plot_vel_power_spec.py
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""
)

JOB_RESTART_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
touch "$FILE"
(
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py
    python plot_vel_power_spec.py
) &

mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1 | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py
    python plot_vel_power_spec.py
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""
)


CONFIG_TEMPLATE = """
GPU

VISCOSITY
NAVIER_STOKES
TURBULENCE
GAMMA=1.0001

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K={}                                   # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension
"""


PARAM_TEMPLATE = """
Tend       20.48
CourantFac 0.5

DesiredDumps     10
PixelsFieldMaps  256

ShearViscosity     2e-4
ThermalDiffusivity 0

OutputDir  ./data

DesiredPowerspectra 64
PowerspectraDelay 5.12

IsoSoundSpeed 1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2

StDtFreq    0.005            \% update frequency
StDecay     1.0              \% coherence timescale ts



StEnergy    0.1

StSolWeight  1.0     \% set to 1 for purely solenoidal driving
StAmplFac    0.1
StSeed       42

MinimumSlabsPerCPURank  0

WriteRestartAfter       82800
WriteCheckpointAfter    7200

"""

run_shell = partial(subprocess.call, shell=True)

degree_ks = [
    1,
    2,
    3,
]
ncs = [
    32,
    64,
    128,
    256,
]


ngpus_dict = {32: 1, 64: 1, 128: 2, 256: 4}
time_limits = {1: 24, 2: 24, 3: 24}

OUTPUT_FOLDER_TEMPLATE = "output/supersonic_turbulence_ns/NC{:03d}_k{:d}"

if __name__ == "__main__":
    for nc in ncs:
        for degree in degree_ks:
            ngpu = ngpus_dict[nc]
            problem = dg_python.Problem(nc, degree, 20.48, ngpu)
            output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, degree)

            config = CONFIG_TEMPLATE.format(degree, nc)
            param = PARAM_TEMPLATE

            dg_python.prepare_code_run(
                problem,
                args["cluster"],
                output_folder,
                "24:00:00",
                args["dry_run"],
                config_template=config,
                param_template=param.format(time_limits[degree] * 3600 - 100),
                job_template=JOB_TEMPLATE.format(time_limits[degree], ngpu, nc, degree),
                job_restart_template=JOB_RESTART_TEMPLATE.format(
                    time_limits[degree], ngpu, nc, degree
                ),
            )
