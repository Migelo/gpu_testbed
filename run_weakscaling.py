import argparse

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())
ND = 3


CONFIG_TEMPLATE = """GPU
VISCOSITY
ART_VISCOSITY
NAVIER_STOKES
BENCHMARK

ENABLE_POSITIVITY_LIMITING

ND=3                                          # sets number of dimensions
DEGREE_K={}                                   # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension"""

PARAM_TEMPLATE = """Tend       20.48
CourantFac 0.5

DesiredDumps     1
PixelsFieldMaps  2

ShearViscosity     0
ThermalDiffusivity 0

OutputDir  ./data

ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01

MinimumSlabsPerCPURank  0

WriteRestartAfter       82800
WriteCheckpointAfter    7200"""

JOB_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=06:00:00
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks={}
#SBATCH --job-name c{}k{}
#SBATCH --constraint="gpu"
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

module load git
module load anaconda/3/2021.11
module load gcc/11
module load openmpi/4
module load fftw-serial
module load hdf5-serial
module load gsl
module load hwloc
module load cuda/11.6

make -j 1

srun ./run.run param.txt | tee log.txt
"""

OUTPUT_FOLDER_TEMPLATE = "output/weakscaling_gpu/NC{:03d}_k{:d}"
ngpus_dict = {128: 1, 160: 2, 200: 4, 256: 8, 320: 16, 384: 32, 512: 64}
ncs = (128, 160, 200, 256, 320, 384, 512)

for order in range(0, 6):
    for nc in ncs:
        ngpu = ngpus_dict[nc]
        problem = dg_python.Problem(nc, order, 20.48, ngpu)
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, order)

        config = CONFIG_TEMPLATE.format(order, nc)
        param = PARAM_TEMPLATE
        job = JOB_TEMPLATE.format(problem.n_gpu, problem.nc, problem.order)

        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            output_folder,
            args["dry_run"],
            config_template=config,
            param_template=param,
            job_template=job,
            no_restart=True,
        )


# # CPU test
CONFIG_TEMPLATE = CONFIG_TEMPLATE.replace("GPU", "")
OUTPUT_FOLDER_TEMPLATE = OUTPUT_FOLDER_TEMPLATE.replace("_gpu", "_cpu")


JOB_HEADER_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=1-
#SBATCH --ntasks-per-node=16
#SBATCH --ntasks={0}
#SBATCH --job-name c{1}k{2}
#SBATCH --partition=p.24h
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


"""

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j 1
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
"""
)

ncpus_dict = {
    128: 1 * 4,
    160: 2 * 4,
    200: 4 * 4,
    256: 8 * 4,
    320: 16 * 4,
    384: 32 * 4,
    512: 64 * 4,
}
for order in range(0, 6):
    for nc in ncs:
        ncpu = ncpus_dict[nc]
        problem = dg_python.Problem(nc, order, 20.48, ncpu)
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, order)

        config = CONFIG_TEMPLATE.format(order, nc)
        param = PARAM_TEMPLATE
        job = JOB_TEMPLATE.format(ncpu, problem.nc, problem.order)

        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            OUTPUT_FOLDER_TEMPLATE,
            args["dry_run"],
            config_template=CONFIG_TEMPLATE.format(problem.order, problem.nc),
            param_template=PARAM_TEMPLATE,
            job_template=JOB_TEMPLATE.format(problem.n_gpu, problem.nc, problem.order),
            no_restart=True,
        )
