import argparse
import subprocess
from functools import partial
from itertools import product

import dg_python

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Do not submit the slurm job",
)
args = vars(parser.parse_args())

assert args["cluster"] in ("raven", "freya", "rusty")


def format_job_script(
    template: str,
    cluster: str,
    nnodes: int,
    job_name: str,
):
    if cluster == "rusty":
        return template.format(job_name)
    else:
        return template.format(job_name, nnodes)


modules = {
    "raven": """
module load anaconda/3/2021.11
module load gcc/11
module load openmpi/4
module load fftw-serial
module load hdf5-serial
module load gsl
module load git
module load hwloc
module load cuda/11.4
""",
    "freya": "",
    "rusty": """
module purge
module load git ffmpeg cmake
module load gcc/11
module load openmpi/4
module load gsl/2.7.1
module load fftw/3.3.10
module load hdf5/1.8.22
module load hwloc/2.9.0
module load cuda/12.0
""",
}

gpu_config = {
    "freya": """
#SBATCH --ntasks-per-node=32
#SBATCH --nodes={}
#SBATCH --partition=p.24h
##SBATCH --gres=gpu:a100:4
#SBATCH --exclusive
""",
    "raven": """
#SBATCH --ntasks-per-node=4
#SBATCH --nodes={}
#SBATCH --partition=gpu
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive
""",
    "rusty": """
#SBATCH -p gpupreempt
#SBATCH -q gpupreempt
#SBATCH --ntasks-per-node=4
#SBATCH --gpus-per-node=4
#SBATCH -C a100
#SBATCH -n 16""",
}

run_command = {
    "freya": "mpiexec -np $SLURM_NPROCS ./run.run param.txt | tee log.txt",
    "raven": "mpiexec -np $SLURM_NPROCS ./run.run param.txt | tee log.txt",
    "rusty": "mpirun --map-by socket:pe=$OMP_NUM_THREADS -np $SLURM_NPROCS"
    " ./run.run param.txt | tee log.txt",
}

JOB_HEADER_TEMPLATE = (
    """#!/bin/bash -l
##SBATCH --mail-type=BEGIN,END,FAIL
##SBATCH --mail-user=cernetic@mpa-garching.mpg.de
##SBATCH --time=24:00:00
#SBATCH --job-name {}
"""
    + gpu_config[args["cluster"]]
    + """
echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

"""
    + modules[args["cluster"]]
)

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
"""
    + run_command[args["cluster"]]
    + """
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    python plots.py || echo "plots.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)

JOB_RESTART_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
touch "$FILE"
(
    #mamba activate ~/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
    python plot_vel_power_spec.py || echo "plots.py failed in some way"
) &

mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1 | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    #mamba activate ~/conda-envs/py39
    python plots.py || echo "plots.py failed in some way"
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
    sleep 60
else
    echo "Things have gone awry, quitting."
fi
"""
)


CONFIG_TEMPLATE = """
#GPU

SINGLE_SHOCK

VISCOSITY
ART_VISCOSITY

ENABLE_POSITIVITY_LIMITING

DATACUBE_OUTPUT

ND=3                                          # sets number of dimensions
DEGREE_K={}                                   # order of the DG scheme
DG_NUM_CELLS_1D={}                            # number of cells per dimension
"""


PARAM_TEMPLATE = r"""
Tend       0.0876032
CourantFac 0.3

DesiredDumps     128
PixelsFieldMaps  1024

DesiredCubeDumps 10
PixelsFieldCubes {:d}


ArtViscMax              1.0
ArtViscShockGrowth      2.5
ArtViscDecay            0.5
ArtViscWiggleGrowth     0.2
ArtViscWiggleOnset      0.01

OutputDir  ./data

#ThermalDiffusivity 0.0
#ShearViscosity     0.0

MinimumSlabsPerCPURank  0

WriteRestartAfter       86500
WriteCheckpointAfter    86400
"""

run_shell = partial(subprocess.call, shell=True)

degree_ks = [
    1,
    2,
    3,
]
ncs = [
    64,
    128,
    256,
]

products = list(product(degree_ks, ncs))
print(f"Submitting {len(list(products))} jobs.")

OUTPUT_FOLDER_TEMPLATE = (
    "/freya/ptmp/mpa/mihac/gpu_testbed2/output/SingleShock3D/n{:d}_k{:d}/"
)

if __name__ == "__main__":
    for degree, nc in products:
        nnodes = 1
        if (degree + 1) * nc > 64 * 4:
            nnodes = 2
        problem = dg_python.Problem(nc, degree, 0.256, 4 * nnodes)
        output_folder = OUTPUT_FOLDER_TEMPLATE.format(nc, degree)

        config = CONFIG_TEMPLATE.format(degree, nc)
        param = PARAM_TEMPLATE.format((degree + 1) * nc)
        job_name = "n{:d}k{:d}".format(nc, degree)

        job_script = format_job_script(JOB_TEMPLATE, args["cluster"], nnodes, job_name)
        job_restart_script = format_job_script(
            JOB_RESTART_TEMPLATE, args["cluster"], nnodes, job_name
        )

        dg_python.prepare_code_run(
            problem,
            args["cluster"],
            output_folder,
            args["dry_run"],
            "24:00:00",
            config_template=config,
            param_template=param,
            job_template=job_script,
            job_restart_template=job_restart_script,
        )
