import argparse

import dg_python
from dg_python.scaling_tests import Problem

parser = argparse.ArgumentParser(description="Description of your program")
parser.add_argument("cluster", type=str, help="Which cluster are we running on")
parser.add_argument(
    "--dry-run",
    "-n",
    action="store_false",
    help="Which cluster are we running on",
)
args = vars(parser.parse_args())
ND = 3

CONFIG_TEMPLATE = """
GAMMA=7./5
ISENTROPIC_VORTEX

ND=2L                                          # sets number of dimensions
DEGREE_K={}L                                   # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D={}L                            # number of cells per dimension
"""


PARAM_TEMPLATE = """
Tend       8
CourantFac 0.5

DesiredDumps     1
PixelsFieldMaps  10000

OutputDir  ./data

MinimumSlabsPerCPURank  1

WriteRestartAfter       82800
WriteCheckpointAfter    7200

"""

JOB_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=04:30:00
#SBATCH --ntasks-per-node=40
#SBATCH --ntasks={}
#SBATCH --job-name c{}k{}
#SBATCH --partition=p.24h
##SBATCH --gres=gpu:a100:4
##SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo


FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
grep Nlimited_total log.txt | cut -f11 -d' ' >| nlimited_p.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    source activate /freya/u/mihac/conda-envs/py39
    python plots.py |& tee plots.log
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""


problems = []

for order in range(1, 4):
    for nc in (
        64,
        128,
        256,
        512,
        1024,
    ):
        problems.append(Problem(nc, order, endtime=10, n_gpu=0))

for problem in problems:
    dg_python.prepare_code_run(
        problem,
        args["cluster"],
        "/freya/ptmp/mpa/mihac/paper/isentropic/NC{:03d}_k{:d}",
        "24:00:00",
        args["dry_run"],
        config_template=CONFIG_TEMPLATE.format(problem.order, problem.nc),
        param_template=PARAM_TEMPLATE,
        job_template=JOB_TEMPLATE.format(32, problem.nc, problem.order),
    )
