#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time=24:00:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --job-name multiGPU
#SBATCH --partition=p.gpu
#SBATCH --gres=gpu:2

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

mpiexec -np $SLURM_NPROCS  ./run.run  param.txt


FILE=./.finished
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
fi