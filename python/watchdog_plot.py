import os
import time
from glob import glob
from multiprocessing import Process, Queue

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from python.plots import (
    get_number_of_fields,
    plot_slice,
    plot_timestep,
    plot_turbulence_txt,
)

NF = 0


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if event.is_directory is False:
            if "field_{}_".format(NF - 1) in event.src_path:
                print("adding {} to queue".format(event.src_path))
                request_queue.put(event.src_path)


class Worker(Process):
    global simulation_dir

    def __init__(self, queue, i):
        super(Worker, self).__init__()
        self.queue = queue
        self.id = i

    def run(self):
        print("worker started!")
        while True:
            time.sleep(1)
            msg = self.queue.get()
            if msg is None:
                print("worker exiting")
                break
            else:
                print("worker {} got this task: {}".format(self.id, msg))
                time.sleep(2)
                plot_timestep(
                    path=msg, force_npix=True, savefig_path=savefig_path, nf=NF
                )
                plot_slice(int(msg[-8:-4]), msg[:-21], vlims=(0.8, 2.2))
                plot_turbulence_txt(path=msg[:-21])


def start_watchdog(simulation_dir):
    # sleep for 10 seconds to allow the initial snap to be written
    time.sleep(10)
    if simulation_dir[-1] != "/":
        simulation_dir += "/"

    print("simulation_dir=" + simulation_dir)
    global savefig_path
    savefig_path = simulation_dir
    global NF
    NF = get_number_of_fields(simulation_dir)
    print("NF=" + str(NF))
    observer = Observer()
    print("simulation_dir=" + simulation_dir + "data")
    observer.schedule(MyHandler(), path=simulation_dir + "data", recursive=False)
    observer.start()

    # set up a request queue for processing shapshots
    N_WORKERS = 4
    global request_queue
    request_queue = Queue()
    workers = []
    for i in range(N_WORKERS):
        workers.append(Worker(request_queue, i))
        workers[-1].start()
    initial_files = glob(simulation_dir + "data/field_{}_*".format(NF - 1))
    time.sleep(3)
    for file in initial_files:
        print("adding {} to queue".format(file))
        request_queue.put(file)

    finished = True
    while finished:
        if os.path.isfile(simulation_dir + "data/.finished"):
            print("found .finished\nexiting!")
            for worker in workers:
                worker.terminate()
            time.sleep(5)
            finished = False

    observer.stop()
    observer.join()
