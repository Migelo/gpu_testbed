import os
import re
import struct
import subprocess
import sys
from functools import partial
from glob import glob
from multiprocessing import Pool

import matplotlib
import numpy as np
from scipy.stats import binned_statistic

using_proplot = False
if len(sys.argv) > 2:
    if sys.argv[2] == "ci":
        import matplotlib.pyplot as plt
else:
    using_proplot = True
    import proplot as plt


matplotlib.use("Agg")

TURB_STAT_FNAME_TEMPLATE = "./data/turbstat_{:04d}.dat"
FNAME_TEMPLATE = "./data/field_{}_{:04d}.dat"
NC = None
NPIX = None


def get_param_from_config(parameter: str):
    numeric_const_pattern = (
        r"[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?"
    )

    rx = re.compile(numeric_const_pattern, re.VERBOSE)

    with open("Config.sh") as f:
        lines = f.read().splitlines()
    for line in lines:
        if not line.startswith("#") and parameter in line:
            return_value = line.split("=")[1]
            return rx.findall(return_value)[0]
    return None


GAMMA = get_param_from_config("GAMMA")
if GAMMA is None:
    GAMMA = 5 / 3
else:
    GAMMA = float(GAMMA)


def is_param_in_config(parameter: str):
    with open("Config.sh") as f:
        lines = f.read().splitlines()
    for line in lines:
        if not line.startswith("#") and parameter in line:
            return True
    return False


ENABLE_DYE = is_param_in_config("ENABLE_DYE")
ART_VISCOSITY = is_param_in_config("ART_VISCOSITY")


def get_order():
    order = int(get_param_from_config("DEGREE_K")) + 1
    assert order >= 0
    return order


ORDER = get_order()


def get_dimension():
    nd = int(get_param_from_config("ND"))
    assert nd > 0
    return nd


ND = get_dimension()


def get_cells_per_dimension():
    nc = int(get_param_from_config("DG_NUM_CELLS_1D"))
    assert nc > 0
    return nc


NC = get_cells_per_dimension()


def read_turb_stat(i: int, path=""):
    global NC
    path = path + TURB_STAT_FNAME_TEMPLATE.format(i)
    if NC is None:
        with open(path, mode="rb") as file:
            file_content = file.read(4)
            NC = struct.unpack("i", file_content)[0]

    return np.fromfile(path, offset=4, dtype=np.double)


def get_available_shapshots(path=""):
    files = glob(path + FNAME_TEMPLATE[:-13] + "0_*.dat")
    snapshot_numbers = [int(x[-8:-4]) for x in files]
    start, end = np.min(snapshot_numbers), np.max(snapshot_numbers)
    return start, end


def get_number_of_fields(path=""):
    files_in_output_dir = glob(path + "data/field_?_0000.dat")
    nf = len(files_in_output_dir)
    assert nf > 0, "Output files missing."
    # assert nf in (4, 5), "NF can only be 4 or 5. Got {}".format(nf)
    return nf


NF = get_number_of_fields()
NF_WITHOUT_ART_VISC = NF - ART_VISCOSITY


def read_field(path: str, npix: int = None, debug: bool = False):
    if npix is None:
        if debug:
            print(f"reading file {path}")
        with open(path, mode="rb") as file:
            file_content = file.read(4)
            npix = struct.unpack("i", file_content)[0]
            npix = int(npix)
            if debug:
                print(f"npix={npix}")

    data = np.fromfile(path, offset=4 + 8, dtype=np.double)
    if ND == 3:
        if debug:
            print("detected a 3D dataset")
        offset = npix**2
        return_data = data[offset * 2 : offset * 3].reshape(npix, npix)
    elif data.size == npix**2 + NC**2:  # 2D case, with smootness indicator
        if debug:
            print("detected a 2D dataset with smoothness indicator")
        return_data = data[: npix**2].reshape(npix, npix)
    elif ND == 2:  # 2D case
        if debug:
            print("detected a 2D dataset")
        return_data = data.reshape(npix, npix)
    elif ND == 1:  # 1D
        if debug:
            print(
                f"detected a 1D dataset with {data.size=} and {npix=} "
                f"{data.size == npix} {data.size == npix*2} {data.size == npix*3}"
            )
        # data = np.fromfile(path, offset=4, dtype=np.double)
        if data.size == npix:
            if debug:
                print("the data hold: fields")
            return_data = data
        elif data.size == npix * 2:  # check if we have smoothness indicator
            if debug:
                print("the data hold: fields, smoothness")
            smoothness = data[data.size // 2 :]
            return_data = data[: data.size // 2]
            return return_data, smoothness
        elif data.size == npix * 3:
            if debug:
                print("the data hold: fields, smoothness, rate")
            return_data, smoothness, _ = np.array_split(data, 3)
            return return_data, smoothness
    else:
        print("unrecognized amount of data, terminating")
        sys.exit(-1)

    return return_data


def get_vlimits(
    i_start: int,
    i_end: int,
    number_snaps_to_sample: int = 10,
    mode: str = "percentile",
    percentiles: np.ndarray = (5, 95),
) -> np.array:
    # parse inputs and do some validation
    if i_end < i_start:
        i_end = i_start
    assert mode in ("minmax", "percentile")
    if mode == "percentile":
        assert percentiles is not None
        assert len(percentiles) == 2

    # read all data for one field and update the vlim array accordingly
    vlim = np.zeros((NF, 2))
    snaps_to_sample = np.unique(
        np.linspace(i_start, i_end, number_snaps_to_sample, dtype=int)
    )

    fields = (
        "rho",
        "vx",
        "p",
    )
    all_data = {}
    rho_arr = []
    vx_arr = []
    vy_arr = []
    vz_arr = []
    p_arr = []
    dye_arr = [(0, 1)]

    print(snaps_to_sample)
    for _, item in enumerate(snaps_to_sample):
        print(f"sampling {FNAME_TEMPLATE.format(0, item)}")
        rho = np.array(read_field(FNAME_TEMPLATE.format(0, item)))
        rho_arr.append(rho)
        irho = 1 / rho
        vx = np.array(read_field(FNAME_TEMPLATE.format(1, item))) * irho
        vx_arr.append(vx)
        if ND == 1:
            p = np.array(read_field(FNAME_TEMPLATE.format(2, item)))
            p = (p - 0.5 * rho * (vx**2)) * (GAMMA - 1)
            p_arr.append(p)
        if ND == 2:
            fields = (
                "rho",
                "vx",
                "vy",
                "p",
            )
            vy = np.array(read_field(FNAME_TEMPLATE.format(2, item))) * irho
            vy_arr.append(vy)
            p = np.array(read_field(FNAME_TEMPLATE.format(3, item)))
            p = (p - 0.5 * rho * (vx**2 + vy**2)) * (GAMMA - 1)
            p_arr.append(p)
        if ND == 3:
            fields = (
                "rho",
                "vx",
                "vy",
                "vz",
                "p",
            )
            vy = np.array(read_field(FNAME_TEMPLATE.format(2, item))) * irho
            vy_arr.append(vy)
            vz = np.array(read_field(FNAME_TEMPLATE.format(3, item))) * irho
            vz_arr.append(vz)
            p = np.array(read_field(FNAME_TEMPLATE.format(4, item)))
            p = (p - 0.5 * rho * (vx**2 + vy**2 + vz**2)) * (GAMMA - 1)
            p_arr.append(p)

    all_data["rho"] = np.array(rho_arr)
    all_data["vx"] = np.array(vx_arr)
    if ND > 1:
        all_data["vy"] = np.array(vy_arr)
    if ND > 2:
        all_data["vz"] = np.array(vz_arr)
    all_data["p"] = np.array(p_arr)
    if ENABLE_DYE:
        fields.append("dye")
        all_data["dye"] = np.array(dye_arr)

    for i, f in enumerate(fields):
        # print(f"{i=} {f=}")
        data = all_data[f]
        if mode == "minmax":
            vmin = np.min(data)
            vmax = np.max(data)
        elif mode == "percentile":
            vmin = np.percentile(data, percentiles[0])
            vmax = np.percentile(data, percentiles[1])
        vlim[i, 0] = vmin
        vlim[i, 1] = vmax
    if ENABLE_DYE:
        vlim[-1, 0] = 0
        vlim[-1, 1] = 1
    return vlim


def plot_timestep(
    i=None,
    vlim=None,
    logscale=False,
    path=None,
    savefig_path="",
):
    if ND < 2:
        return

    # layout = [1, NF]
    # if NF == 4:
    #     layout = (2, 2)
    # nrows, ncols = layout[0], layout[1]
    if path is not None:
        i = int(path[-8:-4])
    labels = ("density", "vx", "vy", "vz", "P")

    if ND == 1:
        labels = ("density", "vx", "P")
        ncols = 3
        nrows = 1
    elif ND == 2:
        if NF == 5:
            labels = ("density", "vx", "vy", "P", "dye c")
            ncols = 5
            nrows = 1
        else:
            labels = ("density", "vx", "vy", "P")
            ncols = 2
            nrows = 2
    else:  # 3D
        if not ENABLE_DYE:
            labels = ("density", "vx", "vy", "vz", "P")
            ncols = 5
            nrows = 1
        elif ENABLE_DYE:
            labels = ("density", "vx", "vy", "vz", "P", "dye c")
            ncols = 6
            nrows = 1
        else:
            sys.exit("unable to understand data format")
    print(f"creating a figure with {nrows * ncols} axes, {nrows=} {ncols=}")
    f, axes = plt.subplots(ncols=ncols, nrows=nrows, sharey=False)
    n_axes = nrows * ncols
    # f, ax = plt.subplots()
    # a.set_xticks([x for x in np.linspace(0, npix, 6)])
    # a.set_xticklabels(["{:3.1f}".format(x) for x in np.linspace(0, 1, 6)])
    # a.set_xlabel("x")

    # fig, ax = plt.subplots(nrows=nrows, ncols=ncols)
    # ax.tick_params(bottom=True, top=True, left=True, right=True)
    # fig, ax = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, dpi=150)
    if len(sys.argv) > 1 and not using_proplot:
        axes = axes.ravel()
    f.set_facecolor("white")
    f.suptitle("Timestep = {}".format(i), fontsize=20)

    # ax[0].set_title(r"$\rho$", fontsize=fontsize)
    # ax[1].set_title(r"$v_x$", fontsize=fontsize)
    # ax[2].set_title(r"$v_y$", fontsize=fontsize)
    # if NF == 4:
    #     ax[3].set_title(r"$E_{tot}$", fontsize=fontsize)
    # elif NF == 5:
    #     ax[3].set_title(r"$v_z$", fontsize=fontsize)
    #     ax[4].set_title(r"$P$", fontsize=fontsize)

    data = []

    for ff in range(n_axes):
        fname = FNAME_TEMPLATE.format(ff, i)
        if path is not None:
            fname = path
        data.append(read_field(fname))
        if len(data[0].shape) == 1:
            return

        # we only log density and pressure
        if logscale:
            if f not in (1, 2, 3):
                data[-1] = np.log10(data[-1])

    rho = data[0]
    if True:
        irho = 1 / rho
        vx = data[1] * irho
        vy = data[2] * irho
        data[1] = vx
        data[2] = vy
        if NF == 5 and not ENABLE_DYE:
            vz = data[3] * irho
            p = (data[4] - 0.5 * rho * (vx**2 + vy**2 + vz**2)) * (7 / 5 - 1)
            data[3] = vz
            data[4] = p
        if NF == 5 and ENABLE_DYE:
            dye_c = data[4] * irho
            p = (data[3] - 0.5 * rho * (vx**2 + vy**2)) * (7 / 5 - 1)
            data[3] = p
            data[4] = dye_c
        if NF == 4:
            p = (data[3] - 0.5 * rho * (vx**2 + vy**2)) * (7 / 5 - 1)
            data[3] = p
        for ff in range(n_axes):
            ax = axes[ff]

            cmap = "viridis"
            if ENABLE_DYE and ff == n_axes - 1:
                cmap = "bwr"
            im = ax.imshow(
                data[ff].T,
                vmin=vlim[ff, 0],
                vmax=vlim[ff, 1],
                cmap=cmap,
                origin="lower",
            )
            # fig.colorbar(cax, ax=ax.flatten()[f], fraction=0.046, pad=0.04)
            if using_proplot:
                cax = ax.colorbar(im, loc="r")
                cax.set_label(f"{labels[ff]}")
            else:
                cax = f.colorbar(im, ax=ax)
                cax.set_label(f"{labels[ff]}")

    # ff = 0
    # im = ax[ff].imshow(data[ff].T, vmin=vlim[ff, 0], vmax=vlim[ff, 1], origin="lower")
    # # fig.colorbar(cax, ax=ax.flatten()[f], fraction=0.046, pad=0.04)
    # cax = ax[ff].colorbar(im, loc="r")
    # cax.set_label(f"{labels[ff]}")

    # if using_proplot:
    #     ax.format(grid=False)

    f.savefig(f"{savefig_path}img/{i:04d}.jpg", transparent=False)
    plt.close(f)


def calculate_radial_profile(
    field, bins=50, mode="mean", return_bins=True, offset=None
):
    field = np.array(field)
    xx, yy = np.meshgrid(np.arange(field.shape[0]), np.arange(field.shape[1]))
    if offset is None:
        x_offset = 0
        y_offset = 0
    else:
        x_offset = offset[0]
        y_offset = offset[1]
    xx -= x_offset
    yy -= y_offset
    distance = np.sqrt(xx**2 + yy**2)

    try:
        bins = int(bins)
        assert bins > 1
        distance_bins = np.linspace(0, field.shape[0] // 2, bins)
    except TypeError:
        distance_bins = bins

    if mode == "mean":
        func = np.mean
    elif mode == "sum":
        func = np.sum
    elif mode == "min":
        func = np.min
    elif mode == "max":
        func = np.max

    binned_qty = np.zeros(distance_bins.size - 1)
    for i, _ in enumerate(distance_bins[:-1]):
        mask = (distance >= distance_bins[i]) & (distance < distance_bins[i + 1])
        binned_qty[i] = func(field[mask])
    if return_bins:
        return distance_bins, binned_qty
    return binned_qty


def radial_profile_plot(i, field, ylim, bins, offset):
    field_id = {"rho": 0, "vx": 1, "vy": 2, "e": 3}[field]
    field_label = [r"$\rho$", r"$v_x$", r"$v_y$", r"$e$"][field_id]
    field_data = read_field(FNAME_TEMPLATE.format(field_id, i))
    if len(field_data.shape) > 1:
        return

    bins, radial_profile = calculate_radial_profile(
        field_data, return_bins=True, bins=bins, offset=offset
    )

    # plotting
    f, ax = plt.subplots()
    ax.set_xlabel(r"$r$ [a.u.]")
    ax.set_ylabel(field_label)
    ax.set_ylim(*ylim)
    ax.plot(bins[:-1], radial_profile)
    plt.savefig(
        "img/radial_profile_{}_{:04d}.png".format(field_id, i), transparent=False
    )
    plt.close(f)


def radial_profile_plot_multiprocessing(
    field: str,
    start: int,
    stop: int,
    ylim,
    bins: int = 50,
    autofind_center: bool = False,
):
    # skip for 1D
    if ND < 2:
        return

    field_id = {"rho": 0, "vx": 1, "vy": 2, "e": 3}[field]
    field_data = read_field(FNAME_TEMPLATE.format(field_id, 0))

    if autofind_center:
        where = np.argwhere(field_data == field_data.max())
        x_offset = (where.max() - where.min()) // 2 + where.min()
        y_offset = x_offset
    else:
        x_offset = field_data.shape[0] // 2
        y_offset = field_data.shape[1] // 2

    func = partial(
        radial_profile_plot,
        field=field,
        ylim=ylim,
        bins=bins,
        offset=[x_offset, y_offset],
    )

    with Pool(nproc) as p:
        p.map(func, np.arange(start, stop + 1, 1, dtype=int))

    command = (
        "nice -n20 ffmpeg -y -r 20 -i img/radial_profile_{}_%04d.png"
        " -c:v libx264 -vf 'fps=25,format=yuv420p,scale=1920:trunc(ow/a/2)*2' "
        "radial_profile_{}.mp4"
    ).format(field_id, field)

    if os.path.isfile("img/radial_profile_0_0000.png"):
        subprocess.call(
            command,
            shell=True,
        )
    else:
        print("skip plotting radial profiles")


def plot_slice(i, path, vlims=None):
    # get order from Config.sh
    order = get_order()

    data = []

    for f in range(NF):
        fname = path + FNAME_TEMPLATE.format(f, i)
        # if path is not None:
        #     print("path is not None")
        #     fname = path
        data.append(read_field(fname))
        if ND == 1:
            data[f] = data[f][0]

    rho = data[0]
    irho = 1 / rho
    vx = data[1] * irho
    vy = data[2] * irho
    data[1] = vx
    data[2] = vy
    if NF == 5 and not ENABLE_DYE:
        vz = data[3] * irho
        p = (data[4] - 0.5 * rho * (vx**2 + vy**2 + vz**2)) * (7 / 5 - 1)
        data[3] = vz
        data[4] = p
    if NF == 5 and ENABLE_DYE:
        dye_c = data[4] * irho
        p = (data[3] - 0.5 * rho * (vx**2 + vy**2)) * (7 / 5 - 1)
        data[3] = p
        data[4] = dye_c
    if NF == 4:
        p = (data[3] - 0.5 * rho * (vx**2 + vy**2)) * (7 / 5 - 1)
        data[3] = p

    if ND > 1:  # 2D data
        npix = data[0].shape[1]
        for f in range(NF):
            data[f] = data[f][:, npix // 2]
    elif ND == 1:  # 1D
        plot_data, smoothness = read_field(path + FNAME_TEMPLATE.format(0, i))
        npix = plot_data.size

    labels = ("density", "vx", "vy", "vz", "P")

    x = np.arange(0, npix, 1, dtype=int)
    if ND == 1:
        labels = ("density", "vx", "P")
        f, axes = plt.subplots(ncols=3, nrows=1, sharey=False)
    elif ND == 2:
        labels = ("density", "vx", "vy", "P")
        f, axes = plt.subplots(ncols=2, nrows=2, sharey=False)
    else:  # 3D
        if NF == 5 and not ENABLE_DYE:
            labels = ("density", "vx", "vy", "vz", "P")
        elif NF == 5 and ENABLE_DYE:
            labels = ("density", "vx", "vy", "P", "dye c")
        f, axes = plt.subplots(ncols=5, nrows=1, sharey=False)

    if len(sys.argv) > 1 and not using_proplot:
        axes = axes.ravel()

    for ii, ax in enumerate(axes):
        ax.set_ylabel(labels[ii])
        ax.set_xticks([x for x in np.linspace(0, npix, 6)])
        ax.set_xticklabels(["{:3.1f}".format(x) for x in np.linspace(0, 1, 6)])
        ax.set_xlabel("x")

    if vlims is not None:
        vlims_copy = vlims * 1
        vlims_copy[0] -= 0.05
        vlims_copy[1] += 0.05

    f.suptitle(
        "timestep={} nc={} order={}".format(i, NC, order),
        y=1.08,
    )

    # plot
    for j, ax in enumerate(axes):
        ax.plot(x, data[j], label=labels[j])

    # mark areas where artificial viscosity is injected
    # ax.fill_between(
    #     x,
    #     -100,
    #     100,
    #     color="red",
    #     alpha=0.6,
    #     where=np.log10(smoothness) > threshold - KAPPA,
    # )

    # plot smoothnes indicator
    if "smoothness" in locals():
        threshold = 0.022 / order / order
        ax2 = ax[0].twinx()
        if using_proplot:
            ax2.format(yformatter="log", yscale="log")
        else:
            ax2.set_yscale("log")
        # (ax2_label,) =
        ax2.plot(x, smoothness, c="k", ls="--", alpha=0.5, label="smoothness")
        # ax2.set_yscale("log")
        ax2.set_ylim([threshold / 100, 1])
        ax2.set_ylabel("Smoothness indicator")

        # add legends
        # p = [ax_label, ax2_label]
        # ax.legend(
        #     p,
        #     [p_.get_label() for p_ in p],
        #     # bbox_to_anchor=(0, 1.02, 1, 0.2),
        #     loc="right",
        #     # mode="expand",
        #     # borderaxespad=0,
        #     ncol=2,
        # )

        # plot threshold as a horizontal line
        ax2.axhline(threshold, ls="dotted", c="grey")
        # ax2.axhline(10 ** (threshold + KAPPA), ls="dotted", c="grey")

    # save figure
    f.savefig(path + "img/density_slice_{:04d}.png".format(i))

    # clear memory
    plt.close(f)


def bin_data_along_axis(bins: np.array, data: np.array, axis: int) -> np.array:
    axis_set = set(np.arange(0, data.shape[1]))
    assert axis in axis_set, "Chosen axis does not exist in data array."
    axis_set.discard(axis)
    data_binned = np.zeros((data.shape[1], bins.size - 1))
    data_binned[axis] = bins[:-1]
    for axis_idx in axis_set:
        data_binned[axis_idx], _, _ = binned_statistic(
            data[:, axis], data[:, axis_idx], bins=bins
        )
    return data_binned


def plot_turbulence_txt(path=""):
    # dg->Time, dg->TurbInjectedEnergy, dg->TurbDissipatedEnergy, Mach
    # dtype = np.dtype(
    #     [
    #         ("Time", "<f8"),
    #         ("TurbInjectedEnergy", "<f8"),
    #         ("TurbDissipatedEnergy", "<f8"),
    #         ("Mach", "<f8"),
    #     ]
    # )
    if os.path.isfile(path + "./data/turbulence.txt"):
        data = np.loadtxt(path + "./data/turbulence.txt")
        plot_limiter_injection = False
        if data.shape[1] == 6:
            plot_limiter_injection = True
        t_min = 0
        t_max = data[:, 0].max()
        t_bins = np.linspace(t_min, t_max, 1000 + 1)
        data = bin_data_along_axis(t_bins, data, 0)

        f, ax = plt.subplots(nrows=1, refwidth="88mm")
        ax.set_xlabel("Time")
        ax.set_ylabel("Energy (au)")
        h1 = ax.plot(data[0], data[1], label="Injected energy")
        h2 = ax.plot(data[0], data[2], label="Dissipated energy")
        if plot_limiter_injection:
            hl1 = ax.plot(data[0], data[4], label="Limiter injected mass")
            hl2 = ax.plot(data[0], data[5], label="Limiter injected energy")
        #   ax.legend((hl1, hl2), loc="lower right")

        ax2 = ax.twinx()
        ax2.set_ylabel("Mach number")
        h3 = ax2.plot(data[0], data[3], "--", c="grey", label="Mach number")
        ax.legend((h1, h2, h3, hl1, hl2), loc="top")
        f.savefig(path + "turb.jpg")
        plt.close(f)


if __name__ == "__main__":
    print(
        f"{ND=}\n{ORDER=}\n{GAMMA=}\n{NC=}\n{ENABLE_DYE=}\n{ART_VISCOSITY=}\n{NF=}\n{NF_WITHOUT_ART_VISC}"
    )

    nproc = min(os.cpu_count(), 10)
    if len(sys.argv) > 1:
        nproc = int(sys.argv[1])
    print("nproc={}".format(nproc))

    start, stop = get_available_shapshots()
    print("processing {} snapshot".format(stop - start))

    if os.path.isfile("data/turbulence.txt"):
        print("plotting injected kinetic energy")
        plot_turbulence_txt()
        print("injected kinetic energy plot done")

    if not os.path.isdir("img"):
        os.mkdir("img")

    print("getting vlimits")
    mode = "percentile"
    if ND == 1:
        mode = "minmax"
    vlims = get_vlimits(
        start,
        stop - 1,
        mode=mode,
        percentiles=(1, 99),
    )
    print("vlimits calculated")
    print("plotting radial profile")
    radial_profile_plot_multiprocessing(
        "rho", start, stop, vlims[0], bins=100, autofind_center=True
    )
    print("radial profile plot done")

    func = partial(plot_timestep, vlim=vlims)
    func_plot_slice = partial(plot_slice, path="", vlims=vlims[0])
    with Pool(nproc) as p:
        print("plotting fields")
        p.map(func, np.arange(start, stop + 1, 1, dtype=int))
    command = (
        "nice -n20 ffmpeg -y -r 20 -i img/%04d.png"
        " -c:v libx264 -vf 'fps=25,format=yuv420p,scale=1920:trunc(ow/a/2)*2' "
        "fields.mp4"
    )
    if os.path.isfile("img/0000.png") and os.path.isfile("img/0001.png"):
        print(f"running \n{command}")
        subprocess.call(command, shell=True)
        print("field plots done")
    else:
        print("skipped plotting fields")

    with Pool(nproc) as p:
        print("plotting fields")

        print("plotting density slices")
        p.map(func_plot_slice, np.arange(start, stop + 1, 1, dtype=int))
        command = (
            "nice -n20 ffmpeg -y -r 20 -i img/density_slice_%04d.png"
            " -c:v libx264 -vf 'fps=25,format=yuv420p,scale=1920:trunc(ow/a/2)*2' "
            "density_slice.mp4"
        )
        if os.path.isfile("img/density_slice_0000.png"):
            subprocess.call(command, shell=True)
            print("density slices plot done")
        else:
            print("skipped plotting density slices")
