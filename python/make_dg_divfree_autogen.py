#!/usr/bin/env python2

import itertools
import re
from argparse import ArgumentParser
from collections import OrderedDict

import numpy as np
import sympy
from sympy.printing.c import C89CodePrinter


class MyCodePrinter(C89CodePrinter):
    """Sympy code printer using * instead of pow() for integer exponentiation"""

    def _print_Pow(self, expr):
        if expr.exp.is_integer and expr.exp.is_number:
            return "(" + "*".join(["(" + self._print(expr.base) + ")"] * expr.exp) + ")"
        else:
            return super(MyCodePrinter, self)._print_Pow(expr)

    def _print_Rational(self, expr):
        """Don't use long doubles for rationals"""
        p, q = int(expr.p), int(expr.q)
        return "(%d.0/%d.0)" % (p, q)


cprinter = MyCodePrinter()
cprint = lambda expr: cprinter.doprint(expr)


def inner(a, b):
    if isinstance(a, sympy.Matrix):
        return a.dot(b)
    else:
        return a * b


def legendre(n, v):
    return sympy.legendre_poly(n, v)


def clip_degree(d):
    "Clip polynomial degree to -1 to avoid -inf"
    return -1 if d == sympy.S.NegativeInfinity else d


def total_poly_degree(p, vlist):
    "Total degree for multivariate polynomial p in all variables in vlist"
    p = sympy.Poly(p, *vlist)
    # Max over monomials of the total degree of each monomial
    return max(sum(clip_degree(degs)) for degs in p.monoms())


def poly_degrees(p, vlist):
    "Degrees of multivariate polynomial p in each of the variables in vlist"
    p = sympy.Poly(p, *vlist)
    return tuple(clip_degree(d) for d in p.degree_list())


# def poly_leading_term(p, vlist):
#    "Leading term in multivariate polynomial p"
#    p = sympy.Poly(p, *vlist)
#    terms = sorted(p.terms(), key=lambda (deg, coef): (-max(deg), deg))
#    return terms[0]


def vector_poly_leading_coef(vp, vlist):
    terms = [poly_leading_term(p, vlist) for p in vp]
    term = None
    for t in terms:
        if t[1] == 0:
            continue
        if (term is None) or (sum(t[0]) > sum(term[0])):
            term = t
    assert term is not None
    assert term[1] != 0
    return term[1]


def make_canonical_vector(i, ndim):
    assert i >= 0 and i < ndim
    e = sympy.Matrix(ndim * [0])
    e[i] = 1
    return e


class Cell(object):
    @property
    def vars(self):
        raise NotImplementedError()

    @property
    def ndim(self):
        return len(self.vars)

    def integrate(self, f):
        integrand = f
        for v in self.vars:
            integrand = sympy.integrate(integrand, (v, -1, 1))
        return integrand

    def dotp(self, f, g):
        """Dot product on cell of 2 (scalar or vector) functions"""
        return self.integrate(inner(f, g))

    def gen_scalar_basis(self, deg, deg_min=0, return_indices=False):
        """Generate Legendre scalar basis functions for degree <= deg"""
        for d in range(deg_min, deg + 1):
            for idx in itertools.product(*(self.ndim * [range(d + 1)])):
                idx = tuple(reversed(idx))
                if sum(idx) != d:
                    continue
                if return_indices:
                    yield idx, sympy.prod(
                        [legendre(k, v) for k, v in zip(idx, self.vars)]
                    )
                else:
                    yield sympy.prod([legendre(k, v) for k, v in zip(idx, self.vars)])

    def gen_divfree_basis_nonorthogonal(self, deg):
        raise NotImplementedError()

    def gen_divfree_basis_orthogonal(self, deg):
        """Gram-Schmidt orthogonalized basis"""
        ortho_basis = []

        for v in self.gen_divfree_basis_nonorthogonal(deg):
            for b in ortho_basis:
                p = self.dotp(v, b)
                m = self.dotp(b, b)
                assert m != 0
                v -= (p / m) * b
            if self.dotp(v, v) != 0:
                ortho_basis.append(v)
                assert self.divergence(v) == 0
                yield v

    def count_divfree_dof(self, deg):
        return self.ndim * len(list(self.gen_scalar_basis(deg))) - len(
            list(self.gen_scalar_basis(deg - 1))
        )

    def divergence(self, vector_poly):
        return sum(sympy.diff(p, v) for (p, v) in zip(vector_poly, self.vars))


def gen_first_divfree_vectors(deg, ndim):
    """Hardcode the first few basis vectors for div-free bases."""
    assert deg == 0
    # Degree 0: piecewise-constant components
    for dim in range(ndim):
        v = sympy.Matrix(ndim * [0])
        v[dim] = 1
        yield v


class Cell2D(Cell):
    vars = sympy.symbols("x y")

    def curl2d(self, f):
        x, y = self.vars
        d = sympy.diff
        return sympy.Matrix([d(f, y), -d(f, x)])

    def gen_divfree_basis_nonorthogonal(self, deg):
        for b in gen_first_divfree_vectors(0, 2):
            yield b
        for b in self.gen_scalar_basis(deg + 1, deg_min=2):
            yield self.curl2d(b)


class Cell3D(Cell):
    vars = sympy.symbols("x y z")

    def curl3d(self, vx, vy, vz):
        x, y, z = self.vars
        d = sympy.diff
        return sympy.Matrix(
            [d(vz, y) - d(vy, z), d(vx, z) - d(vz, x), d(vy, x) - d(vx, y)]
        )

    def gen_divfree_basis_nonorthogonal(self, deg):
        for b in gen_first_divfree_vectors(0, 3):
            yield b
        for b in self.gen_scalar_basis(deg + 1, deg_min=2):
            yield self.curl3d(b, 0, 0)
            yield self.curl3d(0, b, 0)
            yield self.curl3d(0, 0, b)


class BasisMixin(object):
    def make_expr(self, weight_name):
        w = [
            sympy.symbols("{w}[{i}]".format(w=weight_name, i=i))
            for i in range(len(self.basis))
        ]
        return w, sum(w[i] * b for i, b in enumerate(self.basis))

    def mass_matrix(self):
        return sympy.Matrix(
            [[self.cell.dotp(bi, bj) for bj in self.basis] for bi in self.basis]
        )

    def projected_coeff(self, expr, ib):
        b = self.basis[ib]
        return self.cell.dotp(expr, b) / self.cell.dotp(b, b)

    def projected_expr(self, expr):
        pieces = [self.projected_coeff(expr, ib) * b for ib, b in enumerate(self.basis)]
        return sum(
            pieces[1:], pieces[0]
        )  # Tricky way to do sum(pieces) so that sympy does not complain about type mismatches


class Basis(BasisMixin):
    def __init__(self, cell, deg):
        self.cell = cell
        self.ndim = cell.ndim
        self.vars = cell.vars
        self.deg = deg

        self.basis = sorted(
            cell.gen_divfree_basis_orthogonal(deg), key=self.get_vpoly_sort_key
        )
        assert len(self.basis) == cell.count_divfree_dof(deg)

        # Set last index for uniform and linear weights
        for i, v in enumerate(self.basis):
            info = self.get_vpoly_order_info(v)
            if info["deg_tot"] == 0:
                self.last_index_uniform = i
            if info["deg_tot"] == 1:
                self.last_index_linear = i

        self.first_index_linear = self.last_index_uniform + 1
        self.first_index_nonlinear = self.last_index_linear + 1

    def get_vpoly_order_info(self, vector_poly):
        deg_by_comp = np.array([total_poly_degree(c, self.vars) for c in vector_poly])
        return dict(
            deg_by_var=np.array([poly_degrees(c, self.vars) for c in vector_poly]).max(
                axis=1
            ),
            deg_by_comp=deg_by_comp,
            deg_tot=max(deg_by_comp),
        )

    def get_vpoly_sort_key(self, vector_poly):
        info = self.get_vpoly_order_info(vector_poly)
        # Secret sauce to get a nice ordering of the basis functions
        return (
            info["deg_tot"],
            sum(info["deg_by_comp"]),
            tuple(-info["deg_by_var"]),
            tuple(-info["deg_by_comp"]),
        )

    def solve_derivatives(self):
        "Compute and return mappings weights <-> gradients"
        # NOTE: we only consider valid gradients whose divergence are 0
        # In this case, only the linear weights contribute/are influenced by
        # the gradients.
        # Otherwise, if the gradients have non-zero divergence then there will
        # be some spill-over to higher order (deg >= 3) nonlinear weights

        # Assemble a linear vector function grad_f of all space variables with
        # ndim^2 gradient symbols in total
        grad_f = sympy.Matrix(self.ndim * [0])
        grad_symbols = OrderedDict()
        for icomp in range(self.ndim):
            for ivar, var in enumerate(self.vars):
                sym = sympy.symbols(
                    "dv{}_d{}".format(str(self.vars[icomp]), str(self.vars[ivar]))
                )
                grad_f[icomp] += var * sym
                grad_symbols[(icomp, ivar)] = sym

        # Project grad_f onto the basis functions to obtain the weights
        # as a function of the gradients
        # At the same time, we assemble in `eqs` the equations to solve the
        # gradients from the weights

        # This is the expression for the divergence based on gradient symbols
        expr_div = sum(grad_symbols[(i, i)] for i in range(self.ndim))
        eqs = [sympy.Eq(expr_div, 0)]  # Should always hold

        expr_w = OrderedDict()  # w(gradients)
        for i, v in enumerate(self.basis[: self.first_index_nonlinear]):
            w = sympy.symbols("w[{}]".format(i))
            proj = self.cell.dotp(v, grad_f) / self.cell.dotp(
                v, v
            )  # L2 projection onto basis vector v
            if proj != 0:
                expr_w[w] = proj
                eqs.append(sympy.Eq(expr_w[w], w))

        # Now solve for the gradients given the weights
        expr_grad = sympy.solve(eqs, grad_symbols.values())  # gradients(w)

        # Check that we have the right number of eqs
        assert len(expr_grad) == self.ndim**2  # ndim * ndim gradients
        assert (
            len(expr_w) == self.ndim**2 - 1
        )  # one less weight, because of div-free constraint

        # Return both mappings
        return expr_w, expr_grad

    def solve_averages(self):
        "Compute and return mappings weights <-> averages"
        # Assemble a uniform vector field uni_f for each component
        uni_symbols = [sympy.symbols("v{}".format(str(v))) for v in self.vars]
        uni_f = sympy.Matrix(uni_symbols)

        # Project uni_f onto the basis functions to obtain the weights
        # as a function of the gradients
        # At the same time, we assemble in `eqs` the equations to solve the
        # gradients from the weights
        eqs = []
        expr_w = OrderedDict()  # w(averages)
        for i, v in enumerate(self.basis[: self.first_index_linear]):
            w = sympy.symbols("w[{}]".format(i))
            proj = self.cell.dotp(v, uni_f) / self.cell.dotp(
                v, v
            )  # L2 projection onto basis vector v
            if proj != 0:
                expr_w[w] = proj
                eqs.append(sympy.Eq(expr_w[w], w))

        # Now solve for the gradients given the weights
        expr_uni = sympy.solve(eqs, uni_symbols)  # averages(w)

        # Check that we have the right number of eqs
        assert len(expr_uni) == len(expr_w) == self.ndim

        # Return both mappings
        return expr_w, expr_uni


class SwitchCasePrinter(object):
    def __init__(self, varname):
        self.text = "  switch ({})".format(varname) + "\n" + "    {"

    def add_case(self, val, code_lines):
        self.text += "\n      case ({}):".format(val)
        for ln in code_lines:
            self.text += "\n        {}".format(ln)
        self.text += "\n        break;"

    def finalize(self):
        self.text += (
            "\n      default:" + "\n        abort();" + "\n        break;" + "\n    }"
        )
        return self.text


def make_code_mass_diag(basis):
    m = basis.mass_matrix()
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_mass_diag(int bfunc, double * mdiag)".format(
            ndim=basis.ndim
        )
        + " {\n"
    )
    s = SwitchCasePrinter("bfunc")
    for b in range(len(basis.basis)):
        s.add_case(b, ["*mdiag = {};".format(cprint(m[b, b]))])
    code += s.finalize() + "\n}\n"
    return code


def make_code_bfunc_eval(basis):
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_bfunc_eval(int bfunc, {loc_vars}, {vec_vars})".format(
            loc_vars=", ".join(["double {}".format(str(v)) for v in basis.vars]),
            vec_vars=", ".join(["double * v{}".format(str(v)) for v in basis.vars]),
            ndim=basis.ndim,
        )
        + " {\n"
    )
    s = SwitchCasePrinter("bfunc")
    for b in range(len(basis.basis)):
        bfunc = basis.basis[b]
        s.add_case(
            b,
            [
                "*v{comp} = {code};".format(comp=str(v), code=cprint(bfunc[i]))
                for i, v in enumerate(basis.vars)
            ],
        )
    code += s.finalize() + "\n}\n"
    return code


def make_code_bfunc_grad(basis):
    code = ""
    for diff_var in basis.vars:  # Direction of derivative
        code += (
            "inline static void dg_divfree_autogen_{ndim}d_bfunc_diff_{diff}(int bfunc, {loc_vars}, {vec_vars})".format(
                loc_vars=", ".join(["double {}".format(str(v)) for v in basis.vars]),
                vec_vars=", ".join(
                    [
                        "double * dv{}_d{}".format(str(v), str(diff_var))
                        for v in basis.vars
                    ]
                ),
                ndim=basis.ndim,
                diff=str(diff_var),
            )
            + " {\n"
        )
        s = SwitchCasePrinter("bfunc")
        for b in range(len(basis.basis)):
            diff = sympy.diff(basis.basis[b], diff_var)
            s.add_case(
                b,
                [
                    "*dv{comp}_d{diff} = {code};".format(
                        comp=str(v), diff=str(diff_var), code=cprint(diff[i])
                    )
                    for i, v in enumerate(basis.vars)
                ],
            )
        code += s.finalize() + "\n}\n"

    return code


def make_code_get_averages(basis):
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_get_averages(int wsize, const double w[], {avg_vars})".format(
            avg_vars=", ".join(
                ["double * v{}".format(str(comp_var)) for comp_var in basis.vars]
            ),
            ndim=basis.ndim,
        )
        + " {\n"
    )

    expr_w, expr_avg = basis.solve_averages()

    # Check that we can safely read from the weights
    max_index = max(
        [int(re.match("^w\[(\d+)\]$", str(expr)).group(1)) for expr in expr_w.keys()]
    )
    code += "  assert({} < wsize);\n".format(max_index)

    for avg_var, expr in expr_avg.iteritems():
        code += "  *{avg} = {expr};\n".format(avg=str(avg_var), expr=cprint(expr))
    return code + "}\n"


def make_code_set_averages(basis):
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_set_averages({avg_vars}, int wsize, double w[])".format(
            avg_vars=", ".join(
                ["double v{}".format(str(comp_var)) for comp_var in basis.vars]
            ),
            ndim=basis.ndim,
        )
        + " {\n"
    )

    expr_w, expr_avg = basis.solve_averages()

    # Check that we can safely write to the weights
    max_index = max(
        [int(re.match("^w\[(\d+)\]$", str(expr)).group(1)) for expr in expr_w.keys()]
    )
    code += "  assert({} < wsize);\n".format(max_index)

    for w_var, expr in expr_w.iteritems():
        code += "  {w} = {expr};\n".format(w=str(w_var), expr=cprint(expr))
    return code + "}\n"


def make_code_get_slopes(basis):
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_get_slopes(int wsize, const double w[], {slope_vars})".format(
            slope_vars=", ".join(
                [
                    "double * dv{}_d{}".format(str(comp_var), str(diff_var))
                    for comp_var in basis.vars
                    for diff_var in basis.vars
                ]
            ),
            ndim=basis.ndim,
        )
        + " {\n"
    )

    expr_w, expr_grad = basis.solve_derivatives()

    # Check that we can safely read from the weights
    max_index = max(
        [int(re.match("^w\[(\d+)\]$", str(expr)).group(1)) for expr in expr_w.keys()]
    )
    code += "  assert({} < wsize);\n".format(max_index)

    for slope_var, expr in expr_grad.iteritems():
        code += "  *{slope} = {expr};\n".format(slope=str(slope_var), expr=cprint(expr))
    return code + "}\n"


def make_code_set_slopes(basis):
    code = (
        "inline static void dg_divfree_autogen_{ndim}d_set_slopes({slope_vars}, int wsize, double w[])".format(
            slope_vars=", ".join(
                [
                    "double dv{}_d{}".format(str(comp_var), str(diff_var))
                    for comp_var in basis.vars
                    for diff_var in basis.vars
                ]
            ),
            ndim=basis.ndim,
        )
        + " {\n"
    )

    expr_w, expr_grad = basis.solve_derivatives()

    # Check that input slopes are div-free
    code += "  const double div = {};\n".format(
        " + ".join(["dv{v}_d{v}".format(v=str(v)) for v in basis.vars])
    )
    code += "  const double tol = 1e-13;\n"
    code += "  assert(fabs(div) < tol);\n"

    # Check that we can safely write to the weights
    max_index = max(
        [int(re.match("^w\[(\d+)\]$", str(expr)).group(1)) for expr in expr_w.keys()]
    )
    code += "  assert({} < wsize);\n".format(max_index)

    for w_var, expr in expr_w.iteritems():
        code += "  {w} = {expr};\n".format(w=str(w_var), expr=cprint(expr))
    return code + "}\n"


if __name__ == "__main__":

    parser = ArgumentParser(
        description="generate C code for divergence-free basis functions"
    )
    parser.add_argument(
        "--max-degree",
        type=int,
        default=3,
        help="maximum polynomial degree to generate",
    )
    args = parser.parse_args()

    code = r"""/*!
* \copyright   This file is part of the AREPO code developed by Volker Springel.
* \copyright   Copyright (C) 2013  by Volker Springel (volker.springel@h-its.org)
* \copyright   and contributing authors.
*
* \file        src/dg/dg_divfree_autogen.h
* \date        09/2017
* \author      Thomas Guillet
* \brief       Auto-generated code for divergence-free basis functions
* \details
*
*
* \par Major modifications and contributions:
*
* - DD.MM.YYYY Description
*/

#ifndef DG_AUTOGEN_DIV0_H
#define DG_AUTOGEN_DIV0_H

#include <stdlib.h>
#include <assert.h>
#include <math.h>

"""

    def gen_snippets():
        for cell in [Cell2D(), Cell3D()]:
            basis = Basis(cell, args.max_degree)
            yield make_code_mass_diag(basis)
            yield make_code_bfunc_eval(basis)
            yield make_code_bfunc_grad(basis)
            yield make_code_get_averages(basis)
            yield make_code_set_averages(basis)
            yield make_code_get_slopes(basis)
            yield make_code_set_slopes(basis)

    code += "\n\n".join(list(gen_snippets()))

    code += "\n#endif // DG_AUTOGEN_DIV0_H\n"

    print(code)
