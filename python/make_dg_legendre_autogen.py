from argparse import ArgumentParser
from collections import OrderedDict

import numpy as np

from make_dg_divfree_autogen import Basis as DivFreeBasis
from make_dg_divfree_autogen import (
    BasisMixin,
    Cell2D,
    Cell3D,
    SwitchCasePrinter,
    cprint,
    total_poly_degree,
)


class LegendreBasis(BasisMixin):
    def __init__(self, cell, deg):
        self.cell = cell
        self.ndim = cell.ndim
        self.vars = cell.vars
        self.deg = deg

        self.basis = list(cell.gen_scalar_basis(deg))

        self.basis_deg = [total_poly_degree(b, self.vars) for b in self.basis]
        assert np.all(
            np.diff(self.basis_deg) >= 0
        )  # check basis in increasing order of degree
        assert set(self.basis_deg) == set(range(0, self.deg + 1))
        self.num_bfunc_per_deg = [
            sum(bdeg <= d for bdeg in self.basis_deg) for d in range(0, self.deg + 1)
        ]

        # Set last index for uniform and linear weights
        for i, v in enumerate(self.basis):
            deg_tot = total_poly_degree(v, self.vars)
            if deg_tot == 0:
                self.last_index_uniform = i
            if deg_tot == 1:
                self.last_index_linear = i

        self.first_index_linear = self.last_index_uniform + 1
        self.first_index_nonlinear = self.last_index_linear + 1


def make_code_bfunc_eval(basis):
    code = (
        "inline static void dg_legendre_autogen_{ndim}d_bfunc_eval(int bfunc, {loc_vars}, double * v)".format(
            loc_vars=", ".join(["double {}".format(str(v)) for v in basis.vars]),
            ndim=basis.ndim,
        )
        + " {\n"
    )
    s = SwitchCasePrinter("bfunc")
    for b in range(len(basis.basis)):
        bfunc = basis.basis[b]
        s.add_case(b, ["*v = {code};".format(code=cprint(bfunc))])
    code += s.finalize() + "\n}\n"
    return code


def get_basis(nd, max_degree=5):
    if nd == 2:
        return LegendreBasis(Cell2D(), max_degree)
    if nd == 3:
        return LegendreBasis(Cell3D(), max_degree)
