#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 17:45:43 2021

@author: cernetic
"""
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from make_dg_legendre_autogen import get_basis

# from scipy.special import legendre
# import sympy


class Grid:
    """
    Grid consists of all metadata connected to the simulation and holds all
    Cell objects.
    """

    def __init__(self, input_file):
        self.NF = 2
        self.read_data(input_file)

    def read_data(self, file: str):
        """
        read in the data from the code output

        file : str    file containing the data

        The metadata is contained in the first lines, until we encounter a
        line consisting of only "-".
        We store the metadata in a dictionary so there is no pre-defined list
        of data that needs to be present. We still check and quit if the
        minimum set of parameters are missing.
        """

        data = None
        with open(file, "r") as f:
            data = f.read().splitlines()

        metadata_flag = True
        self.metadata = {}
        i_metadata = 0
        for i, line in enumerate(data):
            if metadata_flag:
                if line == "--------":
                    metadata_flag = False
                    print("Metadata read:")
                    print(self.metadata)
                    print("Reading data")
                    i_metadata = i + 1
                    break
                else:
                    line_split = line.split(" ")
                    self.metadata[line_split[0]] = int(line_split[1])

        # set grid properties from the metadata dict
        for key in self.metadata.keys():
            setattr(self, key, self.metadata[key])
        self.CELLS_PER_SIDE = int(self.NC ** (1 / self.ND))

        # generate basis functions
        self.basis = get_basis(self.ND, max_degree=self.DEGREE_K)

        # allocate array for cells
        self.cells = np.empty(self.NC, dtype=Cell)

        # mask out the metadata
        data = np.genfromtxt(file, delimiter=",", skip_header=i_metadata + 1)

        # cell centres
        self.cell_centres = np.zeros([self.NC, self.ND])

        # populate cells with data
        for c in range(self.NC):
            mask = data[:, 0] == c
            coor = self.cell_id_to_coor(c)
            self.cells[c] = Cell(c, self.metadata, coor, self.basis, data[mask])

            self.cell_centres[c] = coor

    def cell_id_to_coor(self, idx):
        coor = []
        coor.append(idx % self.CELLS_PER_SIDE)
        coor.append((idx % (self.CELLS_PER_SIDE ** 2)) // self.CELLS_PER_SIDE)

        if self.ND == 3:
            coor.append(idx // (self.CELLS_PER_SIDE ** 2))
        return np.array(coor, dtype=int)

    def world_coor_to_cell_id_and_center(self, coor):
        # each cell is a square with sides [-1, 1]
        assert np.any(np.array(coor) >= 0)
        if self.ND == 2:
            coor = coor[:2]
        idx = None
        center = None
        if self.ND == 2:
            x, y = coor
            if x >= self.CELLS_PER_SIDE * 2 or y >= self.CELLS_PER_SIDE * 2:
                return np.nan, np.nan
            idx = int(x) + int(y) * self.CELLS_PER_SIDE
            center = np.array([x // 2 + 1.0, y // 2 + 1.0, 0], dtype=int)
        if self.ND == 3:
            x, y, z = coor
            if (
                x >= self.CELLS_PER_SIDE * 2
                or y >= self.CELLS_PER_SIDE * 2
                or z >= self.CELLS_PER_SIDE * 2
            ):
                return np.nan, np.nan
            idx = (
                int(x // 2)
                + int(y // 2) * self.CELLS_PER_SIDE
                + int(z // 2) * self.CELLS_PER_SIDE ** 2
            )
            # TODO: divide by cell width, not just 2
            center = np.array([x, y, z], dtype=int) // 2 * 2 + 1
        return idx, center

    def world_to_cell_coor(self, coor):
        _, center = self.world_coor_to_cell_id_and_center(coor)
        # print("center {}".format(center))
        # print(np.array(coor), center)
        return 2 * (np.array(coor) - center)

    def get_value_at_world_coor(self, world_coor, field: str):
        """
        Get value of field f at coords x, y.

        Parameters
        ----------
        world_coor : array-like
            coordinats.
        field : str
            which field.

        Returns
        -------
        field : float.
            value of field f at coor.

        """
        cell_id, _ = self.world_coor_to_cell_id_and_center(world_coor)
        local_coor = self.world_to_cell_coor(world_coor)
        if not cell_id < self.NC:
            return np.nan
        return self.cells[cell_id].get_value_at_coord(local_coor, field)

    def plot(
        self,
        field: str,
        x_min: float,
        x_max: float,
        y_min: float,
        y_max: float,
        z: float = 0,
        z_axis: int = 2,
        Nx: int = 10,
        Ny: int = 10,
    ):
        """
        Plot the field on a grid of [[x_min, x_max], [y_min, y_max]].

        Parameters
        ----------
        field : str
            One of rho, px, py, pz, e.
        x_min : float
            The minimum x world coordinate.
        x_max : float
            The maximum x world coordinate.
        y_min : float
            The minimum y world coordinate.
        y_max : float
            The maximum y world coordinate.
        z : float
            Value of z-coordinate. The default is 0.
        z_axis : int
            Select the axis through which to slice. The default is 2.
        Nx : int, optional
            Number of pixels in x-direction. The default is 10.
        Ny : int, optional
            Number of pixels in y-direction. The default is 10.

        Returns
        -------
        None.

        """

        # sanity checks
        assert x_min >= 0 and x_max and y_min >= 0 and y_max >= 0 and z >= 0
        assert x_min < x_max
        assert y_min < y_max
        assert z_axis in (0, 1, 2)
        if self.ND == 2:
            assert z == 0
        xx = np.linspace(x_min, x_max, Nx)
        yy = np.linspace(y_min, y_max, Ny)

        # allocate plotting data
        self.plot_data = np.zeros([Nx, Ny], dtype=np.single)

        # go through every pixel
        for j, y in enumerate(yy):
            for i, x in enumerate(xx):
                if z_axis == 0:
                    x_query, y_query, z_query = y, z, x
                if z_axis == 1:
                    x_query, y_query, z_query = x, z, y
                if z_axis == 2:
                    x_query, y_query, z_query = x, y, z
                self.plot_data[i, j] = self.get_value_at_world_coor(
                    [x_query, y_query, z_query], field
                )

        f, ax = plt.subplots(dpi=200)
        #         cax = ax.imshow(np.flip(self.plot_data, 1).T,
        cax = ax.imshow(
            np.flip(self.plot_data, 1).T, extent=[x_min, x_max, y_min, y_max]
        )
        plt.colorbar(cax)

    def draw_cell_borders(self):
        patches = []
        for cell in self.cells:
            patches.append(cell.draw_borders())
        return patches

    def generate_cell_centres(self):
        self.cell_centres = np.zeros(self.NC, self.ND)
        for c in range(self.NC):
            self.cell_centres[c] = self.world_coor_to_cell_id_and_center()


class Cell:
    """
    Cell holds all cell related metadata and Qpoint objects.
    """

    def __init__(
        self, cell_id: int, metadata: dict, center: np.array, basis, weights: np.array
    ):
        self.cell_id = cell_id
        self.center = center
        self.basis = basis

        for key in metadata.keys():
            setattr(self, key, metadata[key])

        self.read_weights(weights)

    def basis_at_coord(self, coor: np.array):
        bfunc = np.zeros(self.NB, dtype="f")
        for b in range(self.NB):
            try:
                x, y = coor
            except ValueError:
                x, y, z = coor

            to_eval = self.basis.basis[b]
            bfunc[b] = eval(str(to_eval))
        return bfunc

    def read_weights(self, weights):
        self.weights = np.array(weights[:, 2:])

    def draw_borders(self):
        # Create a Rectangle patch
        rect = patches.Rectangle(
            self.center[:2], 2, 2, linewidth=1, edgecolor="k", facecolor="none"
        )
        return rect

    def get_value_at_coord(self, coor: np.array, field: str):
        """
        Get value of field at x, y.
        f = Sigma_i w_i theta_i

        Parameters
        ----------
        coor : array-like
            coordinates.
        field : str
            DESCRIPTION.

        Returns
        -------
        value : float
            value of field at coor.

        """
        if self.ND == 3:
            fields = {"rho": 0, "px": 1, "py": 2, "pz": 3, "e": 4}
        if self.ND == 2:
            fields = {"rho": 0, "px": 1, "py": 2, "e": 3}
        f_idx = fields[field]
        basis = self.basis_at_coord(coor)
        return np.sum(basis * self.weights[f_idx])


if __name__ == "__main__":
    grid = Grid("qpoints.txt")
