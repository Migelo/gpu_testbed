import argparse
import math

import numpy as np

# capacity given in gigabytes, e.g. power of 10, NOT 2
# testing shows about 2 GBs are allways allocated on A100
gpus = {"p100": 16e9, "v100": 32e9, "a100": (40 - 2) * 1e9}


def calculate_memory_usage(
    nd: int,
    order: int,
    n_cells_per_dimension: int,
    turbulence: bool = False,
    verbose: bool = True,
) -> int:
    """Calculate memory usage of a run with given parameters.

    Args:
        nd (int): Number of spatial dimensions.
        order (int): Spatial order of the method.
        n_cells_per_dimension (int): Number of cells per dimension.
        verbose (bool, optional): Increase verbosity. Defaults to True.

    Returns:
        int: Memory usage in bytes.
    """
    n_qpoints_per_dimension = order + 1
    dg_order = order + 1

    if dg_order == 1:
        rk_stages = 1
    elif dg_order == 2:
        rk_stages = 2
    elif dg_order == 3:
        rk_stages = 3
    else:
        rk_stages = 5

    if nd == 2:
        nf = 4
        nb = (order + 1) * (order + 2) / 2
        n_inner_qpoints = (n_qpoints_per_dimension) * (n_qpoints_per_dimension)
        n_outer_qpoints = n_qpoints_per_dimension
        nc = n_cells_per_dimension * n_cells_per_dimension

    if nd == 3:
        nf = 5
        nb = (order + 1) * (order + 2) * (order + 3) / 6
        n_inner_qpoints = (
            (n_qpoints_per_dimension)
            * (n_qpoints_per_dimension)
            * (n_qpoints_per_dimension)
        )
        n_outer_qpoints = (n_qpoints_per_dimension) * (n_qpoints_per_dimension)
        nc = n_cells_per_dimension * n_cells_per_dimension * n_cells_per_dimension

    memory_usage = 0
    # main data
    n_weights = nb * nf * nc
    memory_usage += n_weights

    # basis function
    # internal
    memory_usage += nb * n_inner_qpoints
    # internal derivatives of every order in every dimension
    memory_usage += n_inner_qpoints * nb * nd
    # external basis
    memory_usage += 2 * nd * nb * n_outer_qpoints

    # timestep data
    # temporary weight storage
    memory_usage += n_weights
    # dw/dt
    memory_usage += n_weights * rk_stages
    # dwdt ghost cells
    states_on_plane = n_cells_per_dimension * n_outer_qpoints
    if nd == 3:
        states_on_plane *= n_cells_per_dimension
    memory_usage += states_on_plane
    weights_on_plane = n_cells_per_dimension * nb * nf
    if nd == 3:
        weights_on_plane *= n_cells_per_dimension
    memory_usage += weights_on_plane

    # various functions need one array to reduce data for all cells
    memory_usage += nc

    # limiter
    numw = n_cells_per_dimension * nf
    if nd == 3:
        numw *= n_cells_per_dimension
    memory_usage += 2 * numw

    # turbulence driving
    if turbulence:
        StKmin = 6.27
        StKmax = 12.57

        ikxmax = int(1 * StKmax / (2 * np.pi))
        ikymax = int(1 * StKmax / (2 * np.pi))
        if nd == 3:
            ikzmax = int(1 * StKmax / (2 * np.pi))
        else:
            ikzmax = 0
        StNModes = 0
        for ikx in range(-ikxmax, ikxmax + 1):
            for iky in range(-ikymax, ikymax + 1):
                for ikz in range(-ikzmax, ikzmax + 1):
                    if ikx < 0:
                        continue

                    if ikx == 0 and iky < 0:
                        continue

                    if ikx == 0 and iky == 0 and ikz < 0:
                        continue

                    kx = 2.0 * np.pi * ikx / 1
                    ky = 2.0 * np.pi * iky / 1
                    kz = 2.0 * np.pi * ikz / 1
                    k = np.sqrt(kx * kx + ky * ky + kz * kz)

                    if k > 0 and k >= StKmin and k <= StKmax:
                        StNModes += 1
        memory_usage += StNModes * 10

    # double precision
    memory_usage *= 8

    if verbose:
        print("Predicted memory usage: {:4.1f} GB".format(memory_usage / (1024**3)))

    return memory_usage


def calc_ndof(d: int, k: int, c: int, verbose=False) -> int:
    """Calculate the degrees of freedom for a run.

    Args:
        d (int): Number of dimensions.
        k (int): Spatial order of the method.
        c (int): Number of cells per dimension.
        verbose (bool, optional): [description]. Defaults to False.

    Returns:
        int: [description]
    """
    nd = d
    order = k
    n_cells_per_dimension = c
    n_qpoints_per_dimension = order + 1

    if nd == 2:
        n_inner_qpoints = (n_qpoints_per_dimension) * (n_qpoints_per_dimension)
        nc = n_cells_per_dimension * n_cells_per_dimension

    if nd == 3:
        n_inner_qpoints = (
            (n_qpoints_per_dimension)
            * (n_qpoints_per_dimension)
            * (n_qpoints_per_dimension)
        )
        nc = n_cells_per_dimension * n_cells_per_dimension * n_cells_per_dimension

    ndof = nc * n_inner_qpoints

    if verbose:
        print(f"ndof = {ndof}")

    return ndof


def get_n_gpus(
    nd: int, order: int, n_cells_per_dimension: int, gpu: str, verbose: bool = True
) -> int:
    memory_usage = calculate_memory_usage(nd, order, n_cells_per_dimension, verbose)

    memory_available_per_gpu = gpus[gpu]
    ngpus = math.ceil(memory_usage / memory_available_per_gpu)
    if verbose:
        print("got {} x {}".format(ngpus, gpu))
    return int(ngpus)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Estimate memory usage.")
    parser.add_argument(
        "dimensions", metavar="d", type=int, help="an integer for the accumulator"
    )
    parser.add_argument(
        "order", metavar="k", type=int, help="sum the integers (default: find the max)"
    )
    parser.add_argument(
        "ncells", metavar="c", type=int, help="sum the integers (default: find the max)"
    )

    parser.add_argument("gpu", metavar="g", type=str, help="which gpu?")

    args = parser.parse_args()

    nd = args["dimensions"]
    order = args["order"]
    n_cells_per_dimension = args["ncells"]

    get_n_gpus(nd, order, n_cells_per_dimension, args["gpu"])
