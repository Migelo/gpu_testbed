from pathlib import Path

from .memory_estimates import *
from .prepare_run import prepare_code_run
from .scaling_tests import (
    General_scaling_result,
    Problem,
    Scaling_result_base,
    Weak_scaling_result,
    calc_half_problem_size,
    calc_max_problem_size_power_two,
    clusters,
    find_max_problem_for_given_gpus,
    get_problems_to_run,
    get_timestep_duration_from_log,
    get_weak_scaling_problems_to_run,
    process_weak_scaling_results,
    round_up_to_next_power_of_two,
)
from .templates import generate_config_template, generate_jobs, generate_parameter_file

print("Running" if __name__ == "__main__" else "Importing", Path(__file__).resolve())
