CONFIG_TEMPLATE = """
GPU

TURBULENCE

ND=3L                                           # sets number of dimensions
DEGREE_K={}L                                    # order of the DG scheme, i.e. order of 1D polynomials to form the basis functions
DG_NUM_CELLS_1D={}L                             # number of cells per dimension
"""


PARAM_TEMPLATE = """
Tend       {}
CourantFac 0.5

DesiredDumps       {}
PixelsFieldMaps    {}

OutputDir  ./data


IsoSoundSpeed 1.0

StKmin       6.27
StKmax       12.57
StSpectForm  2

StDtFreq    {}            \% update frequency
StDecay     1.0              \% coherence timescale ts



StEnergy    0.1

StSolWeight  1.0     \% set to 1 for purely solenoidal driving
StAmplFac    {}
StSeed       42

MinimumSlabsPerCPURank  0
WriteRestartAfter {}
WriteCheckpointAfter {}
"""


JOB_HEADER_TEMPLATE = """#!/bin/bash -l
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=cernetic@mpa-garching.mpg.de
#SBATCH --time={}
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks={}
#SBATCH --job-name c{}k{}
#SBATCH --partition={}
#SBATCH --gres=gpu:a100:4
#SBATCH --exclusive

echo
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"
echo
echo

{}

"""

JOB_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
make -j 2
rm -f data/* img/* vlims.npy
mkdir -p data/restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt | tee log.txt
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    module load anaconda && source activate py39 && python plots.py |& tee plots.log
    python plot_vel_power_spec.py
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""
)


JOB_RESTART_TEMPLATE = (
    JOB_HEADER_TEMPLATE
    + """
FILE=./.finished
RESTART=./.restart
touch "$FILE"
mpiexec -np "$SLURM_NPROCS" ./run.run param.txt 1
if [ ! -f "$FILE" ] ; then
    echo "Simulation done!"
    module load anaconda && source activate py39 && python plots.py |& tee plots.log
    python plot_vel_power_spec.py
elif [ -f "$RESTART"  ]; then
    rm -f "$RESTART"
    echo "Reached end of time, exiting and submitting a restart job."
    sbatch job_restart.sh
else
    echo "Things have gone awry, quitting."
fi
"""
)


MODULES_DICT = {
    "raven": """
module load anaconda
module load gcc/8
module load openmpi/4
module load fftw-serial
module load hdf5-serial
module load gsl
module load git
module load hwloc
module load cuda/11.4
""",
    "freya": "",
    "freyavolta": "",
}

SLURM_PARTITION_DICT = {"raven": "gpu", "freya": "p.gpu.ampere", "freyavolta": "p.gpu"}


def generate_config_template(order: int, nc: int) -> str:
    """generate Config.sh

    Args:
        order (int): Spatial order of the DG method
        nc (int): Number of cells per dimension

    Returns:
        str: text of the Config.sh file
    """
    return CONFIG_TEMPLATE.format(order, nc)


def generate_parameter_file(
    t_end: float,
    n_dumps: int = 512,
    pixels: int = 320,
    driving_freq: float = 0.005,
    amplification_factor: float = 0.1,
    write_restart_after: int = 82800,
    write_checkpoint_after: int = 7200,
) -> str:
    """generate param.txt

    Args:
        t_end (float): Runtime of the simulation
        n_dumps (int, optional): Number of dumps to produce. Defaults to 512.
        pixels (int, optional): Resolution of the dumps. Defaults to 320.
        driving_freq (float, optional): How often to update turbulence driving. Defaults to 0.005.
        amplification_factor (float, optional): Strength of the driving. Defaults to 0.1.
        write_restart_after (int, optional): After how many seconds to write out the restart files. Defaults to 82800 (23h).
        write_checkpoint_after (int, optional): After how many seconds to write out the checkpoint files. Defaults to 7200 (2h).

    Returns:
        str: Contents of param.txt
    """
    return PARAM_TEMPLATE.format(
        t_end,
        n_dumps,
        pixels,
        driving_freq,
        amplification_factor,
        write_restart_after,
        write_checkpoint_after,
    )


def generate_jobs(
    cluster: str, n_tasks: int, job_runtime: float, nc: int, order: int
) -> tuple:
    """generate job.sh and job_restart.sh

    Args:
        cluster (str): what cluster are we running on, for modules
        n_tasks (int): Number of tasks/gpus we need.
        job_runtime (float): End time of simulation.
        nc (int): Number of cells per dimension.
        order (int): Spatial order of the DG method.

    Returns:
        tuple: job.sh and job_restart.sh text
    """
    return (
        JOB_TEMPLATE.format(
            job_runtime,
            n_tasks,
            nc,
            order,
            SLURM_PARTITION_DICT[cluster],
            MODULES_DICT[cluster],
        ),
        JOB_RESTART_TEMPLATE.format(
            job_runtime,
            n_tasks,
            nc,
            order,
            SLURM_PARTITION_DICT[cluster],
            MODULES_DICT[cluster],
        ),
    )
