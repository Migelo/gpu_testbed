import os
from glob import glob
from numbers import Number

import numpy as np

from .memory_estimates import calc_ndof, get_n_gpus

clusters = {"raven": 128 * 4, "freya": 40, "freyavolta": 40}


class Problem:
    """This class containts the main parameters of any run."""

    def __init__(self, nc: int, order: int, endtime: float, n_gpu: int) -> None:
        """[summary]

        Args:
            nc (int): Number of cells per dimension.
            order (int): Spatial order of the DG method.
            endtime (float): Simulation endtime.
            n_gpu (int): Number of GPUs to use.
        """
        self.nc = int(nc)
        self.order = int(order)
        self.endtime = float(endtime)
        self.n_gpu = int(n_gpu)


class Scaling_result_base:
    """Base class containing results of a scaling run."""

    def __init__(
        self,
        success: bool,
        nc: int = 0,
        order: int = 0,
    ) -> None:
        self.success = success
        self.nc = nc
        self.order = order
        self.ndof = calc_ndof(3, order, nc)


class Weak_scaling_result(Scaling_result_base):
    """Class containing results of a weak scaling run."""

    def __init__(
        self,
        success: bool,
        nc: int = 0,
        order: int = 0,
        total_runtime: int = 0,
    ) -> None:
        super().__init__(success, nc, order)
        self.total_runtime = total_runtime


class General_scaling_result(Scaling_result_base):
    """Class containing results of a scaling run."""

    def __init__(
        self,
        success: bool,
        nc: int = 0,
        order: int = 0,
        median_timestep_duration: int = 0,
        timestep_std: int = 0,
    ) -> None:
        super().__init__(success, nc, order)
        self.median_timestep_duration = median_timestep_duration
        self.timestep_std = timestep_std


class Strong_scaling_result(General_scaling_result):
    """Class containing results of a scaling run."""

    def __init__(
        self,
        success: bool,
        ngpu: int,
        nc: int,
        order: int,
        median_timestep_duration: int,
        timestep_std: int,
    ) -> None:
        super().__init__(success, nc, order, median_timestep_duration, timestep_std)
        self.ngpu = ngpu


def make_number_even(number: Number) -> int:
    """Converts number to an even number by rounding down.

    Args:
        number (Number): Number to be made even

    Returns:
        int: An even number.
    """
    return number // 2 * 2


def round_up_to_next_power_of_two(number: Number) -> int:
    if number < 1:
        number = 1
    number = np.power(2, int(np.ceil(np.log2(number))))
    print(number)
    number = np.max((4, number))
    return number


def calc_max_problem_size(
    max_gpu: int, order: int, nd=3, verbose: bool = False
) -> tuple[int, int]:
    n_cells_per_dimension = 10000
    n_gpu = np.inf
    while n_gpu > max_gpu:
        n_gpu = get_n_gpus(nd, order, n_cells_per_dimension, "a100", verbose=verbose)
        n_cells_per_dimension -= 2
    # once the max problem size is found we need to modify it to
    # ensure every GPU gets an even number of slabs
    n_cells_per_dimension, n_gpu = find_suitable_problem_size(
        n_cells_per_dimension, n_gpu
    )
    return (n_cells_per_dimension, n_gpu)


def calc_max_problem_size_power_two(
    max_gpu: int, order: int, nd=3, verbose: bool = False
) -> tuple[int, int]:
    n_cells_per_dimension = 65536
    n_gpu = np.inf
    while n_gpu > max_gpu:
        n_cells_per_dimension //= 2
        if verbose:
            print(
                (
                    f"Checking if problem size {n_cells_per_dimension} "
                    f"fits on {max_gpu} GPUs"
                )
            )
        n_gpu = get_n_gpus(nd, order, n_cells_per_dimension, "a100", verbose=verbose)
        n_gpu = round_up_to_next_power_of_two(n_gpu)
        if verbose:
            print(f"after rounding up to the nearest power of two we get {n_gpu}")
    if verbose:
        print(
            (
                f"Found a suitable problem size {n_cells_per_dimension} "
                f"that fits on {n_gpu} GPUs"
            )
        )
    return (n_cells_per_dimension, n_gpu)


def find_suitable_problem_size(
    n_cells_per_dimension: int, n_gpu: int
) -> tuple[int, int]:
    """Find a problem size at which all gpus get an even number of slabs to process.

    Args:
        n_cells_per_dimension (int): cells per dimension
        n_gpu (int): number of gpus

    Returns:
        int: suitable number of cells per dimension
        int: number of gpus needed for this problem
        int: number of nodes needed for this problem
    """
    n_cells_per_gpu = make_number_even(n_cells_per_dimension / n_gpu)
    n_cells_per_dimension = n_cells_per_gpu * n_gpu
    return (n_cells_per_dimension, n_gpu)


def calc_half_problem_size(
    problem_size: int, order: int, gpu: str, nd: int = 3
) -> tuple[int, int, int]:
    """Calculate size of a problem with half the total number of cells.

    Args:
        problem_size (int): Problem size to find the half for.
        order (int): Spatial order of the method.
        gpu (str): GPU we are running on.
        nd (int, optional): Number of dimensions. Defaults to 3.

    Returns:
        tuple[int, int, int]: Number of cells per dimenions of the halved problem,
                              number of GPUs, number of nodes
    """
    cell_number = problem_size**nd
    half_cell_number = cell_number // 2
    half_problem_size = np.power(half_cell_number, 1 / nd)
    half_problem_size = int(half_problem_size)
    n_gpu = get_n_gpus(nd, order, half_problem_size, gpu, False)
    half_problem_size, n_gpu = find_suitable_problem_size(half_problem_size, n_gpu)

    return (half_problem_size, n_gpu)


def get_weak_scaling_problems_to_run(
    cluster: str,
    order: int,
    nd: int = 3,
    verbose: bool = False,
) -> list[Problem]:
    problem_list = []
    max_problem_size, n_gpu = calc_max_problem_size_power_two(
        clusters[cluster], order, verbose=verbose
    )
    problem_list.append(Problem(max_problem_size, order, 1.28, n_gpu))

    counter = 1
    while n_gpu // 2**nd >= 1:
        # for i in range(int(np.log2(n_gpu) - 3), 1, -3):
        n_gpu //= 2**nd
        if verbose:
            print(
                (
                    f"adding a problem with size {max_problem_size / 2 ** counter} "
                    f"on {n_gpu} GPUs"
                )
            )
        problem_list.append(
            Problem(max_problem_size / 2**counter, order, 1.28, n_gpu)
        )
        counter += 1

    return problem_list


def find_max_problem_for_given_gpus(nd: int, order: int, gpu: str, n: int) -> int:
    n_cells_per_dimension = 10000
    n_gpu = 9999999

    while n_gpu > n:
        n_gpu = get_n_gpus(nd, order, n_cells_per_dimension, gpu, False)
        n_cells_per_dimension -= 1

    return make_number_even(make_number_even(n_cells_per_dimension) // n_gpu) * n_gpu


def get_problems_to_run(
    cluster: str,
    order: int,
    nd: int = 3,
    min_problem_size: int = 8,
    verbose: bool = False,
) -> list:
    problem_list = []
    max_problem_size, n_gpu = calc_max_problem_size(clusters[cluster], order)

    if max_problem_size < min_problem_size:
        return []

    problem_list.append(Problem(max_problem_size, order, 1.28, n_gpu))

    half_problem_size, n_gpu = calc_half_problem_size(
        max_problem_size, order, "a100", nd=nd
    )

    i = 0
    got_one_gpu = n_gpu > 1
    while got_one_gpu:
        if i == 0:
            if half_problem_size < min_problem_size:
                return problem_list
            problem_list.append(Problem(half_problem_size, order, 1.28, n_gpu))
            i += 1
            continue
        half_problem_size, n_gpu = calc_half_problem_size(
            half_problem_size, order, "a100", nd=nd
        )
        # sanity checks
        if half_problem_size < 8:
            return problem_list
        problem_list.append(Problem(half_problem_size, order, 1.28, n_gpu))
        if n_gpu == 1:
            got_one_gpu = False
    return problem_list


def get_timestep_duration_from_log(
    path: str, skip_first_n: int = 1
) -> "np.ndarray[float, float]":
    """Get median timestep duration and std. Skip the first timestep.

    Args:
        path (str): Path to the folder with the log.

    Returns:
        NDArray[float, float]: median timestep duration, std
    """
    out_data = []
    with open(path + "/log.txt", "r") as f:
        data = f.readlines()
    for line in data:
        if "Step took" in line:
            out_data.append(int(line.split(" ")[2]))
    results = np.array(out_data, dtype=np.int64)
    results = results[skip_first_n:]

    return np.mean(results), results.std()


def get_total_runtime_from_log(path: str, verbose: bool = True) -> np.int64:
    """Parse the run log and get median timestep duration and std.

    Args:
        path (str): Path to the folder with the log.
        verbose (bool): Print debug data.

    Returns:
        NDArray[float, float]: median timestep duration, std
    """
    with open(path + "/log.txt", "r") as f:
        data = f.readlines()
    for line in data:
        if "[SCALING_TIMER]" in line:
            runtime = np.int64(line.split(" ")[6])
            if verbose:
                print(f"[get_total_runtime_from_log] got runtime = {runtime}")
            return runtime
    print("[get_total_runtime_from_log] error: didn't find runtime, returing 0")
    return 0


def did_run_finish(path: str, verbose: bool = False) -> bool:
    """Parse the run log and get median timestep duration and std.

    Args:
        path (str): Path to the folder with the log.
        verbose (bool): Print debug data.

    Returns:
        bool: True if the run finished, False if it did not
    """
    logpath = path + "/log.txt"
    if verbose:
        print(f"[did_run_finish] processing logfile {logpath}, ", end="")
    if os.path.exists(logpath):
        with open(logpath, "r") as f:
            data = f.readlines()
        for line in data:
            if "[SCALING_TIMER]" in line:
                if verbose:
                    print("exists")
                return True
    # print("does not exist")
    return False


def get_nc_and_order_from_folder(folder_name: str) -> tuple[int, int]:
    """Parses the folder name and returs NC and order of the run.

    Args:
        folder_name (str): Path to the folder where the run is.

    Returns:
        tuple[int, int]: nc, order
    """
    # keep just the folder name
    if folder_name[-1] == "/":
        folder_name = folder_name[:-1]
    folder_name = folder_name.split("/")[-1]
    nc, order = folder_name.split("_")
    return int(nc[2:]), int(order[1:])


def get_nc_and_order_from_folder_strong(folder_name: str) -> tuple[int, int, int]:
    """Parses the folder name and returs NC and order of a strong scaling run.

    Args:
        folder_name (str): Path to the folder where the run is.

    Returns:
        tuple[int, int]: nc, order
    """
    # keep just the folder name
    if folder_name[-1] == "/":
        folder_name = folder_name[:-1]
    folder_name = folder_name.split("/")[-1]
    nc, order, ngpu = folder_name.split("_")
    return int(nc[2:]), int(order[1:]), int(ngpu)


def get_timestep_durations(output_folder: str) -> list[General_scaling_result]:
    output_folders = glob(output_folder + "/*")

    timings = []
    for output_folder in output_folders:
        # finish = did_run_finish(output_folder)
        # print(f"{output_folder=} {finish=}")
        # if True:
        try:
            nc, order = get_nc_and_order_from_folder(output_folder)
            median_timestep_duration, timestep_std = get_timestep_duration_from_log(
                output_folder
            )
            timings.append(
                General_scaling_result(
                    True, nc, order, median_timestep_duration, timestep_std
                )
            )
        except FileNotFoundError:
            timings.append(General_scaling_result(False))

    return timings


def get_timestep_durations_strong(output_folder: str) -> list[General_scaling_result]:
    output_folders = glob(output_folder + "/*")

    timings = []
    for output_folder in output_folders:
        finish = did_run_finish(output_folder)
        if finish:
            nc, order, ngpu = get_nc_and_order_from_folder_strong(output_folder)
            median_timestep_duration, timestep_std = get_timestep_duration_from_log(
                output_folder
            )
            timings.append(
                Strong_scaling_result(
                    finish, ngpu, nc, order, median_timestep_duration, timestep_std
                )
            )
        else:
            timings.append(General_scaling_result(finish))

    return timings


def process_weak_scaling_results(
    output_folder: str, verbose: bool = False
) -> list[Weak_scaling_result]:
    output_folders = glob(output_folder + "/*")

    timings = []
    for output_folder in output_folders:
        finish = did_run_finish(output_folder, verbose)
        if finish:
            nc, order = get_nc_and_order_from_folder(output_folder)
            total_runtime = get_total_runtime_from_log(output_folder, verbose)
            timings.append(Weak_scaling_result(finish, nc, order, total_runtime))
        else:
            timings.append(Weak_scaling_result(finish))

    return timings
