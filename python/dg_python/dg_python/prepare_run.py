import os
import shutil
import subprocess
from functools import partial
from pathlib import Path

from .scaling_tests import Problem
from .templates import generate_config_template, generate_jobs, generate_parameter_file

run_shell = partial(subprocess.call, shell=True)


def prepare_code_run(
    problem: Problem,
    cluster: str,
    output_folder: str,
    run: bool,
    job_runtime: str = None,
    output_folder_already_formatted: bool = False,
    config_template: str = None,
    no_restart: bool = False,
    param_template: str = None,
    job_template: str = None,
    job_restart_template: str = None,
    link_folders: bool = False,
):
    nc = problem.nc
    order = problem.order
    endtime = problem.endtime
    n_gpu = problem.n_gpu
    if not output_folder_already_formatted:
        output_folder = output_folder.format(nc, order)
    path = Path(output_folder)

    path.mkdir(parents=True)
    os.mkdir(output_folder + "/data")
    os.mkdir(output_folder + "/data/restart")
    os.mkdir(output_folder + "/img")
    with open(output_folder + "/Config.sh", "w") as text_file:
        if config_template is None:
            text_file.write(generate_config_template(order, nc))
        else:
            text_file.write(config_template)
    with open(output_folder + "/param.txt", "w") as text_file:
        if param_template is None:
            text_file.write(generate_parameter_file(endtime, 1))
        else:
            text_file.write(param_template)
    with open(output_folder + "/job.sh", "w") as text_file:
        if job_template is None:
            text_file.write(generate_jobs(cluster, n_gpu, job_runtime, nc, order)[0])
        else:
            text_file.write(job_template)
    if not no_restart:
        with open(output_folder + "/job_restart.sh", "w") as text_file:
            if job_restart_template is None:
                text_file.write(
                    generate_jobs(cluster, n_gpu, job_runtime, nc, order)[1]
                )
            else:
                text_file.write(job_restart_template)

    shutil.copy("python/plots.py", output_folder)
    shutil.copy("Makefile", output_folder)
    shutil.copy("Template-Config.sh", output_folder)
    shutil.copy("Template-Makefile.systype", output_folder)
    shutil.copy("defines_extra", output_folder)
    shutil.copy("python/plot_vel_power_spec.py", output_folder)
    if link_folders:
        os.symlink(os.path.abspath("") + "/src/", output_folder + "/src")
        os.symlink(
            os.path.abspath("") + "/buildsystem/", output_folder + "/buildsystem"
        )
        os.symlink(os.path.abspath("") + "/python/", output_folder + "/python")
    else:
        shutil.copytree("./src", output_folder + "/src")
        shutil.copytree("./buildsystem", output_folder + "/buildsystem")
        shutil.copytree(
            "./python",
            output_folder + "/python",
            ignore=shutil.ignore_patterns("*.ipynb"),
        )

    if run:
        run_shell("cd {} && sbatch job.sh".format(output_folder))
