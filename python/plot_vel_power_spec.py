import matplotlib.pyplot as plt
import numpy as np

box = 1.0

fpath = "./data/powerspec_vel_{:03d}.txt"


def plot_vel_power_spectra():
    next_file_exists = True
    i = 1
    while next_file_exists:
        fname = fpath.format(i)
        try:
            with open(fname) as f:
                float(f.readline())
                grid = int(f.readline())
                bins = int(f.readline())
                vel_disp = np.array(
                    [f.readline(), f.readline(), f.readline()], dtype=np.double
                )
            da = np.genfromtxt(fpath.format(i), skip_header=6, dtype=np.double)

            knyquist = 2 * np.pi / box * grid / 2

            K = da[:, 0]
            ModePow = da[:, 1]
            ModeCount = da[:, 2]
            SumPower = da[:, 3]

            print(i, np.sum(ModeCount * ModePow), np.sum(vel_disp))

            MinModeCount = 1
            TargetBinNummer = 200

            MinDlogK = (np.log10(np.max(K)) - np.log10(np.min(K))) / TargetBinNummer

            istart = 0
            ind = [istart]

            k_list = [0]
            power_list = [0]
            count_list = [0]

            while istart < bins:
                count = np.sum(ModeCount[ind])
                deltak = np.log10(np.max(K[ind])) - np.log10(np.min(K[ind]))

                if (deltak > MinDlogK) and (count > MinModeCount):
                    d2 = np.sum(SumPower[ind]) / np.sum(ModeCount[ind])
                    kk = np.sum(K[ind] * ModeCount[ind]) / np.sum(ModeCount[ind])

                    k_list.append(kk)
                    power_list.append(d2)
                    count_list.append(np.sum(ModeCount[ind]))
                    istart = istart + 1
                    ind = [istart]
                else:
                    istart = istart + 1
                    ind.append(istart)

            k_list = np.array(k_list)
            power_list = np.array(power_list)
            count_list = np.array(count_list)

            E_list = (
                k_list**2
            ) * power_list  # convert to energy-spectrum, E(k) = k^2 * P(k)

            f, ax = plt.subplots()

            ax.set_xlabel("k [ h/kpc ]")
            ax.set_ylabel("E(k)")
            ax.set_xlim((2 * np.pi / box / 1.5, 1.5 * knyquist))

            ax.loglog(k_list, E_list)

            ax.loglog(k_list, E_list[4] * (k_list / k_list[3]) ** (-5.0 / 3))

            f.savefig("powerspec_vel_{:03d}.jpg".format(i))

            plt.close(f)
            i += 1
        except FileNotFoundError:
            print(f"file {fname} does not exist, stopping powerspectra plotting")
            next_file_exists = False


if __name__ == "__main__":
    plot_vel_power_spectra()
